/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

#include <string>
#include <map>
#include "../aliyun-cpp-sdk-core/utils.h"
#include "../aliyun-cpp-sdk-core/DefaultProfile.h"
#include "../aliyun-cpp-sdk-core/BaseRequest.h"
#include "../aliyun-cpp-sdk-core/ApiBase.h"
#include "RdsService.h"

using std::string;
using namespace AliYunCore;

namespace AliYunRds {

RdsClient::RdsClient(DefaultProfile &pf):Profile(pf){}
void RdsClient::setProfile(DefaultProfile &pf){
	this->Profile = pf;
}
string RdsClient::getResponse(BaseRequest &request){
	request.RegionId = this->Profile.RegionId;		
	request.AccessKeyId = this->Profile.AccessKeyId;
	request.AccessKeySecret = this->Profile.AccessKeySecret;
	return ApiBase::getResponse(request);
}
RdsAddTagsToResourceRequest::RdsAddTagsToResourceRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "rds";
	this->Action = "AddTagsToResource";
	this->Version = "2014-08-15";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["ClientToken"] = "";
	this->KPMap["ClientToken"] = "Query";
	this->KVMap["proxyId"] = "";
	this->KPMap["proxyId"] = "Query";
	this->KVMap["DBInstanceId"] = "";
	this->KPMap["DBInstanceId"] = "Query";
	this->KVMap["Tag.1.key"] = "";
	this->KPMap["Tag.1.key"] = "Query";
	this->KVMap["Tag.2.key"] = "";
	this->KPMap["Tag.2.key"] = "Query";
	this->KVMap["Tag.3.key"] = "";
	this->KPMap["Tag.3.key"] = "Query";
	this->KVMap["Tag.4.key"] = "";
	this->KPMap["Tag.4.key"] = "Query";
	this->KVMap["Tag.5.key"] = "";
	this->KPMap["Tag.5.key"] = "Query";
	this->KVMap["Tag.1.value"] = "";
	this->KPMap["Tag.1.value"] = "Query";
	this->KVMap["Tag.2.value"] = "";
	this->KPMap["Tag.2.value"] = "Query";
	this->KVMap["Tag.3.value"] = "";
	this->KPMap["Tag.3.value"] = "Query";
	this->KVMap["Tag.4.value"] = "";
	this->KPMap["Tag.4.value"] = "Query";
	this->KVMap["Tag.5.value"] = "";
	this->KPMap["Tag.5.value"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";

}
RdsAllocateInstancePrivateConnectionRequest::RdsAllocateInstancePrivateConnectionRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "rds";
	this->Action = "AllocateInstancePrivateConnection";
	this->Version = "2014-08-15";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["DBInstanceId"] = "";
	this->KPMap["DBInstanceId"] = "Query";
	this->KVMap["ConnectionStringPrefix"] = "";
	this->KPMap["ConnectionStringPrefix"] = "Query";
	this->KVMap["Port"] = "";
	this->KPMap["Port"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";

}
RdsAllocateInstancePublicConnectionRequest::RdsAllocateInstancePublicConnectionRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "rds";
	this->Action = "AllocateInstancePublicConnection";
	this->Version = "2014-08-15";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["DBInstanceId"] = "";
	this->KPMap["DBInstanceId"] = "Query";
	this->KVMap["ConnectionStringPrefix"] = "";
	this->KPMap["ConnectionStringPrefix"] = "Query";
	this->KVMap["Port"] = "";
	this->KPMap["Port"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";

}
RdsBatchGrantAccountPrivilegeRequest::RdsBatchGrantAccountPrivilegeRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "rds";
	this->Action = "BatchGrantAccountPrivilege";
	this->Version = "2014-08-15";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["DBInstanceId"] = "";
	this->KPMap["DBInstanceId"] = "Query";
	this->KVMap["AccountName"] = "";
	this->KPMap["AccountName"] = "Query";
	this->KVMap["DBName"] = "";
	this->KPMap["DBName"] = "Query";
	this->KVMap["AccountPrivilege"] = "";
	this->KPMap["AccountPrivilege"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";

}
RdsBatchRevokeAccountPrivilegeRequest::RdsBatchRevokeAccountPrivilegeRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "rds";
	this->Action = "BatchRevokeAccountPrivilege";
	this->Version = "2014-08-15";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["DBInstanceId"] = "";
	this->KPMap["DBInstanceId"] = "Query";
	this->KVMap["AccountName"] = "";
	this->KPMap["AccountName"] = "Query";
	this->KVMap["DBName"] = "";
	this->KPMap["DBName"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";

}
RdsCancelImportRequest::RdsCancelImportRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "rds";
	this->Action = "CancelImport";
	this->Version = "2014-08-15";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["DBInstanceId"] = "";
	this->KPMap["DBInstanceId"] = "Query";
	this->KVMap["ImportId"] = "";
	this->KPMap["ImportId"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";

}
RdsCheckAccountNameAvailableRequest::RdsCheckAccountNameAvailableRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "rds";
	this->Action = "CheckAccountNameAvailable";
	this->Version = "2014-08-15";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["ClientToken"] = "";
	this->KPMap["ClientToken"] = "Query";
	this->KVMap["DBInstanceId"] = "";
	this->KPMap["DBInstanceId"] = "Query";
	this->KVMap["AccountName"] = "";
	this->KPMap["AccountName"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";

}
RdsCheckDBNameAvailableRequest::RdsCheckDBNameAvailableRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "rds";
	this->Action = "CheckDBNameAvailable";
	this->Version = "2014-08-15";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["ClientToken"] = "";
	this->KPMap["ClientToken"] = "Query";
	this->KVMap["DBInstanceId"] = "";
	this->KPMap["DBInstanceId"] = "Query";
	this->KVMap["DBName"] = "";
	this->KPMap["DBName"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";

}
RdsCreateAccountRequest::RdsCreateAccountRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "rds";
	this->Action = "CreateAccount";
	this->Version = "2014-08-15";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["DBInstanceId"] = "";
	this->KPMap["DBInstanceId"] = "Query";
	this->KVMap["AccountName"] = "";
	this->KPMap["AccountName"] = "Query";
	this->KVMap["AccountPassword"] = "";
	this->KPMap["AccountPassword"] = "Query";
	this->KVMap["AccountDescription"] = "";
	this->KPMap["AccountDescription"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";

}
RdsCreateAccountForInnerRequest::RdsCreateAccountForInnerRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "rds";
	this->Action = "CreateAccountForInner";
	this->Version = "2014-08-15";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["DBInstanceId"] = "";
	this->KPMap["DBInstanceId"] = "Query";
	this->KVMap["AccountName"] = "";
	this->KPMap["AccountName"] = "Query";
	this->KVMap["AccountPassword"] = "";
	this->KPMap["AccountPassword"] = "Query";
	this->KVMap["DBName"] = "";
	this->KPMap["DBName"] = "Query";
	this->KVMap["AccountPrivilege"] = "";
	this->KPMap["AccountPrivilege"] = "Query";
	this->KVMap["AccountDescription"] = "";
	this->KPMap["AccountDescription"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";

}
RdsCreateBackupRequest::RdsCreateBackupRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "rds";
	this->Action = "CreateBackup";
	this->Version = "2014-08-15";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["DBInstanceId"] = "";
	this->KPMap["DBInstanceId"] = "Query";
	this->KVMap["DBName"] = "";
	this->KPMap["DBName"] = "Query";
	this->KVMap["BackupMethod"] = "";
	this->KPMap["BackupMethod"] = "Query";
	this->KVMap["BackupType"] = "";
	this->KPMap["BackupType"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";

}
RdsCreateDatabaseRequest::RdsCreateDatabaseRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "rds";
	this->Action = "CreateDatabase";
	this->Version = "2014-08-15";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["DBInstanceId"] = "";
	this->KPMap["DBInstanceId"] = "Query";
	this->KVMap["DBName"] = "";
	this->KPMap["DBName"] = "Query";
	this->KVMap["CharacterSetName"] = "";
	this->KPMap["CharacterSetName"] = "Query";
	this->KVMap["DBDescription"] = "";
	this->KPMap["DBDescription"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";

}
RdsCreateDatabaseForInnerRequest::RdsCreateDatabaseForInnerRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "rds";
	this->Action = "CreateDatabaseForInner";
	this->Version = "2014-08-15";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["DBInstanceId"] = "";
	this->KPMap["DBInstanceId"] = "Query";
	this->KVMap["DBName"] = "";
	this->KPMap["DBName"] = "Query";
	this->KVMap["CharacterSetName"] = "";
	this->KPMap["CharacterSetName"] = "Query";
	this->KVMap["DBDescription"] = "";
	this->KPMap["DBDescription"] = "Query";
	this->KVMap["AccountName"] = "";
	this->KPMap["AccountName"] = "Query";
	this->KVMap["AccountPrivilege"] = "";
	this->KPMap["AccountPrivilege"] = "Query";
	this->KVMap["AccountPassword"] = "";
	this->KPMap["AccountPassword"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";

}
RdsCreateDBInstanceRequest::RdsCreateDBInstanceRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "rds";
	this->Action = "CreateDBInstance";
	this->Version = "2014-08-15";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["ClientToken"] = "";
	this->KPMap["ClientToken"] = "Query";
	this->KVMap["Engine"] = "";
	this->KPMap["Engine"] = "Query";
	this->KVMap["EngineVersion"] = "";
	this->KPMap["EngineVersion"] = "Query";
	this->KVMap["DBInstanceClass"] = "";
	this->KPMap["DBInstanceClass"] = "Query";
	this->KVMap["DBInstanceStorage"] = "";
	this->KPMap["DBInstanceStorage"] = "Query";
	this->KVMap["SystemDBCharset"] = "";
	this->KPMap["SystemDBCharset"] = "Query";
	this->KVMap["DBInstanceNetType"] = "";
	this->KPMap["DBInstanceNetType"] = "Query";
	this->KVMap["DBInstanceDescription"] = "";
	this->KPMap["DBInstanceDescription"] = "Query";
	this->KVMap["SecurityIPList"] = "";
	this->KPMap["SecurityIPList"] = "Query";
	this->KVMap["PayType"] = "";
	this->KPMap["PayType"] = "Query";
	this->KVMap["ZoneId"] = "";
	this->KPMap["ZoneId"] = "Query";
	this->KVMap["InstanceNetworkType"] = "";
	this->KPMap["InstanceNetworkType"] = "Query";
	this->KVMap["ConnectionMode"] = "";
	this->KPMap["ConnectionMode"] = "Query";
	this->KVMap["VPCId"] = "";
	this->KPMap["VPCId"] = "Query";
	this->KVMap["VSwitchId"] = "";
	this->KPMap["VSwitchId"] = "Query";
	this->KVMap["PrivateIpAddress"] = "";
	this->KPMap["PrivateIpAddress"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";

}
RdsCreateDBInstanceForChannelRequest::RdsCreateDBInstanceForChannelRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "rds";
	this->Action = "CreateDBInstanceForChannel";
	this->Version = "2014-08-15";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["ClientToken"] = "";
	this->KPMap["ClientToken"] = "Query";
	this->KVMap["SystemDBCharset"] = "";
	this->KPMap["SystemDBCharset"] = "Query";
	this->KVMap["Engine"] = "";
	this->KPMap["Engine"] = "Query";
	this->KVMap["EngineVersion"] = "";
	this->KPMap["EngineVersion"] = "Query";
	this->KVMap["DBInstanceClass"] = "";
	this->KPMap["DBInstanceClass"] = "Query";
	this->KVMap["DBInstanceStorage"] = "";
	this->KPMap["DBInstanceStorage"] = "Query";
	this->KVMap["DBInstanceNetType"] = "";
	this->KPMap["DBInstanceNetType"] = "Query";
	this->KVMap["DBInstanceDescription"] = "";
	this->KPMap["DBInstanceDescription"] = "Query";
	this->KVMap["SecurityIPList"] = "";
	this->KPMap["SecurityIPList"] = "Query";
	this->KVMap["AccountName"] = "";
	this->KPMap["AccountName"] = "Query";
	this->KVMap["AccountPassword"] = "";
	this->KPMap["AccountPassword"] = "Query";
	this->KVMap["PayType"] = "";
	this->KPMap["PayType"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";

}
RdsCreateDBInstanceforFirstPayRequest::RdsCreateDBInstanceforFirstPayRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "rds";
	this->Action = "CreateDBInstanceforFirstPay";
	this->Version = "2014-08-15";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["uid"] = "";
	this->KPMap["uid"] = "Query";
	this->KVMap["bid"] = "";
	this->KPMap["bid"] = "Query";
	this->KVMap["uidLoginEmail"] = "";
	this->KPMap["uidLoginEmail"] = "Query";
	this->KVMap["bidLoginEmail"] = "";
	this->KPMap["bidLoginEmail"] = "Query";
	this->KVMap["Engine"] = "";
	this->KPMap["Engine"] = "Query";
	this->KVMap["EngineVersion"] = "";
	this->KPMap["EngineVersion"] = "Query";
	this->KVMap["DBInstanceClass"] = "";
	this->KPMap["DBInstanceClass"] = "Query";
	this->KVMap["DBInstanceStorage"] = "";
	this->KPMap["DBInstanceStorage"] = "Query";
	this->KVMap["DBInstanceNetType"] = "";
	this->KPMap["DBInstanceNetType"] = "Query";
	this->KVMap["CharacterSetName"] = "";
	this->KPMap["CharacterSetName"] = "Query";
	this->KVMap["DBInstanceRemarks"] = "";
	this->KPMap["DBInstanceRemarks"] = "Query";
	this->KVMap["ClientToken"] = "";
	this->KPMap["ClientToken"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";

}
RdsCreatePostpaidDBInstanceRequest::RdsCreatePostpaidDBInstanceRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "rds";
	this->Action = "CreatePostpaidDBInstance";
	this->Version = "2014-08-15";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["ClientToken"] = "";
	this->KPMap["ClientToken"] = "Query";
	this->KVMap["Engine"] = "";
	this->KPMap["Engine"] = "Query";
	this->KVMap["EngineVersion"] = "";
	this->KPMap["EngineVersion"] = "Query";
	this->KVMap["DBInstanceClass"] = "";
	this->KPMap["DBInstanceClass"] = "Query";
	this->KVMap["DBInstanceStorage"] = "";
	this->KPMap["DBInstanceStorage"] = "Query";
	this->KVMap["DBInstanceNetType"] = "";
	this->KPMap["DBInstanceNetType"] = "Query";
	this->KVMap["DBInstanceDescription"] = "";
	this->KPMap["DBInstanceDescription"] = "Query";
	this->KVMap["SecurityIPList"] = "";
	this->KPMap["SecurityIPList"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";

}
RdsCreatePostpaidDBInstanceForChannelRequest::RdsCreatePostpaidDBInstanceForChannelRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "rds";
	this->Action = "CreatePostpaidDBInstanceForChannel";
	this->Version = "2014-08-15";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["ClientToken"] = "";
	this->KPMap["ClientToken"] = "Query";
	this->KVMap["Engine"] = "";
	this->KPMap["Engine"] = "Query";
	this->KVMap["EngineVersion"] = "";
	this->KPMap["EngineVersion"] = "Query";
	this->KVMap["DBInstanceClass"] = "";
	this->KPMap["DBInstanceClass"] = "Query";
	this->KVMap["DBInstanceStorage"] = "";
	this->KPMap["DBInstanceStorage"] = "Query";
	this->KVMap["DBInstanceNetType"] = "";
	this->KPMap["DBInstanceNetType"] = "Query";
	this->KVMap["DBInstanceDescription"] = "";
	this->KPMap["DBInstanceDescription"] = "Query";
	this->KVMap["SecurityIPList"] = "";
	this->KPMap["SecurityIPList"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";

}
RdsCreateReadOnlyDBInstanceRequest::RdsCreateReadOnlyDBInstanceRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "rds";
	this->Action = "CreateReadOnlyDBInstance";
	this->Version = "2014-08-15";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["ClientToken"] = "";
	this->KPMap["ClientToken"] = "Query";
	this->KVMap["ZoneId"] = "";
	this->KPMap["ZoneId"] = "Query";
	this->KVMap["DBInstanceId"] = "";
	this->KPMap["DBInstanceId"] = "Query";
	this->KVMap["DBInstanceClass"] = "";
	this->KPMap["DBInstanceClass"] = "Query";
	this->KVMap["DBInstanceStorage"] = "";
	this->KPMap["DBInstanceStorage"] = "Query";
	this->KVMap["EngineVersion"] = "";
	this->KPMap["EngineVersion"] = "Query";
	this->KVMap["PayType"] = "";
	this->KPMap["PayType"] = "Query";
	this->KVMap["DBInstanceDescription"] = "";
	this->KPMap["DBInstanceDescription"] = "Query";
	this->KVMap["InstanceNetworkType"] = "";
	this->KPMap["InstanceNetworkType"] = "Query";
	this->KVMap["VPCId"] = "";
	this->KPMap["VPCId"] = "Query";
	this->KVMap["VSwitchId"] = "";
	this->KPMap["VSwitchId"] = "Query";
	this->KVMap["PrivateIpAddress"] = "";
	this->KPMap["PrivateIpAddress"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";

}
RdsCreateTempDBInstanceRequest::RdsCreateTempDBInstanceRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "rds";
	this->Action = "CreateTempDBInstance";
	this->Version = "2014-08-15";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["DBInstanceId"] = "";
	this->KPMap["DBInstanceId"] = "Query";
	this->KVMap["BackupId"] = "";
	this->KPMap["BackupId"] = "Query";
	this->KVMap["RestoreTime"] = "";
	this->KPMap["RestoreTime"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";

}
RdsCreateUploadPathForSQLServerRequest::RdsCreateUploadPathForSQLServerRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "rds";
	this->Action = "CreateUploadPathForSQLServer";
	this->Version = "2014-08-15";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["DBInstanceId"] = "";
	this->KPMap["DBInstanceId"] = "Query";
	this->KVMap["DBName"] = "";
	this->KPMap["DBName"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";

}
RdsDeleteAccountRequest::RdsDeleteAccountRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "rds";
	this->Action = "DeleteAccount";
	this->Version = "2014-08-15";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["DBInstanceId"] = "";
	this->KPMap["DBInstanceId"] = "Query";
	this->KVMap["AccountName"] = "";
	this->KPMap["AccountName"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";

}
RdsDeleteDatabaseRequest::RdsDeleteDatabaseRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "rds";
	this->Action = "DeleteDatabase";
	this->Version = "2014-08-15";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["DBInstanceId"] = "";
	this->KPMap["DBInstanceId"] = "Query";
	this->KVMap["DBName"] = "";
	this->KPMap["DBName"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";

}
RdsDeleteDBInstanceRequest::RdsDeleteDBInstanceRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "rds";
	this->Action = "DeleteDBInstance";
	this->Version = "2014-08-15";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["ClientToken"] = "";
	this->KPMap["ClientToken"] = "Query";
	this->KVMap["DBInstanceId"] = "";
	this->KPMap["DBInstanceId"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";

}
RdsDescibeImportsFromDatabaseRequest::RdsDescibeImportsFromDatabaseRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "rds";
	this->Action = "DescibeImportsFromDatabase";
	this->Version = "2014-08-15";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["ClientToken"] = "";
	this->KPMap["ClientToken"] = "Query";
	this->KVMap["DBInstanceId"] = "";
	this->KPMap["DBInstanceId"] = "Query";
	this->KVMap["Engine"] = "";
	this->KPMap["Engine"] = "Query";
	this->KVMap["ImportId"] = "";
	this->KPMap["ImportId"] = "Query";
	this->KVMap["StartTime"] = "";
	this->KPMap["StartTime"] = "Query";
	this->KVMap["EndTime"] = "";
	this->KPMap["EndTime"] = "Query";
	this->KVMap["PageSize"] = "";
	this->KPMap["PageSize"] = "Query";
	this->KVMap["PageNumber"] = "";
	this->KPMap["PageNumber"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";

}
RdsDescribeAbnormalDBInstancesRequest::RdsDescribeAbnormalDBInstancesRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "rds";
	this->Action = "DescribeAbnormalDBInstances";
	this->Version = "2014-08-15";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["ClientToken"] = "";
	this->KPMap["ClientToken"] = "Query";
	this->KVMap["proxyId"] = "";
	this->KPMap["proxyId"] = "Query";
	this->KVMap["DBInstanceId"] = "";
	this->KPMap["DBInstanceId"] = "Query";
	this->KVMap["PageSize"] = "";
	this->KPMap["PageSize"] = "Query";
	this->KVMap["PageNumber"] = "";
	this->KPMap["PageNumber"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";

}
RdsDescribeAccountsRequest::RdsDescribeAccountsRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "rds";
	this->Action = "DescribeAccounts";
	this->Version = "2014-08-15";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["DBInstanceId"] = "";
	this->KPMap["DBInstanceId"] = "Query";
	this->KVMap["AccountName"] = "";
	this->KPMap["AccountName"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";

}
RdsDescribeBackupPolicyRequest::RdsDescribeBackupPolicyRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "rds";
	this->Action = "DescribeBackupPolicy";
	this->Version = "2014-08-15";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["DBInstanceId"] = "";
	this->KPMap["DBInstanceId"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";

}
RdsDescribeBackupsRequest::RdsDescribeBackupsRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "rds";
	this->Action = "DescribeBackups";
	this->Version = "2014-08-15";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["DBInstanceId"] = "";
	this->KPMap["DBInstanceId"] = "Query";
	this->KVMap["BackupId"] = "";
	this->KPMap["BackupId"] = "Query";
	this->KVMap["BackupLocation"] = "";
	this->KPMap["BackupLocation"] = "Query";
	this->KVMap["BackupStatus"] = "";
	this->KPMap["BackupStatus"] = "Query";
	this->KVMap["BackupMode"] = "";
	this->KPMap["BackupMode"] = "Query";
	this->KVMap["StartTime"] = "";
	this->KPMap["StartTime"] = "Query";
	this->KVMap["EndTime"] = "";
	this->KPMap["EndTime"] = "Query";
	this->KVMap["PageSize"] = "";
	this->KPMap["PageSize"] = "Query";
	this->KVMap["PageNumber"] = "";
	this->KPMap["PageNumber"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";

}
RdsDescribeBackupTasksRequest::RdsDescribeBackupTasksRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "rds";
	this->Action = "DescribeBackupTasks";
	this->Version = "2014-08-15";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["ClientToken"] = "";
	this->KPMap["ClientToken"] = "Query";
	this->KVMap["Flag"] = "";
	this->KPMap["Flag"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";
	this->KVMap["DBInstanceId"] = "";
	this->KPMap["DBInstanceId"] = "Query";
	this->KVMap["BackupJobId"] = "";
	this->KPMap["BackupJobId"] = "Query";
	this->KVMap["BackupMode"] = "";
	this->KPMap["BackupMode"] = "Query";
	this->KVMap["BackupJobStatus"] = "";
	this->KPMap["BackupJobStatus"] = "Query";

}
RdsDescribeBinlogFilesRequest::RdsDescribeBinlogFilesRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "rds";
	this->Action = "DescribeBinlogFiles";
	this->Version = "2014-08-15";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["DBInstanceId"] = "";
	this->KPMap["DBInstanceId"] = "Query";
	this->KVMap["StartTime"] = "";
	this->KPMap["StartTime"] = "Query";
	this->KVMap["EndTime"] = "";
	this->KPMap["EndTime"] = "Query";
	this->KVMap["PageSize"] = "";
	this->KPMap["PageSize"] = "Query";
	this->KVMap["PageNumber"] = "";
	this->KPMap["PageNumber"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";

}
RdsDescribeDatabasesRequest::RdsDescribeDatabasesRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "rds";
	this->Action = "DescribeDatabases";
	this->Version = "2014-08-15";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["DBInstanceId"] = "";
	this->KPMap["DBInstanceId"] = "Query";
	this->KVMap["DBName"] = "";
	this->KPMap["DBName"] = "Query";
	this->KVMap["DBStatus"] = "";
	this->KPMap["DBStatus"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";

}
RdsDescribeDBInstanceAttributeRequest::RdsDescribeDBInstanceAttributeRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "rds";
	this->Action = "DescribeDBInstanceAttribute";
	this->Version = "2014-08-15";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["ClientToken"] = "";
	this->KPMap["ClientToken"] = "Query";
	this->KVMap["DBInstanceId"] = "";
	this->KPMap["DBInstanceId"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";

}
RdsDescribeDBInstanceIPArrayListRequest::RdsDescribeDBInstanceIPArrayListRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "rds";
	this->Action = "DescribeDBInstanceIPArrayList";
	this->Version = "2014-08-15";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["DBInstanceId"] = "";
	this->KPMap["DBInstanceId"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";

}
RdsDescribeDBInstanceNetInfoRequest::RdsDescribeDBInstanceNetInfoRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "rds";
	this->Action = "DescribeDBInstanceNetInfo";
	this->Version = "2014-08-15";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["ClientToken"] = "";
	this->KPMap["ClientToken"] = "Query";
	this->KVMap["DBInstanceId"] = "";
	this->KPMap["DBInstanceId"] = "Query";
	this->KVMap["Flag"] = "";
	this->KPMap["Flag"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";

}
RdsDescribeDBInstanceNetInfoForChannelRequest::RdsDescribeDBInstanceNetInfoForChannelRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "rds";
	this->Action = "DescribeDBInstanceNetInfoForChannel";
	this->Version = "2014-08-15";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["ClientToken"] = "";
	this->KPMap["ClientToken"] = "Query";
	this->KVMap["DBInstanceId"] = "";
	this->KPMap["DBInstanceId"] = "Query";
	this->KVMap["Flag"] = "";
	this->KPMap["Flag"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";

}
RdsDescribeDBInstancePerformanceRequest::RdsDescribeDBInstancePerformanceRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "rds";
	this->Action = "DescribeDBInstancePerformance";
	this->Version = "2014-08-15";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["ClientToken"] = "";
	this->KPMap["ClientToken"] = "Query";
	this->KVMap["DBInstanceId"] = "";
	this->KPMap["DBInstanceId"] = "Query";
	this->KVMap["Key"] = "";
	this->KPMap["Key"] = "Query";
	this->KVMap["StartTime"] = "";
	this->KPMap["StartTime"] = "Query";
	this->KVMap["EndTime"] = "";
	this->KPMap["EndTime"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";

}
RdsDescribeDBInstancesRequest::RdsDescribeDBInstancesRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "rds";
	this->Action = "DescribeDBInstances";
	this->Version = "2014-08-15";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["ClientToken"] = "";
	this->KPMap["ClientToken"] = "Query";
	this->KVMap["proxyId"] = "";
	this->KPMap["proxyId"] = "Query";
	this->KVMap["Engine"] = "";
	this->KPMap["Engine"] = "Query";
	this->KVMap["DBInstanceStatus"] = "";
	this->KPMap["DBInstanceStatus"] = "Query";
	this->KVMap["SearchKey"] = "";
	this->KPMap["SearchKey"] = "Query";
	this->KVMap["DBInstanceId"] = "";
	this->KPMap["DBInstanceId"] = "Query";
	this->KVMap["DBInstanceType"] = "";
	this->KPMap["DBInstanceType"] = "Query";
	this->KVMap["PageSize"] = "";
	this->KPMap["PageSize"] = "Query";
	this->KVMap["PageNumber"] = "";
	this->KPMap["PageNumber"] = "Query";
	this->KVMap["InstanceNetworkType"] = "";
	this->KPMap["InstanceNetworkType"] = "Query";
	this->KVMap["ConnectionMode"] = "";
	this->KPMap["ConnectionMode"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";

}
RdsDescribeDBInstancesByExpireTimeRequest::RdsDescribeDBInstancesByExpireTimeRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "rds";
	this->Action = "DescribeDBInstancesByExpireTime";
	this->Version = "2014-08-15";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["ClientToken"] = "";
	this->KPMap["ClientToken"] = "Query";
	this->KVMap["proxyId"] = "";
	this->KPMap["proxyId"] = "Query";
	this->KVMap["ExpirePeriod"] = "";
	this->KPMap["ExpirePeriod"] = "Query";
	this->KVMap["PageSize"] = "";
	this->KPMap["PageSize"] = "Query";
	this->KVMap["PageNumber"] = "";
	this->KPMap["PageNumber"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";

}
RdsDescribeDBInstancesByPerformanceRequest::RdsDescribeDBInstancesByPerformanceRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "rds";
	this->Action = "DescribeDBInstancesByPerformance";
	this->Version = "2014-08-15";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["ClientToken"] = "";
	this->KPMap["ClientToken"] = "Query";
	this->KVMap["proxyId"] = "";
	this->KPMap["proxyId"] = "Query";
	this->KVMap["DBInstanceId"] = "";
	this->KPMap["DBInstanceId"] = "Query";
	this->KVMap["PageSize"] = "";
	this->KPMap["PageSize"] = "Query";
	this->KVMap["PageNumber"] = "";
	this->KPMap["PageNumber"] = "Query";
	this->KVMap["SortMethod"] = "";
	this->KPMap["SortMethod"] = "Query";
	this->KVMap["SortKey"] = "";
	this->KPMap["SortKey"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";

}
RdsDescribeErrorLogsRequest::RdsDescribeErrorLogsRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "rds";
	this->Action = "DescribeErrorLogs";
	this->Version = "2014-08-15";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["DBInstanceId"] = "";
	this->KPMap["DBInstanceId"] = "Query";
	this->KVMap["StartTime"] = "";
	this->KPMap["StartTime"] = "Query";
	this->KVMap["EndTime"] = "";
	this->KPMap["EndTime"] = "Query";
	this->KVMap["PageSize"] = "";
	this->KPMap["PageSize"] = "Query";
	this->KVMap["PageNumber"] = "";
	this->KPMap["PageNumber"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";

}
RdsDescribeFilesForSQLServerRequest::RdsDescribeFilesForSQLServerRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "rds";
	this->Action = "DescribeFilesForSQLServer";
	this->Version = "2014-08-15";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["DBInstanceId"] = "";
	this->KPMap["DBInstanceId"] = "Query";
	this->KVMap["StartTime"] = "";
	this->KPMap["StartTime"] = "Query";
	this->KVMap["EndTime"] = "";
	this->KPMap["EndTime"] = "Query";
	this->KVMap["PageSize"] = "";
	this->KPMap["PageSize"] = "Query";
	this->KVMap["PageNumber"] = "";
	this->KPMap["PageNumber"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";

}
RdsDescribeImportsForSQLServerRequest::RdsDescribeImportsForSQLServerRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "rds";
	this->Action = "DescribeImportsForSQLServer";
	this->Version = "2014-08-15";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["DBInstanceId"] = "";
	this->KPMap["DBInstanceId"] = "Query";
	this->KVMap["ImportId"] = "";
	this->KPMap["ImportId"] = "Query";
	this->KVMap["StartTime"] = "";
	this->KPMap["StartTime"] = "Query";
	this->KVMap["EndTime"] = "";
	this->KPMap["EndTime"] = "Query";
	this->KVMap["PageSize"] = "";
	this->KPMap["PageSize"] = "Query";
	this->KVMap["PageNumber"] = "";
	this->KPMap["PageNumber"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";

}
RdsDescribeModifyParameterLogRequest::RdsDescribeModifyParameterLogRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "rds";
	this->Action = "DescribeModifyParameterLog";
	this->Version = "2014-08-15";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["DBInstanceId"] = "";
	this->KPMap["DBInstanceId"] = "Query";
	this->KVMap["StartTime"] = "";
	this->KPMap["StartTime"] = "Query";
	this->KVMap["EndTime"] = "";
	this->KPMap["EndTime"] = "Query";
	this->KVMap["PageSize"] = "";
	this->KPMap["PageSize"] = "Query";
	this->KVMap["PageNumber"] = "";
	this->KPMap["PageNumber"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";

}
RdsDescribeOperationLogsRequest::RdsDescribeOperationLogsRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "rds";
	this->Action = "DescribeOperationLogs";
	this->Version = "2014-08-15";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["DBInstanceId"] = "";
	this->KPMap["DBInstanceId"] = "Query";
	this->KVMap["SearchKey"] = "";
	this->KPMap["SearchKey"] = "Query";
	this->KVMap["DBInstanceUseType"] = "";
	this->KPMap["DBInstanceUseType"] = "Query";
	this->KVMap["StartTime"] = "";
	this->KPMap["StartTime"] = "Query";
	this->KVMap["EndTime"] = "";
	this->KPMap["EndTime"] = "Query";
	this->KVMap["PageSize"] = "";
	this->KPMap["PageSize"] = "Query";
	this->KVMap["PageNumber"] = "";
	this->KPMap["PageNumber"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";

}
RdsDescribeOptimizeAdviceByDBARequest::RdsDescribeOptimizeAdviceByDBARequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "rds";
	this->Action = "DescribeOptimizeAdviceByDBA";
	this->Version = "2014-08-15";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["DBInstanceId"] = "";
	this->KPMap["DBInstanceId"] = "Query";
	this->KVMap["PageSize"] = "";
	this->KPMap["PageSize"] = "Query";
	this->KVMap["PageNumber"] = "";
	this->KPMap["PageNumber"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";

}
RdsDescribeOptimizeAdviceOnBigTableRequest::RdsDescribeOptimizeAdviceOnBigTableRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "rds";
	this->Action = "DescribeOptimizeAdviceOnBigTable";
	this->Version = "2014-08-15";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["DBInstanceId"] = "";
	this->KPMap["DBInstanceId"] = "Query";
	this->KVMap["PageSize"] = "";
	this->KPMap["PageSize"] = "Query";
	this->KVMap["PageNumber"] = "";
	this->KPMap["PageNumber"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";

}
RdsDescribeOptimizeAdviceOnExcessIndexRequest::RdsDescribeOptimizeAdviceOnExcessIndexRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "rds";
	this->Action = "DescribeOptimizeAdviceOnExcessIndex";
	this->Version = "2014-08-15";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["DBInstanceId"] = "";
	this->KPMap["DBInstanceId"] = "Query";
	this->KVMap["PageSize"] = "";
	this->KPMap["PageSize"] = "Query";
	this->KVMap["PageNumber"] = "";
	this->KPMap["PageNumber"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";

}
RdsDescribeOptimizeAdviceOnMissIndexRequest::RdsDescribeOptimizeAdviceOnMissIndexRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "rds";
	this->Action = "DescribeOptimizeAdviceOnMissIndex";
	this->Version = "2014-08-15";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["DBInstanceId"] = "";
	this->KPMap["DBInstanceId"] = "Query";
	this->KVMap["PageSize"] = "";
	this->KPMap["PageSize"] = "Query";
	this->KVMap["PageNumber"] = "";
	this->KPMap["PageNumber"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";

}
RdsDescribeOptimizeAdviceOnMissPKRequest::RdsDescribeOptimizeAdviceOnMissPKRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "rds";
	this->Action = "DescribeOptimizeAdviceOnMissPK";
	this->Version = "2014-08-15";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["DBInstanceId"] = "";
	this->KPMap["DBInstanceId"] = "Query";
	this->KVMap["PageSize"] = "";
	this->KPMap["PageSize"] = "Query";
	this->KVMap["PageNumber"] = "";
	this->KPMap["PageNumber"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";

}
RdsDescribeOptimizeAdviceOnStorageRequest::RdsDescribeOptimizeAdviceOnStorageRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "rds";
	this->Action = "DescribeOptimizeAdviceOnStorage";
	this->Version = "2014-08-15";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["DBInstanceId"] = "";
	this->KPMap["DBInstanceId"] = "Query";
	this->KVMap["PageSize"] = "";
	this->KPMap["PageSize"] = "Query";
	this->KVMap["PageNumber"] = "";
	this->KPMap["PageNumber"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";

}
RdsDescribeParametersRequest::RdsDescribeParametersRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "rds";
	this->Action = "DescribeParameters";
	this->Version = "2014-08-15";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["ClientToken"] = "";
	this->KPMap["ClientToken"] = "Query";
	this->KVMap["DBInstanceId"] = "";
	this->KPMap["DBInstanceId"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";

}
RdsDescribeParameterTemplatesRequest::RdsDescribeParameterTemplatesRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "rds";
	this->Action = "DescribeParameterTemplates";
	this->Version = "2014-08-15";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["ClientToken"] = "";
	this->KPMap["ClientToken"] = "Query";
	this->KVMap["Engine"] = "";
	this->KPMap["Engine"] = "Query";
	this->KVMap["EngineVersion"] = "";
	this->KPMap["EngineVersion"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";

}
RdsDescribePreCheckResultsRequest::RdsDescribePreCheckResultsRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "rds";
	this->Action = "DescribePreCheckResults";
	this->Version = "2014-08-15";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["ClientToken"] = "";
	this->KPMap["ClientToken"] = "Query";
	this->KVMap["DBInstanceId"] = "";
	this->KPMap["DBInstanceId"] = "Query";
	this->KVMap["PreCheckId"] = "";
	this->KPMap["PreCheckId"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";

}
RdsDescribeRealtimeDiagnosesRequest::RdsDescribeRealtimeDiagnosesRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "rds";
	this->Action = "DescribeRealtimeDiagnoses";
	this->Version = "2014-08-15";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["DBInstanceId"] = "";
	this->KPMap["DBInstanceId"] = "Query";
	this->KVMap["StartTime"] = "";
	this->KPMap["StartTime"] = "Query";
	this->KVMap["EndTime"] = "";
	this->KPMap["EndTime"] = "Query";
	this->KVMap["PageSize"] = "";
	this->KPMap["PageSize"] = "Query";
	this->KVMap["PageNumber"] = "";
	this->KPMap["PageNumber"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";

}
RdsDescribeRegionsRequest::RdsDescribeRegionsRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "rds";
	this->Action = "DescribeRegions";
	this->Version = "2014-08-15";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["ClientToken"] = "";
	this->KPMap["ClientToken"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";

}
RdsDescribeResourceUsageRequest::RdsDescribeResourceUsageRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "rds";
	this->Action = "DescribeResourceUsage";
	this->Version = "2014-08-15";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["ClientToken"] = "";
	this->KPMap["ClientToken"] = "Query";
	this->KVMap["DBInstanceId"] = "";
	this->KPMap["DBInstanceId"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";

}
RdsDescribeSlowLogRecordsRequest::RdsDescribeSlowLogRecordsRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "rds";
	this->Action = "DescribeSlowLogRecords";
	this->Version = "2014-08-15";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["DBInstanceId"] = "";
	this->KPMap["DBInstanceId"] = "Query";
	this->KVMap["SQLId"] = "";
	this->KPMap["SQLId"] = "Query";
	this->KVMap["StartTime"] = "";
	this->KPMap["StartTime"] = "Query";
	this->KVMap["EndTime"] = "";
	this->KPMap["EndTime"] = "Query";
	this->KVMap["DBName"] = "";
	this->KPMap["DBName"] = "Query";
	this->KVMap["PageSize"] = "";
	this->KPMap["PageSize"] = "Query";
	this->KVMap["PageNumber"] = "";
	this->KPMap["PageNumber"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";

}
RdsDescribeSlowLogsRequest::RdsDescribeSlowLogsRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "rds";
	this->Action = "DescribeSlowLogs";
	this->Version = "2014-08-15";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["DBInstanceId"] = "";
	this->KPMap["DBInstanceId"] = "Query";
	this->KVMap["StartTime"] = "";
	this->KPMap["StartTime"] = "Query";
	this->KVMap["EndTime"] = "";
	this->KPMap["EndTime"] = "Query";
	this->KVMap["DBName"] = "";
	this->KPMap["DBName"] = "Query";
	this->KVMap["SortKey"] = "";
	this->KPMap["SortKey"] = "Query";
	this->KVMap["PageSize"] = "";
	this->KPMap["PageSize"] = "Query";
	this->KVMap["PageNumber"] = "";
	this->KPMap["PageNumber"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";

}
RdsDescribeSQLInjectionInfosRequest::RdsDescribeSQLInjectionInfosRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "rds";
	this->Action = "DescribeSQLInjectionInfos";
	this->Version = "2014-08-15";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["DBInstanceId"] = "";
	this->KPMap["DBInstanceId"] = "Query";
	this->KVMap["StartTime"] = "";
	this->KPMap["StartTime"] = "Query";
	this->KVMap["EndTime"] = "";
	this->KPMap["EndTime"] = "Query";
	this->KVMap["PageSize"] = "";
	this->KPMap["PageSize"] = "Query";
	this->KVMap["PageNumber"] = "";
	this->KPMap["PageNumber"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";

}
RdsDescribeSQLLogRecordsRequest::RdsDescribeSQLLogRecordsRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "rds";
	this->Action = "DescribeSQLLogRecords";
	this->Version = "2014-08-15";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["ClientToken"] = "";
	this->KPMap["ClientToken"] = "Query";
	this->KVMap["DBInstanceId"] = "";
	this->KPMap["DBInstanceId"] = "Query";
	this->KVMap["SQLId"] = "";
	this->KPMap["SQLId"] = "Query";
	this->KVMap["QueryKeywords"] = "";
	this->KPMap["QueryKeywords"] = "Query";
	this->KVMap["StartTime"] = "";
	this->KPMap["StartTime"] = "Query";
	this->KVMap["EndTime"] = "";
	this->KPMap["EndTime"] = "Query";
	this->KVMap["PageSize"] = "";
	this->KPMap["PageSize"] = "Query";
	this->KVMap["PageNumber"] = "";
	this->KPMap["PageNumber"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";

}
RdsDescribeSQLLogReportListRequest::RdsDescribeSQLLogReportListRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "rds";
	this->Action = "DescribeSQLLogReportList";
	this->Version = "2014-08-15";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["DBInstanceId"] = "";
	this->KPMap["DBInstanceId"] = "Query";
	this->KVMap["StartTime"] = "";
	this->KPMap["StartTime"] = "Query";
	this->KVMap["EndTime"] = "";
	this->KPMap["EndTime"] = "Query";
	this->KVMap["PageSize"] = "";
	this->KPMap["PageSize"] = "Query";
	this->KVMap["PageNumber"] = "";
	this->KPMap["PageNumber"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";

}
RdsDescribeSQLLogReportsRequest::RdsDescribeSQLLogReportsRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "rds";
	this->Action = "DescribeSQLLogReports";
	this->Version = "2014-08-15";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["ClientToken"] = "";
	this->KPMap["ClientToken"] = "Query";
	this->KVMap["DBInstanceId"] = "";
	this->KPMap["DBInstanceId"] = "Query";
	this->KVMap["StartTime"] = "";
	this->KPMap["StartTime"] = "Query";
	this->KVMap["EndTime"] = "";
	this->KPMap["EndTime"] = "Query";
	this->KVMap["ReportType"] = "";
	this->KPMap["ReportType"] = "Query";
	this->KVMap["PageSize"] = "";
	this->KPMap["PageSize"] = "Query";
	this->KVMap["PageNumber"] = "";
	this->KPMap["PageNumber"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";

}
RdsDescribeTasksRequest::RdsDescribeTasksRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "rds";
	this->Action = "DescribeTasks";
	this->Version = "2014-08-15";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["DBInstanceId"] = "";
	this->KPMap["DBInstanceId"] = "Query";
	this->KVMap["StartTime"] = "";
	this->KPMap["StartTime"] = "Query";
	this->KVMap["EndTime"] = "";
	this->KPMap["EndTime"] = "Query";
	this->KVMap["PageSize"] = "";
	this->KPMap["PageSize"] = "Query";
	this->KVMap["PageNumber"] = "";
	this->KPMap["PageNumber"] = "Query";
	this->KVMap["Status"] = "";
	this->KPMap["Status"] = "Query";
	this->KVMap["TaskAction"] = "";
	this->KPMap["TaskAction"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";

}
RdsDescribeVpcZoneNosRequest::RdsDescribeVpcZoneNosRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "rds";
	this->Action = "DescribeVpcZoneNos";
	this->Version = "2014-08-15";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["ClientToken"] = "";
	this->KPMap["ClientToken"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";
	this->KVMap["Region"] = "";
	this->KPMap["Region"] = "Query";
	this->KVMap["ZoneId"] = "";
	this->KPMap["ZoneId"] = "Query";

}
RdsExtractBackupFromOASRequest::RdsExtractBackupFromOASRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "rds";
	this->Action = "ExtractBackupFromOAS";
	this->Version = "2014-08-15";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["ClientToken"] = "";
	this->KPMap["ClientToken"] = "Query";
	this->KVMap["DBInstanceId"] = "";
	this->KPMap["DBInstanceId"] = "Query";
	this->KVMap["BackupId"] = "";
	this->KPMap["BackupId"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";

}
RdsGrantAccountPrivilegeRequest::RdsGrantAccountPrivilegeRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "rds";
	this->Action = "GrantAccountPrivilege";
	this->Version = "2014-08-15";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["DBInstanceId"] = "";
	this->KPMap["DBInstanceId"] = "Query";
	this->KVMap["AccountName"] = "";
	this->KPMap["AccountName"] = "Query";
	this->KVMap["DBName"] = "";
	this->KPMap["DBName"] = "Query";
	this->KVMap["AccountPrivilege"] = "";
	this->KPMap["AccountPrivilege"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";

}
RdsImportDatabaseBetweenInstancesRequest::RdsImportDatabaseBetweenInstancesRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "rds";
	this->Action = "ImportDatabaseBetweenInstances";
	this->Version = "2014-08-15";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["DBInstanceId"] = "";
	this->KPMap["DBInstanceId"] = "Query";
	this->KVMap["SourceDBInstanceId"] = "";
	this->KPMap["SourceDBInstanceId"] = "Query";
	this->KVMap["DBInfo"] = "";
	this->KPMap["DBInfo"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";

}
RdsImportDataForSQLServerRequest::RdsImportDataForSQLServerRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "rds";
	this->Action = "ImportDataForSQLServer";
	this->Version = "2014-08-15";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["DBInstanceId"] = "";
	this->KPMap["DBInstanceId"] = "Query";
	this->KVMap["FileName"] = "";
	this->KPMap["FileName"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";

}
RdsImportDataFromDatabaseRequest::RdsImportDataFromDatabaseRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "rds";
	this->Action = "ImportDataFromDatabase";
	this->Version = "2014-08-15";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["DBInstanceId"] = "";
	this->KPMap["DBInstanceId"] = "Query";
	this->KVMap["SourceDatabaseIp"] = "";
	this->KPMap["SourceDatabaseIp"] = "Query";
	this->KVMap["SourceDatabasePort"] = "";
	this->KPMap["SourceDatabasePort"] = "Query";
	this->KVMap["SourceDatabaseUserName"] = "";
	this->KPMap["SourceDatabaseUserName"] = "Query";
	this->KVMap["SourceDatabasePassword"] = "";
	this->KPMap["SourceDatabasePassword"] = "Query";
	this->KVMap["ImportDataType"] = "";
	this->KPMap["ImportDataType"] = "Query";
	this->KVMap["IsLockTable"] = "";
	this->KPMap["IsLockTable"] = "Query";
	this->KVMap["SourceDatabaseDBNames"] = "";
	this->KPMap["SourceDatabaseDBNames"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";

}
RdsLockDBInstanceRequest::RdsLockDBInstanceRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "rds";
	this->Action = "LockDBInstance";
	this->Version = "2014-08-15";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["DBInstanceId"] = "";
	this->KPMap["DBInstanceId"] = "Query";
	this->KVMap["LockReason"] = "";
	this->KPMap["LockReason"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";

}
RdsMigrateToOtherZoneRequest::RdsMigrateToOtherZoneRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "rds";
	this->Action = "MigrateToOtherZone";
	this->Version = "2014-08-15";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["DBInstanceId"] = "";
	this->KPMap["DBInstanceId"] = "Query";
	this->KVMap["ZoneId"] = "";
	this->KPMap["ZoneId"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";

}
RdsModifyAccountDescriptionRequest::RdsModifyAccountDescriptionRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "rds";
	this->Action = "ModifyAccountDescription";
	this->Version = "2014-08-15";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["DBInstanceId"] = "";
	this->KPMap["DBInstanceId"] = "Query";
	this->KVMap["AccountName"] = "";
	this->KPMap["AccountName"] = "Query";
	this->KVMap["AccountDescription"] = "";
	this->KPMap["AccountDescription"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";

}
RdsModifyBackupPolicyRequest::RdsModifyBackupPolicyRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "rds";
	this->Action = "ModifyBackupPolicy";
	this->Version = "2014-08-15";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["DBInstanceId"] = "";
	this->KPMap["DBInstanceId"] = "Query";
	this->KVMap["PreferredBackupTime"] = "";
	this->KPMap["PreferredBackupTime"] = "Query";
	this->KVMap["PreferredBackupPeriod"] = "";
	this->KPMap["PreferredBackupPeriod"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";

}
RdsModifyDBDescriptionRequest::RdsModifyDBDescriptionRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "rds";
	this->Action = "ModifyDBDescription";
	this->Version = "2014-08-15";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["DBInstanceId"] = "";
	this->KPMap["DBInstanceId"] = "Query";
	this->KVMap["DBName"] = "";
	this->KPMap["DBName"] = "Query";
	this->KVMap["DBDescription"] = "";
	this->KPMap["DBDescription"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";

}
RdsModifyDBInstanceConnectionModeRequest::RdsModifyDBInstanceConnectionModeRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "rds";
	this->Action = "ModifyDBInstanceConnectionMode";
	this->Version = "2014-08-15";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["DBInstanceId"] = "";
	this->KPMap["DBInstanceId"] = "Query";
	this->KVMap["ConnectionMode"] = "";
	this->KPMap["ConnectionMode"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";

}
RdsModifyDBInstanceConnectionStringRequest::RdsModifyDBInstanceConnectionStringRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "rds";
	this->Action = "ModifyDBInstanceConnectionString";
	this->Version = "2014-08-15";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["DBInstanceId"] = "";
	this->KPMap["DBInstanceId"] = "Query";
	this->KVMap["CurrentConnectionString"] = "";
	this->KPMap["CurrentConnectionString"] = "Query";
	this->KVMap["ConnectionStringPrefix"] = "";
	this->KPMap["ConnectionStringPrefix"] = "Query";
	this->KVMap["Port"] = "";
	this->KPMap["Port"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";

}
RdsModifyDBInstanceDescriptionRequest::RdsModifyDBInstanceDescriptionRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "rds";
	this->Action = "ModifyDBInstanceDescription";
	this->Version = "2014-08-15";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["ClientToken"] = "";
	this->KPMap["ClientToken"] = "Query";
	this->KVMap["DBInstanceId"] = "";
	this->KPMap["DBInstanceId"] = "Query";
	this->KVMap["DBInstanceDescription"] = "";
	this->KPMap["DBInstanceDescription"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";

}
RdsModifyDBInstanceMaintainTimeRequest::RdsModifyDBInstanceMaintainTimeRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "rds";
	this->Action = "ModifyDBInstanceMaintainTime";
	this->Version = "2014-08-15";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["ClientToken"] = "";
	this->KPMap["ClientToken"] = "Query";
	this->KVMap["DBInstanceId"] = "";
	this->KPMap["DBInstanceId"] = "Query";
	this->KVMap["MaintainTime"] = "";
	this->KPMap["MaintainTime"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";

}
RdsModifyDBInstanceNetworkTypeRequest::RdsModifyDBInstanceNetworkTypeRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "rds";
	this->Action = "ModifyDBInstanceNetworkType";
	this->Version = "2014-08-15";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["DBInstanceId"] = "";
	this->KPMap["DBInstanceId"] = "Query";
	this->KVMap["InstanceNetworkType"] = "";
	this->KPMap["InstanceNetworkType"] = "Query";
	this->KVMap["VPCId"] = "";
	this->KPMap["VPCId"] = "Query";
	this->KVMap["VSwitchId"] = "";
	this->KPMap["VSwitchId"] = "Query";
	this->KVMap["PrivateIpAddress"] = "";
	this->KPMap["PrivateIpAddress"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";

}
RdsModifyDBInstanceSpecRequest::RdsModifyDBInstanceSpecRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "rds";
	this->Action = "ModifyDBInstanceSpec";
	this->Version = "2014-08-15";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["ClientToken"] = "";
	this->KPMap["ClientToken"] = "Query";
	this->KVMap["DBInstanceId"] = "";
	this->KPMap["DBInstanceId"] = "Query";
	this->KVMap["DBInstanceClass"] = "";
	this->KPMap["DBInstanceClass"] = "Query";
	this->KVMap["DBInstanceStorage"] = "";
	this->KPMap["DBInstanceStorage"] = "Query";
	this->KVMap["PayType"] = "";
	this->KPMap["PayType"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";

}
RdsModifyParameterRequest::RdsModifyParameterRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "rds";
	this->Action = "ModifyParameter";
	this->Version = "2014-08-15";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["ClientToken"] = "";
	this->KPMap["ClientToken"] = "Query";
	this->KVMap["DBInstanceId"] = "";
	this->KPMap["DBInstanceId"] = "Query";
	this->KVMap["Parameters"] = "";
	this->KPMap["Parameters"] = "Query";
	this->KVMap["Forcerestart"] = "";
	this->KPMap["Forcerestart"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";

}
RdsModifyPostpaidDBInstanceSpecRequest::RdsModifyPostpaidDBInstanceSpecRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "rds";
	this->Action = "ModifyPostpaidDBInstanceSpec";
	this->Version = "2014-08-15";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["ClientToken"] = "";
	this->KPMap["ClientToken"] = "Query";
	this->KVMap["DBInstanceId"] = "";
	this->KPMap["DBInstanceId"] = "Query";
	this->KVMap["DBInstanceClass"] = "";
	this->KPMap["DBInstanceClass"] = "Query";
	this->KVMap["DBInstanceStorage"] = "";
	this->KPMap["DBInstanceStorage"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";

}
RdsModifySecurityIpsRequest::RdsModifySecurityIpsRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "rds";
	this->Action = "ModifySecurityIps";
	this->Version = "2014-08-15";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["ClientToken"] = "";
	this->KPMap["ClientToken"] = "Query";
	this->KVMap["DBInstanceId"] = "";
	this->KPMap["DBInstanceId"] = "Query";
	this->KVMap["SecurityIps"] = "";
	this->KPMap["SecurityIps"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";

}
RdsPreCheckBeforeImportDataRequest::RdsPreCheckBeforeImportDataRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "rds";
	this->Action = "PreCheckBeforeImportData";
	this->Version = "2014-08-15";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["ClientToken"] = "";
	this->KPMap["ClientToken"] = "Query";
	this->KVMap["DBInstanceId"] = "";
	this->KPMap["DBInstanceId"] = "Query";
	this->KVMap["SourceDatabaseIp"] = "";
	this->KPMap["SourceDatabaseIp"] = "Query";
	this->KVMap["SourceDatabasePort"] = "";
	this->KPMap["SourceDatabasePort"] = "Query";
	this->KVMap["SourceDatabaseUserName"] = "";
	this->KPMap["SourceDatabaseUserName"] = "Query";
	this->KVMap["SourceDatabasePassword"] = "";
	this->KPMap["SourceDatabasePassword"] = "Query";
	this->KVMap["ImportDataType"] = "";
	this->KPMap["ImportDataType"] = "Query";
	this->KVMap["SourceDatabaseDBNames"] = "";
	this->KPMap["SourceDatabaseDBNames"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";

}
RdsPurgeDBInstanceLogRequest::RdsPurgeDBInstanceLogRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "rds";
	this->Action = "PurgeDBInstanceLog";
	this->Version = "2014-08-15";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["ClientToken"] = "";
	this->KPMap["ClientToken"] = "Query";
	this->KVMap["DBInstanceId"] = "";
	this->KPMap["DBInstanceId"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";

}
RdsReleaseInstancePublicConnectionRequest::RdsReleaseInstancePublicConnectionRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "rds";
	this->Action = "ReleaseInstancePublicConnection";
	this->Version = "2014-08-15";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["DBInstanceId"] = "";
	this->KPMap["DBInstanceId"] = "Query";
	this->KVMap["CurrentConnectionString"] = "";
	this->KPMap["CurrentConnectionString"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";

}
RdsRemoveTagsFromResourceRequest::RdsRemoveTagsFromResourceRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "rds";
	this->Action = "RemoveTagsFromResource";
	this->Version = "2014-08-15";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["ClientToken"] = "";
	this->KPMap["ClientToken"] = "Query";
	this->KVMap["proxyId"] = "";
	this->KPMap["proxyId"] = "Query";
	this->KVMap["DBInstanceId"] = "";
	this->KPMap["DBInstanceId"] = "Query";
	this->KVMap["Tag.1.key"] = "";
	this->KPMap["Tag.1.key"] = "Query";
	this->KVMap["Tag.2.key"] = "";
	this->KPMap["Tag.2.key"] = "Query";
	this->KVMap["Tag.3.key"] = "";
	this->KPMap["Tag.3.key"] = "Query";
	this->KVMap["Tag.4.key"] = "";
	this->KPMap["Tag.4.key"] = "Query";
	this->KVMap["Tag.5.key"] = "";
	this->KPMap["Tag.5.key"] = "Query";
	this->KVMap["Tag.1.value"] = "";
	this->KPMap["Tag.1.value"] = "Query";
	this->KVMap["Tag.2.value"] = "";
	this->KPMap["Tag.2.value"] = "Query";
	this->KVMap["Tag.3.value"] = "";
	this->KPMap["Tag.3.value"] = "Query";
	this->KVMap["Tag.4.value"] = "";
	this->KPMap["Tag.4.value"] = "Query";
	this->KVMap["Tag.5.value"] = "";
	this->KPMap["Tag.5.value"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";

}
RdsResetAccountForPGRequest::RdsResetAccountForPGRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "rds";
	this->Action = "ResetAccountForPG";
	this->Version = "2014-08-15";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["DBInstanceId"] = "";
	this->KPMap["DBInstanceId"] = "Query";
	this->KVMap["AccountName"] = "";
	this->KPMap["AccountName"] = "Query";
	this->KVMap["AccountPassword"] = "";
	this->KPMap["AccountPassword"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";

}
RdsResetAccountPasswordRequest::RdsResetAccountPasswordRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "rds";
	this->Action = "ResetAccountPassword";
	this->Version = "2014-08-15";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["DBInstanceId"] = "";
	this->KPMap["DBInstanceId"] = "Query";
	this->KVMap["AccountName"] = "";
	this->KPMap["AccountName"] = "Query";
	this->KVMap["AccountPassword"] = "";
	this->KPMap["AccountPassword"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";

}
RdsRestartDBInstanceRequest::RdsRestartDBInstanceRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "rds";
	this->Action = "RestartDBInstance";
	this->Version = "2014-08-15";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["ClientToken"] = "";
	this->KPMap["ClientToken"] = "Query";
	this->KVMap["DBInstanceId"] = "";
	this->KPMap["DBInstanceId"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";

}
RdsRestoreDBInstanceRequest::RdsRestoreDBInstanceRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "rds";
	this->Action = "RestoreDBInstance";
	this->Version = "2014-08-15";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["ClientToken"] = "";
	this->KPMap["ClientToken"] = "Query";
	this->KVMap["DBInstanceId"] = "";
	this->KPMap["DBInstanceId"] = "Query";
	this->KVMap["BackupId"] = "";
	this->KPMap["BackupId"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";

}
RdsRevokeAccountPrivilegeRequest::RdsRevokeAccountPrivilegeRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "rds";
	this->Action = "RevokeAccountPrivilege";
	this->Version = "2014-08-15";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["DBInstanceId"] = "";
	this->KPMap["DBInstanceId"] = "Query";
	this->KVMap["AccountName"] = "";
	this->KPMap["AccountName"] = "Query";
	this->KVMap["DBName"] = "";
	this->KPMap["DBName"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";

}
RdsStartDBInstanceDiagnoseRequest::RdsStartDBInstanceDiagnoseRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "rds";
	this->Action = "StartDBInstanceDiagnose";
	this->Version = "2014-08-15";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["ClientToken"] = "";
	this->KPMap["ClientToken"] = "Query";
	this->KVMap["proxyId"] = "";
	this->KPMap["proxyId"] = "Query";
	this->KVMap["DBInstanceId"] = "";
	this->KPMap["DBInstanceId"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";

}
RdsStopSyncingRequest::RdsStopSyncingRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "rds";
	this->Action = "StopSyncing";
	this->Version = "2014-08-15";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["ClientToken"] = "";
	this->KPMap["ClientToken"] = "Query";
	this->KVMap["DBInstanceId"] = "";
	this->KPMap["DBInstanceId"] = "Query";
	this->KVMap["ImportId"] = "";
	this->KPMap["ImportId"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";

}
RdsSwitchDBInstanceNetTypeRequest::RdsSwitchDBInstanceNetTypeRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "rds";
	this->Action = "SwitchDBInstanceNetType";
	this->Version = "2014-08-15";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["ClientToken"] = "";
	this->KPMap["ClientToken"] = "Query";
	this->KVMap["DBInstanceId"] = "";
	this->KPMap["DBInstanceId"] = "Query";
	this->KVMap["ConnectionStringPrefix"] = "";
	this->KPMap["ConnectionStringPrefix"] = "Query";
	this->KVMap["Port"] = "";
	this->KPMap["Port"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";

}
RdsUnlockDBInstanceRequest::RdsUnlockDBInstanceRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "rds";
	this->Action = "UnlockDBInstance";
	this->Version = "2014-08-15";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["DBInstanceId"] = "";
	this->KPMap["DBInstanceId"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";

}
RdsUpgradeDBInstanceEngineVersionRequest::RdsUpgradeDBInstanceEngineVersionRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "rds";
	this->Action = "UpgradeDBInstanceEngineVersion";
	this->Version = "2014-08-15";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["ClientToken"] = "";
	this->KPMap["ClientToken"] = "Query";
	this->KVMap["DBInstanceId"] = "";
	this->KPMap["DBInstanceId"] = "Query";
	this->KVMap["EngineVersion"] = "";
	this->KPMap["EngineVersion"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";

}

	
}