/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
 
#ifndef ALIYUNRDS_RDSSERVICE_H_
#define ALIYUNRDS_RDSSERVICE_H_

#include "../aliyun-cpp-sdk-core/BaseRequest.h"
#include "../aliyun-cpp-sdk-core/ApiBase.h"

namespace AliYunRds {

class RdsClient : public AliYunCore::ApiBase {
private:
	AliYunCore::DefaultProfile &Profile;
public:
	RdsClient(AliYunCore::DefaultProfile &pf);
	void setProfile(AliYunCore::DefaultProfile &pf);
	std::string getResponse(AliYunCore::BaseRequest &request);
	~RdsClient(){};
};
class RdsAddTagsToResourceRequest : public AliYunCore::BaseRequest {
public:
	RdsAddTagsToResourceRequest();
};
class RdsAllocateInstancePrivateConnectionRequest : public AliYunCore::BaseRequest {
public:
	RdsAllocateInstancePrivateConnectionRequest();
};
class RdsAllocateInstancePublicConnectionRequest : public AliYunCore::BaseRequest {
public:
	RdsAllocateInstancePublicConnectionRequest();
};
class RdsBatchGrantAccountPrivilegeRequest : public AliYunCore::BaseRequest {
public:
	RdsBatchGrantAccountPrivilegeRequest();
};
class RdsBatchRevokeAccountPrivilegeRequest : public AliYunCore::BaseRequest {
public:
	RdsBatchRevokeAccountPrivilegeRequest();
};
class RdsCancelImportRequest : public AliYunCore::BaseRequest {
public:
	RdsCancelImportRequest();
};
class RdsCheckAccountNameAvailableRequest : public AliYunCore::BaseRequest {
public:
	RdsCheckAccountNameAvailableRequest();
};
class RdsCheckDBNameAvailableRequest : public AliYunCore::BaseRequest {
public:
	RdsCheckDBNameAvailableRequest();
};
class RdsCreateAccountRequest : public AliYunCore::BaseRequest {
public:
	RdsCreateAccountRequest();
};
class RdsCreateAccountForInnerRequest : public AliYunCore::BaseRequest {
public:
	RdsCreateAccountForInnerRequest();
};
class RdsCreateBackupRequest : public AliYunCore::BaseRequest {
public:
	RdsCreateBackupRequest();
};
class RdsCreateDatabaseRequest : public AliYunCore::BaseRequest {
public:
	RdsCreateDatabaseRequest();
};
class RdsCreateDatabaseForInnerRequest : public AliYunCore::BaseRequest {
public:
	RdsCreateDatabaseForInnerRequest();
};
class RdsCreateDBInstanceRequest : public AliYunCore::BaseRequest {
public:
	RdsCreateDBInstanceRequest();
};
class RdsCreateDBInstanceForChannelRequest : public AliYunCore::BaseRequest {
public:
	RdsCreateDBInstanceForChannelRequest();
};
class RdsCreateDBInstanceforFirstPayRequest : public AliYunCore::BaseRequest {
public:
	RdsCreateDBInstanceforFirstPayRequest();
};
class RdsCreatePostpaidDBInstanceRequest : public AliYunCore::BaseRequest {
public:
	RdsCreatePostpaidDBInstanceRequest();
};
class RdsCreatePostpaidDBInstanceForChannelRequest : public AliYunCore::BaseRequest {
public:
	RdsCreatePostpaidDBInstanceForChannelRequest();
};
class RdsCreateReadOnlyDBInstanceRequest : public AliYunCore::BaseRequest {
public:
	RdsCreateReadOnlyDBInstanceRequest();
};
class RdsCreateTempDBInstanceRequest : public AliYunCore::BaseRequest {
public:
	RdsCreateTempDBInstanceRequest();
};
class RdsCreateUploadPathForSQLServerRequest : public AliYunCore::BaseRequest {
public:
	RdsCreateUploadPathForSQLServerRequest();
};
class RdsDeleteAccountRequest : public AliYunCore::BaseRequest {
public:
	RdsDeleteAccountRequest();
};
class RdsDeleteDatabaseRequest : public AliYunCore::BaseRequest {
public:
	RdsDeleteDatabaseRequest();
};
class RdsDeleteDBInstanceRequest : public AliYunCore::BaseRequest {
public:
	RdsDeleteDBInstanceRequest();
};
class RdsDescibeImportsFromDatabaseRequest : public AliYunCore::BaseRequest {
public:
	RdsDescibeImportsFromDatabaseRequest();
};
class RdsDescribeAbnormalDBInstancesRequest : public AliYunCore::BaseRequest {
public:
	RdsDescribeAbnormalDBInstancesRequest();
};
class RdsDescribeAccountsRequest : public AliYunCore::BaseRequest {
public:
	RdsDescribeAccountsRequest();
};
class RdsDescribeBackupPolicyRequest : public AliYunCore::BaseRequest {
public:
	RdsDescribeBackupPolicyRequest();
};
class RdsDescribeBackupsRequest : public AliYunCore::BaseRequest {
public:
	RdsDescribeBackupsRequest();
};
class RdsDescribeBackupTasksRequest : public AliYunCore::BaseRequest {
public:
	RdsDescribeBackupTasksRequest();
};
class RdsDescribeBinlogFilesRequest : public AliYunCore::BaseRequest {
public:
	RdsDescribeBinlogFilesRequest();
};
class RdsDescribeDatabasesRequest : public AliYunCore::BaseRequest {
public:
	RdsDescribeDatabasesRequest();
};
class RdsDescribeDBInstanceAttributeRequest : public AliYunCore::BaseRequest {
public:
	RdsDescribeDBInstanceAttributeRequest();
};
class RdsDescribeDBInstanceIPArrayListRequest : public AliYunCore::BaseRequest {
public:
	RdsDescribeDBInstanceIPArrayListRequest();
};
class RdsDescribeDBInstanceNetInfoRequest : public AliYunCore::BaseRequest {
public:
	RdsDescribeDBInstanceNetInfoRequest();
};
class RdsDescribeDBInstanceNetInfoForChannelRequest : public AliYunCore::BaseRequest {
public:
	RdsDescribeDBInstanceNetInfoForChannelRequest();
};
class RdsDescribeDBInstancePerformanceRequest : public AliYunCore::BaseRequest {
public:
	RdsDescribeDBInstancePerformanceRequest();
};
class RdsDescribeDBInstancesRequest : public AliYunCore::BaseRequest {
public:
	RdsDescribeDBInstancesRequest();
};
class RdsDescribeDBInstancesByExpireTimeRequest : public AliYunCore::BaseRequest {
public:
	RdsDescribeDBInstancesByExpireTimeRequest();
};
class RdsDescribeDBInstancesByPerformanceRequest : public AliYunCore::BaseRequest {
public:
	RdsDescribeDBInstancesByPerformanceRequest();
};
class RdsDescribeErrorLogsRequest : public AliYunCore::BaseRequest {
public:
	RdsDescribeErrorLogsRequest();
};
class RdsDescribeFilesForSQLServerRequest : public AliYunCore::BaseRequest {
public:
	RdsDescribeFilesForSQLServerRequest();
};
class RdsDescribeImportsForSQLServerRequest : public AliYunCore::BaseRequest {
public:
	RdsDescribeImportsForSQLServerRequest();
};
class RdsDescribeModifyParameterLogRequest : public AliYunCore::BaseRequest {
public:
	RdsDescribeModifyParameterLogRequest();
};
class RdsDescribeOperationLogsRequest : public AliYunCore::BaseRequest {
public:
	RdsDescribeOperationLogsRequest();
};
class RdsDescribeOptimizeAdviceByDBARequest : public AliYunCore::BaseRequest {
public:
	RdsDescribeOptimizeAdviceByDBARequest();
};
class RdsDescribeOptimizeAdviceOnBigTableRequest : public AliYunCore::BaseRequest {
public:
	RdsDescribeOptimizeAdviceOnBigTableRequest();
};
class RdsDescribeOptimizeAdviceOnExcessIndexRequest : public AliYunCore::BaseRequest {
public:
	RdsDescribeOptimizeAdviceOnExcessIndexRequest();
};
class RdsDescribeOptimizeAdviceOnMissIndexRequest : public AliYunCore::BaseRequest {
public:
	RdsDescribeOptimizeAdviceOnMissIndexRequest();
};
class RdsDescribeOptimizeAdviceOnMissPKRequest : public AliYunCore::BaseRequest {
public:
	RdsDescribeOptimizeAdviceOnMissPKRequest();
};
class RdsDescribeOptimizeAdviceOnStorageRequest : public AliYunCore::BaseRequest {
public:
	RdsDescribeOptimizeAdviceOnStorageRequest();
};
class RdsDescribeParametersRequest : public AliYunCore::BaseRequest {
public:
	RdsDescribeParametersRequest();
};
class RdsDescribeParameterTemplatesRequest : public AliYunCore::BaseRequest {
public:
	RdsDescribeParameterTemplatesRequest();
};
class RdsDescribePreCheckResultsRequest : public AliYunCore::BaseRequest {
public:
	RdsDescribePreCheckResultsRequest();
};
class RdsDescribeRealtimeDiagnosesRequest : public AliYunCore::BaseRequest {
public:
	RdsDescribeRealtimeDiagnosesRequest();
};
class RdsDescribeRegionsRequest : public AliYunCore::BaseRequest {
public:
	RdsDescribeRegionsRequest();
};
class RdsDescribeResourceUsageRequest : public AliYunCore::BaseRequest {
public:
	RdsDescribeResourceUsageRequest();
};
class RdsDescribeSlowLogRecordsRequest : public AliYunCore::BaseRequest {
public:
	RdsDescribeSlowLogRecordsRequest();
};
class RdsDescribeSlowLogsRequest : public AliYunCore::BaseRequest {
public:
	RdsDescribeSlowLogsRequest();
};
class RdsDescribeSQLInjectionInfosRequest : public AliYunCore::BaseRequest {
public:
	RdsDescribeSQLInjectionInfosRequest();
};
class RdsDescribeSQLLogRecordsRequest : public AliYunCore::BaseRequest {
public:
	RdsDescribeSQLLogRecordsRequest();
};
class RdsDescribeSQLLogReportListRequest : public AliYunCore::BaseRequest {
public:
	RdsDescribeSQLLogReportListRequest();
};
class RdsDescribeSQLLogReportsRequest : public AliYunCore::BaseRequest {
public:
	RdsDescribeSQLLogReportsRequest();
};
class RdsDescribeTasksRequest : public AliYunCore::BaseRequest {
public:
	RdsDescribeTasksRequest();
};
class RdsDescribeVpcZoneNosRequest : public AliYunCore::BaseRequest {
public:
	RdsDescribeVpcZoneNosRequest();
};
class RdsExtractBackupFromOASRequest : public AliYunCore::BaseRequest {
public:
	RdsExtractBackupFromOASRequest();
};
class RdsGrantAccountPrivilegeRequest : public AliYunCore::BaseRequest {
public:
	RdsGrantAccountPrivilegeRequest();
};
class RdsImportDatabaseBetweenInstancesRequest : public AliYunCore::BaseRequest {
public:
	RdsImportDatabaseBetweenInstancesRequest();
};
class RdsImportDataForSQLServerRequest : public AliYunCore::BaseRequest {
public:
	RdsImportDataForSQLServerRequest();
};
class RdsImportDataFromDatabaseRequest : public AliYunCore::BaseRequest {
public:
	RdsImportDataFromDatabaseRequest();
};
class RdsLockDBInstanceRequest : public AliYunCore::BaseRequest {
public:
	RdsLockDBInstanceRequest();
};
class RdsMigrateToOtherZoneRequest : public AliYunCore::BaseRequest {
public:
	RdsMigrateToOtherZoneRequest();
};
class RdsModifyAccountDescriptionRequest : public AliYunCore::BaseRequest {
public:
	RdsModifyAccountDescriptionRequest();
};
class RdsModifyBackupPolicyRequest : public AliYunCore::BaseRequest {
public:
	RdsModifyBackupPolicyRequest();
};
class RdsModifyDBDescriptionRequest : public AliYunCore::BaseRequest {
public:
	RdsModifyDBDescriptionRequest();
};
class RdsModifyDBInstanceConnectionModeRequest : public AliYunCore::BaseRequest {
public:
	RdsModifyDBInstanceConnectionModeRequest();
};
class RdsModifyDBInstanceConnectionStringRequest : public AliYunCore::BaseRequest {
public:
	RdsModifyDBInstanceConnectionStringRequest();
};
class RdsModifyDBInstanceDescriptionRequest : public AliYunCore::BaseRequest {
public:
	RdsModifyDBInstanceDescriptionRequest();
};
class RdsModifyDBInstanceMaintainTimeRequest : public AliYunCore::BaseRequest {
public:
	RdsModifyDBInstanceMaintainTimeRequest();
};
class RdsModifyDBInstanceNetworkTypeRequest : public AliYunCore::BaseRequest {
public:
	RdsModifyDBInstanceNetworkTypeRequest();
};
class RdsModifyDBInstanceSpecRequest : public AliYunCore::BaseRequest {
public:
	RdsModifyDBInstanceSpecRequest();
};
class RdsModifyParameterRequest : public AliYunCore::BaseRequest {
public:
	RdsModifyParameterRequest();
};
class RdsModifyPostpaidDBInstanceSpecRequest : public AliYunCore::BaseRequest {
public:
	RdsModifyPostpaidDBInstanceSpecRequest();
};
class RdsModifySecurityIpsRequest : public AliYunCore::BaseRequest {
public:
	RdsModifySecurityIpsRequest();
};
class RdsPreCheckBeforeImportDataRequest : public AliYunCore::BaseRequest {
public:
	RdsPreCheckBeforeImportDataRequest();
};
class RdsPurgeDBInstanceLogRequest : public AliYunCore::BaseRequest {
public:
	RdsPurgeDBInstanceLogRequest();
};
class RdsReleaseInstancePublicConnectionRequest : public AliYunCore::BaseRequest {
public:
	RdsReleaseInstancePublicConnectionRequest();
};
class RdsRemoveTagsFromResourceRequest : public AliYunCore::BaseRequest {
public:
	RdsRemoveTagsFromResourceRequest();
};
class RdsResetAccountForPGRequest : public AliYunCore::BaseRequest {
public:
	RdsResetAccountForPGRequest();
};
class RdsResetAccountPasswordRequest : public AliYunCore::BaseRequest {
public:
	RdsResetAccountPasswordRequest();
};
class RdsRestartDBInstanceRequest : public AliYunCore::BaseRequest {
public:
	RdsRestartDBInstanceRequest();
};
class RdsRestoreDBInstanceRequest : public AliYunCore::BaseRequest {
public:
	RdsRestoreDBInstanceRequest();
};
class RdsRevokeAccountPrivilegeRequest : public AliYunCore::BaseRequest {
public:
	RdsRevokeAccountPrivilegeRequest();
};
class RdsStartDBInstanceDiagnoseRequest : public AliYunCore::BaseRequest {
public:
	RdsStartDBInstanceDiagnoseRequest();
};
class RdsStopSyncingRequest : public AliYunCore::BaseRequest {
public:
	RdsStopSyncingRequest();
};
class RdsSwitchDBInstanceNetTypeRequest : public AliYunCore::BaseRequest {
public:
	RdsSwitchDBInstanceNetTypeRequest();
};
class RdsUnlockDBInstanceRequest : public AliYunCore::BaseRequest {
public:
	RdsUnlockDBInstanceRequest();
};
class RdsUpgradeDBInstanceEngineVersionRequest : public AliYunCore::BaseRequest {
public:
	RdsUpgradeDBInstanceEngineVersionRequest();
};


}

#endif