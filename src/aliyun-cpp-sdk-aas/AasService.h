/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
 
#ifndef ALIYUNAAS_AASSERVICE_H_
#define ALIYUNAAS_AASSERVICE_H_

#include "../aliyun-cpp-sdk-core/BaseRequest.h"
#include "../aliyun-cpp-sdk-core/ApiBase.h"

namespace AliYunAas {

class AasClient : public AliYunCore::ApiBase {
private:
	AliYunCore::DefaultProfile &Profile;
public:
	AasClient(AliYunCore::DefaultProfile &pf);
	void setProfile(AliYunCore::DefaultProfile &pf);
	std::string getResponse(AliYunCore::BaseRequest &request);
	~AasClient(){};
};
class AasCreateAccessKeyForAccountRequest : public AliYunCore::BaseRequest {
public:
	AasCreateAccessKeyForAccountRequest();
};
class AasCreateAliyunAccountRequest : public AliYunCore::BaseRequest {
public:
	AasCreateAliyunAccountRequest();
};
class AasDeleteAccessKeyForAccountRequest : public AliYunCore::BaseRequest {
public:
	AasDeleteAccessKeyForAccountRequest();
};
class AasGetBasicInfoForAccountRequest : public AliYunCore::BaseRequest {
public:
	AasGetBasicInfoForAccountRequest();
};
class AasListAccessKeysForAccountRequest : public AliYunCore::BaseRequest {
public:
	AasListAccessKeysForAccountRequest();
};
class AasUpdateAccessKeyStatusForAccountRequest : public AliYunCore::BaseRequest {
public:
	AasUpdateAccessKeyStatusForAccountRequest();
};


}

#endif