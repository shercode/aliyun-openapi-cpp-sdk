/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

#include <string>
#include <map>
#include "../aliyun-cpp-sdk-core/utils.h"
#include "../aliyun-cpp-sdk-core/DefaultProfile.h"
#include "../aliyun-cpp-sdk-core/BaseRequest.h"
#include "../aliyun-cpp-sdk-core/ApiBase.h"
#include "AasService.h"

using std::string;
using namespace AliYunCore;

namespace AliYunAas {

AasClient::AasClient(DefaultProfile &pf):Profile(pf){}
void AasClient::setProfile(DefaultProfile &pf){
	this->Profile = pf;
}
string AasClient::getResponse(BaseRequest &request){
	request.RegionId = this->Profile.RegionId;		
	request.AccessKeyId = this->Profile.AccessKeyId;
	request.AccessKeySecret = this->Profile.AccessKeySecret;
	return ApiBase::getResponse(request);
}
AasCreateAccessKeyForAccountRequest::AasCreateAccessKeyForAccountRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "aas";
	this->Action = "CreateAccessKeyForAccount";
	this->Version = "2015-07-01";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["PK"] = "";
	this->KPMap["PK"] = "Query";
	this->KVMap["AKSecret"] = "";
	this->KPMap["AKSecret"] = "Query";

}
AasCreateAliyunAccountRequest::AasCreateAliyunAccountRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "aas";
	this->Action = "CreateAliyunAccount";
	this->Version = "2015-07-01";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["AliyunId"] = "";
	this->KPMap["AliyunId"] = "Query";

}
AasDeleteAccessKeyForAccountRequest::AasDeleteAccessKeyForAccountRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "aas";
	this->Action = "DeleteAccessKeyForAccount";
	this->Version = "2015-07-01";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["PK"] = "";
	this->KPMap["PK"] = "Query";
	this->KVMap["AKId"] = "";
	this->KPMap["AKId"] = "Query";

}
AasGetBasicInfoForAccountRequest::AasGetBasicInfoForAccountRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "aas";
	this->Action = "GetBasicInfoForAccount";
	this->Version = "2015-07-01";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["AliyunId"] = "";
	this->KPMap["AliyunId"] = "Query";

}
AasListAccessKeysForAccountRequest::AasListAccessKeysForAccountRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "aas";
	this->Action = "ListAccessKeysForAccount";
	this->Version = "2015-07-01";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["PK"] = "";
	this->KPMap["PK"] = "Query";
	this->KVMap["AKStatus"] = "";
	this->KPMap["AKStatus"] = "Query";
	this->KVMap["AKType"] = "";
	this->KPMap["AKType"] = "Query";

}
AasUpdateAccessKeyStatusForAccountRequest::AasUpdateAccessKeyStatusForAccountRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "aas";
	this->Action = "UpdateAccessKeyStatusForAccount";
	this->Version = "2015-07-01";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["PK"] = "";
	this->KPMap["PK"] = "Query";
	this->KVMap["AKId"] = "";
	this->KPMap["AKId"] = "Query";
	this->KVMap["AKStatus"] = "";
	this->KPMap["AKStatus"] = "Query";

}

	
}