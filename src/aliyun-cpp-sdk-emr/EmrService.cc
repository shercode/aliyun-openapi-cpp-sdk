/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

#include <string>
#include <map>
#include "../aliyun-cpp-sdk-core/utils.h"
#include "../aliyun-cpp-sdk-core/DefaultProfile.h"
#include "../aliyun-cpp-sdk-core/BaseRequest.h"
#include "../aliyun-cpp-sdk-core/ApiBase.h"
#include "EmrService.h"

using std::string;
using namespace AliYunCore;

namespace AliYunEmr {

EmrClient::EmrClient(DefaultProfile &pf):Profile(pf){}
void EmrClient::setProfile(DefaultProfile &pf){
	this->Profile = pf;
}
string EmrClient::getResponse(BaseRequest &request){
	request.RegionId = this->Profile.RegionId;		
	request.AccessKeyId = this->Profile.AccessKeyId;
	request.AccessKeySecret = this->Profile.AccessKeySecret;
	return ApiBase::getResponse(request);
}
EmrCreateClusterRequest::EmrCreateClusterRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "emr";
	this->Action = "CreateCluster";
	this->Version = "2015-09-10";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["Name"] = "";
	this->KPMap["Name"] = "Query";
	this->KVMap["ZoneId"] = "";
	this->KPMap["ZoneId"] = "Query";
	this->KVMap["LogEnable"] = "";
	this->KPMap["LogEnable"] = "Query";
	this->KVMap["LogPath"] = "";
	this->KPMap["LogPath"] = "Query";
	this->KVMap["SecurityGroup"] = "";
	this->KPMap["SecurityGroup"] = "Query";
	this->KVMap["IsOpenPublicIp"] = "";
	this->KPMap["IsOpenPublicIp"] = "Query";
	this->KVMap["SecurityGroupName"] = "";
	this->KPMap["SecurityGroupName"] = "Query";
	this->KVMap["EmrVer"] = "";
	this->KPMap["EmrVer"] = "Query";
	this->KVMap["ClusterType"] = "";
	this->KPMap["ClusterType"] = "Query";
	this->KVMap["Install"] = "";
	this->KPMap["Install"] = "Query";
	this->KVMap["MasterIndex"] = "";
	this->KPMap["MasterIndex"] = "Query";
	this->KVMap["EcsOrder"] = "";
	this->KPMap["EcsOrder"] = "Query";
	this->KVMap["PayType"] = "";
	this->KPMap["PayType"] = "Query";
	this->KVMap["Period"] = "";
	this->KPMap["Period"] = "Query";
	this->KVMap["EmrRole4ECS"] = "";
	this->KPMap["EmrRole4ECS"] = "Query";
	this->KVMap["EmrRole4Oss"] = "";
	this->KPMap["EmrRole4Oss"] = "Query";

}
EmrCreateExecutePlanRequest::EmrCreateExecutePlanRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "emr";
	this->Action = "CreateExecutePlan";
	this->Version = "2015-09-10";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["ClusterId"] = "";
	this->KPMap["ClusterId"] = "Query";
	this->KVMap["Name"] = "";
	this->KPMap["Name"] = "Query";
	this->KVMap["Strategy"] = "";
	this->KPMap["Strategy"] = "Query";
	this->KVMap["TimeInterval"] = "";
	this->KPMap["TimeInterval"] = "Query";
	this->KVMap["StartTime"] = "";
	this->KPMap["StartTime"] = "Query";
	this->KVMap["TimeUnit"] = "";
	this->KPMap["TimeUnit"] = "Query";
	this->KVMap["JobId"] = "";
	this->KPMap["JobId"] = "Query";

}
EmrCreateExecutePlanWithClusterRequest::EmrCreateExecutePlanWithClusterRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "emr";
	this->Action = "CreateExecutePlanWithCluster";
	this->Version = "2015-09-10";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["ClusterName"] = "";
	this->KPMap["ClusterName"] = "Query";
	this->KVMap["ZoneId"] = "";
	this->KPMap["ZoneId"] = "Query";
	this->KVMap["LogEnable"] = "";
	this->KPMap["LogEnable"] = "Query";
	this->KVMap["LogPath"] = "";
	this->KPMap["LogPath"] = "Query";
	this->KVMap["SecurityGroup"] = "";
	this->KPMap["SecurityGroup"] = "Query";
	this->KVMap["IsOpenPublicIp"] = "";
	this->KPMap["IsOpenPublicIp"] = "Query";
	this->KVMap["SecurityGroupName"] = "";
	this->KPMap["SecurityGroupName"] = "Query";
	this->KVMap["EmrVer"] = "";
	this->KPMap["EmrVer"] = "Query";
	this->KVMap["ClusterType"] = "";
	this->KPMap["ClusterType"] = "Query";
	this->KVMap["Install"] = "";
	this->KPMap["Install"] = "Query";
	this->KVMap["MasterIndex"] = "";
	this->KPMap["MasterIndex"] = "Query";
	this->KVMap["EcsOrder"] = "";
	this->KPMap["EcsOrder"] = "Query";
	this->KVMap["EmrRole4ECS"] = "";
	this->KPMap["EmrRole4ECS"] = "Query";
	this->KVMap["EmrRole4Oss"] = "";
	this->KPMap["EmrRole4Oss"] = "Query";
	this->KVMap["PayType"] = "";
	this->KPMap["PayType"] = "Query";
	this->KVMap["Period"] = "";
	this->KPMap["Period"] = "Query";
	this->KVMap["Name"] = "";
	this->KPMap["Name"] = "Query";
	this->KVMap["Strategy"] = "";
	this->KPMap["Strategy"] = "Query";
	this->KVMap["TimeInterval"] = "";
	this->KPMap["TimeInterval"] = "Query";
	this->KVMap["StartTime"] = "";
	this->KPMap["StartTime"] = "Query";
	this->KVMap["TimeUnit"] = "";
	this->KPMap["TimeUnit"] = "Query";
	this->KVMap["JobId"] = "";
	this->KPMap["JobId"] = "Query";

}
EmrCreateJobRequest::EmrCreateJobRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "emr";
	this->Action = "CreateJob";
	this->Version = "2015-09-10";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["JobName"] = "";
	this->KPMap["JobName"] = "Query";
	this->KVMap["JobType"] = "";
	this->KPMap["JobType"] = "Query";
	this->KVMap["Parameter"] = "";
	this->KPMap["Parameter"] = "Query";
	this->KVMap["JobFailAct"] = "";
	this->KPMap["JobFailAct"] = "Query";

}
EmrDeleteExecutePlanRequest::EmrDeleteExecutePlanRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "emr";
	this->Action = "DeleteExecutePlan";
	this->Version = "2015-09-10";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["Id"] = "";
	this->KPMap["Id"] = "Query";

}
EmrDeleteJobRequest::EmrDeleteJobRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "emr";
	this->Action = "DeleteJob";
	this->Version = "2015-09-10";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["Id"] = "";
	this->KPMap["Id"] = "Query";

}
EmrDescribeClusterRequest::EmrDescribeClusterRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "emr";
	this->Action = "DescribeCluster";
	this->Version = "2015-09-10";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["ClusterId"] = "";
	this->KPMap["ClusterId"] = "Query";

}
EmrDescribeExecutePlanRequest::EmrDescribeExecutePlanRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "emr";
	this->Action = "DescribeExecutePlan";
	this->Version = "2015-09-10";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["Id"] = "";
	this->KPMap["Id"] = "Query";

}
EmrDescribeJobRequest::EmrDescribeJobRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "emr";
	this->Action = "DescribeJob";
	this->Version = "2015-09-10";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["Id"] = "";
	this->KPMap["Id"] = "Query";

}
EmrJobResourceRequest::EmrJobResourceRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "emr";
	this->Action = "JobResource";
	this->Version = "2015-09-10";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["Bucket"] = "";
	this->KPMap["Bucket"] = "Query";
	this->KVMap["Path"] = "";
	this->KPMap["Path"] = "Query";

}
EmrKillExecutePlanRecordNodeRequest::EmrKillExecutePlanRecordNodeRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "emr";
	this->Action = "KillExecutePlanRecordNode";
	this->Version = "2015-09-10";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["ExecutePlanRecordNodeId"] = "";
	this->KPMap["ExecutePlanRecordNodeId"] = "Query";

}
EmrListClustersRequest::EmrListClustersRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "emr";
	this->Action = "ListClusters";
	this->Version = "2015-09-10";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["ClusterType"] = "";
	this->KPMap["ClusterType"] = "Query";
	this->KVMap["PayType"] = "";
	this->KPMap["PayType"] = "Query";
	this->KVMap["Status"] = "";
	this->KPMap["Status"] = "Query";
	this->KVMap["IsDesc"] = "";
	this->KPMap["IsDesc"] = "Query";
	this->KVMap["PageNumber"] = "";
	this->KPMap["PageNumber"] = "Query";
	this->KVMap["PageSize"] = "";
	this->KPMap["PageSize"] = "Query";

}
EmrListConfigTypeRequest::EmrListConfigTypeRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "emr";
	this->Action = "ListConfigType";
	this->Version = "2015-09-10";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 

}
EmrListExecutePlanExecuteRecordNodesRequest::EmrListExecutePlanExecuteRecordNodesRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "emr";
	this->Action = "ListExecutePlanExecuteRecordNodes";
	this->Version = "2015-09-10";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["ExecutePlanExecRecordId"] = "";
	this->KPMap["ExecutePlanExecRecordId"] = "Query";
	this->KVMap["IsDesc"] = "";
	this->KPMap["IsDesc"] = "Query";
	this->KVMap["PageNumber"] = "";
	this->KPMap["PageNumber"] = "Query";
	this->KVMap["PageSize"] = "";
	this->KPMap["PageSize"] = "Query";

}
EmrListExecutePlanExecuteRecordsRequest::EmrListExecutePlanExecuteRecordsRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "emr";
	this->Action = "ListExecutePlanExecuteRecords";
	this->Version = "2015-09-10";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["ExecutePlanId"] = "";
	this->KPMap["ExecutePlanId"] = "Query";
	this->KVMap["IsDesc"] = "";
	this->KPMap["IsDesc"] = "Query";
	this->KVMap["Status"] = "";
	this->KPMap["Status"] = "Query";
	this->KVMap["PageNumber"] = "";
	this->KPMap["PageNumber"] = "Query";
	this->KVMap["PageSize"] = "";
	this->KPMap["PageSize"] = "Query";

}
EmrListExecutePlanJobsRequest::EmrListExecutePlanJobsRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "emr";
	this->Action = "ListExecutePlanJobs";
	this->Version = "2015-09-10";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["ExecutePlanExecuteNodeId"] = "";
	this->KPMap["ExecutePlanExecuteNodeId"] = "Query";
	this->KVMap["ExecutePlanExecRecordId"] = "";
	this->KPMap["ExecutePlanExecRecordId"] = "Query";
	this->KVMap["IsDesc"] = "";
	this->KPMap["IsDesc"] = "Query";
	this->KVMap["PageNumber"] = "";
	this->KPMap["PageNumber"] = "Query";
	this->KVMap["PageSize"] = "";
	this->KPMap["PageSize"] = "Query";

}
EmrListExecutePlanNodeInstancesRequest::EmrListExecutePlanNodeInstancesRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "emr";
	this->Action = "ListExecutePlanNodeInstances";
	this->Version = "2015-09-10";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["ExecutePlanWorkNodeId"] = "";
	this->KPMap["ExecutePlanWorkNodeId"] = "Query";

}
EmrListExecutePlansRequest::EmrListExecutePlansRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "emr";
	this->Action = "ListExecutePlans";
	this->Version = "2015-09-10";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["Strategy"] = "";
	this->KPMap["Strategy"] = "Query";
	this->KVMap["Status"] = "";
	this->KPMap["Status"] = "Query";
	this->KVMap["IsDesc"] = "";
	this->KPMap["IsDesc"] = "Query";
	this->KVMap["PageNumber"] = "";
	this->KPMap["PageNumber"] = "Query";
	this->KVMap["PageSize"] = "";
	this->KPMap["PageSize"] = "Query";

}
EmrListJobsRequest::EmrListJobsRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "emr";
	this->Action = "ListJobs";
	this->Version = "2015-09-10";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["IsDesc"] = "";
	this->KPMap["IsDesc"] = "Query";
	this->KVMap["PageNumber"] = "";
	this->KPMap["PageNumber"] = "Query";
	this->KVMap["PageSize"] = "";
	this->KPMap["PageSize"] = "Query";

}
EmrListRegionsRequest::EmrListRegionsRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "emr";
	this->Action = "ListRegions";
	this->Version = "2015-09-10";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 

}
EmrModifyClusterNameRequest::EmrModifyClusterNameRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "emr";
	this->Action = "ModifyClusterName";
	this->Version = "2015-09-10";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["ClusterId"] = "";
	this->KPMap["ClusterId"] = "Query";
	this->KVMap["ClusterName"] = "";
	this->KPMap["ClusterName"] = "Query";

}
EmrModifyExecutePlanRequest::EmrModifyExecutePlanRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "emr";
	this->Action = "ModifyExecutePlan";
	this->Version = "2015-09-10";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["ClusterId"] = "";
	this->KPMap["ClusterId"] = "Query";
	this->KVMap["ExecutePlanId"] = "";
	this->KPMap["ExecutePlanId"] = "Query";
	this->KVMap["Name"] = "";
	this->KPMap["Name"] = "Query";
	this->KVMap["Strategy"] = "";
	this->KPMap["Strategy"] = "Query";
	this->KVMap["TimeInterval"] = "";
	this->KPMap["TimeInterval"] = "Query";
	this->KVMap["StartTime"] = "";
	this->KPMap["StartTime"] = "Query";
	this->KVMap["TimeUnit"] = "";
	this->KPMap["TimeUnit"] = "Query";
	this->KVMap["JobId"] = "";
	this->KPMap["JobId"] = "Query";

}
EmrModifyExecutePlanWithClusterRequest::EmrModifyExecutePlanWithClusterRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "emr";
	this->Action = "ModifyExecutePlanWithCluster";
	this->Version = "2015-09-10";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["ClusterName"] = "";
	this->KPMap["ClusterName"] = "Query";
	this->KVMap["ZoneId"] = "";
	this->KPMap["ZoneId"] = "Query";
	this->KVMap["LogEnable"] = "";
	this->KPMap["LogEnable"] = "Query";
	this->KVMap["LogPath"] = "";
	this->KPMap["LogPath"] = "Query";
	this->KVMap["SecurityGroup"] = "";
	this->KPMap["SecurityGroup"] = "Query";
	this->KVMap["IsOpenPublicIp"] = "";
	this->KPMap["IsOpenPublicIp"] = "Query";
	this->KVMap["SecurityGroupName"] = "";
	this->KPMap["SecurityGroupName"] = "Query";
	this->KVMap["EmrVer"] = "";
	this->KPMap["EmrVer"] = "Query";
	this->KVMap["ClusterType"] = "";
	this->KPMap["ClusterType"] = "Query";
	this->KVMap["Install"] = "";
	this->KPMap["Install"] = "Query";
	this->KVMap["MasterIndex"] = "";
	this->KPMap["MasterIndex"] = "Query";
	this->KVMap["EcsOrder"] = "";
	this->KPMap["EcsOrder"] = "Query";
	this->KVMap["EmrRole4ECS"] = "";
	this->KPMap["EmrRole4ECS"] = "Query";
	this->KVMap["EmrRole4Oss"] = "";
	this->KPMap["EmrRole4Oss"] = "Query";
	this->KVMap["PayType"] = "";
	this->KPMap["PayType"] = "Query";
	this->KVMap["Period"] = "";
	this->KPMap["Period"] = "Query";
	this->KVMap["ExecutePlanId"] = "";
	this->KPMap["ExecutePlanId"] = "Query";
	this->KVMap["Name"] = "";
	this->KPMap["Name"] = "Query";
	this->KVMap["Strategy"] = "";
	this->KPMap["Strategy"] = "Query";
	this->KVMap["TimeInterval"] = "";
	this->KPMap["TimeInterval"] = "Query";
	this->KVMap["StartTime"] = "";
	this->KPMap["StartTime"] = "Query";
	this->KVMap["TimeUnit"] = "";
	this->KPMap["TimeUnit"] = "Query";
	this->KVMap["JobId"] = "";
	this->KPMap["JobId"] = "Query";

}
EmrModifyJobRequest::EmrModifyJobRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "emr";
	this->Action = "ModifyJob";
	this->Version = "2015-09-10";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["Id"] = "";
	this->KPMap["Id"] = "Query";
	this->KVMap["Name"] = "";
	this->KPMap["Name"] = "Query";
	this->KVMap["Type"] = "";
	this->KPMap["Type"] = "Query";
	this->KVMap["EnvParameter"] = "";
	this->KPMap["EnvParameter"] = "Query";
	this->KVMap["FailAct"] = "";
	this->KPMap["FailAct"] = "Query";

}
EmrReleaseClusterRequest::EmrReleaseClusterRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "emr";
	this->Action = "ReleaseCluster";
	this->Version = "2015-09-10";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["ClusterId"] = "";
	this->KPMap["ClusterId"] = "Query";

}
EmrResumeExecutePlanRequest::EmrResumeExecutePlanRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "emr";
	this->Action = "ResumeExecutePlan";
	this->Version = "2015-09-10";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["Id"] = "";
	this->KPMap["Id"] = "Query";

}
EmrRunExecutePlanRequest::EmrRunExecutePlanRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "emr";
	this->Action = "RunExecutePlan";
	this->Version = "2015-09-10";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["ExecutePlanId"] = "";
	this->KPMap["ExecutePlanId"] = "Query";

}
EmrStopExecutePlanRequest::EmrStopExecutePlanRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "emr";
	this->Action = "StopExecutePlan";
	this->Version = "2015-09-10";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["Id"] = "";
	this->KPMap["Id"] = "Query";

}

	
}