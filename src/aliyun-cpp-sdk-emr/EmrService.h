/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
 
#ifndef ALIYUNEMR_EMRSERVICE_H_
#define ALIYUNEMR_EMRSERVICE_H_

#include "../aliyun-cpp-sdk-core/BaseRequest.h"
#include "../aliyun-cpp-sdk-core/ApiBase.h"

namespace AliYunEmr {

class EmrClient : public AliYunCore::ApiBase {
private:
	AliYunCore::DefaultProfile &Profile;
public:
	EmrClient(AliYunCore::DefaultProfile &pf);
	void setProfile(AliYunCore::DefaultProfile &pf);
	std::string getResponse(AliYunCore::BaseRequest &request);
	~EmrClient(){};
};
class EmrCreateClusterRequest : public AliYunCore::BaseRequest {
public:
	EmrCreateClusterRequest();
};
class EmrCreateExecutePlanRequest : public AliYunCore::BaseRequest {
public:
	EmrCreateExecutePlanRequest();
};
class EmrCreateExecutePlanWithClusterRequest : public AliYunCore::BaseRequest {
public:
	EmrCreateExecutePlanWithClusterRequest();
};
class EmrCreateJobRequest : public AliYunCore::BaseRequest {
public:
	EmrCreateJobRequest();
};
class EmrDeleteExecutePlanRequest : public AliYunCore::BaseRequest {
public:
	EmrDeleteExecutePlanRequest();
};
class EmrDeleteJobRequest : public AliYunCore::BaseRequest {
public:
	EmrDeleteJobRequest();
};
class EmrDescribeClusterRequest : public AliYunCore::BaseRequest {
public:
	EmrDescribeClusterRequest();
};
class EmrDescribeExecutePlanRequest : public AliYunCore::BaseRequest {
public:
	EmrDescribeExecutePlanRequest();
};
class EmrDescribeJobRequest : public AliYunCore::BaseRequest {
public:
	EmrDescribeJobRequest();
};
class EmrJobResourceRequest : public AliYunCore::BaseRequest {
public:
	EmrJobResourceRequest();
};
class EmrKillExecutePlanRecordNodeRequest : public AliYunCore::BaseRequest {
public:
	EmrKillExecutePlanRecordNodeRequest();
};
class EmrListClustersRequest : public AliYunCore::BaseRequest {
public:
	EmrListClustersRequest();
};
class EmrListConfigTypeRequest : public AliYunCore::BaseRequest {
public:
	EmrListConfigTypeRequest();
};
class EmrListExecutePlanExecuteRecordNodesRequest : public AliYunCore::BaseRequest {
public:
	EmrListExecutePlanExecuteRecordNodesRequest();
};
class EmrListExecutePlanExecuteRecordsRequest : public AliYunCore::BaseRequest {
public:
	EmrListExecutePlanExecuteRecordsRequest();
};
class EmrListExecutePlanJobsRequest : public AliYunCore::BaseRequest {
public:
	EmrListExecutePlanJobsRequest();
};
class EmrListExecutePlanNodeInstancesRequest : public AliYunCore::BaseRequest {
public:
	EmrListExecutePlanNodeInstancesRequest();
};
class EmrListExecutePlansRequest : public AliYunCore::BaseRequest {
public:
	EmrListExecutePlansRequest();
};
class EmrListJobsRequest : public AliYunCore::BaseRequest {
public:
	EmrListJobsRequest();
};
class EmrListRegionsRequest : public AliYunCore::BaseRequest {
public:
	EmrListRegionsRequest();
};
class EmrModifyClusterNameRequest : public AliYunCore::BaseRequest {
public:
	EmrModifyClusterNameRequest();
};
class EmrModifyExecutePlanRequest : public AliYunCore::BaseRequest {
public:
	EmrModifyExecutePlanRequest();
};
class EmrModifyExecutePlanWithClusterRequest : public AliYunCore::BaseRequest {
public:
	EmrModifyExecutePlanWithClusterRequest();
};
class EmrModifyJobRequest : public AliYunCore::BaseRequest {
public:
	EmrModifyJobRequest();
};
class EmrReleaseClusterRequest : public AliYunCore::BaseRequest {
public:
	EmrReleaseClusterRequest();
};
class EmrResumeExecutePlanRequest : public AliYunCore::BaseRequest {
public:
	EmrResumeExecutePlanRequest();
};
class EmrRunExecutePlanRequest : public AliYunCore::BaseRequest {
public:
	EmrRunExecutePlanRequest();
};
class EmrStopExecutePlanRequest : public AliYunCore::BaseRequest {
public:
	EmrStopExecutePlanRequest();
};


}

#endif