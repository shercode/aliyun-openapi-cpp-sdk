/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

#ifndef ALIYUNCORE_UTILS_H_
#define ALIYUNCORE_UTILS_H_

namespace AliYunCore{
	std::string Base64Encode(const unsigned char* Data,int DataByte);
	std::string Base64Decode(const char* Data,int DataByte,int& OutByte);
	std::string MakeRand();
	std::string UTCTime();
	std::string UrlEncode(const std::string &str);
	std::string UrlDecode(const std::string &str);
	std::string EncodeValue(const std::string &str);
	std::string MakeQueryStr(std::map<std::string,std::string> &container);
	int HashHMAC(const char * algo,const char * key, unsigned int key_length,const char * input, unsigned int input_length,unsigned char * &output, unsigned int &output_length);
	void EncodeParamValue(std::map<std::string,std::string> &container);
	int CurlCallBack(void *buffer, size_t size, size_t nmemb, void *stream);
	unsigned char ToHex(unsigned char x);
	unsigned char FromHex(unsigned char x);
}
#endif /* ALIYUNCORE_UTILS_H_ */
