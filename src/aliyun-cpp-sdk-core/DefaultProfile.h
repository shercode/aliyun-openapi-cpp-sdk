/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

#ifndef ALIYUNCORE_DEFAULTPROFILE_H_
#define ALIYUNCORE_DEFAULTPROFILE_H_

namespace AliYunCore{
	class DefaultProfile {
	public:
		std::string RegionId;
		std::string AccessKeyId;
		std::string AccessKeySecret;
	public:
		DefaultProfile();
		DefaultProfile(const std::string &RegionId, const std::string &AccessKeyId, const std::string &AccessKeySecret);
		void setRegionId(const std::string &RegionId);
		void setAccessKeyId(const std::string &AccessKeyId);
		void setAccessKeySecret(const std::string &AccessKeySecret);
	};
}
#endif /* ALIYUNCORE_DEFAULTPROFILE_H_ */
