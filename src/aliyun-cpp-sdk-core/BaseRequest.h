/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

#ifndef ALIYUNCORE_BASEREQUEST_H_
#define ALIYUNCORE_BASEREQUEST_H_

#include <map>

namespace AliYunCore{
	class BaseRequest{
	public:
		std::string HttpMethod;
		std::string Protocol;
		std::string Domain;
		std::string Path;
		std::string ProductName;
		std::string Action;
		std::string Version;
		std::string Format;
		std::string RegionId;
		std::string AccessKeyId;
		std::string AccessKeySecret;
		std::string SignatureMethod;
		std::string SignatureVersion;

		std::map<std::string, std::string> KVMap;
		std::map<std::string, std::string> KPMap;
	public:
		bool addParameter(std::string paramName, std::string paramValue, std::string paramPossition="Query");
		bool removeParameter(std::string paramName);
		virtual ~BaseRequest(){}
	};
}
#endif /* ALIYUNCORE_BASEREQUEST_H_ */
