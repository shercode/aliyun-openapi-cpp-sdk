/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
 
#ifndef ALIYUNECS_ECSSERVICE_H_
#define ALIYUNECS_ECSSERVICE_H_

#include "../aliyun-cpp-sdk-core/BaseRequest.h"
#include "../aliyun-cpp-sdk-core/ApiBase.h"

namespace AliYunEcs {

class EcsClient : public AliYunCore::ApiBase {
private:
	AliYunCore::DefaultProfile &Profile;
public:
	EcsClient(AliYunCore::DefaultProfile &pf);
	void setProfile(AliYunCore::DefaultProfile &pf);
	std::string getResponse(AliYunCore::BaseRequest &request);
	~EcsClient(){};
};
class EcsAddTagsRequest : public AliYunCore::BaseRequest {
public:
	EcsAddTagsRequest();
};
class EcsAllocateEipAddressRequest : public AliYunCore::BaseRequest {
public:
	EcsAllocateEipAddressRequest();
};
class EcsAllocatePublicIpAddressRequest : public AliYunCore::BaseRequest {
public:
	EcsAllocatePublicIpAddressRequest();
};
class EcsAssociateEipAddressRequest : public AliYunCore::BaseRequest {
public:
	EcsAssociateEipAddressRequest();
};
class EcsAssociateHaVipRequest : public AliYunCore::BaseRequest {
public:
	EcsAssociateHaVipRequest();
};
class EcsAttachDiskRequest : public AliYunCore::BaseRequest {
public:
	EcsAttachDiskRequest();
};
class EcsAuthorizeSecurityGroupRequest : public AliYunCore::BaseRequest {
public:
	EcsAuthorizeSecurityGroupRequest();
};
class EcsAuthorizeSecurityGroupEgressRequest : public AliYunCore::BaseRequest {
public:
	EcsAuthorizeSecurityGroupEgressRequest();
};
class EcsCancelCopyImageRequest : public AliYunCore::BaseRequest {
public:
	EcsCancelCopyImageRequest();
};
class EcsCheckAutoSnapshotPolicyRequest : public AliYunCore::BaseRequest {
public:
	EcsCheckAutoSnapshotPolicyRequest();
};
class EcsCheckDiskEnableAutoSnapshotValidationRequest : public AliYunCore::BaseRequest {
public:
	EcsCheckDiskEnableAutoSnapshotValidationRequest();
};
class EcsCopyImageRequest : public AliYunCore::BaseRequest {
public:
	EcsCopyImageRequest();
};
class EcsCreateDiskRequest : public AliYunCore::BaseRequest {
public:
	EcsCreateDiskRequest();
};
class EcsCreateHaVipRequest : public AliYunCore::BaseRequest {
public:
	EcsCreateHaVipRequest();
};
class EcsCreateImageRequest : public AliYunCore::BaseRequest {
public:
	EcsCreateImageRequest();
};
class EcsCreateInstanceRequest : public AliYunCore::BaseRequest {
public:
	EcsCreateInstanceRequest();
};
class EcsCreateRouteEntryRequest : public AliYunCore::BaseRequest {
public:
	EcsCreateRouteEntryRequest();
};
class EcsCreateSecurityGroupRequest : public AliYunCore::BaseRequest {
public:
	EcsCreateSecurityGroupRequest();
};
class EcsCreateSnapshotRequest : public AliYunCore::BaseRequest {
public:
	EcsCreateSnapshotRequest();
};
class EcsCreateVpcRequest : public AliYunCore::BaseRequest {
public:
	EcsCreateVpcRequest();
};
class EcsCreateVSwitchRequest : public AliYunCore::BaseRequest {
public:
	EcsCreateVSwitchRequest();
};
class EcsDeleteDiskRequest : public AliYunCore::BaseRequest {
public:
	EcsDeleteDiskRequest();
};
class EcsDeleteHaVipRequest : public AliYunCore::BaseRequest {
public:
	EcsDeleteHaVipRequest();
};
class EcsDeleteImageRequest : public AliYunCore::BaseRequest {
public:
	EcsDeleteImageRequest();
};
class EcsDeleteInstanceRequest : public AliYunCore::BaseRequest {
public:
	EcsDeleteInstanceRequest();
};
class EcsDeleteRouteEntryRequest : public AliYunCore::BaseRequest {
public:
	EcsDeleteRouteEntryRequest();
};
class EcsDeleteSecurityGroupRequest : public AliYunCore::BaseRequest {
public:
	EcsDeleteSecurityGroupRequest();
};
class EcsDeleteSnapshotRequest : public AliYunCore::BaseRequest {
public:
	EcsDeleteSnapshotRequest();
};
class EcsDeleteVpcRequest : public AliYunCore::BaseRequest {
public:
	EcsDeleteVpcRequest();
};
class EcsDeleteVSwitchRequest : public AliYunCore::BaseRequest {
public:
	EcsDeleteVSwitchRequest();
};
class EcsDescribeAutoSnapshotPolicyRequest : public AliYunCore::BaseRequest {
public:
	EcsDescribeAutoSnapshotPolicyRequest();
};
class EcsDescribeDiskMonitorDataRequest : public AliYunCore::BaseRequest {
public:
	EcsDescribeDiskMonitorDataRequest();
};
class EcsDescribeDisksRequest : public AliYunCore::BaseRequest {
public:
	EcsDescribeDisksRequest();
};
class EcsDescribeEipAddressesRequest : public AliYunCore::BaseRequest {
public:
	EcsDescribeEipAddressesRequest();
};
class EcsDescribeEipMonitorDataRequest : public AliYunCore::BaseRequest {
public:
	EcsDescribeEipMonitorDataRequest();
};
class EcsDescribeHaVipsRequest : public AliYunCore::BaseRequest {
public:
	EcsDescribeHaVipsRequest();
};
class EcsDescribeImagesRequest : public AliYunCore::BaseRequest {
public:
	EcsDescribeImagesRequest();
};
class EcsDescribeImageSharePermissionRequest : public AliYunCore::BaseRequest {
public:
	EcsDescribeImageSharePermissionRequest();
};
class EcsDescribeInstanceAttributeRequest : public AliYunCore::BaseRequest {
public:
	EcsDescribeInstanceAttributeRequest();
};
class EcsDescribeInstanceMonitorDataRequest : public AliYunCore::BaseRequest {
public:
	EcsDescribeInstanceMonitorDataRequest();
};
class EcsDescribeInstancesRequest : public AliYunCore::BaseRequest {
public:
	EcsDescribeInstancesRequest();
};
class EcsDescribeInstanceStatusRequest : public AliYunCore::BaseRequest {
public:
	EcsDescribeInstanceStatusRequest();
};
class EcsDescribeInstanceTypesRequest : public AliYunCore::BaseRequest {
public:
	EcsDescribeInstanceTypesRequest();
};
class EcsDescribeInstanceVncPasswdRequest : public AliYunCore::BaseRequest {
public:
	EcsDescribeInstanceVncPasswdRequest();
};
class EcsDescribeInstanceVncUrlRequest : public AliYunCore::BaseRequest {
public:
	EcsDescribeInstanceVncUrlRequest();
};
class EcsDescribeLimitationRequest : public AliYunCore::BaseRequest {
public:
	EcsDescribeLimitationRequest();
};
class EcsDescribeRegionsRequest : public AliYunCore::BaseRequest {
public:
	EcsDescribeRegionsRequest();
};
class EcsDescribeResourceByTagsRequest : public AliYunCore::BaseRequest {
public:
	EcsDescribeResourceByTagsRequest();
};
class EcsDescribeRouteTablesRequest : public AliYunCore::BaseRequest {
public:
	EcsDescribeRouteTablesRequest();
};
class EcsDescribeSecurityGroupAttributeRequest : public AliYunCore::BaseRequest {
public:
	EcsDescribeSecurityGroupAttributeRequest();
};
class EcsDescribeSecurityGroupsRequest : public AliYunCore::BaseRequest {
public:
	EcsDescribeSecurityGroupsRequest();
};
class EcsDescribeSnapshotsRequest : public AliYunCore::BaseRequest {
public:
	EcsDescribeSnapshotsRequest();
};
class EcsDescribeTagKeysRequest : public AliYunCore::BaseRequest {
public:
	EcsDescribeTagKeysRequest();
};
class EcsDescribeTagsRequest : public AliYunCore::BaseRequest {
public:
	EcsDescribeTagsRequest();
};
class EcsDescribeVpcsRequest : public AliYunCore::BaseRequest {
public:
	EcsDescribeVpcsRequest();
};
class EcsDescribeVRoutersRequest : public AliYunCore::BaseRequest {
public:
	EcsDescribeVRoutersRequest();
};
class EcsDescribeVSwitchesRequest : public AliYunCore::BaseRequest {
public:
	EcsDescribeVSwitchesRequest();
};
class EcsDescribeZonesRequest : public AliYunCore::BaseRequest {
public:
	EcsDescribeZonesRequest();
};
class EcsDetachDiskRequest : public AliYunCore::BaseRequest {
public:
	EcsDetachDiskRequest();
};
class EcsJoinSecurityGroupRequest : public AliYunCore::BaseRequest {
public:
	EcsJoinSecurityGroupRequest();
};
class EcsLeaveSecurityGroupRequest : public AliYunCore::BaseRequest {
public:
	EcsLeaveSecurityGroupRequest();
};
class EcsModifyAutoSnapshotPolicyRequest : public AliYunCore::BaseRequest {
public:
	EcsModifyAutoSnapshotPolicyRequest();
};
class EcsModifyDiskAttributeRequest : public AliYunCore::BaseRequest {
public:
	EcsModifyDiskAttributeRequest();
};
class EcsModifyEipAddressAttributeRequest : public AliYunCore::BaseRequest {
public:
	EcsModifyEipAddressAttributeRequest();
};
class EcsModifyHaVipAttributeRequest : public AliYunCore::BaseRequest {
public:
	EcsModifyHaVipAttributeRequest();
};
class EcsModifyImageAttributeRequest : public AliYunCore::BaseRequest {
public:
	EcsModifyImageAttributeRequest();
};
class EcsModifyImageShareGroupPermissionRequest : public AliYunCore::BaseRequest {
public:
	EcsModifyImageShareGroupPermissionRequest();
};
class EcsModifyImageSharePermissionRequest : public AliYunCore::BaseRequest {
public:
	EcsModifyImageSharePermissionRequest();
};
class EcsModifyInstanceAttributeRequest : public AliYunCore::BaseRequest {
public:
	EcsModifyInstanceAttributeRequest();
};
class EcsModifyInstanceNetworkSpecRequest : public AliYunCore::BaseRequest {
public:
	EcsModifyInstanceNetworkSpecRequest();
};
class EcsModifyInstanceVncPasswdRequest : public AliYunCore::BaseRequest {
public:
	EcsModifyInstanceVncPasswdRequest();
};
class EcsModifyInstanceVpcAttributeRequest : public AliYunCore::BaseRequest {
public:
	EcsModifyInstanceVpcAttributeRequest();
};
class EcsModifySecurityGroupAttributeRequest : public AliYunCore::BaseRequest {
public:
	EcsModifySecurityGroupAttributeRequest();
};
class EcsModifySnapshotAttributeRequest : public AliYunCore::BaseRequest {
public:
	EcsModifySnapshotAttributeRequest();
};
class EcsModifyVpcAttributeRequest : public AliYunCore::BaseRequest {
public:
	EcsModifyVpcAttributeRequest();
};
class EcsModifyVRouterAttributeRequest : public AliYunCore::BaseRequest {
public:
	EcsModifyVRouterAttributeRequest();
};
class EcsModifyVSwitchAttributeRequest : public AliYunCore::BaseRequest {
public:
	EcsModifyVSwitchAttributeRequest();
};
class EcsRebootInstanceRequest : public AliYunCore::BaseRequest {
public:
	EcsRebootInstanceRequest();
};
class EcsReInitDiskRequest : public AliYunCore::BaseRequest {
public:
	EcsReInitDiskRequest();
};
class EcsReleaseEipAddressRequest : public AliYunCore::BaseRequest {
public:
	EcsReleaseEipAddressRequest();
};
class EcsRemoveTagsRequest : public AliYunCore::BaseRequest {
public:
	EcsRemoveTagsRequest();
};
class EcsRenewInstanceRequest : public AliYunCore::BaseRequest {
public:
	EcsRenewInstanceRequest();
};
class EcsReplaceSystemDiskRequest : public AliYunCore::BaseRequest {
public:
	EcsReplaceSystemDiskRequest();
};
class EcsResetDiskRequest : public AliYunCore::BaseRequest {
public:
	EcsResetDiskRequest();
};
class EcsResizeDiskRequest : public AliYunCore::BaseRequest {
public:
	EcsResizeDiskRequest();
};
class EcsRevokeSecurityGroupRequest : public AliYunCore::BaseRequest {
public:
	EcsRevokeSecurityGroupRequest();
};
class EcsRevokeSecurityGroupEgressRequest : public AliYunCore::BaseRequest {
public:
	EcsRevokeSecurityGroupEgressRequest();
};
class EcsStartInstanceRequest : public AliYunCore::BaseRequest {
public:
	EcsStartInstanceRequest();
};
class EcsStopInstanceRequest : public AliYunCore::BaseRequest {
public:
	EcsStopInstanceRequest();
};
class EcsUnassociateEipAddressRequest : public AliYunCore::BaseRequest {
public:
	EcsUnassociateEipAddressRequest();
};
class EcsUnassociateHaVipRequest : public AliYunCore::BaseRequest {
public:
	EcsUnassociateHaVipRequest();
};


}

#endif