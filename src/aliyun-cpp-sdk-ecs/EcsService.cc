/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

#include <string>
#include <map>
#include "../aliyun-cpp-sdk-core/utils.h"
#include "../aliyun-cpp-sdk-core/DefaultProfile.h"
#include "../aliyun-cpp-sdk-core/BaseRequest.h"
#include "../aliyun-cpp-sdk-core/ApiBase.h"
#include "EcsService.h"

using std::string;
using namespace AliYunCore;

namespace AliYunEcs {

EcsClient::EcsClient(DefaultProfile &pf):Profile(pf){}
void EcsClient::setProfile(DefaultProfile &pf){
	this->Profile = pf;
}
string EcsClient::getResponse(BaseRequest &request){
	request.RegionId = this->Profile.RegionId;		
	request.AccessKeyId = this->Profile.AccessKeyId;
	request.AccessKeySecret = this->Profile.AccessKeySecret;
	return ApiBase::getResponse(request);
}
EcsAddTagsRequest::EcsAddTagsRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "ecs";
	this->Action = "AddTags";
	this->Version = "2014-05-26";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["ResourceType"] = "";
	this->KPMap["ResourceType"] = "Query";
	this->KVMap["ResourceId"] = "";
	this->KPMap["ResourceId"] = "Query";
	this->KVMap["Tag.1.Key"] = "";
	this->KPMap["Tag.1.Key"] = "Query";
	this->KVMap["Tag.2.Key"] = "";
	this->KPMap["Tag.2.Key"] = "Query";
	this->KVMap["Tag.3.Key"] = "";
	this->KPMap["Tag.3.Key"] = "Query";
	this->KVMap["Tag.4.Key"] = "";
	this->KPMap["Tag.4.Key"] = "Query";
	this->KVMap["Tag.5.Key"] = "";
	this->KPMap["Tag.5.Key"] = "Query";
	this->KVMap["Tag.1.Value"] = "";
	this->KPMap["Tag.1.Value"] = "Query";
	this->KVMap["Tag.2.Value"] = "";
	this->KPMap["Tag.2.Value"] = "Query";
	this->KVMap["Tag.3.Value"] = "";
	this->KPMap["Tag.3.Value"] = "Query";
	this->KVMap["Tag.4.Value"] = "";
	this->KPMap["Tag.4.Value"] = "Query";
	this->KVMap["Tag.5.Value"] = "";
	this->KPMap["Tag.5.Value"] = "Query";

}
EcsAllocateEipAddressRequest::EcsAllocateEipAddressRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "ecs";
	this->Action = "AllocateEipAddress";
	this->Version = "2014-05-26";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["Bandwidth"] = "";
	this->KPMap["Bandwidth"] = "Query";
	this->KVMap["InternetChargeType"] = "";
	this->KPMap["InternetChargeType"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";

}
EcsAllocatePublicIpAddressRequest::EcsAllocatePublicIpAddressRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "ecs";
	this->Action = "AllocatePublicIpAddress";
	this->Version = "2014-05-26";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["InstanceId"] = "";
	this->KPMap["InstanceId"] = "Query";
	this->KVMap["IpAddress"] = "";
	this->KPMap["IpAddress"] = "Query";
	this->KVMap["VlanId"] = "";
	this->KPMap["VlanId"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";

}
EcsAssociateEipAddressRequest::EcsAssociateEipAddressRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "ecs";
	this->Action = "AssociateEipAddress";
	this->Version = "2014-05-26";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["AllocationId"] = "";
	this->KPMap["AllocationId"] = "Query";
	this->KVMap["InstanceId"] = "";
	this->KPMap["InstanceId"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";
	this->KVMap["InstanceType"] = "";
	this->KPMap["InstanceType"] = "Query";

}
EcsAssociateHaVipRequest::EcsAssociateHaVipRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "ecs";
	this->Action = "AssociateHaVip";
	this->Version = "2014-05-26";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";
	this->KVMap["ClientToken"] = "";
	this->KPMap["ClientToken"] = "Query";
	this->KVMap["HaVipId"] = "";
	this->KPMap["HaVipId"] = "Query";
	this->KVMap["InstanceId"] = "";
	this->KPMap["InstanceId"] = "Query";

}
EcsAttachDiskRequest::EcsAttachDiskRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "ecs";
	this->Action = "AttachDisk";
	this->Version = "2014-05-26";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["InstanceId"] = "";
	this->KPMap["InstanceId"] = "Query";
	this->KVMap["DiskId"] = "";
	this->KPMap["DiskId"] = "Query";
	this->KVMap["Device"] = "";
	this->KPMap["Device"] = "Query";
	this->KVMap["DeleteWithInstance"] = "";
	this->KPMap["DeleteWithInstance"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";

}
EcsAuthorizeSecurityGroupRequest::EcsAuthorizeSecurityGroupRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "ecs";
	this->Action = "AuthorizeSecurityGroup";
	this->Version = "2014-05-26";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["SecurityGroupId"] = "";
	this->KPMap["SecurityGroupId"] = "Query";
	this->KVMap["IpProtocol"] = "";
	this->KPMap["IpProtocol"] = "Query";
	this->KVMap["PortRange"] = "";
	this->KPMap["PortRange"] = "Query";
	this->KVMap["SourceGroupId"] = "";
	this->KPMap["SourceGroupId"] = "Query";
	this->KVMap["SourceGroupOwnerAccount"] = "";
	this->KPMap["SourceGroupOwnerAccount"] = "Query";
	this->KVMap["SourceCidrIp"] = "";
	this->KPMap["SourceCidrIp"] = "Query";
	this->KVMap["Policy"] = "";
	this->KPMap["Policy"] = "Query";
	this->KVMap["Priority"] = "";
	this->KPMap["Priority"] = "Query";
	this->KVMap["NicType"] = "";
	this->KPMap["NicType"] = "Query";
	this->KVMap["ClientToken"] = "";
	this->KPMap["ClientToken"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";

}
EcsAuthorizeSecurityGroupEgressRequest::EcsAuthorizeSecurityGroupEgressRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "ecs";
	this->Action = "AuthorizeSecurityGroupEgress";
	this->Version = "2014-05-26";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["SecurityGroupId"] = "";
	this->KPMap["SecurityGroupId"] = "Query";
	this->KVMap["IpProtocol"] = "";
	this->KPMap["IpProtocol"] = "Query";
	this->KVMap["PortRange"] = "";
	this->KPMap["PortRange"] = "Query";
	this->KVMap["DestGroupId"] = "";
	this->KPMap["DestGroupId"] = "Query";
	this->KVMap["DestGroupOwnerAccount"] = "";
	this->KPMap["DestGroupOwnerAccount"] = "Query";
	this->KVMap["DestCidrIp"] = "";
	this->KPMap["DestCidrIp"] = "Query";
	this->KVMap["Policy"] = "";
	this->KPMap["Policy"] = "Query";
	this->KVMap["Priority"] = "";
	this->KPMap["Priority"] = "Query";
	this->KVMap["NicType"] = "";
	this->KPMap["NicType"] = "Query";
	this->KVMap["ClientToken"] = "";
	this->KPMap["ClientToken"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";

}
EcsCancelCopyImageRequest::EcsCancelCopyImageRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "ecs";
	this->Action = "CancelCopyImage";
	this->Version = "2014-05-26";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["ImageId"] = "";
	this->KPMap["ImageId"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";

}
EcsCheckAutoSnapshotPolicyRequest::EcsCheckAutoSnapshotPolicyRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "ecs";
	this->Action = "CheckAutoSnapshotPolicy";
	this->Version = "2014-05-26";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";
	this->KVMap["SystemDiskPolicyEnabled"] = "";
	this->KPMap["SystemDiskPolicyEnabled"] = "Query";
	this->KVMap["SystemDiskPolicyTimePeriod"] = "";
	this->KPMap["SystemDiskPolicyTimePeriod"] = "Query";
	this->KVMap["SystemDiskPolicyRetentionDays"] = "";
	this->KPMap["SystemDiskPolicyRetentionDays"] = "Query";
	this->KVMap["SystemDiskPolicyRetentionLastWeek"] = "";
	this->KPMap["SystemDiskPolicyRetentionLastWeek"] = "Query";
	this->KVMap["DataDiskPolicyEnabled"] = "";
	this->KPMap["DataDiskPolicyEnabled"] = "Query";
	this->KVMap["DataDiskPolicyTimePeriod"] = "";
	this->KPMap["DataDiskPolicyTimePeriod"] = "Query";
	this->KVMap["DataDiskPolicyRetentionDays"] = "";
	this->KPMap["DataDiskPolicyRetentionDays"] = "Query";
	this->KVMap["DataDiskPolicyRetentionLastWeek"] = "";
	this->KPMap["DataDiskPolicyRetentionLastWeek"] = "Query";

}
EcsCheckDiskEnableAutoSnapshotValidationRequest::EcsCheckDiskEnableAutoSnapshotValidationRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "ecs";
	this->Action = "CheckDiskEnableAutoSnapshotValidation";
	this->Version = "2014-05-26";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";
	this->KVMap["DiskIds"] = "";
	this->KPMap["DiskIds"] = "Query";

}
EcsCopyImageRequest::EcsCopyImageRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "ecs";
	this->Action = "CopyImage";
	this->Version = "2014-05-26";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["DestinationImageName"] = "";
	this->KPMap["DestinationImageName"] = "Query";
	this->KVMap["DestinationDescription"] = "";
	this->KPMap["DestinationDescription"] = "Query";
	this->KVMap["ImageId"] = "";
	this->KPMap["ImageId"] = "Query";
	this->KVMap["DestinationRegionId"] = "";
	this->KPMap["DestinationRegionId"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";

}
EcsCreateDiskRequest::EcsCreateDiskRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "ecs";
	this->Action = "CreateDisk";
	this->Version = "2014-05-26";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["ZoneId"] = "";
	this->KPMap["ZoneId"] = "Query";
	this->KVMap["SnapshotId"] = "";
	this->KPMap["SnapshotId"] = "Query";
	this->KVMap["DiskName"] = "";
	this->KPMap["DiskName"] = "Query";
	this->KVMap["Size"] = "";
	this->KPMap["Size"] = "Query";
	this->KVMap["DiskCategory"] = "";
	this->KPMap["DiskCategory"] = "Query";
	this->KVMap["Description"] = "";
	this->KPMap["Description"] = "Query";
	this->KVMap["ClientToken"] = "";
	this->KPMap["ClientToken"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";

}
EcsCreateHaVipRequest::EcsCreateHaVipRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "ecs";
	this->Action = "CreateHaVip";
	this->Version = "2014-05-26";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";
	this->KVMap["ClientToken"] = "";
	this->KPMap["ClientToken"] = "Query";
	this->KVMap["VSwitchId"] = "";
	this->KPMap["VSwitchId"] = "Query";
	this->KVMap["IpAddress"] = "";
	this->KPMap["IpAddress"] = "Query";
	this->KVMap["Description"] = "";
	this->KPMap["Description"] = "Query";

}
EcsCreateImageRequest::EcsCreateImageRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "ecs";
	this->Action = "CreateImage";
	this->Version = "2014-05-26";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["SnapshotId"] = "";
	this->KPMap["SnapshotId"] = "Query";
	this->KVMap["ImageName"] = "";
	this->KPMap["ImageName"] = "Query";
	this->KVMap["ImageVersion"] = "";
	this->KPMap["ImageVersion"] = "Query";
	this->KVMap["Description"] = "";
	this->KPMap["Description"] = "Query";
	this->KVMap["ClientToken"] = "";
	this->KPMap["ClientToken"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";

}
EcsCreateInstanceRequest::EcsCreateInstanceRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "ecs";
	this->Action = "CreateInstance";
	this->Version = "2014-05-26";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["ImageId"] = "";
	this->KPMap["ImageId"] = "Query";
	this->KVMap["InstanceType"] = "";
	this->KPMap["InstanceType"] = "Query";
	this->KVMap["SecurityGroupId"] = "";
	this->KPMap["SecurityGroupId"] = "Query";
	this->KVMap["InstanceName"] = "";
	this->KPMap["InstanceName"] = "Query";
	this->KVMap["InternetChargeType"] = "";
	this->KPMap["InternetChargeType"] = "Query";
	this->KVMap["InternetMaxBandwidthIn"] = "";
	this->KPMap["InternetMaxBandwidthIn"] = "Query";
	this->KVMap["InternetMaxBandwidthOut"] = "";
	this->KPMap["InternetMaxBandwidthOut"] = "Query";
	this->KVMap["HostName"] = "";
	this->KPMap["HostName"] = "Query";
	this->KVMap["Password"] = "";
	this->KPMap["Password"] = "Query";
	this->KVMap["ZoneId"] = "";
	this->KPMap["ZoneId"] = "Query";
	this->KVMap["ClusterId"] = "";
	this->KPMap["ClusterId"] = "Query";
	this->KVMap["ClientToken"] = "";
	this->KPMap["ClientToken"] = "Query";
	this->KVMap["VlanId"] = "";
	this->KPMap["VlanId"] = "Query";
	this->KVMap["InnerIpAddress"] = "";
	this->KPMap["InnerIpAddress"] = "Query";
	this->KVMap["SystemDisk.Category"] = "";
	this->KPMap["SystemDisk.Category"] = "Query";
	this->KVMap["SystemDisk.DiskName"] = "";
	this->KPMap["SystemDisk.DiskName"] = "Query";
	this->KVMap["SystemDisk.Description"] = "";
	this->KPMap["SystemDisk.Description"] = "Query";
	this->KVMap["DataDisk.1.Size"] = "";
	this->KPMap["DataDisk.1.Size"] = "Query";
	this->KVMap["DataDisk.1.Category"] = "";
	this->KPMap["DataDisk.1.Category"] = "Query";
	this->KVMap["DataDisk.1.SnapshotId"] = "";
	this->KPMap["DataDisk.1.SnapshotId"] = "Query";
	this->KVMap["DataDisk.1.DiskName"] = "";
	this->KPMap["DataDisk.1.DiskName"] = "Query";
	this->KVMap["DataDisk.1.Description"] = "";
	this->KPMap["DataDisk.1.Description"] = "Query";
	this->KVMap["DataDisk.1.Device"] = "";
	this->KPMap["DataDisk.1.Device"] = "Query";
	this->KVMap["DataDisk.1.DeleteWithInstance"] = "";
	this->KPMap["DataDisk.1.DeleteWithInstance"] = "Query";
	this->KVMap["DataDisk.2.Size"] = "";
	this->KPMap["DataDisk.2.Size"] = "Query";
	this->KVMap["DataDisk.2.Category"] = "";
	this->KPMap["DataDisk.2.Category"] = "Query";
	this->KVMap["DataDisk.2.SnapshotId"] = "";
	this->KPMap["DataDisk.2.SnapshotId"] = "Query";
	this->KVMap["DataDisk.2.DiskName"] = "";
	this->KPMap["DataDisk.2.DiskName"] = "Query";
	this->KVMap["DataDisk.2.Description"] = "";
	this->KPMap["DataDisk.2.Description"] = "Query";
	this->KVMap["DataDisk.2.Device"] = "";
	this->KPMap["DataDisk.2.Device"] = "Query";
	this->KVMap["DataDisk.2.DeleteWithInstance"] = "";
	this->KPMap["DataDisk.2.DeleteWithInstance"] = "Query";
	this->KVMap["DataDisk.3.Size"] = "";
	this->KPMap["DataDisk.3.Size"] = "Query";
	this->KVMap["DataDisk.3.Category"] = "";
	this->KPMap["DataDisk.3.Category"] = "Query";
	this->KVMap["DataDisk.3.SnapshotId"] = "";
	this->KPMap["DataDisk.3.SnapshotId"] = "Query";
	this->KVMap["DataDisk.3.DiskName"] = "";
	this->KPMap["DataDisk.3.DiskName"] = "Query";
	this->KVMap["DataDisk.3.Description"] = "";
	this->KPMap["DataDisk.3.Description"] = "Query";
	this->KVMap["DataDisk.3.Device"] = "";
	this->KPMap["DataDisk.3.Device"] = "Query";
	this->KVMap["DataDisk.3.DeleteWithInstance"] = "";
	this->KPMap["DataDisk.3.DeleteWithInstance"] = "Query";
	this->KVMap["DataDisk.4.Size"] = "";
	this->KPMap["DataDisk.4.Size"] = "Query";
	this->KVMap["DataDisk.4.Category"] = "";
	this->KPMap["DataDisk.4.Category"] = "Query";
	this->KVMap["DataDisk.4.SnapshotId"] = "";
	this->KPMap["DataDisk.4.SnapshotId"] = "Query";
	this->KVMap["DataDisk.4.DiskName"] = "";
	this->KPMap["DataDisk.4.DiskName"] = "Query";
	this->KVMap["DataDisk.4.Description"] = "";
	this->KPMap["DataDisk.4.Description"] = "Query";
	this->KVMap["DataDisk.4.Device"] = "";
	this->KPMap["DataDisk.4.Device"] = "Query";
	this->KVMap["DataDisk.4.DeleteWithInstance"] = "";
	this->KPMap["DataDisk.4.DeleteWithInstance"] = "Query";
	this->KVMap["NodeControllerId"] = "";
	this->KPMap["NodeControllerId"] = "Query";
	this->KVMap["Description"] = "";
	this->KPMap["Description"] = "Query";
	this->KVMap["VSwitchId"] = "";
	this->KPMap["VSwitchId"] = "Query";
	this->KVMap["PrivateIpAddress"] = "";
	this->KPMap["PrivateIpAddress"] = "Query";
	this->KVMap["IoOptimized"] = "";
	this->KPMap["IoOptimized"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";
	this->KVMap["UseAdditionalService"] = "";
	this->KPMap["UseAdditionalService"] = "Query";
	this->KVMap["InstanceChargeType"] = "";
	this->KPMap["InstanceChargeType"] = "Query";
	this->KVMap["Period"] = "";
	this->KPMap["Period"] = "Query";

}
EcsCreateRouteEntryRequest::EcsCreateRouteEntryRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "ecs";
	this->Action = "CreateRouteEntry";
	this->Version = "2014-05-26";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["RouteTableId"] = "";
	this->KPMap["RouteTableId"] = "Query";
	this->KVMap["DestinationCidrBlock"] = "";
	this->KPMap["DestinationCidrBlock"] = "Query";
	this->KVMap["NextHopId"] = "";
	this->KPMap["NextHopId"] = "Query";
	this->KVMap["ClientToken"] = "";
	this->KPMap["ClientToken"] = "Query";
	this->KVMap["NextHopType"] = "";
	this->KPMap["NextHopType"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";

}
EcsCreateSecurityGroupRequest::EcsCreateSecurityGroupRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "ecs";
	this->Action = "CreateSecurityGroup";
	this->Version = "2014-05-26";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["Description"] = "";
	this->KPMap["Description"] = "Query";
	this->KVMap["ClientToken"] = "";
	this->KPMap["ClientToken"] = "Query";
	this->KVMap["SecurityGroupName"] = "";
	this->KPMap["SecurityGroupName"] = "Query";
	this->KVMap["VpcId"] = "";
	this->KPMap["VpcId"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";

}
EcsCreateSnapshotRequest::EcsCreateSnapshotRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "ecs";
	this->Action = "CreateSnapshot";
	this->Version = "2014-05-26";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["DiskId"] = "";
	this->KPMap["DiskId"] = "Query";
	this->KVMap["SnapshotName"] = "";
	this->KPMap["SnapshotName"] = "Query";
	this->KVMap["Description"] = "";
	this->KPMap["Description"] = "Query";
	this->KVMap["ClientToken"] = "";
	this->KPMap["ClientToken"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";

}
EcsCreateVpcRequest::EcsCreateVpcRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "ecs";
	this->Action = "CreateVpc";
	this->Version = "2014-05-26";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["CidrBlock"] = "";
	this->KPMap["CidrBlock"] = "Query";
	this->KVMap["VpcName"] = "";
	this->KPMap["VpcName"] = "Query";
	this->KVMap["Description"] = "";
	this->KPMap["Description"] = "Query";
	this->KVMap["ClientToken"] = "";
	this->KPMap["ClientToken"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";
	this->KVMap["UserCidr"] = "";
	this->KPMap["UserCidr"] = "Query";

}
EcsCreateVSwitchRequest::EcsCreateVSwitchRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "ecs";
	this->Action = "CreateVSwitch";
	this->Version = "2014-05-26";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["ZoneId"] = "";
	this->KPMap["ZoneId"] = "Query";
	this->KVMap["CidrBlock"] = "";
	this->KPMap["CidrBlock"] = "Query";
	this->KVMap["VpcId"] = "";
	this->KPMap["VpcId"] = "Query";
	this->KVMap["VSwitchName"] = "";
	this->KPMap["VSwitchName"] = "Query";
	this->KVMap["Description"] = "";
	this->KPMap["Description"] = "Query";
	this->KVMap["ClientToken"] = "";
	this->KPMap["ClientToken"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";

}
EcsDeleteDiskRequest::EcsDeleteDiskRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "ecs";
	this->Action = "DeleteDisk";
	this->Version = "2014-05-26";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["DiskId"] = "";
	this->KPMap["DiskId"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";

}
EcsDeleteHaVipRequest::EcsDeleteHaVipRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "ecs";
	this->Action = "DeleteHaVip";
	this->Version = "2014-05-26";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";
	this->KVMap["ClientToken"] = "";
	this->KPMap["ClientToken"] = "Query";
	this->KVMap["HaVipId"] = "";
	this->KPMap["HaVipId"] = "Query";

}
EcsDeleteImageRequest::EcsDeleteImageRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "ecs";
	this->Action = "DeleteImage";
	this->Version = "2014-05-26";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["ImageId"] = "";
	this->KPMap["ImageId"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";

}
EcsDeleteInstanceRequest::EcsDeleteInstanceRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "ecs";
	this->Action = "DeleteInstance";
	this->Version = "2014-05-26";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["InstanceId"] = "";
	this->KPMap["InstanceId"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";

}
EcsDeleteRouteEntryRequest::EcsDeleteRouteEntryRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "ecs";
	this->Action = "DeleteRouteEntry";
	this->Version = "2014-05-26";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["RouteTableId"] = "";
	this->KPMap["RouteTableId"] = "Query";
	this->KVMap["DestinationCidrBlock"] = "";
	this->KPMap["DestinationCidrBlock"] = "Query";
	this->KVMap["NextHopId"] = "";
	this->KPMap["NextHopId"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";

}
EcsDeleteSecurityGroupRequest::EcsDeleteSecurityGroupRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "ecs";
	this->Action = "DeleteSecurityGroup";
	this->Version = "2014-05-26";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["SecurityGroupId"] = "";
	this->KPMap["SecurityGroupId"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";

}
EcsDeleteSnapshotRequest::EcsDeleteSnapshotRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "ecs";
	this->Action = "DeleteSnapshot";
	this->Version = "2014-05-26";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["SnapshotId"] = "";
	this->KPMap["SnapshotId"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";

}
EcsDeleteVpcRequest::EcsDeleteVpcRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "ecs";
	this->Action = "DeleteVpc";
	this->Version = "2014-05-26";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["VpcId"] = "";
	this->KPMap["VpcId"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";

}
EcsDeleteVSwitchRequest::EcsDeleteVSwitchRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "ecs";
	this->Action = "DeleteVSwitch";
	this->Version = "2014-05-26";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["VSwitchId"] = "";
	this->KPMap["VSwitchId"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";

}
EcsDescribeAutoSnapshotPolicyRequest::EcsDescribeAutoSnapshotPolicyRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "ecs";
	this->Action = "DescribeAutoSnapshotPolicy";
	this->Version = "2014-05-26";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";

}
EcsDescribeDiskMonitorDataRequest::EcsDescribeDiskMonitorDataRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "ecs";
	this->Action = "DescribeDiskMonitorData";
	this->Version = "2014-05-26";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["DiskId"] = "";
	this->KPMap["DiskId"] = "Query";
	this->KVMap["StartTime"] = "";
	this->KPMap["StartTime"] = "Query";
	this->KVMap["EndTime"] = "";
	this->KPMap["EndTime"] = "Query";
	this->KVMap["Period"] = "";
	this->KPMap["Period"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";

}
EcsDescribeDisksRequest::EcsDescribeDisksRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "ecs";
	this->Action = "DescribeDisks";
	this->Version = "2014-05-26";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["ZoneId"] = "";
	this->KPMap["ZoneId"] = "Query";
	this->KVMap["DiskIds"] = "";
	this->KPMap["DiskIds"] = "Query";
	this->KVMap["InstanceId"] = "";
	this->KPMap["InstanceId"] = "Query";
	this->KVMap["DiskType"] = "";
	this->KPMap["DiskType"] = "Query";
	this->KVMap["Category"] = "";
	this->KPMap["Category"] = "Query";
	this->KVMap["Status"] = "";
	this->KPMap["Status"] = "Query";
	this->KVMap["SnapshotId"] = "";
	this->KPMap["SnapshotId"] = "Query";
	this->KVMap["Portable"] = "";
	this->KPMap["Portable"] = "Query";
	this->KVMap["DeleteWithInstance"] = "";
	this->KPMap["DeleteWithInstance"] = "Query";
	this->KVMap["DeleteAutoSnapshot"] = "";
	this->KPMap["DeleteAutoSnapshot"] = "Query";
	this->KVMap["PageNumber"] = "";
	this->KPMap["PageNumber"] = "Query";
	this->KVMap["PageSize"] = "";
	this->KPMap["PageSize"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";
	this->KVMap["DiskName"] = "";
	this->KPMap["DiskName"] = "Query";
	this->KVMap["EnableAutoSnapshot"] = "";
	this->KPMap["EnableAutoSnapshot"] = "Query";
	this->KVMap["DiskChargeType"] = "";
	this->KPMap["DiskChargeType"] = "Query";
	this->KVMap["LockReason"] = "";
	this->KPMap["LockReason"] = "Query";
	this->KVMap["Filter.1.Key"] = "";
	this->KPMap["Filter.1.Key"] = "Query";
	this->KVMap["Filter.2.Key"] = "";
	this->KPMap["Filter.2.Key"] = "Query";
	this->KVMap["Filter.1.Value"] = "";
	this->KPMap["Filter.1.Value"] = "Query";
	this->KVMap["Filter.2.Value"] = "";
	this->KPMap["Filter.2.Value"] = "Query";
	this->KVMap["Tag.1.Key"] = "";
	this->KPMap["Tag.1.Key"] = "Query";
	this->KVMap["Tag.2.Key"] = "";
	this->KPMap["Tag.2.Key"] = "Query";
	this->KVMap["Tag.3.Key"] = "";
	this->KPMap["Tag.3.Key"] = "Query";
	this->KVMap["Tag.4.Key"] = "";
	this->KPMap["Tag.4.Key"] = "Query";
	this->KVMap["Tag.5.Key"] = "";
	this->KPMap["Tag.5.Key"] = "Query";
	this->KVMap["Tag.1.Value"] = "";
	this->KPMap["Tag.1.Value"] = "Query";
	this->KVMap["Tag.2.Value"] = "";
	this->KPMap["Tag.2.Value"] = "Query";
	this->KVMap["Tag.3.Value"] = "";
	this->KPMap["Tag.3.Value"] = "Query";
	this->KVMap["Tag.4.Value"] = "";
	this->KPMap["Tag.4.Value"] = "Query";
	this->KVMap["Tag.5.Value"] = "";
	this->KPMap["Tag.5.Value"] = "Query";

}
EcsDescribeEipAddressesRequest::EcsDescribeEipAddressesRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "ecs";
	this->Action = "DescribeEipAddresses";
	this->Version = "2014-05-26";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["Status"] = "";
	this->KPMap["Status"] = "Query";
	this->KVMap["EipAddress"] = "";
	this->KPMap["EipAddress"] = "Query";
	this->KVMap["AllocationId"] = "";
	this->KPMap["AllocationId"] = "Query";
	this->KVMap["PageNumber"] = "";
	this->KPMap["PageNumber"] = "Query";
	this->KVMap["PageSize"] = "";
	this->KPMap["PageSize"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";
	this->KVMap["Filter.1.Key"] = "";
	this->KPMap["Filter.1.Key"] = "Query";
	this->KVMap["Filter.2.Key"] = "";
	this->KPMap["Filter.2.Key"] = "Query";
	this->KVMap["Filter.1.Value"] = "";
	this->KPMap["Filter.1.Value"] = "Query";
	this->KVMap["Filter.2.Value"] = "";
	this->KPMap["Filter.2.Value"] = "Query";
	this->KVMap["LockReason"] = "";
	this->KPMap["LockReason"] = "Query";
	this->KVMap["AssociatedInstanceType"] = "";
	this->KPMap["AssociatedInstanceType"] = "Query";
	this->KVMap["AssociatedInstanceId"] = "";
	this->KPMap["AssociatedInstanceId"] = "Query";

}
EcsDescribeEipMonitorDataRequest::EcsDescribeEipMonitorDataRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "ecs";
	this->Action = "DescribeEipMonitorData";
	this->Version = "2014-05-26";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["AllocationId"] = "";
	this->KPMap["AllocationId"] = "Query";
	this->KVMap["StartTime"] = "";
	this->KPMap["StartTime"] = "Query";
	this->KVMap["EndTime"] = "";
	this->KPMap["EndTime"] = "Query";
	this->KVMap["Period"] = "";
	this->KPMap["Period"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";

}
EcsDescribeHaVipsRequest::EcsDescribeHaVipsRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "ecs";
	this->Action = "DescribeHaVips";
	this->Version = "2014-05-26";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";
	this->KVMap["PageNumber"] = "";
	this->KPMap["PageNumber"] = "Query";
	this->KVMap["PageSize"] = "";
	this->KPMap["PageSize"] = "Query";
	this->KVMap["Filter"] = "";
	this->KPMap["Filter"] = "Query";

}
EcsDescribeImagesRequest::EcsDescribeImagesRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "ecs";
	this->Action = "DescribeImages";
	this->Version = "2014-05-26";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["Status"] = "";
	this->KPMap["Status"] = "Query";
	this->KVMap["ImageId"] = "";
	this->KPMap["ImageId"] = "Query";
	this->KVMap["ShowExpired"] = "";
	this->KPMap["ShowExpired"] = "Query";
	this->KVMap["SnapshotId"] = "";
	this->KPMap["SnapshotId"] = "Query";
	this->KVMap["ImageName"] = "";
	this->KPMap["ImageName"] = "Query";
	this->KVMap["ImageOwnerAlias"] = "";
	this->KPMap["ImageOwnerAlias"] = "Query";
	this->KVMap["PageNumber"] = "";
	this->KPMap["PageNumber"] = "Query";
	this->KVMap["PageSize"] = "";
	this->KPMap["PageSize"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";
	this->KVMap["Filter.1.Key"] = "";
	this->KPMap["Filter.1.Key"] = "Query";
	this->KVMap["Filter.2.Key"] = "";
	this->KPMap["Filter.2.Key"] = "Query";
	this->KVMap["Filter.1.Value"] = "";
	this->KPMap["Filter.1.Value"] = "Query";
	this->KVMap["Filter.2.Value"] = "";
	this->KPMap["Filter.2.Value"] = "Query";
	this->KVMap["Usage"] = "";
	this->KPMap["Usage"] = "Query";
	this->KVMap["Tag.1.Key"] = "";
	this->KPMap["Tag.1.Key"] = "Query";
	this->KVMap["Tag.2.Key"] = "";
	this->KPMap["Tag.2.Key"] = "Query";
	this->KVMap["Tag.3.Key"] = "";
	this->KPMap["Tag.3.Key"] = "Query";
	this->KVMap["Tag.4.Key"] = "";
	this->KPMap["Tag.4.Key"] = "Query";
	this->KVMap["Tag.5.Key"] = "";
	this->KPMap["Tag.5.Key"] = "Query";
	this->KVMap["Tag.1.Value"] = "";
	this->KPMap["Tag.1.Value"] = "Query";
	this->KVMap["Tag.2.Value"] = "";
	this->KPMap["Tag.2.Value"] = "Query";
	this->KVMap["Tag.3.Value"] = "";
	this->KPMap["Tag.3.Value"] = "Query";
	this->KVMap["Tag.4.Value"] = "";
	this->KPMap["Tag.4.Value"] = "Query";
	this->KVMap["Tag.5.Value"] = "";
	this->KPMap["Tag.5.Value"] = "Query";

}
EcsDescribeImageSharePermissionRequest::EcsDescribeImageSharePermissionRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "ecs";
	this->Action = "DescribeImageSharePermission";
	this->Version = "2014-05-26";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["ImageId"] = "";
	this->KPMap["ImageId"] = "Query";
	this->KVMap["PageNumber"] = "";
	this->KPMap["PageNumber"] = "Query";
	this->KVMap["PageSize"] = "";
	this->KPMap["PageSize"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";

}
EcsDescribeInstanceAttributeRequest::EcsDescribeInstanceAttributeRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "ecs";
	this->Action = "DescribeInstanceAttribute";
	this->Version = "2014-05-26";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["InstanceId"] = "";
	this->KPMap["InstanceId"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";

}
EcsDescribeInstanceMonitorDataRequest::EcsDescribeInstanceMonitorDataRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "ecs";
	this->Action = "DescribeInstanceMonitorData";
	this->Version = "2014-05-26";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["InstanceId"] = "";
	this->KPMap["InstanceId"] = "Query";
	this->KVMap["StartTime"] = "";
	this->KPMap["StartTime"] = "Query";
	this->KVMap["EndTime"] = "";
	this->KPMap["EndTime"] = "Query";
	this->KVMap["Period"] = "";
	this->KPMap["Period"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";

}
EcsDescribeInstancesRequest::EcsDescribeInstancesRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "ecs";
	this->Action = "DescribeInstances";
	this->Version = "2014-05-26";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["VpcId"] = "";
	this->KPMap["VpcId"] = "Query";
	this->KVMap["VSwitchId"] = "";
	this->KPMap["VSwitchId"] = "Query";
	this->KVMap["ZoneId"] = "";
	this->KPMap["ZoneId"] = "Query";
	this->KVMap["InstanceNetworkType"] = "";
	this->KPMap["InstanceNetworkType"] = "Query";
	this->KVMap["SecurityGroupId"] = "";
	this->KPMap["SecurityGroupId"] = "Query";
	this->KVMap["InstanceIds"] = "";
	this->KPMap["InstanceIds"] = "Query";
	this->KVMap["PageNumber"] = "";
	this->KPMap["PageNumber"] = "Query";
	this->KVMap["PageSize"] = "";
	this->KPMap["PageSize"] = "Query";
	this->KVMap["InnerIpAddresses"] = "";
	this->KPMap["InnerIpAddresses"] = "Query";
	this->KVMap["PrivateIpAddresses"] = "";
	this->KPMap["PrivateIpAddresses"] = "Query";
	this->KVMap["PublicIpAddresses"] = "";
	this->KPMap["PublicIpAddresses"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";
	this->KVMap["InstanceChargeType"] = "";
	this->KPMap["InstanceChargeType"] = "Query";
	this->KVMap["InternetChargeType"] = "";
	this->KPMap["InternetChargeType"] = "Query";
	this->KVMap["InstanceName"] = "";
	this->KPMap["InstanceName"] = "Query";
	this->KVMap["ImageId"] = "";
	this->KPMap["ImageId"] = "Query";
	this->KVMap["Status"] = "";
	this->KPMap["Status"] = "Query";
	this->KVMap["LockReason"] = "";
	this->KPMap["LockReason"] = "Query";
	this->KVMap["Filter.1.Key"] = "";
	this->KPMap["Filter.1.Key"] = "Query";
	this->KVMap["Filter.2.Key"] = "";
	this->KPMap["Filter.2.Key"] = "Query";
	this->KVMap["Filter.3.Key"] = "";
	this->KPMap["Filter.3.Key"] = "Query";
	this->KVMap["Filter.4.Key"] = "";
	this->KPMap["Filter.4.Key"] = "Query";
	this->KVMap["Filter.1.Value"] = "";
	this->KPMap["Filter.1.Value"] = "Query";
	this->KVMap["Filter.2.Value"] = "";
	this->KPMap["Filter.2.Value"] = "Query";
	this->KVMap["Filter.3.Value"] = "";
	this->KPMap["Filter.3.Value"] = "Query";
	this->KVMap["Filter.4.Value"] = "";
	this->KPMap["Filter.4.Value"] = "Query";
	this->KVMap["DeviceAvailable"] = "";
	this->KPMap["DeviceAvailable"] = "Query";
	this->KVMap["IoOptimized"] = "";
	this->KPMap["IoOptimized"] = "Query";
	this->KVMap["Tag.1.Key"] = "";
	this->KPMap["Tag.1.Key"] = "Query";
	this->KVMap["Tag.2.Key"] = "";
	this->KPMap["Tag.2.Key"] = "Query";
	this->KVMap["Tag.3.Key"] = "";
	this->KPMap["Tag.3.Key"] = "Query";
	this->KVMap["Tag.4.Key"] = "";
	this->KPMap["Tag.4.Key"] = "Query";
	this->KVMap["Tag.5.Key"] = "";
	this->KPMap["Tag.5.Key"] = "Query";
	this->KVMap["Tag.1.Value"] = "";
	this->KPMap["Tag.1.Value"] = "Query";
	this->KVMap["Tag.2.Value"] = "";
	this->KPMap["Tag.2.Value"] = "Query";
	this->KVMap["Tag.3.Value"] = "";
	this->KPMap["Tag.3.Value"] = "Query";
	this->KVMap["Tag.4.Value"] = "";
	this->KPMap["Tag.4.Value"] = "Query";
	this->KVMap["Tag.5.Value"] = "";
	this->KPMap["Tag.5.Value"] = "Query";

}
EcsDescribeInstanceStatusRequest::EcsDescribeInstanceStatusRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "ecs";
	this->Action = "DescribeInstanceStatus";
	this->Version = "2014-05-26";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["ZoneId"] = "";
	this->KPMap["ZoneId"] = "Query";
	this->KVMap["ClusterId"] = "";
	this->KPMap["ClusterId"] = "Query";
	this->KVMap["PageNumber"] = "";
	this->KPMap["PageNumber"] = "Query";
	this->KVMap["PageSize"] = "";
	this->KPMap["PageSize"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";

}
EcsDescribeInstanceTypesRequest::EcsDescribeInstanceTypesRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "ecs";
	this->Action = "DescribeInstanceTypes";
	this->Version = "2014-05-26";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";

}
EcsDescribeInstanceVncPasswdRequest::EcsDescribeInstanceVncPasswdRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "ecs";
	this->Action = "DescribeInstanceVncPasswd";
	this->Version = "2014-05-26";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["InstanceId"] = "";
	this->KPMap["InstanceId"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";

}
EcsDescribeInstanceVncUrlRequest::EcsDescribeInstanceVncUrlRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "ecs";
	this->Action = "DescribeInstanceVncUrl";
	this->Version = "2014-05-26";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["InstanceId"] = "";
	this->KPMap["InstanceId"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";

}
EcsDescribeLimitationRequest::EcsDescribeLimitationRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "ecs";
	this->Action = "DescribeLimitation";
	this->Version = "2014-05-26";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";
	this->KVMap["Limitation"] = "";
	this->KPMap["Limitation"] = "Query";

}
EcsDescribeRegionsRequest::EcsDescribeRegionsRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "ecs";
	this->Action = "DescribeRegions";
	this->Version = "2014-05-26";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";

}
EcsDescribeResourceByTagsRequest::EcsDescribeResourceByTagsRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "ecs";
	this->Action = "DescribeResourceByTags";
	this->Version = "2014-05-26";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["PageSize"] = "";
	this->KPMap["PageSize"] = "Query";
	this->KVMap["PageNumber"] = "";
	this->KPMap["PageNumber"] = "Query";
	this->KVMap["ResourceType"] = "";
	this->KPMap["ResourceType"] = "Query";
	this->KVMap["Tag.1.Key"] = "";
	this->KPMap["Tag.1.Key"] = "Query";
	this->KVMap["Tag.2.Key"] = "";
	this->KPMap["Tag.2.Key"] = "Query";
	this->KVMap["Tag.3.Key"] = "";
	this->KPMap["Tag.3.Key"] = "Query";
	this->KVMap["Tag.4.Key"] = "";
	this->KPMap["Tag.4.Key"] = "Query";
	this->KVMap["Tag.5.Key"] = "";
	this->KPMap["Tag.5.Key"] = "Query";
	this->KVMap["Tag.1.Value"] = "";
	this->KPMap["Tag.1.Value"] = "Query";
	this->KVMap["Tag.2.Value"] = "";
	this->KPMap["Tag.2.Value"] = "Query";
	this->KVMap["Tag.3.Value"] = "";
	this->KPMap["Tag.3.Value"] = "Query";
	this->KVMap["Tag.4.Value"] = "";
	this->KPMap["Tag.4.Value"] = "Query";
	this->KVMap["Tag.5.Value"] = "";
	this->KPMap["Tag.5.Value"] = "Query";

}
EcsDescribeRouteTablesRequest::EcsDescribeRouteTablesRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "ecs";
	this->Action = "DescribeRouteTables";
	this->Version = "2014-05-26";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["VRouterId"] = "";
	this->KPMap["VRouterId"] = "Query";
	this->KVMap["RouteTableId"] = "";
	this->KPMap["RouteTableId"] = "Query";
	this->KVMap["PageNumber"] = "";
	this->KPMap["PageNumber"] = "Query";
	this->KVMap["PageSize"] = "";
	this->KPMap["PageSize"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";

}
EcsDescribeSecurityGroupAttributeRequest::EcsDescribeSecurityGroupAttributeRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "ecs";
	this->Action = "DescribeSecurityGroupAttribute";
	this->Version = "2014-05-26";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["SecurityGroupId"] = "";
	this->KPMap["SecurityGroupId"] = "Query";
	this->KVMap["NicType"] = "";
	this->KPMap["NicType"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";
	this->KVMap["Direction"] = "";
	this->KPMap["Direction"] = "Query";

}
EcsDescribeSecurityGroupsRequest::EcsDescribeSecurityGroupsRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "ecs";
	this->Action = "DescribeSecurityGroups";
	this->Version = "2014-05-26";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["VpcId"] = "";
	this->KPMap["VpcId"] = "Query";
	this->KVMap["PageNumber"] = "";
	this->KPMap["PageNumber"] = "Query";
	this->KVMap["PageSize"] = "";
	this->KPMap["PageSize"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";
	this->KVMap["SecurityGroupIds"] = "";
	this->KPMap["SecurityGroupIds"] = "Query";

}
EcsDescribeSnapshotsRequest::EcsDescribeSnapshotsRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "ecs";
	this->Action = "DescribeSnapshots";
	this->Version = "2014-05-26";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["InstanceId"] = "";
	this->KPMap["InstanceId"] = "Query";
	this->KVMap["DiskId"] = "";
	this->KPMap["DiskId"] = "Query";
	this->KVMap["SnapshotIds"] = "";
	this->KPMap["SnapshotIds"] = "Query";
	this->KVMap["PageNumber"] = "";
	this->KPMap["PageNumber"] = "Query";
	this->KVMap["PageSize"] = "";
	this->KPMap["PageSize"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";
	this->KVMap["SnapshotName"] = "";
	this->KPMap["SnapshotName"] = "Query";
	this->KVMap["Status"] = "";
	this->KPMap["Status"] = "Query";
	this->KVMap["SnapshotType"] = "";
	this->KPMap["SnapshotType"] = "Query";
	this->KVMap["Filter.1.Key"] = "";
	this->KPMap["Filter.1.Key"] = "Query";
	this->KVMap["Filter.2.Key"] = "";
	this->KPMap["Filter.2.Key"] = "Query";
	this->KVMap["Filter.1.Value"] = "";
	this->KPMap["Filter.1.Value"] = "Query";
	this->KVMap["Filter.2.Value"] = "";
	this->KPMap["Filter.2.Value"] = "Query";
	this->KVMap["Usage"] = "";
	this->KPMap["Usage"] = "Query";
	this->KVMap["SourceDiskType"] = "";
	this->KPMap["SourceDiskType"] = "Query";
	this->KVMap["Tag.1.Key"] = "";
	this->KPMap["Tag.1.Key"] = "Query";
	this->KVMap["Tag.2.Key"] = "";
	this->KPMap["Tag.2.Key"] = "Query";
	this->KVMap["Tag.3.Key"] = "";
	this->KPMap["Tag.3.Key"] = "Query";
	this->KVMap["Tag.4.Key"] = "";
	this->KPMap["Tag.4.Key"] = "Query";
	this->KVMap["Tag.5.Key"] = "";
	this->KPMap["Tag.5.Key"] = "Query";
	this->KVMap["Tag.1.Value"] = "";
	this->KPMap["Tag.1.Value"] = "Query";
	this->KVMap["Tag.2.Value"] = "";
	this->KPMap["Tag.2.Value"] = "Query";
	this->KVMap["Tag.3.Value"] = "";
	this->KPMap["Tag.3.Value"] = "Query";
	this->KVMap["Tag.4.Value"] = "";
	this->KPMap["Tag.4.Value"] = "Query";
	this->KVMap["Tag.5.Value"] = "";
	this->KPMap["Tag.5.Value"] = "Query";

}
EcsDescribeTagKeysRequest::EcsDescribeTagKeysRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "ecs";
	this->Action = "DescribeTagKeys";
	this->Version = "2014-05-26";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["PageSize"] = "";
	this->KPMap["PageSize"] = "Query";
	this->KVMap["PageNumber"] = "";
	this->KPMap["PageNumber"] = "Query";
	this->KVMap["ResourceType"] = "";
	this->KPMap["ResourceType"] = "Query";
	this->KVMap["ResourceId"] = "";
	this->KPMap["ResourceId"] = "Query";

}
EcsDescribeTagsRequest::EcsDescribeTagsRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "ecs";
	this->Action = "DescribeTags";
	this->Version = "2014-05-26";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["PageSize"] = "";
	this->KPMap["PageSize"] = "Query";
	this->KVMap["PageNumber"] = "";
	this->KPMap["PageNumber"] = "Query";
	this->KVMap["ResourceType"] = "";
	this->KPMap["ResourceType"] = "Query";
	this->KVMap["ResourceId"] = "";
	this->KPMap["ResourceId"] = "Query";
	this->KVMap["Tag.1.Key"] = "";
	this->KPMap["Tag.1.Key"] = "Query";
	this->KVMap["Tag.2.Key"] = "";
	this->KPMap["Tag.2.Key"] = "Query";
	this->KVMap["Tag.3.Key"] = "";
	this->KPMap["Tag.3.Key"] = "Query";
	this->KVMap["Tag.4.Key"] = "";
	this->KPMap["Tag.4.Key"] = "Query";
	this->KVMap["Tag.5.Key"] = "";
	this->KPMap["Tag.5.Key"] = "Query";
	this->KVMap["Tag.1.Value"] = "";
	this->KPMap["Tag.1.Value"] = "Query";
	this->KVMap["Tag.2.Value"] = "";
	this->KPMap["Tag.2.Value"] = "Query";
	this->KVMap["Tag.3.Value"] = "";
	this->KPMap["Tag.3.Value"] = "Query";
	this->KVMap["Tag.4.Value"] = "";
	this->KPMap["Tag.4.Value"] = "Query";
	this->KVMap["Tag.5.Value"] = "";
	this->KPMap["Tag.5.Value"] = "Query";

}
EcsDescribeVpcsRequest::EcsDescribeVpcsRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "ecs";
	this->Action = "DescribeVpcs";
	this->Version = "2014-05-26";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["VpcId"] = "";
	this->KPMap["VpcId"] = "Query";
	this->KVMap["PageNumber"] = "";
	this->KPMap["PageNumber"] = "Query";
	this->KVMap["PageSize"] = "";
	this->KPMap["PageSize"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";

}
EcsDescribeVRoutersRequest::EcsDescribeVRoutersRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "ecs";
	this->Action = "DescribeVRouters";
	this->Version = "2014-05-26";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["VRouterId"] = "";
	this->KPMap["VRouterId"] = "Query";
	this->KVMap["PageNumber"] = "";
	this->KPMap["PageNumber"] = "Query";
	this->KVMap["PageSize"] = "";
	this->KPMap["PageSize"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";

}
EcsDescribeVSwitchesRequest::EcsDescribeVSwitchesRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "ecs";
	this->Action = "DescribeVSwitches";
	this->Version = "2014-05-26";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["VpcId"] = "";
	this->KPMap["VpcId"] = "Query";
	this->KVMap["VSwitchId"] = "";
	this->KPMap["VSwitchId"] = "Query";
	this->KVMap["ZoneId"] = "";
	this->KPMap["ZoneId"] = "Query";
	this->KVMap["PageNumber"] = "";
	this->KPMap["PageNumber"] = "Query";
	this->KVMap["PageSize"] = "";
	this->KPMap["PageSize"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";

}
EcsDescribeZonesRequest::EcsDescribeZonesRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "ecs";
	this->Action = "DescribeZones";
	this->Version = "2014-05-26";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";

}
EcsDetachDiskRequest::EcsDetachDiskRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "ecs";
	this->Action = "DetachDisk";
	this->Version = "2014-05-26";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["InstanceId"] = "";
	this->KPMap["InstanceId"] = "Query";
	this->KVMap["DiskId"] = "";
	this->KPMap["DiskId"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";

}
EcsJoinSecurityGroupRequest::EcsJoinSecurityGroupRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "ecs";
	this->Action = "JoinSecurityGroup";
	this->Version = "2014-05-26";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["SecurityGroupId"] = "";
	this->KPMap["SecurityGroupId"] = "Query";
	this->KVMap["InstanceId"] = "";
	this->KPMap["InstanceId"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";

}
EcsLeaveSecurityGroupRequest::EcsLeaveSecurityGroupRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "ecs";
	this->Action = "LeaveSecurityGroup";
	this->Version = "2014-05-26";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["SecurityGroupId"] = "";
	this->KPMap["SecurityGroupId"] = "Query";
	this->KVMap["InstanceId"] = "";
	this->KPMap["InstanceId"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";

}
EcsModifyAutoSnapshotPolicyRequest::EcsModifyAutoSnapshotPolicyRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "ecs";
	this->Action = "ModifyAutoSnapshotPolicy";
	this->Version = "2014-05-26";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["SystemDiskPolicyEnabled"] = "";
	this->KPMap["SystemDiskPolicyEnabled"] = "Query";
	this->KVMap["SystemDiskPolicyTimePeriod"] = "";
	this->KPMap["SystemDiskPolicyTimePeriod"] = "Query";
	this->KVMap["SystemDiskPolicyRetentionDays"] = "";
	this->KPMap["SystemDiskPolicyRetentionDays"] = "Query";
	this->KVMap["SystemDiskPolicyRetentionLastWeek"] = "";
	this->KPMap["SystemDiskPolicyRetentionLastWeek"] = "Query";
	this->KVMap["DataDiskPolicyEnabled"] = "";
	this->KPMap["DataDiskPolicyEnabled"] = "Query";
	this->KVMap["DataDiskPolicyTimePeriod"] = "";
	this->KPMap["DataDiskPolicyTimePeriod"] = "Query";
	this->KVMap["DataDiskPolicyRetentionDays"] = "";
	this->KPMap["DataDiskPolicyRetentionDays"] = "Query";
	this->KVMap["DataDiskPolicyRetentionLastWeek"] = "";
	this->KPMap["DataDiskPolicyRetentionLastWeek"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";

}
EcsModifyDiskAttributeRequest::EcsModifyDiskAttributeRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "ecs";
	this->Action = "ModifyDiskAttribute";
	this->Version = "2014-05-26";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["DiskId"] = "";
	this->KPMap["DiskId"] = "Query";
	this->KVMap["DiskName"] = "";
	this->KPMap["DiskName"] = "Query";
	this->KVMap["Description"] = "";
	this->KPMap["Description"] = "Query";
	this->KVMap["DeleteWithInstance"] = "";
	this->KPMap["DeleteWithInstance"] = "Query";
	this->KVMap["DeleteAutoSnapshot"] = "";
	this->KPMap["DeleteAutoSnapshot"] = "Query";
	this->KVMap["EnableAutoSnapshot"] = "";
	this->KPMap["EnableAutoSnapshot"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";

}
EcsModifyEipAddressAttributeRequest::EcsModifyEipAddressAttributeRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "ecs";
	this->Action = "ModifyEipAddressAttribute";
	this->Version = "2014-05-26";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["AllocationId"] = "";
	this->KPMap["AllocationId"] = "Query";
	this->KVMap["Bandwidth"] = "";
	this->KPMap["Bandwidth"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";

}
EcsModifyHaVipAttributeRequest::EcsModifyHaVipAttributeRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "ecs";
	this->Action = "ModifyHaVipAttribute";
	this->Version = "2014-05-26";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";
	this->KVMap["ClientToken"] = "";
	this->KPMap["ClientToken"] = "Query";
	this->KVMap["HaVipId"] = "";
	this->KPMap["HaVipId"] = "Query";
	this->KVMap["Description"] = "";
	this->KPMap["Description"] = "Query";

}
EcsModifyImageAttributeRequest::EcsModifyImageAttributeRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "ecs";
	this->Action = "ModifyImageAttribute";
	this->Version = "2014-05-26";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["ImageId"] = "";
	this->KPMap["ImageId"] = "Query";
	this->KVMap["ImageName"] = "";
	this->KPMap["ImageName"] = "Query";
	this->KVMap["Description"] = "";
	this->KPMap["Description"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";

}
EcsModifyImageShareGroupPermissionRequest::EcsModifyImageShareGroupPermissionRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "ecs";
	this->Action = "ModifyImageShareGroupPermission";
	this->Version = "2014-05-26";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["ImageId"] = "";
	this->KPMap["ImageId"] = "Query";
	this->KVMap["AddGroup.1"] = "";
	this->KPMap["AddGroup.1"] = "Query";
	this->KVMap["RemoveGroup.1"] = "";
	this->KPMap["RemoveGroup.1"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";

}
EcsModifyImageSharePermissionRequest::EcsModifyImageSharePermissionRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "ecs";
	this->Action = "ModifyImageSharePermission";
	this->Version = "2014-05-26";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["ImageId"] = "";
	this->KPMap["ImageId"] = "Query";
	this->KVMap["AddAccount.1"] = "";
	this->KPMap["AddAccount.1"] = "Query";
	this->KVMap["AddAccount.2"] = "";
	this->KPMap["AddAccount.2"] = "Query";
	this->KVMap["AddAccount.3"] = "";
	this->KPMap["AddAccount.3"] = "Query";
	this->KVMap["AddAccount.4"] = "";
	this->KPMap["AddAccount.4"] = "Query";
	this->KVMap["AddAccount.5"] = "";
	this->KPMap["AddAccount.5"] = "Query";
	this->KVMap["AddAccount.6"] = "";
	this->KPMap["AddAccount.6"] = "Query";
	this->KVMap["AddAccount.7"] = "";
	this->KPMap["AddAccount.7"] = "Query";
	this->KVMap["AddAccount.8"] = "";
	this->KPMap["AddAccount.8"] = "Query";
	this->KVMap["AddAccount.9"] = "";
	this->KPMap["AddAccount.9"] = "Query";
	this->KVMap["AddAccount.10"] = "";
	this->KPMap["AddAccount.10"] = "Query";
	this->KVMap["RemoveAccount.1"] = "";
	this->KPMap["RemoveAccount.1"] = "Query";
	this->KVMap["RemoveAccount.2"] = "";
	this->KPMap["RemoveAccount.2"] = "Query";
	this->KVMap["RemoveAccount.3"] = "";
	this->KPMap["RemoveAccount.3"] = "Query";
	this->KVMap["RemoveAccount.4"] = "";
	this->KPMap["RemoveAccount.4"] = "Query";
	this->KVMap["RemoveAccount.5"] = "";
	this->KPMap["RemoveAccount.5"] = "Query";
	this->KVMap["RemoveAccount.6"] = "";
	this->KPMap["RemoveAccount.6"] = "Query";
	this->KVMap["RemoveAccount.7"] = "";
	this->KPMap["RemoveAccount.7"] = "Query";
	this->KVMap["RemoveAccount.8"] = "";
	this->KPMap["RemoveAccount.8"] = "Query";
	this->KVMap["RemoveAccount.9"] = "";
	this->KPMap["RemoveAccount.9"] = "Query";
	this->KVMap["RemoveAccount.10"] = "";
	this->KPMap["RemoveAccount.10"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";

}
EcsModifyInstanceAttributeRequest::EcsModifyInstanceAttributeRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "ecs";
	this->Action = "ModifyInstanceAttribute";
	this->Version = "2014-05-26";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["InstanceId"] = "";
	this->KPMap["InstanceId"] = "Query";
	this->KVMap["Password"] = "";
	this->KPMap["Password"] = "Query";
	this->KVMap["HostName"] = "";
	this->KPMap["HostName"] = "Query";
	this->KVMap["InstanceName"] = "";
	this->KPMap["InstanceName"] = "Query";
	this->KVMap["Description"] = "";
	this->KPMap["Description"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";

}
EcsModifyInstanceNetworkSpecRequest::EcsModifyInstanceNetworkSpecRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "ecs";
	this->Action = "ModifyInstanceNetworkSpec";
	this->Version = "2014-05-26";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["InstanceId"] = "";
	this->KPMap["InstanceId"] = "Query";
	this->KVMap["InternetMaxBandwidthOut"] = "";
	this->KPMap["InternetMaxBandwidthOut"] = "Query";
	this->KVMap["InternetMaxBandwidthIn"] = "";
	this->KPMap["InternetMaxBandwidthIn"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";

}
EcsModifyInstanceVncPasswdRequest::EcsModifyInstanceVncPasswdRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "ecs";
	this->Action = "ModifyInstanceVncPasswd";
	this->Version = "2014-05-26";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["InstanceId"] = "";
	this->KPMap["InstanceId"] = "Query";
	this->KVMap["VncPassword"] = "";
	this->KPMap["VncPassword"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";

}
EcsModifyInstanceVpcAttributeRequest::EcsModifyInstanceVpcAttributeRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "ecs";
	this->Action = "ModifyInstanceVpcAttribute";
	this->Version = "2014-05-26";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["InstanceId"] = "";
	this->KPMap["InstanceId"] = "Query";
	this->KVMap["VSwitchId"] = "";
	this->KPMap["VSwitchId"] = "Query";
	this->KVMap["PrivateIpAddress"] = "";
	this->KPMap["PrivateIpAddress"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";

}
EcsModifySecurityGroupAttributeRequest::EcsModifySecurityGroupAttributeRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "ecs";
	this->Action = "ModifySecurityGroupAttribute";
	this->Version = "2014-05-26";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["SecurityGroupId"] = "";
	this->KPMap["SecurityGroupId"] = "Query";
	this->KVMap["Description"] = "";
	this->KPMap["Description"] = "Query";
	this->KVMap["SecurityGroupName"] = "";
	this->KPMap["SecurityGroupName"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";

}
EcsModifySnapshotAttributeRequest::EcsModifySnapshotAttributeRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "ecs";
	this->Action = "ModifySnapshotAttribute";
	this->Version = "2014-05-26";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";
	this->KVMap["SnapshotId"] = "";
	this->KPMap["SnapshotId"] = "Query";
	this->KVMap["SnapshotName"] = "";
	this->KPMap["SnapshotName"] = "Query";
	this->KVMap["Description"] = "";
	this->KPMap["Description"] = "Query";

}
EcsModifyVpcAttributeRequest::EcsModifyVpcAttributeRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "ecs";
	this->Action = "ModifyVpcAttribute";
	this->Version = "2014-05-26";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["VpcId"] = "";
	this->KPMap["VpcId"] = "Query";
	this->KVMap["Description"] = "";
	this->KPMap["Description"] = "Query";
	this->KVMap["VpcName"] = "";
	this->KPMap["VpcName"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";
	this->KVMap["UserCidr"] = "";
	this->KPMap["UserCidr"] = "Query";

}
EcsModifyVRouterAttributeRequest::EcsModifyVRouterAttributeRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "ecs";
	this->Action = "ModifyVRouterAttribute";
	this->Version = "2014-05-26";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["VRouterId"] = "";
	this->KPMap["VRouterId"] = "Query";
	this->KVMap["VRouterName"] = "";
	this->KPMap["VRouterName"] = "Query";
	this->KVMap["Description"] = "";
	this->KPMap["Description"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";

}
EcsModifyVSwitchAttributeRequest::EcsModifyVSwitchAttributeRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "ecs";
	this->Action = "ModifyVSwitchAttribute";
	this->Version = "2014-05-26";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["VSwitchId"] = "";
	this->KPMap["VSwitchId"] = "Query";
	this->KVMap["VSwitchName"] = "";
	this->KPMap["VSwitchName"] = "Query";
	this->KVMap["Description"] = "";
	this->KPMap["Description"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";

}
EcsRebootInstanceRequest::EcsRebootInstanceRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "ecs";
	this->Action = "RebootInstance";
	this->Version = "2014-05-26";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["InstanceId"] = "";
	this->KPMap["InstanceId"] = "Query";
	this->KVMap["ForceStop"] = "";
	this->KPMap["ForceStop"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";

}
EcsReInitDiskRequest::EcsReInitDiskRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "ecs";
	this->Action = "ReInitDisk";
	this->Version = "2014-05-26";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["DiskId"] = "";
	this->KPMap["DiskId"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";

}
EcsReleaseEipAddressRequest::EcsReleaseEipAddressRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "ecs";
	this->Action = "ReleaseEipAddress";
	this->Version = "2014-05-26";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["AllocationId"] = "";
	this->KPMap["AllocationId"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";

}
EcsRemoveTagsRequest::EcsRemoveTagsRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "ecs";
	this->Action = "RemoveTags";
	this->Version = "2014-05-26";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["ResourceType"] = "";
	this->KPMap["ResourceType"] = "Query";
	this->KVMap["ResourceId"] = "";
	this->KPMap["ResourceId"] = "Query";
	this->KVMap["Tag.1.Key"] = "";
	this->KPMap["Tag.1.Key"] = "Query";
	this->KVMap["Tag.2.Key"] = "";
	this->KPMap["Tag.2.Key"] = "Query";
	this->KVMap["Tag.3.Key"] = "";
	this->KPMap["Tag.3.Key"] = "Query";
	this->KVMap["Tag.4.Key"] = "";
	this->KPMap["Tag.4.Key"] = "Query";
	this->KVMap["Tag.5.Key"] = "";
	this->KPMap["Tag.5.Key"] = "Query";
	this->KVMap["Tag.1.Value"] = "";
	this->KPMap["Tag.1.Value"] = "Query";
	this->KVMap["Tag.2.Value"] = "";
	this->KPMap["Tag.2.Value"] = "Query";
	this->KVMap["Tag.3.Value"] = "";
	this->KPMap["Tag.3.Value"] = "Query";
	this->KVMap["Tag.4.Value"] = "";
	this->KPMap["Tag.4.Value"] = "Query";
	this->KVMap["Tag.5.Value"] = "";
	this->KPMap["Tag.5.Value"] = "Query";

}
EcsRenewInstanceRequest::EcsRenewInstanceRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "ecs";
	this->Action = "RenewInstance";
	this->Version = "2014-05-26";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";
	this->KVMap["InstanceId"] = "";
	this->KPMap["InstanceId"] = "Query";
	this->KVMap["InstanceType"] = "";
	this->KPMap["InstanceType"] = "Query";
	this->KVMap["InternetMaxBandwidthOut"] = "";
	this->KPMap["InternetMaxBandwidthOut"] = "Query";
	this->KVMap["InternetChargeType"] = "";
	this->KPMap["InternetChargeType"] = "Query";
	this->KVMap["Period"] = "";
	this->KPMap["Period"] = "Query";
	this->KVMap["RebootTime"] = "";
	this->KPMap["RebootTime"] = "Query";
	this->KVMap["CovertDiskPortable.1.DiskId"] = "";
	this->KPMap["CovertDiskPortable.1.DiskId"] = "Query";
	this->KVMap["CovertDiskPortable.2.DiskId"] = "";
	this->KPMap["CovertDiskPortable.2.DiskId"] = "Query";
	this->KVMap["CovertDiskPortable.3.DiskId"] = "";
	this->KPMap["CovertDiskPortable.3.DiskId"] = "Query";
	this->KVMap["CovertDiskPortable.4.DiskId"] = "";
	this->KPMap["CovertDiskPortable.4.DiskId"] = "Query";

}
EcsReplaceSystemDiskRequest::EcsReplaceSystemDiskRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "ecs";
	this->Action = "ReplaceSystemDisk";
	this->Version = "2014-05-26";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["InstanceId"] = "";
	this->KPMap["InstanceId"] = "Query";
	this->KVMap["ImageId"] = "";
	this->KPMap["ImageId"] = "Query";
	this->KVMap["ClientToken"] = "";
	this->KPMap["ClientToken"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";
	this->KVMap["UseAdditionalService"] = "";
	this->KPMap["UseAdditionalService"] = "Query";

}
EcsResetDiskRequest::EcsResetDiskRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "ecs";
	this->Action = "ResetDisk";
	this->Version = "2014-05-26";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["DiskId"] = "";
	this->KPMap["DiskId"] = "Query";
	this->KVMap["SnapshotId"] = "";
	this->KPMap["SnapshotId"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";

}
EcsResizeDiskRequest::EcsResizeDiskRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "ecs";
	this->Action = "ResizeDisk";
	this->Version = "2014-05-26";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["DiskId"] = "";
	this->KPMap["DiskId"] = "Query";
	this->KVMap["NewSize"] = "";
	this->KPMap["NewSize"] = "Query";
	this->KVMap["ClientToken"] = "";
	this->KPMap["ClientToken"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";

}
EcsRevokeSecurityGroupRequest::EcsRevokeSecurityGroupRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "ecs";
	this->Action = "RevokeSecurityGroup";
	this->Version = "2014-05-26";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["SecurityGroupId"] = "";
	this->KPMap["SecurityGroupId"] = "Query";
	this->KVMap["IpProtocol"] = "";
	this->KPMap["IpProtocol"] = "Query";
	this->KVMap["PortRange"] = "";
	this->KPMap["PortRange"] = "Query";
	this->KVMap["SourceGroupId"] = "";
	this->KPMap["SourceGroupId"] = "Query";
	this->KVMap["SourceGroupOwnerAccount"] = "";
	this->KPMap["SourceGroupOwnerAccount"] = "Query";
	this->KVMap["SourceCidrIp"] = "";
	this->KPMap["SourceCidrIp"] = "Query";
	this->KVMap["Policy"] = "";
	this->KPMap["Policy"] = "Query";
	this->KVMap["NicType"] = "";
	this->KPMap["NicType"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";

}
EcsRevokeSecurityGroupEgressRequest::EcsRevokeSecurityGroupEgressRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "ecs";
	this->Action = "RevokeSecurityGroupEgress";
	this->Version = "2014-05-26";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["SecurityGroupId"] = "";
	this->KPMap["SecurityGroupId"] = "Query";
	this->KVMap["IpProtocol"] = "";
	this->KPMap["IpProtocol"] = "Query";
	this->KVMap["PortRange"] = "";
	this->KPMap["PortRange"] = "Query";
	this->KVMap["DestGroupId"] = "";
	this->KPMap["DestGroupId"] = "Query";
	this->KVMap["DestGroupOwnerAccount"] = "";
	this->KPMap["DestGroupOwnerAccount"] = "Query";
	this->KVMap["DestCidrIp"] = "";
	this->KPMap["DestCidrIp"] = "Query";
	this->KVMap["Policy"] = "";
	this->KPMap["Policy"] = "Query";
	this->KVMap["NicType"] = "";
	this->KPMap["NicType"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";

}
EcsStartInstanceRequest::EcsStartInstanceRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "ecs";
	this->Action = "StartInstance";
	this->Version = "2014-05-26";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["InstanceId"] = "";
	this->KPMap["InstanceId"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";

}
EcsStopInstanceRequest::EcsStopInstanceRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "ecs";
	this->Action = "StopInstance";
	this->Version = "2014-05-26";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["InstanceId"] = "";
	this->KPMap["InstanceId"] = "Query";
	this->KVMap["ForceStop"] = "";
	this->KPMap["ForceStop"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";

}
EcsUnassociateEipAddressRequest::EcsUnassociateEipAddressRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "ecs";
	this->Action = "UnassociateEipAddress";
	this->Version = "2014-05-26";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["AllocationId"] = "";
	this->KPMap["AllocationId"] = "Query";
	this->KVMap["InstanceId"] = "";
	this->KPMap["InstanceId"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";
	this->KVMap["InstanceType"] = "";
	this->KPMap["InstanceType"] = "Query";

}
EcsUnassociateHaVipRequest::EcsUnassociateHaVipRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "ecs";
	this->Action = "UnassociateHaVip";
	this->Version = "2014-05-26";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";
	this->KVMap["ClientToken"] = "";
	this->KPMap["ClientToken"] = "Query";
	this->KVMap["HaVipId"] = "";
	this->KPMap["HaVipId"] = "Query";
	this->KVMap["InstanceId"] = "";
	this->KPMap["InstanceId"] = "Query";
	this->KVMap["Force"] = "";
	this->KPMap["Force"] = "Query";

}

	
}