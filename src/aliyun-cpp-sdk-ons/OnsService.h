/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
 
#ifndef ALIYUNONS_ONSSERVICE_H_
#define ALIYUNONS_ONSSERVICE_H_

#include "../aliyun-cpp-sdk-core/BaseRequest.h"
#include "../aliyun-cpp-sdk-core/ApiBase.h"

namespace AliYunOns {

class OnsClient : public AliYunCore::ApiBase {
private:
	AliYunCore::DefaultProfile &Profile;
public:
	OnsClient(AliYunCore::DefaultProfile &pf);
	void setProfile(AliYunCore::DefaultProfile &pf);
	std::string getResponse(AliYunCore::BaseRequest &request);
	~OnsClient(){};
};
class OnsOnsCloudGetAppkeyListRequest : public AliYunCore::BaseRequest {
public:
	OnsOnsCloudGetAppkeyListRequest();
};
class OnsOnsClusterListRequest : public AliYunCore::BaseRequest {
public:
	OnsOnsClusterListRequest();
};
class OnsOnsClusterNamesRequest : public AliYunCore::BaseRequest {
public:
	OnsOnsClusterNamesRequest();
};
class OnsOnsConsumerAccumulateRequest : public AliYunCore::BaseRequest {
public:
	OnsOnsConsumerAccumulateRequest();
};
class OnsOnsConsumerGetConnectionRequest : public AliYunCore::BaseRequest {
public:
	OnsOnsConsumerGetConnectionRequest();
};
class OnsOnsConsumerResetOffsetRequest : public AliYunCore::BaseRequest {
public:
	OnsOnsConsumerResetOffsetRequest();
};
class OnsOnsConsumerStatusRequest : public AliYunCore::BaseRequest {
public:
	OnsOnsConsumerStatusRequest();
};
class OnsOnsConsumerTimeSpanRequest : public AliYunCore::BaseRequest {
public:
	OnsOnsConsumerTimeSpanRequest();
};
class OnsOnsEmpowerCreateRequest : public AliYunCore::BaseRequest {
public:
	OnsOnsEmpowerCreateRequest();
};
class OnsOnsEmpowerDeleteRequest : public AliYunCore::BaseRequest {
public:
	OnsOnsEmpowerDeleteRequest();
};
class OnsOnsEmpowerListRequest : public AliYunCore::BaseRequest {
public:
	OnsOnsEmpowerListRequest();
};
class OnsOnsMessageGetByKeyRequest : public AliYunCore::BaseRequest {
public:
	OnsOnsMessageGetByKeyRequest();
};
class OnsOnsMessageGetByMsgIdRequest : public AliYunCore::BaseRequest {
public:
	OnsOnsMessageGetByMsgIdRequest();
};
class OnsOnsMessageGetByTopicRequest : public AliYunCore::BaseRequest {
public:
	OnsOnsMessageGetByTopicRequest();
};
class OnsOnsMessagePushRequest : public AliYunCore::BaseRequest {
public:
	OnsOnsMessagePushRequest();
};
class OnsOnsMessageSendRequest : public AliYunCore::BaseRequest {
public:
	OnsOnsMessageSendRequest();
};
class OnsOnsMessageTraceRequest : public AliYunCore::BaseRequest {
public:
	OnsOnsMessageTraceRequest();
};
class OnsOnsPublishCreateRequest : public AliYunCore::BaseRequest {
public:
	OnsOnsPublishCreateRequest();
};
class OnsOnsPublishDeleteRequest : public AliYunCore::BaseRequest {
public:
	OnsOnsPublishDeleteRequest();
};
class OnsOnsPublishGetRequest : public AliYunCore::BaseRequest {
public:
	OnsOnsPublishGetRequest();
};
class OnsOnsPublishListRequest : public AliYunCore::BaseRequest {
public:
	OnsOnsPublishListRequest();
};
class OnsOnsPublishSearchRequest : public AliYunCore::BaseRequest {
public:
	OnsOnsPublishSearchRequest();
};
class OnsOnsRegionListRequest : public AliYunCore::BaseRequest {
public:
	OnsOnsRegionListRequest();
};
class OnsOnsSubscriptionCreateRequest : public AliYunCore::BaseRequest {
public:
	OnsOnsSubscriptionCreateRequest();
};
class OnsOnsSubscriptionDeleteRequest : public AliYunCore::BaseRequest {
public:
	OnsOnsSubscriptionDeleteRequest();
};
class OnsOnsSubscriptionGetRequest : public AliYunCore::BaseRequest {
public:
	OnsOnsSubscriptionGetRequest();
};
class OnsOnsSubscriptionListRequest : public AliYunCore::BaseRequest {
public:
	OnsOnsSubscriptionListRequest();
};
class OnsOnsSubscriptionSearchRequest : public AliYunCore::BaseRequest {
public:
	OnsOnsSubscriptionSearchRequest();
};
class OnsOnsTopicCreateRequest : public AliYunCore::BaseRequest {
public:
	OnsOnsTopicCreateRequest();
};
class OnsOnsTopicDeleteRequest : public AliYunCore::BaseRequest {
public:
	OnsOnsTopicDeleteRequest();
};
class OnsOnsTopicGetRequest : public AliYunCore::BaseRequest {
public:
	OnsOnsTopicGetRequest();
};
class OnsOnsTopicListRequest : public AliYunCore::BaseRequest {
public:
	OnsOnsTopicListRequest();
};
class OnsOnsTopicSearchRequest : public AliYunCore::BaseRequest {
public:
	OnsOnsTopicSearchRequest();
};
class OnsOnsTopicStatusRequest : public AliYunCore::BaseRequest {
public:
	OnsOnsTopicStatusRequest();
};
class OnsOnsTrendClusterInputTpsRequest : public AliYunCore::BaseRequest {
public:
	OnsOnsTrendClusterInputTpsRequest();
};
class OnsOnsTrendClusterOutputTpsRequest : public AliYunCore::BaseRequest {
public:
	OnsOnsTrendClusterOutputTpsRequest();
};
class OnsOnsTrendGetMachineSarRequest : public AliYunCore::BaseRequest {
public:
	OnsOnsTrendGetMachineSarRequest();
};
class OnsOnsTrendGroupOutputTpsRequest : public AliYunCore::BaseRequest {
public:
	OnsOnsTrendGroupOutputTpsRequest();
};
class OnsOnsTrendTopicInputTpsRequest : public AliYunCore::BaseRequest {
public:
	OnsOnsTrendTopicInputTpsRequest();
};
class OnsOnsWarnAdminRequest : public AliYunCore::BaseRequest {
public:
	OnsOnsWarnAdminRequest();
};
class OnsOnsWarnCreateRequest : public AliYunCore::BaseRequest {
public:
	OnsOnsWarnCreateRequest();
};
class OnsOnsWarnDeleteRequest : public AliYunCore::BaseRequest {
public:
	OnsOnsWarnDeleteRequest();
};
class OnsOnsWarnDisableRequest : public AliYunCore::BaseRequest {
public:
	OnsOnsWarnDisableRequest();
};
class OnsOnsWarnEditorRequest : public AliYunCore::BaseRequest {
public:
	OnsOnsWarnEditorRequest();
};
class OnsOnsWarnEnableRequest : public AliYunCore::BaseRequest {
public:
	OnsOnsWarnEnableRequest();
};
class OnsOnsWarnListRequest : public AliYunCore::BaseRequest {
public:
	OnsOnsWarnListRequest();
};


}

#endif