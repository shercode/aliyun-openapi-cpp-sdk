/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

#include <string>
#include <map>
#include "../aliyun-cpp-sdk-core/utils.h"
#include "../aliyun-cpp-sdk-core/DefaultProfile.h"
#include "../aliyun-cpp-sdk-core/BaseRequest.h"
#include "../aliyun-cpp-sdk-core/ApiBase.h"
#include "OnsService.h"

using std::string;
using namespace AliYunCore;

namespace AliYunOns {

OnsClient::OnsClient(DefaultProfile &pf):Profile(pf){}
void OnsClient::setProfile(DefaultProfile &pf){
	this->Profile = pf;
}
string OnsClient::getResponse(BaseRequest &request){
	request.RegionId = this->Profile.RegionId;		
	request.AccessKeyId = this->Profile.AccessKeyId;
	request.AccessKeySecret = this->Profile.AccessKeySecret;
	return ApiBase::getResponse(request);
}
OnsOnsCloudGetAppkeyListRequest::OnsOnsCloudGetAppkeyListRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "ons";
	this->Action = "OnsCloudGetAppkeyList";
	this->Version = "2015-10-20";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OnsRegionId"] = "";
	this->KPMap["OnsRegionId"] = "Query";
	this->KVMap["OnsPlatform"] = "";
	this->KPMap["OnsPlatform"] = "Query";
	this->KVMap["PreventCache"] = "";
	this->KPMap["PreventCache"] = "Query";
	this->KVMap["IsvId"] = "";
	this->KPMap["IsvId"] = "Query";

}
OnsOnsClusterListRequest::OnsOnsClusterListRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "ons";
	this->Action = "OnsClusterList";
	this->Version = "2015-10-20";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OnsRegionId"] = "";
	this->KPMap["OnsRegionId"] = "Query";
	this->KVMap["OnsPlatform"] = "";
	this->KPMap["OnsPlatform"] = "Query";
	this->KVMap["PreventCache"] = "";
	this->KPMap["PreventCache"] = "Query";
	this->KVMap["Cluster"] = "";
	this->KPMap["Cluster"] = "Query";

}
OnsOnsClusterNamesRequest::OnsOnsClusterNamesRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "ons";
	this->Action = "OnsClusterNames";
	this->Version = "2015-10-20";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OnsRegionId"] = "";
	this->KPMap["OnsRegionId"] = "Query";
	this->KVMap["OnsPlatform"] = "";
	this->KPMap["OnsPlatform"] = "Query";
	this->KVMap["PreventCache"] = "";
	this->KPMap["PreventCache"] = "Query";

}
OnsOnsConsumerAccumulateRequest::OnsOnsConsumerAccumulateRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "ons";
	this->Action = "OnsConsumerAccumulate";
	this->Version = "2015-10-20";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OnsRegionId"] = "";
	this->KPMap["OnsRegionId"] = "Query";
	this->KVMap["OnsPlatform"] = "";
	this->KPMap["OnsPlatform"] = "Query";
	this->KVMap["PreventCache"] = "";
	this->KPMap["PreventCache"] = "Query";
	this->KVMap["ConsumerId"] = "";
	this->KPMap["ConsumerId"] = "Query";
	this->KVMap["Detail"] = "";
	this->KPMap["Detail"] = "Query";

}
OnsOnsConsumerGetConnectionRequest::OnsOnsConsumerGetConnectionRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "ons";
	this->Action = "OnsConsumerGetConnection";
	this->Version = "2015-10-20";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OnsRegionId"] = "";
	this->KPMap["OnsRegionId"] = "Query";
	this->KVMap["OnsPlatform"] = "";
	this->KPMap["OnsPlatform"] = "Query";
	this->KVMap["PreventCache"] = "";
	this->KPMap["PreventCache"] = "Query";
	this->KVMap["ConsumerId"] = "";
	this->KPMap["ConsumerId"] = "Query";

}
OnsOnsConsumerResetOffsetRequest::OnsOnsConsumerResetOffsetRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "ons";
	this->Action = "OnsConsumerResetOffset";
	this->Version = "2015-10-20";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OnsRegionId"] = "";
	this->KPMap["OnsRegionId"] = "Query";
	this->KVMap["OnsPlatform"] = "";
	this->KPMap["OnsPlatform"] = "Query";
	this->KVMap["PreventCache"] = "";
	this->KPMap["PreventCache"] = "Query";
	this->KVMap["ConsumerId"] = "";
	this->KPMap["ConsumerId"] = "Query";
	this->KVMap["Topic"] = "";
	this->KPMap["Topic"] = "Query";
	this->KVMap["ResetTimestamp"] = "";
	this->KPMap["ResetTimestamp"] = "Query";

}
OnsOnsConsumerStatusRequest::OnsOnsConsumerStatusRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "ons";
	this->Action = "OnsConsumerStatus";
	this->Version = "2015-10-20";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OnsRegionId"] = "";
	this->KPMap["OnsRegionId"] = "Query";
	this->KVMap["OnsPlatform"] = "";
	this->KPMap["OnsPlatform"] = "Query";
	this->KVMap["PreventCache"] = "";
	this->KPMap["PreventCache"] = "Query";
	this->KVMap["ConsumerId"] = "";
	this->KPMap["ConsumerId"] = "Query";
	this->KVMap["Detail"] = "";
	this->KPMap["Detail"] = "Query";
	this->KVMap["NeedJstack"] = "";
	this->KPMap["NeedJstack"] = "Query";

}
OnsOnsConsumerTimeSpanRequest::OnsOnsConsumerTimeSpanRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "ons";
	this->Action = "OnsConsumerTimeSpan";
	this->Version = "2015-10-20";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OnsRegionId"] = "";
	this->KPMap["OnsRegionId"] = "Query";
	this->KVMap["OnsPlatform"] = "";
	this->KPMap["OnsPlatform"] = "Query";
	this->KVMap["PreventCache"] = "";
	this->KPMap["PreventCache"] = "Query";
	this->KVMap["ConsumerId"] = "";
	this->KPMap["ConsumerId"] = "Query";
	this->KVMap["Topic"] = "";
	this->KPMap["Topic"] = "Query";

}
OnsOnsEmpowerCreateRequest::OnsOnsEmpowerCreateRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "ons";
	this->Action = "OnsEmpowerCreate";
	this->Version = "2015-10-20";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OnsRegionId"] = "";
	this->KPMap["OnsRegionId"] = "Query";
	this->KVMap["OnsPlatform"] = "";
	this->KPMap["OnsPlatform"] = "Query";
	this->KVMap["PreventCache"] = "";
	this->KPMap["PreventCache"] = "Query";
	this->KVMap["EmpowerUser"] = "";
	this->KPMap["EmpowerUser"] = "Query";
	this->KVMap["Topic"] = "";
	this->KPMap["Topic"] = "Query";
	this->KVMap["Relation"] = "";
	this->KPMap["Relation"] = "Query";

}
OnsOnsEmpowerDeleteRequest::OnsOnsEmpowerDeleteRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "ons";
	this->Action = "OnsEmpowerDelete";
	this->Version = "2015-10-20";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OnsRegionId"] = "";
	this->KPMap["OnsRegionId"] = "Query";
	this->KVMap["OnsPlatform"] = "";
	this->KPMap["OnsPlatform"] = "Query";
	this->KVMap["PreventCache"] = "";
	this->KPMap["PreventCache"] = "Query";
	this->KVMap["EmpowerUser"] = "";
	this->KPMap["EmpowerUser"] = "Query";
	this->KVMap["Topic"] = "";
	this->KPMap["Topic"] = "Query";

}
OnsOnsEmpowerListRequest::OnsOnsEmpowerListRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "ons";
	this->Action = "OnsEmpowerList";
	this->Version = "2015-10-20";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OnsRegionId"] = "";
	this->KPMap["OnsRegionId"] = "Query";
	this->KVMap["OnsPlatform"] = "";
	this->KPMap["OnsPlatform"] = "Query";
	this->KVMap["PreventCache"] = "";
	this->KPMap["PreventCache"] = "Query";
	this->KVMap["EmpowerUser"] = "";
	this->KPMap["EmpowerUser"] = "Query";
	this->KVMap["Topic"] = "";
	this->KPMap["Topic"] = "Query";

}
OnsOnsMessageGetByKeyRequest::OnsOnsMessageGetByKeyRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "ons";
	this->Action = "OnsMessageGetByKey";
	this->Version = "2015-10-20";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OnsRegionId"] = "";
	this->KPMap["OnsRegionId"] = "Query";
	this->KVMap["OnsPlatform"] = "";
	this->KPMap["OnsPlatform"] = "Query";
	this->KVMap["PreventCache"] = "";
	this->KPMap["PreventCache"] = "Query";
	this->KVMap["Topic"] = "";
	this->KPMap["Topic"] = "Query";
	this->KVMap["Key"] = "";
	this->KPMap["Key"] = "Query";

}
OnsOnsMessageGetByMsgIdRequest::OnsOnsMessageGetByMsgIdRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "ons";
	this->Action = "OnsMessageGetByMsgId";
	this->Version = "2015-10-20";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OnsRegionId"] = "";
	this->KPMap["OnsRegionId"] = "Query";
	this->KVMap["OnsPlatform"] = "";
	this->KPMap["OnsPlatform"] = "Query";
	this->KVMap["PreventCache"] = "";
	this->KPMap["PreventCache"] = "Query";
	this->KVMap["MsgId"] = "";
	this->KPMap["MsgId"] = "Query";

}
OnsOnsMessageGetByTopicRequest::OnsOnsMessageGetByTopicRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "ons";
	this->Action = "OnsMessageGetByTopic";
	this->Version = "2015-10-20";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OnsRegionId"] = "";
	this->KPMap["OnsRegionId"] = "Query";
	this->KVMap["OnsPlatform"] = "";
	this->KPMap["OnsPlatform"] = "Query";
	this->KVMap["PreventCache"] = "";
	this->KPMap["PreventCache"] = "Query";
	this->KVMap["Topic"] = "";
	this->KPMap["Topic"] = "Query";

}
OnsOnsMessagePushRequest::OnsOnsMessagePushRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "ons";
	this->Action = "OnsMessagePush";
	this->Version = "2015-10-20";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OnsRegionId"] = "";
	this->KPMap["OnsRegionId"] = "Query";
	this->KVMap["OnsPlatform"] = "";
	this->KPMap["OnsPlatform"] = "Query";
	this->KVMap["PreventCache"] = "";
	this->KPMap["PreventCache"] = "Query";
	this->KVMap["ConsumerId"] = "";
	this->KPMap["ConsumerId"] = "Query";
	this->KVMap["ClientId"] = "";
	this->KPMap["ClientId"] = "Query";
	this->KVMap["MsgId"] = "";
	this->KPMap["MsgId"] = "Query";
	this->KVMap["Topic"] = "";
	this->KPMap["Topic"] = "Query";

}
OnsOnsMessageSendRequest::OnsOnsMessageSendRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "ons";
	this->Action = "OnsMessageSend";
	this->Version = "2015-10-20";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OnsRegionId"] = "";
	this->KPMap["OnsRegionId"] = "Query";
	this->KVMap["OnsPlatform"] = "";
	this->KPMap["OnsPlatform"] = "Query";
	this->KVMap["PreventCache"] = "";
	this->KPMap["PreventCache"] = "Query";
	this->KVMap["ProducerId"] = "";
	this->KPMap["ProducerId"] = "Query";
	this->KVMap["Topic"] = "";
	this->KPMap["Topic"] = "Query";
	this->KVMap["Tag"] = "";
	this->KPMap["Tag"] = "Query";
	this->KVMap["Key"] = "";
	this->KPMap["Key"] = "Query";
	this->KVMap["Message"] = "";
	this->KPMap["Message"] = "Query";

}
OnsOnsMessageTraceRequest::OnsOnsMessageTraceRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "ons";
	this->Action = "OnsMessageTrace";
	this->Version = "2015-10-20";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OnsRegionId"] = "";
	this->KPMap["OnsRegionId"] = "Query";
	this->KVMap["OnsPlatform"] = "";
	this->KPMap["OnsPlatform"] = "Query";
	this->KVMap["PreventCache"] = "";
	this->KPMap["PreventCache"] = "Query";
	this->KVMap["Topic"] = "";
	this->KPMap["Topic"] = "Query";
	this->KVMap["MsgId"] = "";
	this->KPMap["MsgId"] = "Query";

}
OnsOnsPublishCreateRequest::OnsOnsPublishCreateRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "ons";
	this->Action = "OnsPublishCreate";
	this->Version = "2015-10-20";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OnsRegionId"] = "";
	this->KPMap["OnsRegionId"] = "Query";
	this->KVMap["OnsPlatform"] = "";
	this->KPMap["OnsPlatform"] = "Query";
	this->KVMap["PreventCache"] = "";
	this->KPMap["PreventCache"] = "Query";
	this->KVMap["ProducerId"] = "";
	this->KPMap["ProducerId"] = "Query";
	this->KVMap["Topic"] = "";
	this->KPMap["Topic"] = "Query";
	this->KVMap["AppName"] = "";
	this->KPMap["AppName"] = "Query";

}
OnsOnsPublishDeleteRequest::OnsOnsPublishDeleteRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "ons";
	this->Action = "OnsPublishDelete";
	this->Version = "2015-10-20";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OnsRegionId"] = "";
	this->KPMap["OnsRegionId"] = "Query";
	this->KVMap["OnsPlatform"] = "";
	this->KPMap["OnsPlatform"] = "Query";
	this->KVMap["PreventCache"] = "";
	this->KPMap["PreventCache"] = "Query";
	this->KVMap["ProducerId"] = "";
	this->KPMap["ProducerId"] = "Query";
	this->KVMap["Topic"] = "";
	this->KPMap["Topic"] = "Query";

}
OnsOnsPublishGetRequest::OnsOnsPublishGetRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "ons";
	this->Action = "OnsPublishGet";
	this->Version = "2015-10-20";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OnsRegionId"] = "";
	this->KPMap["OnsRegionId"] = "Query";
	this->KVMap["OnsPlatform"] = "";
	this->KPMap["OnsPlatform"] = "Query";
	this->KVMap["PreventCache"] = "";
	this->KPMap["PreventCache"] = "Query";
	this->KVMap["ProducerId"] = "";
	this->KPMap["ProducerId"] = "Query";
	this->KVMap["Topic"] = "";
	this->KPMap["Topic"] = "Query";

}
OnsOnsPublishListRequest::OnsOnsPublishListRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "ons";
	this->Action = "OnsPublishList";
	this->Version = "2015-10-20";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OnsRegionId"] = "";
	this->KPMap["OnsRegionId"] = "Query";
	this->KVMap["OnsPlatform"] = "";
	this->KPMap["OnsPlatform"] = "Query";
	this->KVMap["PreventCache"] = "";
	this->KPMap["PreventCache"] = "Query";

}
OnsOnsPublishSearchRequest::OnsOnsPublishSearchRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "ons";
	this->Action = "OnsPublishSearch";
	this->Version = "2015-10-20";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OnsRegionId"] = "";
	this->KPMap["OnsRegionId"] = "Query";
	this->KVMap["OnsPlatform"] = "";
	this->KPMap["OnsPlatform"] = "Query";
	this->KVMap["PreventCache"] = "";
	this->KPMap["PreventCache"] = "Query";
	this->KVMap["Search"] = "";
	this->KPMap["Search"] = "Query";

}
OnsOnsRegionListRequest::OnsOnsRegionListRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "ons";
	this->Action = "OnsRegionList";
	this->Version = "2015-10-20";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OnsRegionId"] = "";
	this->KPMap["OnsRegionId"] = "Query";
	this->KVMap["OnsPlatform"] = "";
	this->KPMap["OnsPlatform"] = "Query";
	this->KVMap["PreventCache"] = "";
	this->KPMap["PreventCache"] = "Query";

}
OnsOnsSubscriptionCreateRequest::OnsOnsSubscriptionCreateRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "ons";
	this->Action = "OnsSubscriptionCreate";
	this->Version = "2015-10-20";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OnsRegionId"] = "";
	this->KPMap["OnsRegionId"] = "Query";
	this->KVMap["OnsPlatform"] = "";
	this->KPMap["OnsPlatform"] = "Query";
	this->KVMap["PreventCache"] = "";
	this->KPMap["PreventCache"] = "Query";
	this->KVMap["ConsumerId"] = "";
	this->KPMap["ConsumerId"] = "Query";
	this->KVMap["Topic"] = "";
	this->KPMap["Topic"] = "Query";

}
OnsOnsSubscriptionDeleteRequest::OnsOnsSubscriptionDeleteRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "ons";
	this->Action = "OnsSubscriptionDelete";
	this->Version = "2015-10-20";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OnsRegionId"] = "";
	this->KPMap["OnsRegionId"] = "Query";
	this->KVMap["OnsPlatform"] = "";
	this->KPMap["OnsPlatform"] = "Query";
	this->KVMap["PreventCache"] = "";
	this->KPMap["PreventCache"] = "Query";
	this->KVMap["ConsumerId"] = "";
	this->KPMap["ConsumerId"] = "Query";
	this->KVMap["Topic"] = "";
	this->KPMap["Topic"] = "Query";

}
OnsOnsSubscriptionGetRequest::OnsOnsSubscriptionGetRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "ons";
	this->Action = "OnsSubscriptionGet";
	this->Version = "2015-10-20";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OnsRegionId"] = "";
	this->KPMap["OnsRegionId"] = "Query";
	this->KVMap["OnsPlatform"] = "";
	this->KPMap["OnsPlatform"] = "Query";
	this->KVMap["PreventCache"] = "";
	this->KPMap["PreventCache"] = "Query";
	this->KVMap["ConsumerId"] = "";
	this->KPMap["ConsumerId"] = "Query";
	this->KVMap["Topic"] = "";
	this->KPMap["Topic"] = "Query";

}
OnsOnsSubscriptionListRequest::OnsOnsSubscriptionListRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "ons";
	this->Action = "OnsSubscriptionList";
	this->Version = "2015-10-20";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OnsRegionId"] = "";
	this->KPMap["OnsRegionId"] = "Query";
	this->KVMap["OnsPlatform"] = "";
	this->KPMap["OnsPlatform"] = "Query";
	this->KVMap["PreventCache"] = "";
	this->KPMap["PreventCache"] = "Query";

}
OnsOnsSubscriptionSearchRequest::OnsOnsSubscriptionSearchRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "ons";
	this->Action = "OnsSubscriptionSearch";
	this->Version = "2015-10-20";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OnsRegionId"] = "";
	this->KPMap["OnsRegionId"] = "Query";
	this->KVMap["OnsPlatform"] = "";
	this->KPMap["OnsPlatform"] = "Query";
	this->KVMap["PreventCache"] = "";
	this->KPMap["PreventCache"] = "Query";
	this->KVMap["Search"] = "";
	this->KPMap["Search"] = "Query";

}
OnsOnsTopicCreateRequest::OnsOnsTopicCreateRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "ons";
	this->Action = "OnsTopicCreate";
	this->Version = "2015-10-20";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OnsRegionId"] = "";
	this->KPMap["OnsRegionId"] = "Query";
	this->KVMap["OnsPlatform"] = "";
	this->KPMap["OnsPlatform"] = "Query";
	this->KVMap["PreventCache"] = "";
	this->KPMap["PreventCache"] = "Query";
	this->KVMap["Topic"] = "";
	this->KPMap["Topic"] = "Query";
	this->KVMap["Cluster"] = "";
	this->KPMap["Cluster"] = "Query";
	this->KVMap["QueueNum"] = "";
	this->KPMap["QueueNum"] = "Query";
	this->KVMap["Order"] = "";
	this->KPMap["Order"] = "Query";
	this->KVMap["Qps"] = "";
	this->KPMap["Qps"] = "Query";
	this->KVMap["Status"] = "";
	this->KPMap["Status"] = "Query";
	this->KVMap["Remark"] = "";
	this->KPMap["Remark"] = "Query";
	this->KVMap["Appkey"] = "";
	this->KPMap["Appkey"] = "Query";
	this->KVMap["AppName"] = "";
	this->KPMap["AppName"] = "Query";

}
OnsOnsTopicDeleteRequest::OnsOnsTopicDeleteRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "ons";
	this->Action = "OnsTopicDelete";
	this->Version = "2015-10-20";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OnsRegionId"] = "";
	this->KPMap["OnsRegionId"] = "Query";
	this->KVMap["OnsPlatform"] = "";
	this->KPMap["OnsPlatform"] = "Query";
	this->KVMap["PreventCache"] = "";
	this->KPMap["PreventCache"] = "Query";
	this->KVMap["Topic"] = "";
	this->KPMap["Topic"] = "Query";
	this->KVMap["Cluster"] = "";
	this->KPMap["Cluster"] = "Query";

}
OnsOnsTopicGetRequest::OnsOnsTopicGetRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "ons";
	this->Action = "OnsTopicGet";
	this->Version = "2015-10-20";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OnsRegionId"] = "";
	this->KPMap["OnsRegionId"] = "Query";
	this->KVMap["OnsPlatform"] = "";
	this->KPMap["OnsPlatform"] = "Query";
	this->KVMap["PreventCache"] = "";
	this->KPMap["PreventCache"] = "Query";
	this->KVMap["Topic"] = "";
	this->KPMap["Topic"] = "Query";

}
OnsOnsTopicListRequest::OnsOnsTopicListRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "ons";
	this->Action = "OnsTopicList";
	this->Version = "2015-10-20";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OnsRegionId"] = "";
	this->KPMap["OnsRegionId"] = "Query";
	this->KVMap["OnsPlatform"] = "";
	this->KPMap["OnsPlatform"] = "Query";
	this->KVMap["PreventCache"] = "";
	this->KPMap["PreventCache"] = "Query";
	this->KVMap["Topic"] = "";
	this->KPMap["Topic"] = "Query";

}
OnsOnsTopicSearchRequest::OnsOnsTopicSearchRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "ons";
	this->Action = "OnsTopicSearch";
	this->Version = "2015-10-20";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OnsRegionId"] = "";
	this->KPMap["OnsRegionId"] = "Query";
	this->KVMap["OnsPlatform"] = "";
	this->KPMap["OnsPlatform"] = "Query";
	this->KVMap["PreventCache"] = "";
	this->KPMap["PreventCache"] = "Query";
	this->KVMap["Search"] = "";
	this->KPMap["Search"] = "Query";

}
OnsOnsTopicStatusRequest::OnsOnsTopicStatusRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "ons";
	this->Action = "OnsTopicStatus";
	this->Version = "2015-10-20";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OnsRegionId"] = "";
	this->KPMap["OnsRegionId"] = "Query";
	this->KVMap["OnsPlatform"] = "";
	this->KPMap["OnsPlatform"] = "Query";
	this->KVMap["PreventCache"] = "";
	this->KPMap["PreventCache"] = "Query";
	this->KVMap["Topic"] = "";
	this->KPMap["Topic"] = "Query";
	this->KVMap["Detail"] = "";
	this->KPMap["Detail"] = "Query";

}
OnsOnsTrendClusterInputTpsRequest::OnsOnsTrendClusterInputTpsRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "ons";
	this->Action = "OnsTrendClusterInputTps";
	this->Version = "2015-10-20";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OnsRegionId"] = "";
	this->KPMap["OnsRegionId"] = "Query";
	this->KVMap["OnsPlatform"] = "";
	this->KPMap["OnsPlatform"] = "Query";
	this->KVMap["PreventCache"] = "";
	this->KPMap["PreventCache"] = "Query";
	this->KVMap["Cluster"] = "";
	this->KPMap["Cluster"] = "Query";
	this->KVMap["BeginTime"] = "";
	this->KPMap["BeginTime"] = "Query";
	this->KVMap["EndTime"] = "";
	this->KPMap["EndTime"] = "Query";
	this->KVMap["Period"] = "";
	this->KPMap["Period"] = "Query";

}
OnsOnsTrendClusterOutputTpsRequest::OnsOnsTrendClusterOutputTpsRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "ons";
	this->Action = "OnsTrendClusterOutputTps";
	this->Version = "2015-10-20";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OnsRegionId"] = "";
	this->KPMap["OnsRegionId"] = "Query";
	this->KVMap["OnsPlatform"] = "";
	this->KPMap["OnsPlatform"] = "Query";
	this->KVMap["PreventCache"] = "";
	this->KPMap["PreventCache"] = "Query";
	this->KVMap["Cluster"] = "";
	this->KPMap["Cluster"] = "Query";
	this->KVMap["BeginTime"] = "";
	this->KPMap["BeginTime"] = "Query";
	this->KVMap["EndTime"] = "";
	this->KPMap["EndTime"] = "Query";
	this->KVMap["Period"] = "";
	this->KPMap["Period"] = "Query";

}
OnsOnsTrendGetMachineSarRequest::OnsOnsTrendGetMachineSarRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "ons";
	this->Action = "OnsTrendGetMachineSar";
	this->Version = "2015-10-20";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OnsRegionId"] = "";
	this->KPMap["OnsRegionId"] = "Query";
	this->KVMap["OnsPlatform"] = "";
	this->KPMap["OnsPlatform"] = "Query";
	this->KVMap["PreventCache"] = "";
	this->KPMap["PreventCache"] = "Query";
	this->KVMap["HostIp"] = "";
	this->KPMap["HostIp"] = "Query";
	this->KVMap["AppId"] = "";
	this->KPMap["AppId"] = "Query";
	this->KVMap["Metric"] = "";
	this->KPMap["Metric"] = "Query";
	this->KVMap["BeginTime"] = "";
	this->KPMap["BeginTime"] = "Query";
	this->KVMap["EndTime"] = "";
	this->KPMap["EndTime"] = "Query";
	this->KVMap["Period"] = "";
	this->KPMap["Period"] = "Query";

}
OnsOnsTrendGroupOutputTpsRequest::OnsOnsTrendGroupOutputTpsRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "ons";
	this->Action = "OnsTrendGroupOutputTps";
	this->Version = "2015-10-20";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OnsRegionId"] = "";
	this->KPMap["OnsRegionId"] = "Query";
	this->KVMap["OnsPlatform"] = "";
	this->KPMap["OnsPlatform"] = "Query";
	this->KVMap["PreventCache"] = "";
	this->KPMap["PreventCache"] = "Query";
	this->KVMap["ConsumerId"] = "";
	this->KPMap["ConsumerId"] = "Query";
	this->KVMap["Topic"] = "";
	this->KPMap["Topic"] = "Query";
	this->KVMap["BeginTime"] = "";
	this->KPMap["BeginTime"] = "Query";
	this->KVMap["EndTime"] = "";
	this->KPMap["EndTime"] = "Query";
	this->KVMap["Period"] = "";
	this->KPMap["Period"] = "Query";

}
OnsOnsTrendTopicInputTpsRequest::OnsOnsTrendTopicInputTpsRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "ons";
	this->Action = "OnsTrendTopicInputTps";
	this->Version = "2015-10-20";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OnsRegionId"] = "";
	this->KPMap["OnsRegionId"] = "Query";
	this->KVMap["OnsPlatform"] = "";
	this->KPMap["OnsPlatform"] = "Query";
	this->KVMap["PreventCache"] = "";
	this->KPMap["PreventCache"] = "Query";
	this->KVMap["Topic"] = "";
	this->KPMap["Topic"] = "Query";
	this->KVMap["BeginTime"] = "";
	this->KPMap["BeginTime"] = "Query";
	this->KVMap["EndTime"] = "";
	this->KPMap["EndTime"] = "Query";
	this->KVMap["Period"] = "";
	this->KPMap["Period"] = "Query";

}
OnsOnsWarnAdminRequest::OnsOnsWarnAdminRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "ons";
	this->Action = "OnsWarnAdmin";
	this->Version = "2015-10-20";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OnsRegionId"] = "";
	this->KPMap["OnsRegionId"] = "Query";
	this->KVMap["OnsPlatform"] = "";
	this->KPMap["OnsPlatform"] = "Query";
	this->KVMap["PreventCache"] = "";
	this->KPMap["PreventCache"] = "Query";
	this->KVMap["UserId"] = "";
	this->KPMap["UserId"] = "Query";
	this->KVMap["ConsumerId"] = "";
	this->KPMap["ConsumerId"] = "Query";
	this->KVMap["Topic"] = "";
	this->KPMap["Topic"] = "Query";
	this->KVMap["Type"] = "";
	this->KPMap["Type"] = "Query";

}
OnsOnsWarnCreateRequest::OnsOnsWarnCreateRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "ons";
	this->Action = "OnsWarnCreate";
	this->Version = "2015-10-20";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OnsRegionId"] = "";
	this->KPMap["OnsRegionId"] = "Query";
	this->KVMap["OnsPlatform"] = "";
	this->KPMap["OnsPlatform"] = "Query";
	this->KVMap["PreventCache"] = "";
	this->KPMap["PreventCache"] = "Query";
	this->KVMap["ConsumerId"] = "";
	this->KPMap["ConsumerId"] = "Query";
	this->KVMap["Topic"] = "";
	this->KPMap["Topic"] = "Query";
	this->KVMap["Threshold"] = "";
	this->KPMap["Threshold"] = "Query";
	this->KVMap["Contacts"] = "";
	this->KPMap["Contacts"] = "Query";

}
OnsOnsWarnDeleteRequest::OnsOnsWarnDeleteRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "ons";
	this->Action = "OnsWarnDelete";
	this->Version = "2015-10-20";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OnsRegionId"] = "";
	this->KPMap["OnsRegionId"] = "Query";
	this->KVMap["OnsPlatform"] = "";
	this->KPMap["OnsPlatform"] = "Query";
	this->KVMap["PreventCache"] = "";
	this->KPMap["PreventCache"] = "Query";
	this->KVMap["ConsumerId"] = "";
	this->KPMap["ConsumerId"] = "Query";
	this->KVMap["Topic"] = "";
	this->KPMap["Topic"] = "Query";

}
OnsOnsWarnDisableRequest::OnsOnsWarnDisableRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "ons";
	this->Action = "OnsWarnDisable";
	this->Version = "2015-10-20";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OnsRegionId"] = "";
	this->KPMap["OnsRegionId"] = "Query";
	this->KVMap["OnsPlatform"] = "";
	this->KPMap["OnsPlatform"] = "Query";
	this->KVMap["PreventCache"] = "";
	this->KPMap["PreventCache"] = "Query";
	this->KVMap["ConsumerId"] = "";
	this->KPMap["ConsumerId"] = "Query";
	this->KVMap["Topic"] = "";
	this->KPMap["Topic"] = "Query";

}
OnsOnsWarnEditorRequest::OnsOnsWarnEditorRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "ons";
	this->Action = "OnsWarnEditor";
	this->Version = "2015-10-20";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OnsRegionId"] = "";
	this->KPMap["OnsRegionId"] = "Query";
	this->KVMap["OnsPlatform"] = "";
	this->KPMap["OnsPlatform"] = "Query";
	this->KVMap["PreventCache"] = "";
	this->KPMap["PreventCache"] = "Query";
	this->KVMap["ConsumerId"] = "";
	this->KPMap["ConsumerId"] = "Query";
	this->KVMap["Topic"] = "";
	this->KPMap["Topic"] = "Query";
	this->KVMap["Threshold"] = "";
	this->KPMap["Threshold"] = "Query";
	this->KVMap["Contacts"] = "";
	this->KPMap["Contacts"] = "Query";

}
OnsOnsWarnEnableRequest::OnsOnsWarnEnableRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "ons";
	this->Action = "OnsWarnEnable";
	this->Version = "2015-10-20";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OnsRegionId"] = "";
	this->KPMap["OnsRegionId"] = "Query";
	this->KVMap["OnsPlatform"] = "";
	this->KPMap["OnsPlatform"] = "Query";
	this->KVMap["PreventCache"] = "";
	this->KPMap["PreventCache"] = "Query";
	this->KVMap["ConsumerId"] = "";
	this->KPMap["ConsumerId"] = "Query";
	this->KVMap["Topic"] = "";
	this->KPMap["Topic"] = "Query";

}
OnsOnsWarnListRequest::OnsOnsWarnListRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "ons";
	this->Action = "OnsWarnList";
	this->Version = "2015-10-20";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OnsRegionId"] = "";
	this->KPMap["OnsRegionId"] = "Query";
	this->KVMap["OnsPlatform"] = "";
	this->KPMap["OnsPlatform"] = "Query";
	this->KVMap["PreventCache"] = "";
	this->KPMap["PreventCache"] = "Query";
	this->KVMap["ConsumerId"] = "";
	this->KPMap["ConsumerId"] = "Query";
	this->KVMap["Topic"] = "";
	this->KPMap["Topic"] = "Query";

}

	
}