/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
 
#ifndef ALIYUNESS_ESSSERVICE_H_
#define ALIYUNESS_ESSSERVICE_H_

#include "../aliyun-cpp-sdk-core/BaseRequest.h"
#include "../aliyun-cpp-sdk-core/ApiBase.h"

namespace AliYunEss {

class EssClient : public AliYunCore::ApiBase {
private:
	AliYunCore::DefaultProfile &Profile;
public:
	EssClient(AliYunCore::DefaultProfile &pf);
	void setProfile(AliYunCore::DefaultProfile &pf);
	std::string getResponse(AliYunCore::BaseRequest &request);
	~EssClient(){};
};
class EssAttachInstancesRequest : public AliYunCore::BaseRequest {
public:
	EssAttachInstancesRequest();
};
class EssCreateScalingConfigurationRequest : public AliYunCore::BaseRequest {
public:
	EssCreateScalingConfigurationRequest();
};
class EssCreateScalingGroupRequest : public AliYunCore::BaseRequest {
public:
	EssCreateScalingGroupRequest();
};
class EssCreateScalingRuleRequest : public AliYunCore::BaseRequest {
public:
	EssCreateScalingRuleRequest();
};
class EssCreateScheduledTaskRequest : public AliYunCore::BaseRequest {
public:
	EssCreateScheduledTaskRequest();
};
class EssDeleteScalingConfigurationRequest : public AliYunCore::BaseRequest {
public:
	EssDeleteScalingConfigurationRequest();
};
class EssDeleteScalingGroupRequest : public AliYunCore::BaseRequest {
public:
	EssDeleteScalingGroupRequest();
};
class EssDeleteScalingRuleRequest : public AliYunCore::BaseRequest {
public:
	EssDeleteScalingRuleRequest();
};
class EssDeleteScheduledTaskRequest : public AliYunCore::BaseRequest {
public:
	EssDeleteScheduledTaskRequest();
};
class EssDescribeScalingActivitiesRequest : public AliYunCore::BaseRequest {
public:
	EssDescribeScalingActivitiesRequest();
};
class EssDescribeScalingConfigurationsRequest : public AliYunCore::BaseRequest {
public:
	EssDescribeScalingConfigurationsRequest();
};
class EssDescribeScalingGroupsRequest : public AliYunCore::BaseRequest {
public:
	EssDescribeScalingGroupsRequest();
};
class EssDescribeScalingInstancesRequest : public AliYunCore::BaseRequest {
public:
	EssDescribeScalingInstancesRequest();
};
class EssDescribeScalingRulesRequest : public AliYunCore::BaseRequest {
public:
	EssDescribeScalingRulesRequest();
};
class EssDescribeScheduledTasksRequest : public AliYunCore::BaseRequest {
public:
	EssDescribeScheduledTasksRequest();
};
class EssDetachInstancesRequest : public AliYunCore::BaseRequest {
public:
	EssDetachInstancesRequest();
};
class EssDisableScalingGroupRequest : public AliYunCore::BaseRequest {
public:
	EssDisableScalingGroupRequest();
};
class EssEnableScalingGroupRequest : public AliYunCore::BaseRequest {
public:
	EssEnableScalingGroupRequest();
};
class EssExecuteScalingRuleRequest : public AliYunCore::BaseRequest {
public:
	EssExecuteScalingRuleRequest();
};
class EssModifyScalingGroupRequest : public AliYunCore::BaseRequest {
public:
	EssModifyScalingGroupRequest();
};
class EssModifyScalingRuleRequest : public AliYunCore::BaseRequest {
public:
	EssModifyScalingRuleRequest();
};
class EssModifyScheduledTaskRequest : public AliYunCore::BaseRequest {
public:
	EssModifyScheduledTaskRequest();
};
class EssRemoveInstancesRequest : public AliYunCore::BaseRequest {
public:
	EssRemoveInstancesRequest();
};


}

#endif