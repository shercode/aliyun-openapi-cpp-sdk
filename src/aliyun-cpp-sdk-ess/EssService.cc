/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

#include <string>
#include <map>
#include "../aliyun-cpp-sdk-core/utils.h"
#include "../aliyun-cpp-sdk-core/DefaultProfile.h"
#include "../aliyun-cpp-sdk-core/BaseRequest.h"
#include "../aliyun-cpp-sdk-core/ApiBase.h"
#include "EssService.h"

using std::string;
using namespace AliYunCore;

namespace AliYunEss {

EssClient::EssClient(DefaultProfile &pf):Profile(pf){}
void EssClient::setProfile(DefaultProfile &pf){
	this->Profile = pf;
}
string EssClient::getResponse(BaseRequest &request){
	request.RegionId = this->Profile.RegionId;		
	request.AccessKeyId = this->Profile.AccessKeyId;
	request.AccessKeySecret = this->Profile.AccessKeySecret;
	return ApiBase::getResponse(request);
}
EssAttachInstancesRequest::EssAttachInstancesRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "ess";
	this->Action = "AttachInstances";
	this->Version = "2014-08-28";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["ScalingGroupId"] = "";
	this->KPMap["ScalingGroupId"] = "Query";
	this->KVMap["InstanceId.1"] = "";
	this->KPMap["InstanceId.1"] = "Query";
	this->KVMap["InstanceId.2"] = "";
	this->KPMap["InstanceId.2"] = "Query";
	this->KVMap["InstanceId.3"] = "";
	this->KPMap["InstanceId.3"] = "Query";
	this->KVMap["InstanceId.4"] = "";
	this->KPMap["InstanceId.4"] = "Query";
	this->KVMap["InstanceId.5"] = "";
	this->KPMap["InstanceId.5"] = "Query";
	this->KVMap["InstanceId.6"] = "";
	this->KPMap["InstanceId.6"] = "Query";
	this->KVMap["InstanceId.7"] = "";
	this->KPMap["InstanceId.7"] = "Query";
	this->KVMap["InstanceId.8"] = "";
	this->KPMap["InstanceId.8"] = "Query";
	this->KVMap["InstanceId.9"] = "";
	this->KPMap["InstanceId.9"] = "Query";
	this->KVMap["InstanceId.10"] = "";
	this->KPMap["InstanceId.10"] = "Query";
	this->KVMap["InstanceId.11"] = "";
	this->KPMap["InstanceId.11"] = "Query";
	this->KVMap["InstanceId.12"] = "";
	this->KPMap["InstanceId.12"] = "Query";
	this->KVMap["InstanceId.13"] = "";
	this->KPMap["InstanceId.13"] = "Query";
	this->KVMap["InstanceId.14"] = "";
	this->KPMap["InstanceId.14"] = "Query";
	this->KVMap["InstanceId.15"] = "";
	this->KPMap["InstanceId.15"] = "Query";
	this->KVMap["InstanceId.16"] = "";
	this->KPMap["InstanceId.16"] = "Query";
	this->KVMap["InstanceId.17"] = "";
	this->KPMap["InstanceId.17"] = "Query";
	this->KVMap["InstanceId.18"] = "";
	this->KPMap["InstanceId.18"] = "Query";
	this->KVMap["InstanceId.19"] = "";
	this->KPMap["InstanceId.19"] = "Query";
	this->KVMap["InstanceId.20"] = "";
	this->KPMap["InstanceId.20"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";

}
EssCreateScalingConfigurationRequest::EssCreateScalingConfigurationRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "ess";
	this->Action = "CreateScalingConfiguration";
	this->Version = "2014-08-28";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["ScalingGroupId"] = "";
	this->KPMap["ScalingGroupId"] = "Query";
	this->KVMap["ImageId"] = "";
	this->KPMap["ImageId"] = "Query";
	this->KVMap["InstanceType"] = "";
	this->KPMap["InstanceType"] = "Query";
	this->KVMap["SecurityGroupId"] = "";
	this->KPMap["SecurityGroupId"] = "Query";
	this->KVMap["InternetChargeType"] = "";
	this->KPMap["InternetChargeType"] = "Query";
	this->KVMap["InternetMaxBandwidthIn"] = "";
	this->KPMap["InternetMaxBandwidthIn"] = "Query";
	this->KVMap["InternetMaxBandwidthOut"] = "";
	this->KPMap["InternetMaxBandwidthOut"] = "Query";
	this->KVMap["SystemDisk.Category"] = "";
	this->KPMap["SystemDisk.Category"] = "Query";
	this->KVMap["ScalingConfigurationName"] = "";
	this->KPMap["ScalingConfigurationName"] = "Query";
	this->KVMap["DataDisk.1.Size"] = "";
	this->KPMap["DataDisk.1.Size"] = "Query";
	this->KVMap["DataDisk.2.Size"] = "";
	this->KPMap["DataDisk.2.Size"] = "Query";
	this->KVMap["DataDisk.3.Size"] = "";
	this->KPMap["DataDisk.3.Size"] = "Query";
	this->KVMap["DataDisk.4.Size"] = "";
	this->KPMap["DataDisk.4.Size"] = "Query";
	this->KVMap["DataDisk.1.Category"] = "";
	this->KPMap["DataDisk.1.Category"] = "Query";
	this->KVMap["DataDisk.2.Category"] = "";
	this->KPMap["DataDisk.2.Category"] = "Query";
	this->KVMap["DataDisk.3.Category"] = "";
	this->KPMap["DataDisk.3.Category"] = "Query";
	this->KVMap["DataDisk.4.Category"] = "";
	this->KPMap["DataDisk.4.Category"] = "Query";
	this->KVMap["DataDisk.1.SnapshotId"] = "";
	this->KPMap["DataDisk.1.SnapshotId"] = "Query";
	this->KVMap["DataDisk.2.SnapshotId"] = "";
	this->KPMap["DataDisk.2.SnapshotId"] = "Query";
	this->KVMap["DataDisk.3.SnapshotId"] = "";
	this->KPMap["DataDisk.3.SnapshotId"] = "Query";
	this->KVMap["DataDisk.4.SnapshotId"] = "";
	this->KPMap["DataDisk.4.SnapshotId"] = "Query";
	this->KVMap["DataDisk.1.Device"] = "";
	this->KPMap["DataDisk.1.Device"] = "Query";
	this->KVMap["DataDisk.2.Device"] = "";
	this->KPMap["DataDisk.2.Device"] = "Query";
	this->KVMap["DataDisk.3.Device"] = "";
	this->KPMap["DataDisk.3.Device"] = "Query";
	this->KVMap["DataDisk.4.Device"] = "";
	this->KPMap["DataDisk.4.Device"] = "Query";
	this->KVMap["DataDisk.1.DeleteWithInstance"] = "";
	this->KPMap["DataDisk.1.DeleteWithInstance"] = "Query";
	this->KVMap["DataDisk.2.DeleteWithInstance"] = "";
	this->KPMap["DataDisk.2.DeleteWithInstance"] = "Query";
	this->KVMap["DataDisk.3.DeleteWithInstance"] = "";
	this->KPMap["DataDisk.3.DeleteWithInstance"] = "Query";
	this->KVMap["DataDisk.4.DeleteWithInstance"] = "";
	this->KPMap["DataDisk.4.DeleteWithInstance"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";

}
EssCreateScalingGroupRequest::EssCreateScalingGroupRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "ess";
	this->Action = "CreateScalingGroup";
	this->Version = "2014-08-28";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["ScalingGroupName"] = "";
	this->KPMap["ScalingGroupName"] = "Query";
	this->KVMap["MinSize"] = "";
	this->KPMap["MinSize"] = "Query";
	this->KVMap["MaxSize"] = "";
	this->KPMap["MaxSize"] = "Query";
	this->KVMap["DefaultCooldown"] = "";
	this->KPMap["DefaultCooldown"] = "Query";
	this->KVMap["LoadBalancerId"] = "";
	this->KPMap["LoadBalancerId"] = "Query";
	this->KVMap["DBInstanceId.1"] = "";
	this->KPMap["DBInstanceId.1"] = "Query";
	this->KVMap["DBInstanceId.2"] = "";
	this->KPMap["DBInstanceId.2"] = "Query";
	this->KVMap["DBInstanceId.3"] = "";
	this->KPMap["DBInstanceId.3"] = "Query";
	this->KVMap["RemovalPolicy.1"] = "";
	this->KPMap["RemovalPolicy.1"] = "Query";
	this->KVMap["RemovalPolicy.2"] = "";
	this->KPMap["RemovalPolicy.2"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";

}
EssCreateScalingRuleRequest::EssCreateScalingRuleRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "ess";
	this->Action = "CreateScalingRule";
	this->Version = "2014-08-28";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["ScalingGroupId"] = "";
	this->KPMap["ScalingGroupId"] = "Query";
	this->KVMap["ScalingRuleName"] = "";
	this->KPMap["ScalingRuleName"] = "Query";
	this->KVMap["Cooldown"] = "";
	this->KPMap["Cooldown"] = "Query";
	this->KVMap["AdjustmentType"] = "";
	this->KPMap["AdjustmentType"] = "Query";
	this->KVMap["AdjustmentValue"] = "";
	this->KPMap["AdjustmentValue"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";

}
EssCreateScheduledTaskRequest::EssCreateScheduledTaskRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "ess";
	this->Action = "CreateScheduledTask";
	this->Version = "2014-08-28";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["ScheduledTaskName"] = "";
	this->KPMap["ScheduledTaskName"] = "Query";
	this->KVMap["Description"] = "";
	this->KPMap["Description"] = "Query";
	this->KVMap["ScheduledAction"] = "";
	this->KPMap["ScheduledAction"] = "Query";
	this->KVMap["RecurrenceEndTime"] = "";
	this->KPMap["RecurrenceEndTime"] = "Query";
	this->KVMap["LaunchTime"] = "";
	this->KPMap["LaunchTime"] = "Query";
	this->KVMap["RecurrenceType"] = "";
	this->KPMap["RecurrenceType"] = "Query";
	this->KVMap["RecurrenceValue"] = "";
	this->KPMap["RecurrenceValue"] = "Query";
	this->KVMap["TaskEnabled"] = "";
	this->KPMap["TaskEnabled"] = "Query";
	this->KVMap["LaunchExpirationTime"] = "";
	this->KPMap["LaunchExpirationTime"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";

}
EssDeleteScalingConfigurationRequest::EssDeleteScalingConfigurationRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "ess";
	this->Action = "DeleteScalingConfiguration";
	this->Version = "2014-08-28";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["ScalingConfigurationId"] = "";
	this->KPMap["ScalingConfigurationId"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";

}
EssDeleteScalingGroupRequest::EssDeleteScalingGroupRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "ess";
	this->Action = "DeleteScalingGroup";
	this->Version = "2014-08-28";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["ScalingGroupId"] = "";
	this->KPMap["ScalingGroupId"] = "Query";
	this->KVMap["ForceDelete"] = "";
	this->KPMap["ForceDelete"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";

}
EssDeleteScalingRuleRequest::EssDeleteScalingRuleRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "ess";
	this->Action = "DeleteScalingRule";
	this->Version = "2014-08-28";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["ScalingRuleId"] = "";
	this->KPMap["ScalingRuleId"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";

}
EssDeleteScheduledTaskRequest::EssDeleteScheduledTaskRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "ess";
	this->Action = "DeleteScheduledTask";
	this->Version = "2014-08-28";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["ScheduledTaskId"] = "";
	this->KPMap["ScheduledTaskId"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";

}
EssDescribeScalingActivitiesRequest::EssDescribeScalingActivitiesRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "ess";
	this->Action = "DescribeScalingActivities";
	this->Version = "2014-08-28";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["PageNumber"] = "";
	this->KPMap["PageNumber"] = "Query";
	this->KVMap["PageSize"] = "";
	this->KPMap["PageSize"] = "Query";
	this->KVMap["ScalingGroupId"] = "";
	this->KPMap["ScalingGroupId"] = "Query";
	this->KVMap["StatusCode"] = "";
	this->KPMap["StatusCode"] = "Query";
	this->KVMap["ScalingActivityId.1"] = "";
	this->KPMap["ScalingActivityId.1"] = "Query";
	this->KVMap["ScalingActivityId.2"] = "";
	this->KPMap["ScalingActivityId.2"] = "Query";
	this->KVMap["ScalingActivityId.3"] = "";
	this->KPMap["ScalingActivityId.3"] = "Query";
	this->KVMap["ScalingActivityId.4"] = "";
	this->KPMap["ScalingActivityId.4"] = "Query";
	this->KVMap["ScalingActivityId.5"] = "";
	this->KPMap["ScalingActivityId.5"] = "Query";
	this->KVMap["ScalingActivityId.6"] = "";
	this->KPMap["ScalingActivityId.6"] = "Query";
	this->KVMap["ScalingActivityId.7"] = "";
	this->KPMap["ScalingActivityId.7"] = "Query";
	this->KVMap["ScalingActivityId.8"] = "";
	this->KPMap["ScalingActivityId.8"] = "Query";
	this->KVMap["ScalingActivityId.9"] = "";
	this->KPMap["ScalingActivityId.9"] = "Query";
	this->KVMap["ScalingActivityId.10"] = "";
	this->KPMap["ScalingActivityId.10"] = "Query";
	this->KVMap["ScalingActivityId.11"] = "";
	this->KPMap["ScalingActivityId.11"] = "Query";
	this->KVMap["ScalingActivityId.12"] = "";
	this->KPMap["ScalingActivityId.12"] = "Query";
	this->KVMap["ScalingActivityId.13"] = "";
	this->KPMap["ScalingActivityId.13"] = "Query";
	this->KVMap["ScalingActivityId.14"] = "";
	this->KPMap["ScalingActivityId.14"] = "Query";
	this->KVMap["ScalingActivityId.15"] = "";
	this->KPMap["ScalingActivityId.15"] = "Query";
	this->KVMap["ScalingActivityId.16"] = "";
	this->KPMap["ScalingActivityId.16"] = "Query";
	this->KVMap["ScalingActivityId.17"] = "";
	this->KPMap["ScalingActivityId.17"] = "Query";
	this->KVMap["ScalingActivityId.18"] = "";
	this->KPMap["ScalingActivityId.18"] = "Query";
	this->KVMap["ScalingActivityId.19"] = "";
	this->KPMap["ScalingActivityId.19"] = "Query";
	this->KVMap["ScalingActivityId.20"] = "";
	this->KPMap["ScalingActivityId.20"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";

}
EssDescribeScalingConfigurationsRequest::EssDescribeScalingConfigurationsRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "ess";
	this->Action = "DescribeScalingConfigurations";
	this->Version = "2014-08-28";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["PageNumber"] = "";
	this->KPMap["PageNumber"] = "Query";
	this->KVMap["PageSize"] = "";
	this->KPMap["PageSize"] = "Query";
	this->KVMap["ScalingGroupId"] = "";
	this->KPMap["ScalingGroupId"] = "Query";
	this->KVMap["ScalingConfigurationId.1"] = "";
	this->KPMap["ScalingConfigurationId.1"] = "Query";
	this->KVMap["ScalingConfigurationId.2"] = "";
	this->KPMap["ScalingConfigurationId.2"] = "Query";
	this->KVMap["ScalingConfigurationId.3"] = "";
	this->KPMap["ScalingConfigurationId.3"] = "Query";
	this->KVMap["ScalingConfigurationId.4"] = "";
	this->KPMap["ScalingConfigurationId.4"] = "Query";
	this->KVMap["ScalingConfigurationId.5"] = "";
	this->KPMap["ScalingConfigurationId.5"] = "Query";
	this->KVMap["ScalingConfigurationId.6"] = "";
	this->KPMap["ScalingConfigurationId.6"] = "Query";
	this->KVMap["ScalingConfigurationId.7"] = "";
	this->KPMap["ScalingConfigurationId.7"] = "Query";
	this->KVMap["ScalingConfigurationId.8"] = "";
	this->KPMap["ScalingConfigurationId.8"] = "Query";
	this->KVMap["ScalingConfigurationId.9"] = "";
	this->KPMap["ScalingConfigurationId.9"] = "Query";
	this->KVMap["ScalingConfigurationId.10"] = "";
	this->KPMap["ScalingConfigurationId.10"] = "Query";
	this->KVMap["ScalingConfigurationName.1"] = "";
	this->KPMap["ScalingConfigurationName.1"] = "Query";
	this->KVMap["ScalingConfigurationName.2"] = "";
	this->KPMap["ScalingConfigurationName.2"] = "Query";
	this->KVMap["ScalingConfigurationName.3"] = "";
	this->KPMap["ScalingConfigurationName.3"] = "Query";
	this->KVMap["ScalingConfigurationName.4"] = "";
	this->KPMap["ScalingConfigurationName.4"] = "Query";
	this->KVMap["ScalingConfigurationName.5"] = "";
	this->KPMap["ScalingConfigurationName.5"] = "Query";
	this->KVMap["ScalingConfigurationName.6"] = "";
	this->KPMap["ScalingConfigurationName.6"] = "Query";
	this->KVMap["ScalingConfigurationName.7"] = "";
	this->KPMap["ScalingConfigurationName.7"] = "Query";
	this->KVMap["ScalingConfigurationName.8"] = "";
	this->KPMap["ScalingConfigurationName.8"] = "Query";
	this->KVMap["ScalingConfigurationName.9"] = "";
	this->KPMap["ScalingConfigurationName.9"] = "Query";
	this->KVMap["ScalingConfigurationName.10"] = "";
	this->KPMap["ScalingConfigurationName.10"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";

}
EssDescribeScalingGroupsRequest::EssDescribeScalingGroupsRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "ess";
	this->Action = "DescribeScalingGroups";
	this->Version = "2014-08-28";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["PageNumber"] = "";
	this->KPMap["PageNumber"] = "Query";
	this->KVMap["PageSize"] = "";
	this->KPMap["PageSize"] = "Query";
	this->KVMap["ScalingGroupId.1"] = "";
	this->KPMap["ScalingGroupId.1"] = "Query";
	this->KVMap["ScalingGroupId.2"] = "";
	this->KPMap["ScalingGroupId.2"] = "Query";
	this->KVMap["ScalingGroupId.3"] = "";
	this->KPMap["ScalingGroupId.3"] = "Query";
	this->KVMap["ScalingGroupId.4"] = "";
	this->KPMap["ScalingGroupId.4"] = "Query";
	this->KVMap["ScalingGroupId.5"] = "";
	this->KPMap["ScalingGroupId.5"] = "Query";
	this->KVMap["ScalingGroupId.6"] = "";
	this->KPMap["ScalingGroupId.6"] = "Query";
	this->KVMap["ScalingGroupId.7"] = "";
	this->KPMap["ScalingGroupId.7"] = "Query";
	this->KVMap["ScalingGroupId.8"] = "";
	this->KPMap["ScalingGroupId.8"] = "Query";
	this->KVMap["ScalingGroupId.9"] = "";
	this->KPMap["ScalingGroupId.9"] = "Query";
	this->KVMap["ScalingGroupId.10"] = "";
	this->KPMap["ScalingGroupId.10"] = "Query";
	this->KVMap["ScalingGroupId.12"] = "";
	this->KPMap["ScalingGroupId.12"] = "Query";
	this->KVMap["ScalingGroupId.13"] = "";
	this->KPMap["ScalingGroupId.13"] = "Query";
	this->KVMap["ScalingGroupId.14"] = "";
	this->KPMap["ScalingGroupId.14"] = "Query";
	this->KVMap["ScalingGroupId.15"] = "";
	this->KPMap["ScalingGroupId.15"] = "Query";
	this->KVMap["ScalingGroupId.16"] = "";
	this->KPMap["ScalingGroupId.16"] = "Query";
	this->KVMap["ScalingGroupId.17"] = "";
	this->KPMap["ScalingGroupId.17"] = "Query";
	this->KVMap["ScalingGroupId.18"] = "";
	this->KPMap["ScalingGroupId.18"] = "Query";
	this->KVMap["ScalingGroupId.19"] = "";
	this->KPMap["ScalingGroupId.19"] = "Query";
	this->KVMap["ScalingGroupId.20"] = "";
	this->KPMap["ScalingGroupId.20"] = "Query";
	this->KVMap["ScalingGroupName.1"] = "";
	this->KPMap["ScalingGroupName.1"] = "Query";
	this->KVMap["ScalingGroupName.2"] = "";
	this->KPMap["ScalingGroupName.2"] = "Query";
	this->KVMap["ScalingGroupName.3"] = "";
	this->KPMap["ScalingGroupName.3"] = "Query";
	this->KVMap["ScalingGroupName.4"] = "";
	this->KPMap["ScalingGroupName.4"] = "Query";
	this->KVMap["ScalingGroupName.5"] = "";
	this->KPMap["ScalingGroupName.5"] = "Query";
	this->KVMap["ScalingGroupName.6"] = "";
	this->KPMap["ScalingGroupName.6"] = "Query";
	this->KVMap["ScalingGroupName.7"] = "";
	this->KPMap["ScalingGroupName.7"] = "Query";
	this->KVMap["ScalingGroupName.8"] = "";
	this->KPMap["ScalingGroupName.8"] = "Query";
	this->KVMap["ScalingGroupName.9"] = "";
	this->KPMap["ScalingGroupName.9"] = "Query";
	this->KVMap["ScalingGroupName.10"] = "";
	this->KPMap["ScalingGroupName.10"] = "Query";
	this->KVMap["ScalingGroupName.11"] = "";
	this->KPMap["ScalingGroupName.11"] = "Query";
	this->KVMap["ScalingGroupName.12"] = "";
	this->KPMap["ScalingGroupName.12"] = "Query";
	this->KVMap["ScalingGroupName.13"] = "";
	this->KPMap["ScalingGroupName.13"] = "Query";
	this->KVMap["ScalingGroupName.14"] = "";
	this->KPMap["ScalingGroupName.14"] = "Query";
	this->KVMap["ScalingGroupName.15"] = "";
	this->KPMap["ScalingGroupName.15"] = "Query";
	this->KVMap["ScalingGroupName.16"] = "";
	this->KPMap["ScalingGroupName.16"] = "Query";
	this->KVMap["ScalingGroupName.17"] = "";
	this->KPMap["ScalingGroupName.17"] = "Query";
	this->KVMap["ScalingGroupName.18"] = "";
	this->KPMap["ScalingGroupName.18"] = "Query";
	this->KVMap["ScalingGroupName.19"] = "";
	this->KPMap["ScalingGroupName.19"] = "Query";
	this->KVMap["ScalingGroupName.20"] = "";
	this->KPMap["ScalingGroupName.20"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";

}
EssDescribeScalingInstancesRequest::EssDescribeScalingInstancesRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "ess";
	this->Action = "DescribeScalingInstances";
	this->Version = "2014-08-28";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["ScalingGroupId"] = "";
	this->KPMap["ScalingGroupId"] = "Query";
	this->KVMap["ScalingConfigurationId"] = "";
	this->KPMap["ScalingConfigurationId"] = "Query";
	this->KVMap["HealthStatus"] = "";
	this->KPMap["HealthStatus"] = "Query";
	this->KVMap["LifecycleState"] = "";
	this->KPMap["LifecycleState"] = "Query";
	this->KVMap["CreationType"] = "";
	this->KPMap["CreationType"] = "Query";
	this->KVMap["PageNumber"] = "";
	this->KPMap["PageNumber"] = "Query";
	this->KVMap["PageSize"] = "";
	this->KPMap["PageSize"] = "Query";
	this->KVMap["InstanceId.1"] = "";
	this->KPMap["InstanceId.1"] = "Query";
	this->KVMap["InstanceId.2"] = "";
	this->KPMap["InstanceId.2"] = "Query";
	this->KVMap["InstanceId.3"] = "";
	this->KPMap["InstanceId.3"] = "Query";
	this->KVMap["InstanceId.4"] = "";
	this->KPMap["InstanceId.4"] = "Query";
	this->KVMap["InstanceId.5"] = "";
	this->KPMap["InstanceId.5"] = "Query";
	this->KVMap["InstanceId.6"] = "";
	this->KPMap["InstanceId.6"] = "Query";
	this->KVMap["InstanceId.7"] = "";
	this->KPMap["InstanceId.7"] = "Query";
	this->KVMap["InstanceId.8"] = "";
	this->KPMap["InstanceId.8"] = "Query";
	this->KVMap["InstanceId.9"] = "";
	this->KPMap["InstanceId.9"] = "Query";
	this->KVMap["InstanceId.10"] = "";
	this->KPMap["InstanceId.10"] = "Query";
	this->KVMap["InstanceId.11"] = "";
	this->KPMap["InstanceId.11"] = "Query";
	this->KVMap["InstanceId.12"] = "";
	this->KPMap["InstanceId.12"] = "Query";
	this->KVMap["InstanceId.13"] = "";
	this->KPMap["InstanceId.13"] = "Query";
	this->KVMap["InstanceId.14"] = "";
	this->KPMap["InstanceId.14"] = "Query";
	this->KVMap["InstanceId.15"] = "";
	this->KPMap["InstanceId.15"] = "Query";
	this->KVMap["InstanceId.16"] = "";
	this->KPMap["InstanceId.16"] = "Query";
	this->KVMap["InstanceId.17"] = "";
	this->KPMap["InstanceId.17"] = "Query";
	this->KVMap["InstanceId.18"] = "";
	this->KPMap["InstanceId.18"] = "Query";
	this->KVMap["InstanceId.19"] = "";
	this->KPMap["InstanceId.19"] = "Query";
	this->KVMap["InstanceId.20"] = "";
	this->KPMap["InstanceId.20"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";

}
EssDescribeScalingRulesRequest::EssDescribeScalingRulesRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "ess";
	this->Action = "DescribeScalingRules";
	this->Version = "2014-08-28";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["PageNumber"] = "";
	this->KPMap["PageNumber"] = "Query";
	this->KVMap["PageSize"] = "";
	this->KPMap["PageSize"] = "Query";
	this->KVMap["ScalingGroupId"] = "";
	this->KPMap["ScalingGroupId"] = "Query";
	this->KVMap["ScalingRuleId.1"] = "";
	this->KPMap["ScalingRuleId.1"] = "Query";
	this->KVMap["ScalingRuleId.2"] = "";
	this->KPMap["ScalingRuleId.2"] = "Query";
	this->KVMap["ScalingRuleId.3"] = "";
	this->KPMap["ScalingRuleId.3"] = "Query";
	this->KVMap["ScalingRuleId.4"] = "";
	this->KPMap["ScalingRuleId.4"] = "Query";
	this->KVMap["ScalingRuleId.5"] = "";
	this->KPMap["ScalingRuleId.5"] = "Query";
	this->KVMap["ScalingRuleId.6"] = "";
	this->KPMap["ScalingRuleId.6"] = "Query";
	this->KVMap["ScalingRuleId.7"] = "";
	this->KPMap["ScalingRuleId.7"] = "Query";
	this->KVMap["ScalingRuleId.8"] = "";
	this->KPMap["ScalingRuleId.8"] = "Query";
	this->KVMap["ScalingRuleId.9"] = "";
	this->KPMap["ScalingRuleId.9"] = "Query";
	this->KVMap["ScalingRuleId.10"] = "";
	this->KPMap["ScalingRuleId.10"] = "Query";
	this->KVMap["ScalingRuleName.1"] = "";
	this->KPMap["ScalingRuleName.1"] = "Query";
	this->KVMap["ScalingRuleName.2"] = "";
	this->KPMap["ScalingRuleName.2"] = "Query";
	this->KVMap["ScalingRuleName.3"] = "";
	this->KPMap["ScalingRuleName.3"] = "Query";
	this->KVMap["ScalingRuleName.4"] = "";
	this->KPMap["ScalingRuleName.4"] = "Query";
	this->KVMap["ScalingRuleName.5"] = "";
	this->KPMap["ScalingRuleName.5"] = "Query";
	this->KVMap["ScalingRuleName.6"] = "";
	this->KPMap["ScalingRuleName.6"] = "Query";
	this->KVMap["ScalingRuleName.7"] = "";
	this->KPMap["ScalingRuleName.7"] = "Query";
	this->KVMap["ScalingRuleName.8"] = "";
	this->KPMap["ScalingRuleName.8"] = "Query";
	this->KVMap["ScalingRuleName.9"] = "";
	this->KPMap["ScalingRuleName.9"] = "Query";
	this->KVMap["ScalingRuleName.10"] = "";
	this->KPMap["ScalingRuleName.10"] = "Query";
	this->KVMap["ScalingRuleAri.1"] = "";
	this->KPMap["ScalingRuleAri.1"] = "Query";
	this->KVMap["ScalingRuleAri.2"] = "";
	this->KPMap["ScalingRuleAri.2"] = "Query";
	this->KVMap["ScalingRuleAri.3"] = "";
	this->KPMap["ScalingRuleAri.3"] = "Query";
	this->KVMap["ScalingRuleAri.4"] = "";
	this->KPMap["ScalingRuleAri.4"] = "Query";
	this->KVMap["ScalingRuleAri.5"] = "";
	this->KPMap["ScalingRuleAri.5"] = "Query";
	this->KVMap["ScalingRuleAri.6"] = "";
	this->KPMap["ScalingRuleAri.6"] = "Query";
	this->KVMap["ScalingRuleAri.7"] = "";
	this->KPMap["ScalingRuleAri.7"] = "Query";
	this->KVMap["ScalingRuleAri.8"] = "";
	this->KPMap["ScalingRuleAri.8"] = "Query";
	this->KVMap["ScalingRuleAri.9"] = "";
	this->KPMap["ScalingRuleAri.9"] = "Query";
	this->KVMap["ScalingRuleAri.10"] = "";
	this->KPMap["ScalingRuleAri.10"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";

}
EssDescribeScheduledTasksRequest::EssDescribeScheduledTasksRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "ess";
	this->Action = "DescribeScheduledTasks";
	this->Version = "2014-08-28";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["PageNumber"] = "";
	this->KPMap["PageNumber"] = "Query";
	this->KVMap["PageSize"] = "";
	this->KPMap["PageSize"] = "Query";
	this->KVMap["ScheduledAction.1"] = "";
	this->KPMap["ScheduledAction.1"] = "Query";
	this->KVMap["ScheduledAction.2"] = "";
	this->KPMap["ScheduledAction.2"] = "Query";
	this->KVMap["ScheduledAction.3"] = "";
	this->KPMap["ScheduledAction.3"] = "Query";
	this->KVMap["ScheduledAction.4"] = "";
	this->KPMap["ScheduledAction.4"] = "Query";
	this->KVMap["ScheduledAction.5"] = "";
	this->KPMap["ScheduledAction.5"] = "Query";
	this->KVMap["ScheduledAction.6"] = "";
	this->KPMap["ScheduledAction.6"] = "Query";
	this->KVMap["ScheduledAction.7"] = "";
	this->KPMap["ScheduledAction.7"] = "Query";
	this->KVMap["ScheduledAction.8"] = "";
	this->KPMap["ScheduledAction.8"] = "Query";
	this->KVMap["ScheduledAction.9"] = "";
	this->KPMap["ScheduledAction.9"] = "Query";
	this->KVMap["ScheduledAction.10"] = "";
	this->KPMap["ScheduledAction.10"] = "Query";
	this->KVMap["ScheduledAction.11"] = "";
	this->KPMap["ScheduledAction.11"] = "Query";
	this->KVMap["ScheduledAction.12"] = "";
	this->KPMap["ScheduledAction.12"] = "Query";
	this->KVMap["ScheduledAction.13"] = "";
	this->KPMap["ScheduledAction.13"] = "Query";
	this->KVMap["ScheduledAction.14"] = "";
	this->KPMap["ScheduledAction.14"] = "Query";
	this->KVMap["ScheduledAction.15"] = "";
	this->KPMap["ScheduledAction.15"] = "Query";
	this->KVMap["ScheduledAction.16"] = "";
	this->KPMap["ScheduledAction.16"] = "Query";
	this->KVMap["ScheduledAction.17"] = "";
	this->KPMap["ScheduledAction.17"] = "Query";
	this->KVMap["ScheduledAction.18"] = "";
	this->KPMap["ScheduledAction.18"] = "Query";
	this->KVMap["ScheduledAction.19"] = "";
	this->KPMap["ScheduledAction.19"] = "Query";
	this->KVMap["ScheduledAction.20"] = "";
	this->KPMap["ScheduledAction.20"] = "Query";
	this->KVMap["ScheduledTaskId.1"] = "";
	this->KPMap["ScheduledTaskId.1"] = "Query";
	this->KVMap["ScheduledTaskId.2"] = "";
	this->KPMap["ScheduledTaskId.2"] = "Query";
	this->KVMap["ScheduledTaskId.3"] = "";
	this->KPMap["ScheduledTaskId.3"] = "Query";
	this->KVMap["ScheduledTaskId.4"] = "";
	this->KPMap["ScheduledTaskId.4"] = "Query";
	this->KVMap["ScheduledTaskId.5"] = "";
	this->KPMap["ScheduledTaskId.5"] = "Query";
	this->KVMap["ScheduledTaskId.6"] = "";
	this->KPMap["ScheduledTaskId.6"] = "Query";
	this->KVMap["ScheduledTaskId.7"] = "";
	this->KPMap["ScheduledTaskId.7"] = "Query";
	this->KVMap["ScheduledTaskId.8"] = "";
	this->KPMap["ScheduledTaskId.8"] = "Query";
	this->KVMap["ScheduledTaskId.9"] = "";
	this->KPMap["ScheduledTaskId.9"] = "Query";
	this->KVMap["ScheduledTaskId.10"] = "";
	this->KPMap["ScheduledTaskId.10"] = "Query";
	this->KVMap["ScheduledTaskId.11"] = "";
	this->KPMap["ScheduledTaskId.11"] = "Query";
	this->KVMap["ScheduledTaskId.12"] = "";
	this->KPMap["ScheduledTaskId.12"] = "Query";
	this->KVMap["ScheduledTaskId.13"] = "";
	this->KPMap["ScheduledTaskId.13"] = "Query";
	this->KVMap["ScheduledTaskId.14"] = "";
	this->KPMap["ScheduledTaskId.14"] = "Query";
	this->KVMap["ScheduledTaskId.15"] = "";
	this->KPMap["ScheduledTaskId.15"] = "Query";
	this->KVMap["ScheduledTaskId.16"] = "";
	this->KPMap["ScheduledTaskId.16"] = "Query";
	this->KVMap["ScheduledTaskId.17"] = "";
	this->KPMap["ScheduledTaskId.17"] = "Query";
	this->KVMap["ScheduledTaskId.18"] = "";
	this->KPMap["ScheduledTaskId.18"] = "Query";
	this->KVMap["ScheduledTaskId.19"] = "";
	this->KPMap["ScheduledTaskId.19"] = "Query";
	this->KVMap["ScheduledTaskId.20"] = "";
	this->KPMap["ScheduledTaskId.20"] = "Query";
	this->KVMap["ScheduledTaskName.1"] = "";
	this->KPMap["ScheduledTaskName.1"] = "Query";
	this->KVMap["ScheduledTaskName.2"] = "";
	this->KPMap["ScheduledTaskName.2"] = "Query";
	this->KVMap["ScheduledTaskName.3"] = "";
	this->KPMap["ScheduledTaskName.3"] = "Query";
	this->KVMap["ScheduledTaskName.4"] = "";
	this->KPMap["ScheduledTaskName.4"] = "Query";
	this->KVMap["ScheduledTaskName.5"] = "";
	this->KPMap["ScheduledTaskName.5"] = "Query";
	this->KVMap["ScheduledTaskName.6"] = "";
	this->KPMap["ScheduledTaskName.6"] = "Query";
	this->KVMap["ScheduledTaskName.7"] = "";
	this->KPMap["ScheduledTaskName.7"] = "Query";
	this->KVMap["ScheduledTaskName.8"] = "";
	this->KPMap["ScheduledTaskName.8"] = "Query";
	this->KVMap["ScheduledTaskName.9"] = "";
	this->KPMap["ScheduledTaskName.9"] = "Query";
	this->KVMap["ScheduledTaskName.10"] = "";
	this->KPMap["ScheduledTaskName.10"] = "Query";
	this->KVMap["ScheduledTaskName.11"] = "";
	this->KPMap["ScheduledTaskName.11"] = "Query";
	this->KVMap["ScheduledTaskName.12"] = "";
	this->KPMap["ScheduledTaskName.12"] = "Query";
	this->KVMap["ScheduledTaskName.13"] = "";
	this->KPMap["ScheduledTaskName.13"] = "Query";
	this->KVMap["ScheduledTaskName.14"] = "";
	this->KPMap["ScheduledTaskName.14"] = "Query";
	this->KVMap["ScheduledTaskName.15"] = "";
	this->KPMap["ScheduledTaskName.15"] = "Query";
	this->KVMap["ScheduledTaskName.16"] = "";
	this->KPMap["ScheduledTaskName.16"] = "Query";
	this->KVMap["ScheduledTaskName.17"] = "";
	this->KPMap["ScheduledTaskName.17"] = "Query";
	this->KVMap["ScheduledTaskName.18"] = "";
	this->KPMap["ScheduledTaskName.18"] = "Query";
	this->KVMap["ScheduledTaskName.19"] = "";
	this->KPMap["ScheduledTaskName.19"] = "Query";
	this->KVMap["ScheduledTaskName.20"] = "";
	this->KPMap["ScheduledTaskName.20"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";

}
EssDetachInstancesRequest::EssDetachInstancesRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "ess";
	this->Action = "DetachInstances";
	this->Version = "2014-08-28";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["ScalingGroupId"] = "";
	this->KPMap["ScalingGroupId"] = "Query";
	this->KVMap["InstanceId.1"] = "";
	this->KPMap["InstanceId.1"] = "Query";
	this->KVMap["InstanceId.2"] = "";
	this->KPMap["InstanceId.2"] = "Query";
	this->KVMap["InstanceId.3"] = "";
	this->KPMap["InstanceId.3"] = "Query";
	this->KVMap["InstanceId.4"] = "";
	this->KPMap["InstanceId.4"] = "Query";
	this->KVMap["InstanceId.5"] = "";
	this->KPMap["InstanceId.5"] = "Query";
	this->KVMap["InstanceId.6"] = "";
	this->KPMap["InstanceId.6"] = "Query";
	this->KVMap["InstanceId.7"] = "";
	this->KPMap["InstanceId.7"] = "Query";
	this->KVMap["InstanceId.8"] = "";
	this->KPMap["InstanceId.8"] = "Query";
	this->KVMap["InstanceId.9"] = "";
	this->KPMap["InstanceId.9"] = "Query";
	this->KVMap["InstanceId.10"] = "";
	this->KPMap["InstanceId.10"] = "Query";
	this->KVMap["InstanceId.11"] = "";
	this->KPMap["InstanceId.11"] = "Query";
	this->KVMap["InstanceId.12"] = "";
	this->KPMap["InstanceId.12"] = "Query";
	this->KVMap["InstanceId.13"] = "";
	this->KPMap["InstanceId.13"] = "Query";
	this->KVMap["InstanceId.14"] = "";
	this->KPMap["InstanceId.14"] = "Query";
	this->KVMap["InstanceId.15"] = "";
	this->KPMap["InstanceId.15"] = "Query";
	this->KVMap["InstanceId.16"] = "";
	this->KPMap["InstanceId.16"] = "Query";
	this->KVMap["InstanceId.17"] = "";
	this->KPMap["InstanceId.17"] = "Query";
	this->KVMap["InstanceId.18"] = "";
	this->KPMap["InstanceId.18"] = "Query";
	this->KVMap["InstanceId.19"] = "";
	this->KPMap["InstanceId.19"] = "Query";
	this->KVMap["InstanceId.20"] = "";
	this->KPMap["InstanceId.20"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";

}
EssDisableScalingGroupRequest::EssDisableScalingGroupRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "ess";
	this->Action = "DisableScalingGroup";
	this->Version = "2014-08-28";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["ScalingGroupId"] = "";
	this->KPMap["ScalingGroupId"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";

}
EssEnableScalingGroupRequest::EssEnableScalingGroupRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "ess";
	this->Action = "EnableScalingGroup";
	this->Version = "2014-08-28";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["ScalingGroupId"] = "";
	this->KPMap["ScalingGroupId"] = "Query";
	this->KVMap["ActiveScalingConfigurationId"] = "";
	this->KPMap["ActiveScalingConfigurationId"] = "Query";
	this->KVMap["InstanceId.1"] = "";
	this->KPMap["InstanceId.1"] = "Query";
	this->KVMap["InstanceId.2"] = "";
	this->KPMap["InstanceId.2"] = "Query";
	this->KVMap["InstanceId.3"] = "";
	this->KPMap["InstanceId.3"] = "Query";
	this->KVMap["InstanceId.4"] = "";
	this->KPMap["InstanceId.4"] = "Query";
	this->KVMap["InstanceId.5"] = "";
	this->KPMap["InstanceId.5"] = "Query";
	this->KVMap["InstanceId.6"] = "";
	this->KPMap["InstanceId.6"] = "Query";
	this->KVMap["InstanceId.7"] = "";
	this->KPMap["InstanceId.7"] = "Query";
	this->KVMap["InstanceId.8"] = "";
	this->KPMap["InstanceId.8"] = "Query";
	this->KVMap["InstanceId.9"] = "";
	this->KPMap["InstanceId.9"] = "Query";
	this->KVMap["InstanceId.10"] = "";
	this->KPMap["InstanceId.10"] = "Query";
	this->KVMap["InstanceId.11"] = "";
	this->KPMap["InstanceId.11"] = "Query";
	this->KVMap["InstanceId.12"] = "";
	this->KPMap["InstanceId.12"] = "Query";
	this->KVMap["InstanceId.13"] = "";
	this->KPMap["InstanceId.13"] = "Query";
	this->KVMap["InstanceId.14"] = "";
	this->KPMap["InstanceId.14"] = "Query";
	this->KVMap["InstanceId.15"] = "";
	this->KPMap["InstanceId.15"] = "Query";
	this->KVMap["InstanceId.16"] = "";
	this->KPMap["InstanceId.16"] = "Query";
	this->KVMap["InstanceId.17"] = "";
	this->KPMap["InstanceId.17"] = "Query";
	this->KVMap["InstanceId.18"] = "";
	this->KPMap["InstanceId.18"] = "Query";
	this->KVMap["InstanceId.19"] = "";
	this->KPMap["InstanceId.19"] = "Query";
	this->KVMap["InstanceId.20"] = "";
	this->KPMap["InstanceId.20"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";

}
EssExecuteScalingRuleRequest::EssExecuteScalingRuleRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "ess";
	this->Action = "ExecuteScalingRule";
	this->Version = "2014-08-28";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["ScalingRuleAri"] = "";
	this->KPMap["ScalingRuleAri"] = "Query";
	this->KVMap["ClientToken"] = "";
	this->KPMap["ClientToken"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";

}
EssModifyScalingGroupRequest::EssModifyScalingGroupRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "ess";
	this->Action = "ModifyScalingGroup";
	this->Version = "2014-08-28";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["ScalingGroupId"] = "";
	this->KPMap["ScalingGroupId"] = "Query";
	this->KVMap["ScalingGroupName"] = "";
	this->KPMap["ScalingGroupName"] = "Query";
	this->KVMap["MinSize"] = "";
	this->KPMap["MinSize"] = "Query";
	this->KVMap["MaxSize"] = "";
	this->KPMap["MaxSize"] = "Query";
	this->KVMap["DefaultCooldown"] = "";
	this->KPMap["DefaultCooldown"] = "Query";
	this->KVMap["RemovalPolicy.1"] = "";
	this->KPMap["RemovalPolicy.1"] = "Query";
	this->KVMap["RemovalPolicy.2"] = "";
	this->KPMap["RemovalPolicy.2"] = "Query";
	this->KVMap["ActiveScalingConfigurationId"] = "";
	this->KPMap["ActiveScalingConfigurationId"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";

}
EssModifyScalingRuleRequest::EssModifyScalingRuleRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "ess";
	this->Action = "ModifyScalingRule";
	this->Version = "2014-08-28";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["ScalingRuleId"] = "";
	this->KPMap["ScalingRuleId"] = "Query";
	this->KVMap["ScalingRuleName"] = "";
	this->KPMap["ScalingRuleName"] = "Query";
	this->KVMap["Cooldown"] = "";
	this->KPMap["Cooldown"] = "Query";
	this->KVMap["AdjustmentType"] = "";
	this->KPMap["AdjustmentType"] = "Query";
	this->KVMap["AdjustmentValue"] = "";
	this->KPMap["AdjustmentValue"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";

}
EssModifyScheduledTaskRequest::EssModifyScheduledTaskRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "ess";
	this->Action = "ModifyScheduledTask";
	this->Version = "2014-08-28";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["ScheduledTaskId"] = "";
	this->KPMap["ScheduledTaskId"] = "Query";
	this->KVMap["ScheduledTaskName"] = "";
	this->KPMap["ScheduledTaskName"] = "Query";
	this->KVMap["Description"] = "";
	this->KPMap["Description"] = "Query";
	this->KVMap["ScheduledAction"] = "";
	this->KPMap["ScheduledAction"] = "Query";
	this->KVMap["RecurrenceEndTime"] = "";
	this->KPMap["RecurrenceEndTime"] = "Query";
	this->KVMap["LaunchTime"] = "";
	this->KPMap["LaunchTime"] = "Query";
	this->KVMap["RecurrenceType"] = "";
	this->KPMap["RecurrenceType"] = "Query";
	this->KVMap["RecurrenceValue"] = "";
	this->KPMap["RecurrenceValue"] = "Query";
	this->KVMap["TaskEnabled"] = "";
	this->KPMap["TaskEnabled"] = "Query";
	this->KVMap["LaunchExpirationTime"] = "";
	this->KPMap["LaunchExpirationTime"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";

}
EssRemoveInstancesRequest::EssRemoveInstancesRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "ess";
	this->Action = "RemoveInstances";
	this->Version = "2014-08-28";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["ScalingGroupId"] = "";
	this->KPMap["ScalingGroupId"] = "Query";
	this->KVMap["InstanceId.1"] = "";
	this->KPMap["InstanceId.1"] = "Query";
	this->KVMap["InstanceId.2"] = "";
	this->KPMap["InstanceId.2"] = "Query";
	this->KVMap["InstanceId.3"] = "";
	this->KPMap["InstanceId.3"] = "Query";
	this->KVMap["InstanceId.4"] = "";
	this->KPMap["InstanceId.4"] = "Query";
	this->KVMap["InstanceId.5"] = "";
	this->KPMap["InstanceId.5"] = "Query";
	this->KVMap["InstanceId.6"] = "";
	this->KPMap["InstanceId.6"] = "Query";
	this->KVMap["InstanceId.7"] = "";
	this->KPMap["InstanceId.7"] = "Query";
	this->KVMap["InstanceId.8"] = "";
	this->KPMap["InstanceId.8"] = "Query";
	this->KVMap["InstanceId.9"] = "";
	this->KPMap["InstanceId.9"] = "Query";
	this->KVMap["InstanceId.10"] = "";
	this->KPMap["InstanceId.10"] = "Query";
	this->KVMap["InstanceId.11"] = "";
	this->KPMap["InstanceId.11"] = "Query";
	this->KVMap["InstanceId.12"] = "";
	this->KPMap["InstanceId.12"] = "Query";
	this->KVMap["InstanceId.13"] = "";
	this->KPMap["InstanceId.13"] = "Query";
	this->KVMap["InstanceId.14"] = "";
	this->KPMap["InstanceId.14"] = "Query";
	this->KVMap["InstanceId.15"] = "";
	this->KPMap["InstanceId.15"] = "Query";
	this->KVMap["InstanceId.16"] = "";
	this->KPMap["InstanceId.16"] = "Query";
	this->KVMap["InstanceId.17"] = "";
	this->KPMap["InstanceId.17"] = "Query";
	this->KVMap["InstanceId.18"] = "";
	this->KPMap["InstanceId.18"] = "Query";
	this->KVMap["InstanceId.19"] = "";
	this->KPMap["InstanceId.19"] = "Query";
	this->KVMap["InstanceId.20"] = "";
	this->KPMap["InstanceId.20"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";

}

	
}