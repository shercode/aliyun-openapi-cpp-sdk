/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

#include <string>
#include <map>
#include "../aliyun-cpp-sdk-core/utils.h"
#include "../aliyun-cpp-sdk-core/DefaultProfile.h"
#include "../aliyun-cpp-sdk-core/BaseRequest.h"
#include "../aliyun-cpp-sdk-core/ApiBase.h"
#include "RdsRegionService.h"

using std::string;
using namespace AliYunCore;

namespace AliYunRdsRegion {

RdsRegionClient::RdsRegionClient(DefaultProfile &pf):Profile(pf){}
void RdsRegionClient::setProfile(DefaultProfile &pf){
	this->Profile = pf;
}
string RdsRegionClient::getResponse(BaseRequest &request){
	request.RegionId = this->Profile.RegionId;		
	request.AccessKeyId = this->Profile.AccessKeyId;
	request.AccessKeySecret = this->Profile.AccessKeySecret;
	return ApiBase::getResponse(request);
}
RdsRegionAddTagsToResourceRequest::RdsRegionAddTagsToResourceRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "rds-region";
	this->Action = "AddTagsToResource";
	this->Version = "2014-08-15";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["ClientToken"] = "";
	this->KPMap["ClientToken"] = "Query";
	this->KVMap["proxyId"] = "";
	this->KPMap["proxyId"] = "Query";
	this->KVMap["DBInstanceId"] = "";
	this->KPMap["DBInstanceId"] = "Query";
	this->KVMap["Tag.1.key"] = "";
	this->KPMap["Tag.1.key"] = "Query";
	this->KVMap["Tag.2.key"] = "";
	this->KPMap["Tag.2.key"] = "Query";
	this->KVMap["Tag.3.key"] = "";
	this->KPMap["Tag.3.key"] = "Query";
	this->KVMap["Tag.4.key"] = "";
	this->KPMap["Tag.4.key"] = "Query";
	this->KVMap["Tag.5.key"] = "";
	this->KPMap["Tag.5.key"] = "Query";
	this->KVMap["Tag.1.value"] = "";
	this->KPMap["Tag.1.value"] = "Query";
	this->KVMap["Tag.2.value"] = "";
	this->KPMap["Tag.2.value"] = "Query";
	this->KVMap["Tag.3.value"] = "";
	this->KPMap["Tag.3.value"] = "Query";
	this->KVMap["Tag.4.value"] = "";
	this->KPMap["Tag.4.value"] = "Query";
	this->KVMap["Tag.5.value"] = "";
	this->KPMap["Tag.5.value"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";

}
RdsRegionAllocateInstancePrivateConnectionRequest::RdsRegionAllocateInstancePrivateConnectionRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "rds-region";
	this->Action = "AllocateInstancePrivateConnection";
	this->Version = "2014-08-15";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["DBInstanceId"] = "";
	this->KPMap["DBInstanceId"] = "Query";
	this->KVMap["ConnectionStringPrefix"] = "";
	this->KPMap["ConnectionStringPrefix"] = "Query";
	this->KVMap["Port"] = "";
	this->KPMap["Port"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";

}
RdsRegionAllocateInstancePublicConnectionRequest::RdsRegionAllocateInstancePublicConnectionRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "rds-region";
	this->Action = "AllocateInstancePublicConnection";
	this->Version = "2014-08-15";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["DBInstanceId"] = "";
	this->KPMap["DBInstanceId"] = "Query";
	this->KVMap["ConnectionStringPrefix"] = "";
	this->KPMap["ConnectionStringPrefix"] = "Query";
	this->KVMap["Port"] = "";
	this->KPMap["Port"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";

}
RdsRegionBatchGrantAccountPrivilegeRequest::RdsRegionBatchGrantAccountPrivilegeRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "rds-region";
	this->Action = "BatchGrantAccountPrivilege";
	this->Version = "2014-08-15";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["DBInstanceId"] = "";
	this->KPMap["DBInstanceId"] = "Query";
	this->KVMap["AccountName"] = "";
	this->KPMap["AccountName"] = "Query";
	this->KVMap["DBName"] = "";
	this->KPMap["DBName"] = "Query";
	this->KVMap["AccountPrivilege"] = "";
	this->KPMap["AccountPrivilege"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";

}
RdsRegionBatchRevokeAccountPrivilegeRequest::RdsRegionBatchRevokeAccountPrivilegeRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "rds-region";
	this->Action = "BatchRevokeAccountPrivilege";
	this->Version = "2014-08-15";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["DBInstanceId"] = "";
	this->KPMap["DBInstanceId"] = "Query";
	this->KVMap["AccountName"] = "";
	this->KPMap["AccountName"] = "Query";
	this->KVMap["DBName"] = "";
	this->KPMap["DBName"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";

}
RdsRegionCancelImportRequest::RdsRegionCancelImportRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "rds-region";
	this->Action = "CancelImport";
	this->Version = "2014-08-15";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["DBInstanceId"] = "";
	this->KPMap["DBInstanceId"] = "Query";
	this->KVMap["ImportId"] = "";
	this->KPMap["ImportId"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";

}
RdsRegionCheckAccountNameAvailableRequest::RdsRegionCheckAccountNameAvailableRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "rds-region";
	this->Action = "CheckAccountNameAvailable";
	this->Version = "2014-08-15";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["ClientToken"] = "";
	this->KPMap["ClientToken"] = "Query";
	this->KVMap["DBInstanceId"] = "";
	this->KPMap["DBInstanceId"] = "Query";
	this->KVMap["AccountName"] = "";
	this->KPMap["AccountName"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";

}
RdsRegionCheckDBNameAvailableRequest::RdsRegionCheckDBNameAvailableRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "rds-region";
	this->Action = "CheckDBNameAvailable";
	this->Version = "2014-08-15";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["ClientToken"] = "";
	this->KPMap["ClientToken"] = "Query";
	this->KVMap["DBInstanceId"] = "";
	this->KPMap["DBInstanceId"] = "Query";
	this->KVMap["DBName"] = "";
	this->KPMap["DBName"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";

}
RdsRegionCreateAccountRequest::RdsRegionCreateAccountRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "rds-region";
	this->Action = "CreateAccount";
	this->Version = "2014-08-15";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["DBInstanceId"] = "";
	this->KPMap["DBInstanceId"] = "Query";
	this->KVMap["AccountName"] = "";
	this->KPMap["AccountName"] = "Query";
	this->KVMap["AccountPassword"] = "";
	this->KPMap["AccountPassword"] = "Query";
	this->KVMap["AccountDescription"] = "";
	this->KPMap["AccountDescription"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";

}
RdsRegionCreateAccountForInnerRequest::RdsRegionCreateAccountForInnerRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "rds-region";
	this->Action = "CreateAccountForInner";
	this->Version = "2014-08-15";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["DBInstanceId"] = "";
	this->KPMap["DBInstanceId"] = "Query";
	this->KVMap["AccountName"] = "";
	this->KPMap["AccountName"] = "Query";
	this->KVMap["AccountPassword"] = "";
	this->KPMap["AccountPassword"] = "Query";
	this->KVMap["DBName"] = "";
	this->KPMap["DBName"] = "Query";
	this->KVMap["AccountPrivilege"] = "";
	this->KPMap["AccountPrivilege"] = "Query";
	this->KVMap["AccountDescription"] = "";
	this->KPMap["AccountDescription"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";

}
RdsRegionCreateBackupRequest::RdsRegionCreateBackupRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "rds-region";
	this->Action = "CreateBackup";
	this->Version = "2014-08-15";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["DBInstanceId"] = "";
	this->KPMap["DBInstanceId"] = "Query";
	this->KVMap["DBName"] = "";
	this->KPMap["DBName"] = "Query";
	this->KVMap["BackupMethod"] = "";
	this->KPMap["BackupMethod"] = "Query";
	this->KVMap["BackupType"] = "";
	this->KPMap["BackupType"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";

}
RdsRegionCreateDatabaseRequest::RdsRegionCreateDatabaseRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "rds-region";
	this->Action = "CreateDatabase";
	this->Version = "2014-08-15";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["DBInstanceId"] = "";
	this->KPMap["DBInstanceId"] = "Query";
	this->KVMap["DBName"] = "";
	this->KPMap["DBName"] = "Query";
	this->KVMap["CharacterSetName"] = "";
	this->KPMap["CharacterSetName"] = "Query";
	this->KVMap["DBDescription"] = "";
	this->KPMap["DBDescription"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";

}
RdsRegionCreateDatabaseForInnerRequest::RdsRegionCreateDatabaseForInnerRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "rds-region";
	this->Action = "CreateDatabaseForInner";
	this->Version = "2014-08-15";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["DBInstanceId"] = "";
	this->KPMap["DBInstanceId"] = "Query";
	this->KVMap["DBName"] = "";
	this->KPMap["DBName"] = "Query";
	this->KVMap["CharacterSetName"] = "";
	this->KPMap["CharacterSetName"] = "Query";
	this->KVMap["DBDescription"] = "";
	this->KPMap["DBDescription"] = "Query";
	this->KVMap["AccountName"] = "";
	this->KPMap["AccountName"] = "Query";
	this->KVMap["AccountPrivilege"] = "";
	this->KPMap["AccountPrivilege"] = "Query";
	this->KVMap["AccountPassword"] = "";
	this->KPMap["AccountPassword"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";

}
RdsRegionCreateDBInstanceRequest::RdsRegionCreateDBInstanceRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "rds-region";
	this->Action = "CreateDBInstance";
	this->Version = "2014-08-15";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["ClientToken"] = "";
	this->KPMap["ClientToken"] = "Query";
	this->KVMap["Engine"] = "";
	this->KPMap["Engine"] = "Query";
	this->KVMap["EngineVersion"] = "";
	this->KPMap["EngineVersion"] = "Query";
	this->KVMap["DBInstanceClass"] = "";
	this->KPMap["DBInstanceClass"] = "Query";
	this->KVMap["DBInstanceStorage"] = "";
	this->KPMap["DBInstanceStorage"] = "Query";
	this->KVMap["SystemDBCharset"] = "";
	this->KPMap["SystemDBCharset"] = "Query";
	this->KVMap["DBInstanceNetType"] = "";
	this->KPMap["DBInstanceNetType"] = "Query";
	this->KVMap["DBInstanceDescription"] = "";
	this->KPMap["DBInstanceDescription"] = "Query";
	this->KVMap["SecurityIPList"] = "";
	this->KPMap["SecurityIPList"] = "Query";
	this->KVMap["PayType"] = "";
	this->KPMap["PayType"] = "Query";
	this->KVMap["ZoneId"] = "";
	this->KPMap["ZoneId"] = "Query";
	this->KVMap["InstanceNetworkType"] = "";
	this->KPMap["InstanceNetworkType"] = "Query";
	this->KVMap["ConnectionMode"] = "";
	this->KPMap["ConnectionMode"] = "Query";
	this->KVMap["VPCId"] = "";
	this->KPMap["VPCId"] = "Query";
	this->KVMap["VSwitchId"] = "";
	this->KPMap["VSwitchId"] = "Query";
	this->KVMap["PrivateIpAddress"] = "";
	this->KPMap["PrivateIpAddress"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";

}
RdsRegionCreateDBInstanceForChannelRequest::RdsRegionCreateDBInstanceForChannelRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "rds-region";
	this->Action = "CreateDBInstanceForChannel";
	this->Version = "2014-08-15";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["ClientToken"] = "";
	this->KPMap["ClientToken"] = "Query";
	this->KVMap["SystemDBCharset"] = "";
	this->KPMap["SystemDBCharset"] = "Query";
	this->KVMap["Engine"] = "";
	this->KPMap["Engine"] = "Query";
	this->KVMap["EngineVersion"] = "";
	this->KPMap["EngineVersion"] = "Query";
	this->KVMap["DBInstanceClass"] = "";
	this->KPMap["DBInstanceClass"] = "Query";
	this->KVMap["DBInstanceStorage"] = "";
	this->KPMap["DBInstanceStorage"] = "Query";
	this->KVMap["DBInstanceNetType"] = "";
	this->KPMap["DBInstanceNetType"] = "Query";
	this->KVMap["DBInstanceDescription"] = "";
	this->KPMap["DBInstanceDescription"] = "Query";
	this->KVMap["SecurityIPList"] = "";
	this->KPMap["SecurityIPList"] = "Query";
	this->KVMap["AccountName"] = "";
	this->KPMap["AccountName"] = "Query";
	this->KVMap["AccountPassword"] = "";
	this->KPMap["AccountPassword"] = "Query";
	this->KVMap["PayType"] = "";
	this->KPMap["PayType"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";

}
RdsRegionCreateDBInstanceforFirstPayRequest::RdsRegionCreateDBInstanceforFirstPayRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "rds-region";
	this->Action = "CreateDBInstanceforFirstPay";
	this->Version = "2014-08-15";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["uid"] = "";
	this->KPMap["uid"] = "Query";
	this->KVMap["bid"] = "";
	this->KPMap["bid"] = "Query";
	this->KVMap["uidLoginEmail"] = "";
	this->KPMap["uidLoginEmail"] = "Query";
	this->KVMap["bidLoginEmail"] = "";
	this->KPMap["bidLoginEmail"] = "Query";
	this->KVMap["Engine"] = "";
	this->KPMap["Engine"] = "Query";
	this->KVMap["EngineVersion"] = "";
	this->KPMap["EngineVersion"] = "Query";
	this->KVMap["DBInstanceClass"] = "";
	this->KPMap["DBInstanceClass"] = "Query";
	this->KVMap["DBInstanceStorage"] = "";
	this->KPMap["DBInstanceStorage"] = "Query";
	this->KVMap["DBInstanceNetType"] = "";
	this->KPMap["DBInstanceNetType"] = "Query";
	this->KVMap["CharacterSetName"] = "";
	this->KPMap["CharacterSetName"] = "Query";
	this->KVMap["DBInstanceRemarks"] = "";
	this->KPMap["DBInstanceRemarks"] = "Query";
	this->KVMap["ClientToken"] = "";
	this->KPMap["ClientToken"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";

}
RdsRegionCreatePostpaidDBInstanceRequest::RdsRegionCreatePostpaidDBInstanceRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "rds-region";
	this->Action = "CreatePostpaidDBInstance";
	this->Version = "2014-08-15";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["ClientToken"] = "";
	this->KPMap["ClientToken"] = "Query";
	this->KVMap["Engine"] = "";
	this->KPMap["Engine"] = "Query";
	this->KVMap["EngineVersion"] = "";
	this->KPMap["EngineVersion"] = "Query";
	this->KVMap["DBInstanceClass"] = "";
	this->KPMap["DBInstanceClass"] = "Query";
	this->KVMap["DBInstanceStorage"] = "";
	this->KPMap["DBInstanceStorage"] = "Query";
	this->KVMap["DBInstanceNetType"] = "";
	this->KPMap["DBInstanceNetType"] = "Query";
	this->KVMap["DBInstanceDescription"] = "";
	this->KPMap["DBInstanceDescription"] = "Query";
	this->KVMap["SecurityIPList"] = "";
	this->KPMap["SecurityIPList"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";

}
RdsRegionCreatePostpaidDBInstanceForChannelRequest::RdsRegionCreatePostpaidDBInstanceForChannelRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "rds-region";
	this->Action = "CreatePostpaidDBInstanceForChannel";
	this->Version = "2014-08-15";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["ClientToken"] = "";
	this->KPMap["ClientToken"] = "Query";
	this->KVMap["Engine"] = "";
	this->KPMap["Engine"] = "Query";
	this->KVMap["EngineVersion"] = "";
	this->KPMap["EngineVersion"] = "Query";
	this->KVMap["DBInstanceClass"] = "";
	this->KPMap["DBInstanceClass"] = "Query";
	this->KVMap["DBInstanceStorage"] = "";
	this->KPMap["DBInstanceStorage"] = "Query";
	this->KVMap["DBInstanceNetType"] = "";
	this->KPMap["DBInstanceNetType"] = "Query";
	this->KVMap["DBInstanceDescription"] = "";
	this->KPMap["DBInstanceDescription"] = "Query";
	this->KVMap["SecurityIPList"] = "";
	this->KPMap["SecurityIPList"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";

}
RdsRegionCreateReadOnlyDBInstanceRequest::RdsRegionCreateReadOnlyDBInstanceRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "rds-region";
	this->Action = "CreateReadOnlyDBInstance";
	this->Version = "2014-08-15";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["ClientToken"] = "";
	this->KPMap["ClientToken"] = "Query";
	this->KVMap["ZoneId"] = "";
	this->KPMap["ZoneId"] = "Query";
	this->KVMap["DBInstanceId"] = "";
	this->KPMap["DBInstanceId"] = "Query";
	this->KVMap["DBInstanceClass"] = "";
	this->KPMap["DBInstanceClass"] = "Query";
	this->KVMap["DBInstanceStorage"] = "";
	this->KPMap["DBInstanceStorage"] = "Query";
	this->KVMap["EngineVersion"] = "";
	this->KPMap["EngineVersion"] = "Query";
	this->KVMap["PayType"] = "";
	this->KPMap["PayType"] = "Query";
	this->KVMap["DBInstanceDescription"] = "";
	this->KPMap["DBInstanceDescription"] = "Query";
	this->KVMap["InstanceNetworkType"] = "";
	this->KPMap["InstanceNetworkType"] = "Query";
	this->KVMap["VPCId"] = "";
	this->KPMap["VPCId"] = "Query";
	this->KVMap["VSwitchId"] = "";
	this->KPMap["VSwitchId"] = "Query";
	this->KVMap["PrivateIpAddress"] = "";
	this->KPMap["PrivateIpAddress"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";

}
RdsRegionCreateTempDBInstanceRequest::RdsRegionCreateTempDBInstanceRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "rds-region";
	this->Action = "CreateTempDBInstance";
	this->Version = "2014-08-15";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["DBInstanceId"] = "";
	this->KPMap["DBInstanceId"] = "Query";
	this->KVMap["BackupId"] = "";
	this->KPMap["BackupId"] = "Query";
	this->KVMap["RestoreTime"] = "";
	this->KPMap["RestoreTime"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";

}
RdsRegionCreateUploadPathForSQLServerRequest::RdsRegionCreateUploadPathForSQLServerRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "rds-region";
	this->Action = "CreateUploadPathForSQLServer";
	this->Version = "2014-08-15";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["DBInstanceId"] = "";
	this->KPMap["DBInstanceId"] = "Query";
	this->KVMap["DBName"] = "";
	this->KPMap["DBName"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";

}
RdsRegionDeleteAccountRequest::RdsRegionDeleteAccountRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "rds-region";
	this->Action = "DeleteAccount";
	this->Version = "2014-08-15";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["DBInstanceId"] = "";
	this->KPMap["DBInstanceId"] = "Query";
	this->KVMap["AccountName"] = "";
	this->KPMap["AccountName"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";

}
RdsRegionDeleteDatabaseRequest::RdsRegionDeleteDatabaseRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "rds-region";
	this->Action = "DeleteDatabase";
	this->Version = "2014-08-15";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["DBInstanceId"] = "";
	this->KPMap["DBInstanceId"] = "Query";
	this->KVMap["DBName"] = "";
	this->KPMap["DBName"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";

}
RdsRegionDeleteDBInstanceRequest::RdsRegionDeleteDBInstanceRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "rds-region";
	this->Action = "DeleteDBInstance";
	this->Version = "2014-08-15";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["ClientToken"] = "";
	this->KPMap["ClientToken"] = "Query";
	this->KVMap["DBInstanceId"] = "";
	this->KPMap["DBInstanceId"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";

}
RdsRegionDescibeImportsFromDatabaseRequest::RdsRegionDescibeImportsFromDatabaseRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "rds-region";
	this->Action = "DescibeImportsFromDatabase";
	this->Version = "2014-08-15";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["ClientToken"] = "";
	this->KPMap["ClientToken"] = "Query";
	this->KVMap["DBInstanceId"] = "";
	this->KPMap["DBInstanceId"] = "Query";
	this->KVMap["Engine"] = "";
	this->KPMap["Engine"] = "Query";
	this->KVMap["ImportId"] = "";
	this->KPMap["ImportId"] = "Query";
	this->KVMap["StartTime"] = "";
	this->KPMap["StartTime"] = "Query";
	this->KVMap["EndTime"] = "";
	this->KPMap["EndTime"] = "Query";
	this->KVMap["PageSize"] = "";
	this->KPMap["PageSize"] = "Query";
	this->KVMap["PageNumber"] = "";
	this->KPMap["PageNumber"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";

}
RdsRegionDescribeAbnormalDBInstancesRequest::RdsRegionDescribeAbnormalDBInstancesRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "rds-region";
	this->Action = "DescribeAbnormalDBInstances";
	this->Version = "2014-08-15";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["ClientToken"] = "";
	this->KPMap["ClientToken"] = "Query";
	this->KVMap["proxyId"] = "";
	this->KPMap["proxyId"] = "Query";
	this->KVMap["DBInstanceId"] = "";
	this->KPMap["DBInstanceId"] = "Query";
	this->KVMap["PageSize"] = "";
	this->KPMap["PageSize"] = "Query";
	this->KVMap["PageNumber"] = "";
	this->KPMap["PageNumber"] = "Query";
	this->KVMap["Tag.1.key"] = "";
	this->KPMap["Tag.1.key"] = "Query";
	this->KVMap["Tag.2.key"] = "";
	this->KPMap["Tag.2.key"] = "Query";
	this->KVMap["Tag.3.key"] = "";
	this->KPMap["Tag.3.key"] = "Query";
	this->KVMap["Tag.4.key"] = "";
	this->KPMap["Tag.4.key"] = "Query";
	this->KVMap["Tag.5.key"] = "";
	this->KPMap["Tag.5.key"] = "Query";
	this->KVMap["Tag.1.value"] = "";
	this->KPMap["Tag.1.value"] = "Query";
	this->KVMap["Tag.2.value"] = "";
	this->KPMap["Tag.2.value"] = "Query";
	this->KVMap["Tag.3.value"] = "";
	this->KPMap["Tag.3.value"] = "Query";
	this->KVMap["Tag.4.value"] = "";
	this->KPMap["Tag.4.value"] = "Query";
	this->KVMap["Tag.5.value"] = "";
	this->KPMap["Tag.5.value"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";

}
RdsRegionDescribeAccountsRequest::RdsRegionDescribeAccountsRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "rds-region";
	this->Action = "DescribeAccounts";
	this->Version = "2014-08-15";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["DBInstanceId"] = "";
	this->KPMap["DBInstanceId"] = "Query";
	this->KVMap["AccountName"] = "";
	this->KPMap["AccountName"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";

}
RdsRegionDescribeBackupPolicyRequest::RdsRegionDescribeBackupPolicyRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "rds-region";
	this->Action = "DescribeBackupPolicy";
	this->Version = "2014-08-15";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["DBInstanceId"] = "";
	this->KPMap["DBInstanceId"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";

}
RdsRegionDescribeBackupsRequest::RdsRegionDescribeBackupsRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "rds-region";
	this->Action = "DescribeBackups";
	this->Version = "2014-08-15";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["DBInstanceId"] = "";
	this->KPMap["DBInstanceId"] = "Query";
	this->KVMap["BackupId"] = "";
	this->KPMap["BackupId"] = "Query";
	this->KVMap["BackupLocation"] = "";
	this->KPMap["BackupLocation"] = "Query";
	this->KVMap["BackupStatus"] = "";
	this->KPMap["BackupStatus"] = "Query";
	this->KVMap["BackupMode"] = "";
	this->KPMap["BackupMode"] = "Query";
	this->KVMap["StartTime"] = "";
	this->KPMap["StartTime"] = "Query";
	this->KVMap["EndTime"] = "";
	this->KPMap["EndTime"] = "Query";
	this->KVMap["PageSize"] = "";
	this->KPMap["PageSize"] = "Query";
	this->KVMap["PageNumber"] = "";
	this->KPMap["PageNumber"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";

}
RdsRegionDescribeBackupTasksRequest::RdsRegionDescribeBackupTasksRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "rds-region";
	this->Action = "DescribeBackupTasks";
	this->Version = "2014-08-15";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["ClientToken"] = "";
	this->KPMap["ClientToken"] = "Query";
	this->KVMap["Flag"] = "";
	this->KPMap["Flag"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";
	this->KVMap["DBInstanceId"] = "";
	this->KPMap["DBInstanceId"] = "Query";
	this->KVMap["BackupJobId"] = "";
	this->KPMap["BackupJobId"] = "Query";
	this->KVMap["BackupMode"] = "";
	this->KPMap["BackupMode"] = "Query";
	this->KVMap["BackupJobStatus"] = "";
	this->KPMap["BackupJobStatus"] = "Query";

}
RdsRegionDescribeBinlogFilesRequest::RdsRegionDescribeBinlogFilesRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "rds-region";
	this->Action = "DescribeBinlogFiles";
	this->Version = "2014-08-15";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["DBInstanceId"] = "";
	this->KPMap["DBInstanceId"] = "Query";
	this->KVMap["StartTime"] = "";
	this->KPMap["StartTime"] = "Query";
	this->KVMap["EndTime"] = "";
	this->KPMap["EndTime"] = "Query";
	this->KVMap["PageSize"] = "";
	this->KPMap["PageSize"] = "Query";
	this->KVMap["PageNumber"] = "";
	this->KPMap["PageNumber"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";

}
RdsRegionDescribeDatabasesRequest::RdsRegionDescribeDatabasesRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "rds-region";
	this->Action = "DescribeDatabases";
	this->Version = "2014-08-15";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["DBInstanceId"] = "";
	this->KPMap["DBInstanceId"] = "Query";
	this->KVMap["DBName"] = "";
	this->KPMap["DBName"] = "Query";
	this->KVMap["DBStatus"] = "";
	this->KPMap["DBStatus"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";

}
RdsRegionDescribeDBInstanceAttributeRequest::RdsRegionDescribeDBInstanceAttributeRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "rds-region";
	this->Action = "DescribeDBInstanceAttribute";
	this->Version = "2014-08-15";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["ClientToken"] = "";
	this->KPMap["ClientToken"] = "Query";
	this->KVMap["DBInstanceId"] = "";
	this->KPMap["DBInstanceId"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";

}
RdsRegionDescribeDBInstanceIPArrayListRequest::RdsRegionDescribeDBInstanceIPArrayListRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "rds-region";
	this->Action = "DescribeDBInstanceIPArrayList";
	this->Version = "2014-08-15";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["DBInstanceId"] = "";
	this->KPMap["DBInstanceId"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";

}
RdsRegionDescribeDBInstanceNetInfoRequest::RdsRegionDescribeDBInstanceNetInfoRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "rds-region";
	this->Action = "DescribeDBInstanceNetInfo";
	this->Version = "2014-08-15";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["ClientToken"] = "";
	this->KPMap["ClientToken"] = "Query";
	this->KVMap["DBInstanceId"] = "";
	this->KPMap["DBInstanceId"] = "Query";
	this->KVMap["Flag"] = "";
	this->KPMap["Flag"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";

}
RdsRegionDescribeDBInstanceNetInfoForChannelRequest::RdsRegionDescribeDBInstanceNetInfoForChannelRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "rds-region";
	this->Action = "DescribeDBInstanceNetInfoForChannel";
	this->Version = "2014-08-15";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["ClientToken"] = "";
	this->KPMap["ClientToken"] = "Query";
	this->KVMap["DBInstanceId"] = "";
	this->KPMap["DBInstanceId"] = "Query";
	this->KVMap["Flag"] = "";
	this->KPMap["Flag"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";

}
RdsRegionDescribeDBInstancePerformanceRequest::RdsRegionDescribeDBInstancePerformanceRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "rds-region";
	this->Action = "DescribeDBInstancePerformance";
	this->Version = "2014-08-15";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["ClientToken"] = "";
	this->KPMap["ClientToken"] = "Query";
	this->KVMap["DBInstanceId"] = "";
	this->KPMap["DBInstanceId"] = "Query";
	this->KVMap["Key"] = "";
	this->KPMap["Key"] = "Query";
	this->KVMap["StartTime"] = "";
	this->KPMap["StartTime"] = "Query";
	this->KVMap["EndTime"] = "";
	this->KPMap["EndTime"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";

}
RdsRegionDescribeDBInstancesRequest::RdsRegionDescribeDBInstancesRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "rds-region";
	this->Action = "DescribeDBInstances";
	this->Version = "2014-08-15";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["ClientToken"] = "";
	this->KPMap["ClientToken"] = "Query";
	this->KVMap["proxyId"] = "";
	this->KPMap["proxyId"] = "Query";
	this->KVMap["Engine"] = "";
	this->KPMap["Engine"] = "Query";
	this->KVMap["DBInstanceStatus"] = "";
	this->KPMap["DBInstanceStatus"] = "Query";
	this->KVMap["SearchKey"] = "";
	this->KPMap["SearchKey"] = "Query";
	this->KVMap["DBInstanceId"] = "";
	this->KPMap["DBInstanceId"] = "Query";
	this->KVMap["DBInstanceType"] = "";
	this->KPMap["DBInstanceType"] = "Query";
	this->KVMap["PageSize"] = "";
	this->KPMap["PageSize"] = "Query";
	this->KVMap["PageNumber"] = "";
	this->KPMap["PageNumber"] = "Query";
	this->KVMap["InstanceNetworkType"] = "";
	this->KPMap["InstanceNetworkType"] = "Query";
	this->KVMap["ConnectionMode"] = "";
	this->KPMap["ConnectionMode"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";

}
RdsRegionDescribeDBInstancesByExpireTimeRequest::RdsRegionDescribeDBInstancesByExpireTimeRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "rds-region";
	this->Action = "DescribeDBInstancesByExpireTime";
	this->Version = "2014-08-15";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["ClientToken"] = "";
	this->KPMap["ClientToken"] = "Query";
	this->KVMap["proxyId"] = "";
	this->KPMap["proxyId"] = "Query";
	this->KVMap["ExpirePeriod"] = "";
	this->KPMap["ExpirePeriod"] = "Query";
	this->KVMap["PageSize"] = "";
	this->KPMap["PageSize"] = "Query";
	this->KVMap["PageNumber"] = "";
	this->KPMap["PageNumber"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";

}
RdsRegionDescribeDBInstancesByPerformanceRequest::RdsRegionDescribeDBInstancesByPerformanceRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "rds-region";
	this->Action = "DescribeDBInstancesByPerformance";
	this->Version = "2014-08-15";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["ClientToken"] = "";
	this->KPMap["ClientToken"] = "Query";
	this->KVMap["proxyId"] = "";
	this->KPMap["proxyId"] = "Query";
	this->KVMap["DBInstanceId"] = "";
	this->KPMap["DBInstanceId"] = "Query";
	this->KVMap["PageSize"] = "";
	this->KPMap["PageSize"] = "Query";
	this->KVMap["PageNumber"] = "";
	this->KPMap["PageNumber"] = "Query";
	this->KVMap["SortMethod"] = "";
	this->KPMap["SortMethod"] = "Query";
	this->KVMap["SortKey"] = "";
	this->KPMap["SortKey"] = "Query";
	this->KVMap["Tag.1.key"] = "";
	this->KPMap["Tag.1.key"] = "Query";
	this->KVMap["Tag.2.key"] = "";
	this->KPMap["Tag.2.key"] = "Query";
	this->KVMap["Tag.3.key"] = "";
	this->KPMap["Tag.3.key"] = "Query";
	this->KVMap["Tag.4.key"] = "";
	this->KPMap["Tag.4.key"] = "Query";
	this->KVMap["Tag.5.key"] = "";
	this->KPMap["Tag.5.key"] = "Query";
	this->KVMap["Tag.1.value"] = "";
	this->KPMap["Tag.1.value"] = "Query";
	this->KVMap["Tag.2.value"] = "";
	this->KPMap["Tag.2.value"] = "Query";
	this->KVMap["Tag.3.value"] = "";
	this->KPMap["Tag.3.value"] = "Query";
	this->KVMap["Tag.4.value"] = "";
	this->KPMap["Tag.4.value"] = "Query";
	this->KVMap["Tag.5.value"] = "";
	this->KPMap["Tag.5.value"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";

}
RdsRegionDescribeErrorLogsRequest::RdsRegionDescribeErrorLogsRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "rds-region";
	this->Action = "DescribeErrorLogs";
	this->Version = "2014-08-15";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["DBInstanceId"] = "";
	this->KPMap["DBInstanceId"] = "Query";
	this->KVMap["StartTime"] = "";
	this->KPMap["StartTime"] = "Query";
	this->KVMap["EndTime"] = "";
	this->KPMap["EndTime"] = "Query";
	this->KVMap["PageSize"] = "";
	this->KPMap["PageSize"] = "Query";
	this->KVMap["PageNumber"] = "";
	this->KPMap["PageNumber"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";

}
RdsRegionDescribeFilesForSQLServerRequest::RdsRegionDescribeFilesForSQLServerRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "rds-region";
	this->Action = "DescribeFilesForSQLServer";
	this->Version = "2014-08-15";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["DBInstanceId"] = "";
	this->KPMap["DBInstanceId"] = "Query";
	this->KVMap["StartTime"] = "";
	this->KPMap["StartTime"] = "Query";
	this->KVMap["EndTime"] = "";
	this->KPMap["EndTime"] = "Query";
	this->KVMap["PageSize"] = "";
	this->KPMap["PageSize"] = "Query";
	this->KVMap["PageNumber"] = "";
	this->KPMap["PageNumber"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";

}
RdsRegionDescribeImportsForSQLServerRequest::RdsRegionDescribeImportsForSQLServerRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "rds-region";
	this->Action = "DescribeImportsForSQLServer";
	this->Version = "2014-08-15";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["DBInstanceId"] = "";
	this->KPMap["DBInstanceId"] = "Query";
	this->KVMap["ImportId"] = "";
	this->KPMap["ImportId"] = "Query";
	this->KVMap["StartTime"] = "";
	this->KPMap["StartTime"] = "Query";
	this->KVMap["EndTime"] = "";
	this->KPMap["EndTime"] = "Query";
	this->KVMap["PageSize"] = "";
	this->KPMap["PageSize"] = "Query";
	this->KVMap["PageNumber"] = "";
	this->KPMap["PageNumber"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";

}
RdsRegionDescribeModifyParameterLogRequest::RdsRegionDescribeModifyParameterLogRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "rds-region";
	this->Action = "DescribeModifyParameterLog";
	this->Version = "2014-08-15";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["DBInstanceId"] = "";
	this->KPMap["DBInstanceId"] = "Query";
	this->KVMap["StartTime"] = "";
	this->KPMap["StartTime"] = "Query";
	this->KVMap["EndTime"] = "";
	this->KPMap["EndTime"] = "Query";
	this->KVMap["PageSize"] = "";
	this->KPMap["PageSize"] = "Query";
	this->KVMap["PageNumber"] = "";
	this->KPMap["PageNumber"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";

}
RdsRegionDescribeOperationLogsRequest::RdsRegionDescribeOperationLogsRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "rds-region";
	this->Action = "DescribeOperationLogs";
	this->Version = "2014-08-15";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["DBInstanceId"] = "";
	this->KPMap["DBInstanceId"] = "Query";
	this->KVMap["SearchKey"] = "";
	this->KPMap["SearchKey"] = "Query";
	this->KVMap["DBInstanceUseType"] = "";
	this->KPMap["DBInstanceUseType"] = "Query";
	this->KVMap["StartTime"] = "";
	this->KPMap["StartTime"] = "Query";
	this->KVMap["EndTime"] = "";
	this->KPMap["EndTime"] = "Query";
	this->KVMap["PageSize"] = "";
	this->KPMap["PageSize"] = "Query";
	this->KVMap["PageNumber"] = "";
	this->KPMap["PageNumber"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";

}
RdsRegionDescribeOptimizeAdviceByDBARequest::RdsRegionDescribeOptimizeAdviceByDBARequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "rds-region";
	this->Action = "DescribeOptimizeAdviceByDBA";
	this->Version = "2014-08-15";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["DBInstanceId"] = "";
	this->KPMap["DBInstanceId"] = "Query";
	this->KVMap["PageSize"] = "";
	this->KPMap["PageSize"] = "Query";
	this->KVMap["PageNumber"] = "";
	this->KPMap["PageNumber"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";

}
RdsRegionDescribeOptimizeAdviceOnBigTableRequest::RdsRegionDescribeOptimizeAdviceOnBigTableRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "rds-region";
	this->Action = "DescribeOptimizeAdviceOnBigTable";
	this->Version = "2014-08-15";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["DBInstanceId"] = "";
	this->KPMap["DBInstanceId"] = "Query";
	this->KVMap["PageSize"] = "";
	this->KPMap["PageSize"] = "Query";
	this->KVMap["PageNumber"] = "";
	this->KPMap["PageNumber"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";

}
RdsRegionDescribeOptimizeAdviceOnExcessIndexRequest::RdsRegionDescribeOptimizeAdviceOnExcessIndexRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "rds-region";
	this->Action = "DescribeOptimizeAdviceOnExcessIndex";
	this->Version = "2014-08-15";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["DBInstanceId"] = "";
	this->KPMap["DBInstanceId"] = "Query";
	this->KVMap["PageSize"] = "";
	this->KPMap["PageSize"] = "Query";
	this->KVMap["PageNumber"] = "";
	this->KPMap["PageNumber"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";

}
RdsRegionDescribeOptimizeAdviceOnMissIndexRequest::RdsRegionDescribeOptimizeAdviceOnMissIndexRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "rds-region";
	this->Action = "DescribeOptimizeAdviceOnMissIndex";
	this->Version = "2014-08-15";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["DBInstanceId"] = "";
	this->KPMap["DBInstanceId"] = "Query";
	this->KVMap["PageSize"] = "";
	this->KPMap["PageSize"] = "Query";
	this->KVMap["PageNumber"] = "";
	this->KPMap["PageNumber"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";

}
RdsRegionDescribeOptimizeAdviceOnMissPKRequest::RdsRegionDescribeOptimizeAdviceOnMissPKRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "rds-region";
	this->Action = "DescribeOptimizeAdviceOnMissPK";
	this->Version = "2014-08-15";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["DBInstanceId"] = "";
	this->KPMap["DBInstanceId"] = "Query";
	this->KVMap["PageSize"] = "";
	this->KPMap["PageSize"] = "Query";
	this->KVMap["PageNumber"] = "";
	this->KPMap["PageNumber"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";

}
RdsRegionDescribeOptimizeAdviceOnStorageRequest::RdsRegionDescribeOptimizeAdviceOnStorageRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "rds-region";
	this->Action = "DescribeOptimizeAdviceOnStorage";
	this->Version = "2014-08-15";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["DBInstanceId"] = "";
	this->KPMap["DBInstanceId"] = "Query";
	this->KVMap["PageSize"] = "";
	this->KPMap["PageSize"] = "Query";
	this->KVMap["PageNumber"] = "";
	this->KPMap["PageNumber"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";

}
RdsRegionDescribeParametersRequest::RdsRegionDescribeParametersRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "rds-region";
	this->Action = "DescribeParameters";
	this->Version = "2014-08-15";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["ClientToken"] = "";
	this->KPMap["ClientToken"] = "Query";
	this->KVMap["DBInstanceId"] = "";
	this->KPMap["DBInstanceId"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";

}
RdsRegionDescribeParameterTemplatesRequest::RdsRegionDescribeParameterTemplatesRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "rds-region";
	this->Action = "DescribeParameterTemplates";
	this->Version = "2014-08-15";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["ClientToken"] = "";
	this->KPMap["ClientToken"] = "Query";
	this->KVMap["Engine"] = "";
	this->KPMap["Engine"] = "Query";
	this->KVMap["EngineVersion"] = "";
	this->KPMap["EngineVersion"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";

}
RdsRegionDescribePreCheckResultsRequest::RdsRegionDescribePreCheckResultsRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "rds-region";
	this->Action = "DescribePreCheckResults";
	this->Version = "2014-08-15";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["ClientToken"] = "";
	this->KPMap["ClientToken"] = "Query";
	this->KVMap["DBInstanceId"] = "";
	this->KPMap["DBInstanceId"] = "Query";
	this->KVMap["PreCheckId"] = "";
	this->KPMap["PreCheckId"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";

}
RdsRegionDescribeRealtimeDiagnosesRequest::RdsRegionDescribeRealtimeDiagnosesRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "rds-region";
	this->Action = "DescribeRealtimeDiagnoses";
	this->Version = "2014-08-15";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["DBInstanceId"] = "";
	this->KPMap["DBInstanceId"] = "Query";
	this->KVMap["StartTime"] = "";
	this->KPMap["StartTime"] = "Query";
	this->KVMap["EndTime"] = "";
	this->KPMap["EndTime"] = "Query";
	this->KVMap["PageSize"] = "";
	this->KPMap["PageSize"] = "Query";
	this->KVMap["PageNumber"] = "";
	this->KPMap["PageNumber"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";

}
RdsRegionDescribeRegionsRequest::RdsRegionDescribeRegionsRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "rds-region";
	this->Action = "DescribeRegions";
	this->Version = "2014-08-15";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["ClientToken"] = "";
	this->KPMap["ClientToken"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";

}
RdsRegionDescribeResourceUsageRequest::RdsRegionDescribeResourceUsageRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "rds-region";
	this->Action = "DescribeResourceUsage";
	this->Version = "2014-08-15";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["ClientToken"] = "";
	this->KPMap["ClientToken"] = "Query";
	this->KVMap["DBInstanceId"] = "";
	this->KPMap["DBInstanceId"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";

}
RdsRegionDescribeSlowLogRecordsRequest::RdsRegionDescribeSlowLogRecordsRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "rds-region";
	this->Action = "DescribeSlowLogRecords";
	this->Version = "2014-08-15";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["DBInstanceId"] = "";
	this->KPMap["DBInstanceId"] = "Query";
	this->KVMap["SQLId"] = "";
	this->KPMap["SQLId"] = "Query";
	this->KVMap["StartTime"] = "";
	this->KPMap["StartTime"] = "Query";
	this->KVMap["EndTime"] = "";
	this->KPMap["EndTime"] = "Query";
	this->KVMap["DBName"] = "";
	this->KPMap["DBName"] = "Query";
	this->KVMap["PageSize"] = "";
	this->KPMap["PageSize"] = "Query";
	this->KVMap["PageNumber"] = "";
	this->KPMap["PageNumber"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";

}
RdsRegionDescribeSlowLogsRequest::RdsRegionDescribeSlowLogsRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "rds-region";
	this->Action = "DescribeSlowLogs";
	this->Version = "2014-08-15";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["DBInstanceId"] = "";
	this->KPMap["DBInstanceId"] = "Query";
	this->KVMap["StartTime"] = "";
	this->KPMap["StartTime"] = "Query";
	this->KVMap["EndTime"] = "";
	this->KPMap["EndTime"] = "Query";
	this->KVMap["DBName"] = "";
	this->KPMap["DBName"] = "Query";
	this->KVMap["SortKey"] = "";
	this->KPMap["SortKey"] = "Query";
	this->KVMap["PageSize"] = "";
	this->KPMap["PageSize"] = "Query";
	this->KVMap["PageNumber"] = "";
	this->KPMap["PageNumber"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";

}
RdsRegionDescribeSQLInjectionInfosRequest::RdsRegionDescribeSQLInjectionInfosRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "rds-region";
	this->Action = "DescribeSQLInjectionInfos";
	this->Version = "2014-08-15";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["DBInstanceId"] = "";
	this->KPMap["DBInstanceId"] = "Query";
	this->KVMap["StartTime"] = "";
	this->KPMap["StartTime"] = "Query";
	this->KVMap["EndTime"] = "";
	this->KPMap["EndTime"] = "Query";
	this->KVMap["PageSize"] = "";
	this->KPMap["PageSize"] = "Query";
	this->KVMap["PageNumber"] = "";
	this->KPMap["PageNumber"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";

}
RdsRegionDescribeSQLLogRecordsRequest::RdsRegionDescribeSQLLogRecordsRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "rds-region";
	this->Action = "DescribeSQLLogRecords";
	this->Version = "2014-08-15";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["ClientToken"] = "";
	this->KPMap["ClientToken"] = "Query";
	this->KVMap["DBInstanceId"] = "";
	this->KPMap["DBInstanceId"] = "Query";
	this->KVMap["SQLId"] = "";
	this->KPMap["SQLId"] = "Query";
	this->KVMap["QueryKeywords"] = "";
	this->KPMap["QueryKeywords"] = "Query";
	this->KVMap["StartTime"] = "";
	this->KPMap["StartTime"] = "Query";
	this->KVMap["EndTime"] = "";
	this->KPMap["EndTime"] = "Query";
	this->KVMap["PageSize"] = "";
	this->KPMap["PageSize"] = "Query";
	this->KVMap["PageNumber"] = "";
	this->KPMap["PageNumber"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";

}
RdsRegionDescribeSQLLogReportsRequest::RdsRegionDescribeSQLLogReportsRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "rds-region";
	this->Action = "DescribeSQLLogReports";
	this->Version = "2014-08-15";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["ClientToken"] = "";
	this->KPMap["ClientToken"] = "Query";
	this->KVMap["DBInstanceId"] = "";
	this->KPMap["DBInstanceId"] = "Query";
	this->KVMap["StartTime"] = "";
	this->KPMap["StartTime"] = "Query";
	this->KVMap["EndTime"] = "";
	this->KPMap["EndTime"] = "Query";
	this->KVMap["ReportType"] = "";
	this->KPMap["ReportType"] = "Query";
	this->KVMap["PageSize"] = "";
	this->KPMap["PageSize"] = "Query";
	this->KVMap["PageNumber"] = "";
	this->KPMap["PageNumber"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";

}
RdsRegionDescribeTasksRequest::RdsRegionDescribeTasksRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "rds-region";
	this->Action = "DescribeTasks";
	this->Version = "2014-08-15";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["DBInstanceId"] = "";
	this->KPMap["DBInstanceId"] = "Query";
	this->KVMap["StartTime"] = "";
	this->KPMap["StartTime"] = "Query";
	this->KVMap["EndTime"] = "";
	this->KPMap["EndTime"] = "Query";
	this->KVMap["PageSize"] = "";
	this->KPMap["PageSize"] = "Query";
	this->KVMap["PageNumber"] = "";
	this->KPMap["PageNumber"] = "Query";
	this->KVMap["Status"] = "";
	this->KPMap["Status"] = "Query";
	this->KVMap["TaskAction"] = "";
	this->KPMap["TaskAction"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";

}
RdsRegionDescribeVpcZoneNosRequest::RdsRegionDescribeVpcZoneNosRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "rds-region";
	this->Action = "DescribeVpcZoneNos";
	this->Version = "2014-08-15";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["ClientToken"] = "";
	this->KPMap["ClientToken"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";
	this->KVMap["Region"] = "";
	this->KPMap["Region"] = "Query";
	this->KVMap["ZoneId"] = "";
	this->KPMap["ZoneId"] = "Query";

}
RdsRegionExtractBackupFromOASRequest::RdsRegionExtractBackupFromOASRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "rds-region";
	this->Action = "ExtractBackupFromOAS";
	this->Version = "2014-08-15";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["ClientToken"] = "";
	this->KPMap["ClientToken"] = "Query";
	this->KVMap["DBInstanceId"] = "";
	this->KPMap["DBInstanceId"] = "Query";
	this->KVMap["BackupId"] = "";
	this->KPMap["BackupId"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";

}
RdsRegionGrantAccountPrivilegeRequest::RdsRegionGrantAccountPrivilegeRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "rds-region";
	this->Action = "GrantAccountPrivilege";
	this->Version = "2014-08-15";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["DBInstanceId"] = "";
	this->KPMap["DBInstanceId"] = "Query";
	this->KVMap["AccountName"] = "";
	this->KPMap["AccountName"] = "Query";
	this->KVMap["DBName"] = "";
	this->KPMap["DBName"] = "Query";
	this->KVMap["AccountPrivilege"] = "";
	this->KPMap["AccountPrivilege"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";

}
RdsRegionImportDatabaseBetweenInstancesRequest::RdsRegionImportDatabaseBetweenInstancesRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "rds-region";
	this->Action = "ImportDatabaseBetweenInstances";
	this->Version = "2014-08-15";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["DBInstanceId"] = "";
	this->KPMap["DBInstanceId"] = "Query";
	this->KVMap["SourceDBInstanceId"] = "";
	this->KPMap["SourceDBInstanceId"] = "Query";
	this->KVMap["DBInfo"] = "";
	this->KPMap["DBInfo"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";

}
RdsRegionImportDataForSQLServerRequest::RdsRegionImportDataForSQLServerRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "rds-region";
	this->Action = "ImportDataForSQLServer";
	this->Version = "2014-08-15";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["DBInstanceId"] = "";
	this->KPMap["DBInstanceId"] = "Query";
	this->KVMap["FileName"] = "";
	this->KPMap["FileName"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";

}
RdsRegionImportDataFromDatabaseRequest::RdsRegionImportDataFromDatabaseRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "rds-region";
	this->Action = "ImportDataFromDatabase";
	this->Version = "2014-08-15";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["DBInstanceId"] = "";
	this->KPMap["DBInstanceId"] = "Query";
	this->KVMap["SourceDatabaseIp"] = "";
	this->KPMap["SourceDatabaseIp"] = "Query";
	this->KVMap["SourceDatabasePort"] = "";
	this->KPMap["SourceDatabasePort"] = "Query";
	this->KVMap["SourceDatabaseUserName"] = "";
	this->KPMap["SourceDatabaseUserName"] = "Query";
	this->KVMap["SourceDatabasePassword"] = "";
	this->KPMap["SourceDatabasePassword"] = "Query";
	this->KVMap["ImportDataType"] = "";
	this->KPMap["ImportDataType"] = "Query";
	this->KVMap["IsLockTable"] = "";
	this->KPMap["IsLockTable"] = "Query";
	this->KVMap["SourceDatabaseDBNames"] = "";
	this->KPMap["SourceDatabaseDBNames"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";

}
RdsRegionLockDBInstanceRequest::RdsRegionLockDBInstanceRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "rds-region";
	this->Action = "LockDBInstance";
	this->Version = "2014-08-15";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["DBInstanceId"] = "";
	this->KPMap["DBInstanceId"] = "Query";
	this->KVMap["LockReason"] = "";
	this->KPMap["LockReason"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";

}
RdsRegionMigrateToOtherZoneRequest::RdsRegionMigrateToOtherZoneRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "rds-region";
	this->Action = "MigrateToOtherZone";
	this->Version = "2014-08-15";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["DBInstanceId"] = "";
	this->KPMap["DBInstanceId"] = "Query";
	this->KVMap["ZoneId"] = "";
	this->KPMap["ZoneId"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";

}
RdsRegionModifyAccountDescriptionRequest::RdsRegionModifyAccountDescriptionRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "rds-region";
	this->Action = "ModifyAccountDescription";
	this->Version = "2014-08-15";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["DBInstanceId"] = "";
	this->KPMap["DBInstanceId"] = "Query";
	this->KVMap["AccountName"] = "";
	this->KPMap["AccountName"] = "Query";
	this->KVMap["AccountDescription"] = "";
	this->KPMap["AccountDescription"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";

}
RdsRegionModifyBackupPolicyRequest::RdsRegionModifyBackupPolicyRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "rds-region";
	this->Action = "ModifyBackupPolicy";
	this->Version = "2014-08-15";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["DBInstanceId"] = "";
	this->KPMap["DBInstanceId"] = "Query";
	this->KVMap["PreferredBackupTime"] = "";
	this->KPMap["PreferredBackupTime"] = "Query";
	this->KVMap["PreferredBackupPeriod"] = "";
	this->KPMap["PreferredBackupPeriod"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";

}
RdsRegionModifyDBDescriptionRequest::RdsRegionModifyDBDescriptionRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "rds-region";
	this->Action = "ModifyDBDescription";
	this->Version = "2014-08-15";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["DBInstanceId"] = "";
	this->KPMap["DBInstanceId"] = "Query";
	this->KVMap["DBName"] = "";
	this->KPMap["DBName"] = "Query";
	this->KVMap["DBDescription"] = "";
	this->KPMap["DBDescription"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";

}
RdsRegionModifyDBInstanceConnectionModeRequest::RdsRegionModifyDBInstanceConnectionModeRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "rds-region";
	this->Action = "ModifyDBInstanceConnectionMode";
	this->Version = "2014-08-15";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["DBInstanceId"] = "";
	this->KPMap["DBInstanceId"] = "Query";
	this->KVMap["ConnectionMode"] = "";
	this->KPMap["ConnectionMode"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";

}
RdsRegionModifyDBInstanceConnectionStringRequest::RdsRegionModifyDBInstanceConnectionStringRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "rds-region";
	this->Action = "ModifyDBInstanceConnectionString";
	this->Version = "2014-08-15";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["DBInstanceId"] = "";
	this->KPMap["DBInstanceId"] = "Query";
	this->KVMap["CurrentConnectionString"] = "";
	this->KPMap["CurrentConnectionString"] = "Query";
	this->KVMap["ConnectionStringPrefix"] = "";
	this->KPMap["ConnectionStringPrefix"] = "Query";
	this->KVMap["Port"] = "";
	this->KPMap["Port"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";

}
RdsRegionModifyDBInstanceDescriptionRequest::RdsRegionModifyDBInstanceDescriptionRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "rds-region";
	this->Action = "ModifyDBInstanceDescription";
	this->Version = "2014-08-15";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["ClientToken"] = "";
	this->KPMap["ClientToken"] = "Query";
	this->KVMap["DBInstanceId"] = "";
	this->KPMap["DBInstanceId"] = "Query";
	this->KVMap["DBInstanceDescription"] = "";
	this->KPMap["DBInstanceDescription"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";

}
RdsRegionModifyDBInstanceMaintainTimeRequest::RdsRegionModifyDBInstanceMaintainTimeRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "rds-region";
	this->Action = "ModifyDBInstanceMaintainTime";
	this->Version = "2014-08-15";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["ClientToken"] = "";
	this->KPMap["ClientToken"] = "Query";
	this->KVMap["DBInstanceId"] = "";
	this->KPMap["DBInstanceId"] = "Query";
	this->KVMap["MaintainTime"] = "";
	this->KPMap["MaintainTime"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";

}
RdsRegionModifyDBInstanceNetworkTypeRequest::RdsRegionModifyDBInstanceNetworkTypeRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "rds-region";
	this->Action = "ModifyDBInstanceNetworkType";
	this->Version = "2014-08-15";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["DBInstanceId"] = "";
	this->KPMap["DBInstanceId"] = "Query";
	this->KVMap["InstanceNetworkType"] = "";
	this->KPMap["InstanceNetworkType"] = "Query";
	this->KVMap["VPCId"] = "";
	this->KPMap["VPCId"] = "Query";
	this->KVMap["VSwitchId"] = "";
	this->KPMap["VSwitchId"] = "Query";
	this->KVMap["PrivateIpAddress"] = "";
	this->KPMap["PrivateIpAddress"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";

}
RdsRegionModifyDBInstanceSpecRequest::RdsRegionModifyDBInstanceSpecRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "rds-region";
	this->Action = "ModifyDBInstanceSpec";
	this->Version = "2014-08-15";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["ClientToken"] = "";
	this->KPMap["ClientToken"] = "Query";
	this->KVMap["DBInstanceId"] = "";
	this->KPMap["DBInstanceId"] = "Query";
	this->KVMap["DBInstanceClass"] = "";
	this->KPMap["DBInstanceClass"] = "Query";
	this->KVMap["DBInstanceStorage"] = "";
	this->KPMap["DBInstanceStorage"] = "Query";
	this->KVMap["PayType"] = "";
	this->KPMap["PayType"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";

}
RdsRegionModifyParameterRequest::RdsRegionModifyParameterRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "rds-region";
	this->Action = "ModifyParameter";
	this->Version = "2014-08-15";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["ClientToken"] = "";
	this->KPMap["ClientToken"] = "Query";
	this->KVMap["DBInstanceId"] = "";
	this->KPMap["DBInstanceId"] = "Query";
	this->KVMap["Parameters"] = "";
	this->KPMap["Parameters"] = "Query";
	this->KVMap["Forcerestart"] = "";
	this->KPMap["Forcerestart"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";

}
RdsRegionModifyPostpaidDBInstanceSpecRequest::RdsRegionModifyPostpaidDBInstanceSpecRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "rds-region";
	this->Action = "ModifyPostpaidDBInstanceSpec";
	this->Version = "2014-08-15";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["ClientToken"] = "";
	this->KPMap["ClientToken"] = "Query";
	this->KVMap["DBInstanceId"] = "";
	this->KPMap["DBInstanceId"] = "Query";
	this->KVMap["DBInstanceClass"] = "";
	this->KPMap["DBInstanceClass"] = "Query";
	this->KVMap["DBInstanceStorage"] = "";
	this->KPMap["DBInstanceStorage"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";

}
RdsRegionModifySecurityIpsRequest::RdsRegionModifySecurityIpsRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "rds-region";
	this->Action = "ModifySecurityIps";
	this->Version = "2014-08-15";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["ClientToken"] = "";
	this->KPMap["ClientToken"] = "Query";
	this->KVMap["DBInstanceId"] = "";
	this->KPMap["DBInstanceId"] = "Query";
	this->KVMap["SecurityIps"] = "";
	this->KPMap["SecurityIps"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";

}
RdsRegionPreCheckBeforeImportDataRequest::RdsRegionPreCheckBeforeImportDataRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "rds-region";
	this->Action = "PreCheckBeforeImportData";
	this->Version = "2014-08-15";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["ClientToken"] = "";
	this->KPMap["ClientToken"] = "Query";
	this->KVMap["DBInstanceId"] = "";
	this->KPMap["DBInstanceId"] = "Query";
	this->KVMap["SourceDatabaseIp"] = "";
	this->KPMap["SourceDatabaseIp"] = "Query";
	this->KVMap["SourceDatabasePort"] = "";
	this->KPMap["SourceDatabasePort"] = "Query";
	this->KVMap["SourceDatabaseUserName"] = "";
	this->KPMap["SourceDatabaseUserName"] = "Query";
	this->KVMap["SourceDatabasePassword"] = "";
	this->KPMap["SourceDatabasePassword"] = "Query";
	this->KVMap["ImportDataType"] = "";
	this->KPMap["ImportDataType"] = "Query";
	this->KVMap["SourceDatabaseDBNames"] = "";
	this->KPMap["SourceDatabaseDBNames"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";

}
RdsRegionPurgeDBInstanceLogRequest::RdsRegionPurgeDBInstanceLogRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "rds-region";
	this->Action = "PurgeDBInstanceLog";
	this->Version = "2014-08-15";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["ClientToken"] = "";
	this->KPMap["ClientToken"] = "Query";
	this->KVMap["DBInstanceId"] = "";
	this->KPMap["DBInstanceId"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";

}
RdsRegionReleaseInstancePublicConnectionRequest::RdsRegionReleaseInstancePublicConnectionRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "rds-region";
	this->Action = "ReleaseInstancePublicConnection";
	this->Version = "2014-08-15";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["DBInstanceId"] = "";
	this->KPMap["DBInstanceId"] = "Query";
	this->KVMap["CurrentConnectionString"] = "";
	this->KPMap["CurrentConnectionString"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";

}
RdsRegionRemoveTagsFromResourceRequest::RdsRegionRemoveTagsFromResourceRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "rds-region";
	this->Action = "RemoveTagsFromResource";
	this->Version = "2014-08-15";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["ClientToken"] = "";
	this->KPMap["ClientToken"] = "Query";
	this->KVMap["proxyId"] = "";
	this->KPMap["proxyId"] = "Query";
	this->KVMap["DBInstanceId"] = "";
	this->KPMap["DBInstanceId"] = "Query";
	this->KVMap["Tag.1.key"] = "";
	this->KPMap["Tag.1.key"] = "Query";
	this->KVMap["Tag.2.key"] = "";
	this->KPMap["Tag.2.key"] = "Query";
	this->KVMap["Tag.3.key"] = "";
	this->KPMap["Tag.3.key"] = "Query";
	this->KVMap["Tag.4.key"] = "";
	this->KPMap["Tag.4.key"] = "Query";
	this->KVMap["Tag.5.key"] = "";
	this->KPMap["Tag.5.key"] = "Query";
	this->KVMap["Tag.1.value"] = "";
	this->KPMap["Tag.1.value"] = "Query";
	this->KVMap["Tag.2.value"] = "";
	this->KPMap["Tag.2.value"] = "Query";
	this->KVMap["Tag.3.value"] = "";
	this->KPMap["Tag.3.value"] = "Query";
	this->KVMap["Tag.4.value"] = "";
	this->KPMap["Tag.4.value"] = "Query";
	this->KVMap["Tag.5.value"] = "";
	this->KPMap["Tag.5.value"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";

}
RdsRegionResetAccountForPGRequest::RdsRegionResetAccountForPGRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "rds-region";
	this->Action = "ResetAccountForPG";
	this->Version = "2014-08-15";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["DBInstanceId"] = "";
	this->KPMap["DBInstanceId"] = "Query";
	this->KVMap["AccountName"] = "";
	this->KPMap["AccountName"] = "Query";
	this->KVMap["AccountPassword"] = "";
	this->KPMap["AccountPassword"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";

}
RdsRegionResetAccountPasswordRequest::RdsRegionResetAccountPasswordRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "rds-region";
	this->Action = "ResetAccountPassword";
	this->Version = "2014-08-15";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["DBInstanceId"] = "";
	this->KPMap["DBInstanceId"] = "Query";
	this->KVMap["AccountName"] = "";
	this->KPMap["AccountName"] = "Query";
	this->KVMap["AccountPassword"] = "";
	this->KPMap["AccountPassword"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";

}
RdsRegionRestartDBInstanceRequest::RdsRegionRestartDBInstanceRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "rds-region";
	this->Action = "RestartDBInstance";
	this->Version = "2014-08-15";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["ClientToken"] = "";
	this->KPMap["ClientToken"] = "Query";
	this->KVMap["DBInstanceId"] = "";
	this->KPMap["DBInstanceId"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";

}
RdsRegionRestoreDBInstanceRequest::RdsRegionRestoreDBInstanceRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "rds-region";
	this->Action = "RestoreDBInstance";
	this->Version = "2014-08-15";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["ClientToken"] = "";
	this->KPMap["ClientToken"] = "Query";
	this->KVMap["DBInstanceId"] = "";
	this->KPMap["DBInstanceId"] = "Query";
	this->KVMap["BackupId"] = "";
	this->KPMap["BackupId"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";

}
RdsRegionRevokeAccountPrivilegeRequest::RdsRegionRevokeAccountPrivilegeRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "rds-region";
	this->Action = "RevokeAccountPrivilege";
	this->Version = "2014-08-15";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["DBInstanceId"] = "";
	this->KPMap["DBInstanceId"] = "Query";
	this->KVMap["AccountName"] = "";
	this->KPMap["AccountName"] = "Query";
	this->KVMap["DBName"] = "";
	this->KPMap["DBName"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";

}
RdsRegionStartDBInstanceDiagnoseRequest::RdsRegionStartDBInstanceDiagnoseRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "rds-region";
	this->Action = "StartDBInstanceDiagnose";
	this->Version = "2014-08-15";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["ClientToken"] = "";
	this->KPMap["ClientToken"] = "Query";
	this->KVMap["proxyId"] = "";
	this->KPMap["proxyId"] = "Query";
	this->KVMap["DBInstanceId"] = "";
	this->KPMap["DBInstanceId"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";

}
RdsRegionStopSyncingRequest::RdsRegionStopSyncingRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "rds-region";
	this->Action = "StopSyncing";
	this->Version = "2014-08-15";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["ClientToken"] = "";
	this->KPMap["ClientToken"] = "Query";
	this->KVMap["DBInstanceId"] = "";
	this->KPMap["DBInstanceId"] = "Query";
	this->KVMap["ImportId"] = "";
	this->KPMap["ImportId"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";

}
RdsRegionSwitchDBInstanceNetTypeRequest::RdsRegionSwitchDBInstanceNetTypeRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "rds-region";
	this->Action = "SwitchDBInstanceNetType";
	this->Version = "2014-08-15";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["ClientToken"] = "";
	this->KPMap["ClientToken"] = "Query";
	this->KVMap["DBInstanceId"] = "";
	this->KPMap["DBInstanceId"] = "Query";
	this->KVMap["ConnectionStringPrefix"] = "";
	this->KPMap["ConnectionStringPrefix"] = "Query";
	this->KVMap["Port"] = "";
	this->KPMap["Port"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";

}
RdsRegionUnlockDBInstanceRequest::RdsRegionUnlockDBInstanceRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "rds-region";
	this->Action = "UnlockDBInstance";
	this->Version = "2014-08-15";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["DBInstanceId"] = "";
	this->KPMap["DBInstanceId"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";

}
RdsRegionUpgradeDBInstanceEngineVersionRequest::RdsRegionUpgradeDBInstanceEngineVersionRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "rds-region";
	this->Action = "UpgradeDBInstanceEngineVersion";
	this->Version = "2014-08-15";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["ClientToken"] = "";
	this->KPMap["ClientToken"] = "Query";
	this->KVMap["DBInstanceId"] = "";
	this->KPMap["DBInstanceId"] = "Query";
	this->KVMap["EngineVersion"] = "";
	this->KPMap["EngineVersion"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";

}

	
}