/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
 
#ifndef ALIYUNRDSREGION_RDSREGIONSERVICE_H_
#define ALIYUNRDSREGION_RDSREGIONSERVICE_H_

#include "../aliyun-cpp-sdk-core/BaseRequest.h"
#include "../aliyun-cpp-sdk-core/ApiBase.h"

namespace AliYunRdsRegion {

class RdsRegionClient : public AliYunCore::ApiBase {
private:
	AliYunCore::DefaultProfile &Profile;
public:
	RdsRegionClient(AliYunCore::DefaultProfile &pf);
	void setProfile(AliYunCore::DefaultProfile &pf);
	std::string getResponse(AliYunCore::BaseRequest &request);
	~RdsRegionClient(){};
};
class RdsRegionAddTagsToResourceRequest : public AliYunCore::BaseRequest {
public:
	RdsRegionAddTagsToResourceRequest();
};
class RdsRegionAllocateInstancePrivateConnectionRequest : public AliYunCore::BaseRequest {
public:
	RdsRegionAllocateInstancePrivateConnectionRequest();
};
class RdsRegionAllocateInstancePublicConnectionRequest : public AliYunCore::BaseRequest {
public:
	RdsRegionAllocateInstancePublicConnectionRequest();
};
class RdsRegionBatchGrantAccountPrivilegeRequest : public AliYunCore::BaseRequest {
public:
	RdsRegionBatchGrantAccountPrivilegeRequest();
};
class RdsRegionBatchRevokeAccountPrivilegeRequest : public AliYunCore::BaseRequest {
public:
	RdsRegionBatchRevokeAccountPrivilegeRequest();
};
class RdsRegionCancelImportRequest : public AliYunCore::BaseRequest {
public:
	RdsRegionCancelImportRequest();
};
class RdsRegionCheckAccountNameAvailableRequest : public AliYunCore::BaseRequest {
public:
	RdsRegionCheckAccountNameAvailableRequest();
};
class RdsRegionCheckDBNameAvailableRequest : public AliYunCore::BaseRequest {
public:
	RdsRegionCheckDBNameAvailableRequest();
};
class RdsRegionCreateAccountRequest : public AliYunCore::BaseRequest {
public:
	RdsRegionCreateAccountRequest();
};
class RdsRegionCreateAccountForInnerRequest : public AliYunCore::BaseRequest {
public:
	RdsRegionCreateAccountForInnerRequest();
};
class RdsRegionCreateBackupRequest : public AliYunCore::BaseRequest {
public:
	RdsRegionCreateBackupRequest();
};
class RdsRegionCreateDatabaseRequest : public AliYunCore::BaseRequest {
public:
	RdsRegionCreateDatabaseRequest();
};
class RdsRegionCreateDatabaseForInnerRequest : public AliYunCore::BaseRequest {
public:
	RdsRegionCreateDatabaseForInnerRequest();
};
class RdsRegionCreateDBInstanceRequest : public AliYunCore::BaseRequest {
public:
	RdsRegionCreateDBInstanceRequest();
};
class RdsRegionCreateDBInstanceForChannelRequest : public AliYunCore::BaseRequest {
public:
	RdsRegionCreateDBInstanceForChannelRequest();
};
class RdsRegionCreateDBInstanceforFirstPayRequest : public AliYunCore::BaseRequest {
public:
	RdsRegionCreateDBInstanceforFirstPayRequest();
};
class RdsRegionCreatePostpaidDBInstanceRequest : public AliYunCore::BaseRequest {
public:
	RdsRegionCreatePostpaidDBInstanceRequest();
};
class RdsRegionCreatePostpaidDBInstanceForChannelRequest : public AliYunCore::BaseRequest {
public:
	RdsRegionCreatePostpaidDBInstanceForChannelRequest();
};
class RdsRegionCreateReadOnlyDBInstanceRequest : public AliYunCore::BaseRequest {
public:
	RdsRegionCreateReadOnlyDBInstanceRequest();
};
class RdsRegionCreateTempDBInstanceRequest : public AliYunCore::BaseRequest {
public:
	RdsRegionCreateTempDBInstanceRequest();
};
class RdsRegionCreateUploadPathForSQLServerRequest : public AliYunCore::BaseRequest {
public:
	RdsRegionCreateUploadPathForSQLServerRequest();
};
class RdsRegionDeleteAccountRequest : public AliYunCore::BaseRequest {
public:
	RdsRegionDeleteAccountRequest();
};
class RdsRegionDeleteDatabaseRequest : public AliYunCore::BaseRequest {
public:
	RdsRegionDeleteDatabaseRequest();
};
class RdsRegionDeleteDBInstanceRequest : public AliYunCore::BaseRequest {
public:
	RdsRegionDeleteDBInstanceRequest();
};
class RdsRegionDescibeImportsFromDatabaseRequest : public AliYunCore::BaseRequest {
public:
	RdsRegionDescibeImportsFromDatabaseRequest();
};
class RdsRegionDescribeAbnormalDBInstancesRequest : public AliYunCore::BaseRequest {
public:
	RdsRegionDescribeAbnormalDBInstancesRequest();
};
class RdsRegionDescribeAccountsRequest : public AliYunCore::BaseRequest {
public:
	RdsRegionDescribeAccountsRequest();
};
class RdsRegionDescribeBackupPolicyRequest : public AliYunCore::BaseRequest {
public:
	RdsRegionDescribeBackupPolicyRequest();
};
class RdsRegionDescribeBackupsRequest : public AliYunCore::BaseRequest {
public:
	RdsRegionDescribeBackupsRequest();
};
class RdsRegionDescribeBackupTasksRequest : public AliYunCore::BaseRequest {
public:
	RdsRegionDescribeBackupTasksRequest();
};
class RdsRegionDescribeBinlogFilesRequest : public AliYunCore::BaseRequest {
public:
	RdsRegionDescribeBinlogFilesRequest();
};
class RdsRegionDescribeDatabasesRequest : public AliYunCore::BaseRequest {
public:
	RdsRegionDescribeDatabasesRequest();
};
class RdsRegionDescribeDBInstanceAttributeRequest : public AliYunCore::BaseRequest {
public:
	RdsRegionDescribeDBInstanceAttributeRequest();
};
class RdsRegionDescribeDBInstanceIPArrayListRequest : public AliYunCore::BaseRequest {
public:
	RdsRegionDescribeDBInstanceIPArrayListRequest();
};
class RdsRegionDescribeDBInstanceNetInfoRequest : public AliYunCore::BaseRequest {
public:
	RdsRegionDescribeDBInstanceNetInfoRequest();
};
class RdsRegionDescribeDBInstanceNetInfoForChannelRequest : public AliYunCore::BaseRequest {
public:
	RdsRegionDescribeDBInstanceNetInfoForChannelRequest();
};
class RdsRegionDescribeDBInstancePerformanceRequest : public AliYunCore::BaseRequest {
public:
	RdsRegionDescribeDBInstancePerformanceRequest();
};
class RdsRegionDescribeDBInstancesRequest : public AliYunCore::BaseRequest {
public:
	RdsRegionDescribeDBInstancesRequest();
};
class RdsRegionDescribeDBInstancesByExpireTimeRequest : public AliYunCore::BaseRequest {
public:
	RdsRegionDescribeDBInstancesByExpireTimeRequest();
};
class RdsRegionDescribeDBInstancesByPerformanceRequest : public AliYunCore::BaseRequest {
public:
	RdsRegionDescribeDBInstancesByPerformanceRequest();
};
class RdsRegionDescribeErrorLogsRequest : public AliYunCore::BaseRequest {
public:
	RdsRegionDescribeErrorLogsRequest();
};
class RdsRegionDescribeFilesForSQLServerRequest : public AliYunCore::BaseRequest {
public:
	RdsRegionDescribeFilesForSQLServerRequest();
};
class RdsRegionDescribeImportsForSQLServerRequest : public AliYunCore::BaseRequest {
public:
	RdsRegionDescribeImportsForSQLServerRequest();
};
class RdsRegionDescribeModifyParameterLogRequest : public AliYunCore::BaseRequest {
public:
	RdsRegionDescribeModifyParameterLogRequest();
};
class RdsRegionDescribeOperationLogsRequest : public AliYunCore::BaseRequest {
public:
	RdsRegionDescribeOperationLogsRequest();
};
class RdsRegionDescribeOptimizeAdviceByDBARequest : public AliYunCore::BaseRequest {
public:
	RdsRegionDescribeOptimizeAdviceByDBARequest();
};
class RdsRegionDescribeOptimizeAdviceOnBigTableRequest : public AliYunCore::BaseRequest {
public:
	RdsRegionDescribeOptimizeAdviceOnBigTableRequest();
};
class RdsRegionDescribeOptimizeAdviceOnExcessIndexRequest : public AliYunCore::BaseRequest {
public:
	RdsRegionDescribeOptimizeAdviceOnExcessIndexRequest();
};
class RdsRegionDescribeOptimizeAdviceOnMissIndexRequest : public AliYunCore::BaseRequest {
public:
	RdsRegionDescribeOptimizeAdviceOnMissIndexRequest();
};
class RdsRegionDescribeOptimizeAdviceOnMissPKRequest : public AliYunCore::BaseRequest {
public:
	RdsRegionDescribeOptimizeAdviceOnMissPKRequest();
};
class RdsRegionDescribeOptimizeAdviceOnStorageRequest : public AliYunCore::BaseRequest {
public:
	RdsRegionDescribeOptimizeAdviceOnStorageRequest();
};
class RdsRegionDescribeParametersRequest : public AliYunCore::BaseRequest {
public:
	RdsRegionDescribeParametersRequest();
};
class RdsRegionDescribeParameterTemplatesRequest : public AliYunCore::BaseRequest {
public:
	RdsRegionDescribeParameterTemplatesRequest();
};
class RdsRegionDescribePreCheckResultsRequest : public AliYunCore::BaseRequest {
public:
	RdsRegionDescribePreCheckResultsRequest();
};
class RdsRegionDescribeRealtimeDiagnosesRequest : public AliYunCore::BaseRequest {
public:
	RdsRegionDescribeRealtimeDiagnosesRequest();
};
class RdsRegionDescribeRegionsRequest : public AliYunCore::BaseRequest {
public:
	RdsRegionDescribeRegionsRequest();
};
class RdsRegionDescribeResourceUsageRequest : public AliYunCore::BaseRequest {
public:
	RdsRegionDescribeResourceUsageRequest();
};
class RdsRegionDescribeSlowLogRecordsRequest : public AliYunCore::BaseRequest {
public:
	RdsRegionDescribeSlowLogRecordsRequest();
};
class RdsRegionDescribeSlowLogsRequest : public AliYunCore::BaseRequest {
public:
	RdsRegionDescribeSlowLogsRequest();
};
class RdsRegionDescribeSQLInjectionInfosRequest : public AliYunCore::BaseRequest {
public:
	RdsRegionDescribeSQLInjectionInfosRequest();
};
class RdsRegionDescribeSQLLogRecordsRequest : public AliYunCore::BaseRequest {
public:
	RdsRegionDescribeSQLLogRecordsRequest();
};
class RdsRegionDescribeSQLLogReportsRequest : public AliYunCore::BaseRequest {
public:
	RdsRegionDescribeSQLLogReportsRequest();
};
class RdsRegionDescribeTasksRequest : public AliYunCore::BaseRequest {
public:
	RdsRegionDescribeTasksRequest();
};
class RdsRegionDescribeVpcZoneNosRequest : public AliYunCore::BaseRequest {
public:
	RdsRegionDescribeVpcZoneNosRequest();
};
class RdsRegionExtractBackupFromOASRequest : public AliYunCore::BaseRequest {
public:
	RdsRegionExtractBackupFromOASRequest();
};
class RdsRegionGrantAccountPrivilegeRequest : public AliYunCore::BaseRequest {
public:
	RdsRegionGrantAccountPrivilegeRequest();
};
class RdsRegionImportDatabaseBetweenInstancesRequest : public AliYunCore::BaseRequest {
public:
	RdsRegionImportDatabaseBetweenInstancesRequest();
};
class RdsRegionImportDataForSQLServerRequest : public AliYunCore::BaseRequest {
public:
	RdsRegionImportDataForSQLServerRequest();
};
class RdsRegionImportDataFromDatabaseRequest : public AliYunCore::BaseRequest {
public:
	RdsRegionImportDataFromDatabaseRequest();
};
class RdsRegionLockDBInstanceRequest : public AliYunCore::BaseRequest {
public:
	RdsRegionLockDBInstanceRequest();
};
class RdsRegionMigrateToOtherZoneRequest : public AliYunCore::BaseRequest {
public:
	RdsRegionMigrateToOtherZoneRequest();
};
class RdsRegionModifyAccountDescriptionRequest : public AliYunCore::BaseRequest {
public:
	RdsRegionModifyAccountDescriptionRequest();
};
class RdsRegionModifyBackupPolicyRequest : public AliYunCore::BaseRequest {
public:
	RdsRegionModifyBackupPolicyRequest();
};
class RdsRegionModifyDBDescriptionRequest : public AliYunCore::BaseRequest {
public:
	RdsRegionModifyDBDescriptionRequest();
};
class RdsRegionModifyDBInstanceConnectionModeRequest : public AliYunCore::BaseRequest {
public:
	RdsRegionModifyDBInstanceConnectionModeRequest();
};
class RdsRegionModifyDBInstanceConnectionStringRequest : public AliYunCore::BaseRequest {
public:
	RdsRegionModifyDBInstanceConnectionStringRequest();
};
class RdsRegionModifyDBInstanceDescriptionRequest : public AliYunCore::BaseRequest {
public:
	RdsRegionModifyDBInstanceDescriptionRequest();
};
class RdsRegionModifyDBInstanceMaintainTimeRequest : public AliYunCore::BaseRequest {
public:
	RdsRegionModifyDBInstanceMaintainTimeRequest();
};
class RdsRegionModifyDBInstanceNetworkTypeRequest : public AliYunCore::BaseRequest {
public:
	RdsRegionModifyDBInstanceNetworkTypeRequest();
};
class RdsRegionModifyDBInstanceSpecRequest : public AliYunCore::BaseRequest {
public:
	RdsRegionModifyDBInstanceSpecRequest();
};
class RdsRegionModifyParameterRequest : public AliYunCore::BaseRequest {
public:
	RdsRegionModifyParameterRequest();
};
class RdsRegionModifyPostpaidDBInstanceSpecRequest : public AliYunCore::BaseRequest {
public:
	RdsRegionModifyPostpaidDBInstanceSpecRequest();
};
class RdsRegionModifySecurityIpsRequest : public AliYunCore::BaseRequest {
public:
	RdsRegionModifySecurityIpsRequest();
};
class RdsRegionPreCheckBeforeImportDataRequest : public AliYunCore::BaseRequest {
public:
	RdsRegionPreCheckBeforeImportDataRequest();
};
class RdsRegionPurgeDBInstanceLogRequest : public AliYunCore::BaseRequest {
public:
	RdsRegionPurgeDBInstanceLogRequest();
};
class RdsRegionReleaseInstancePublicConnectionRequest : public AliYunCore::BaseRequest {
public:
	RdsRegionReleaseInstancePublicConnectionRequest();
};
class RdsRegionRemoveTagsFromResourceRequest : public AliYunCore::BaseRequest {
public:
	RdsRegionRemoveTagsFromResourceRequest();
};
class RdsRegionResetAccountForPGRequest : public AliYunCore::BaseRequest {
public:
	RdsRegionResetAccountForPGRequest();
};
class RdsRegionResetAccountPasswordRequest : public AliYunCore::BaseRequest {
public:
	RdsRegionResetAccountPasswordRequest();
};
class RdsRegionRestartDBInstanceRequest : public AliYunCore::BaseRequest {
public:
	RdsRegionRestartDBInstanceRequest();
};
class RdsRegionRestoreDBInstanceRequest : public AliYunCore::BaseRequest {
public:
	RdsRegionRestoreDBInstanceRequest();
};
class RdsRegionRevokeAccountPrivilegeRequest : public AliYunCore::BaseRequest {
public:
	RdsRegionRevokeAccountPrivilegeRequest();
};
class RdsRegionStartDBInstanceDiagnoseRequest : public AliYunCore::BaseRequest {
public:
	RdsRegionStartDBInstanceDiagnoseRequest();
};
class RdsRegionStopSyncingRequest : public AliYunCore::BaseRequest {
public:
	RdsRegionStopSyncingRequest();
};
class RdsRegionSwitchDBInstanceNetTypeRequest : public AliYunCore::BaseRequest {
public:
	RdsRegionSwitchDBInstanceNetTypeRequest();
};
class RdsRegionUnlockDBInstanceRequest : public AliYunCore::BaseRequest {
public:
	RdsRegionUnlockDBInstanceRequest();
};
class RdsRegionUpgradeDBInstanceEngineVersionRequest : public AliYunCore::BaseRequest {
public:
	RdsRegionUpgradeDBInstanceEngineVersionRequest();
};


}

#endif