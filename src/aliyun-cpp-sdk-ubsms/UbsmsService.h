/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
 
#ifndef ALIYUNUBSMS_UBSMSSERVICE_H_
#define ALIYUNUBSMS_UBSMSSERVICE_H_

#include "../aliyun-cpp-sdk-core/BaseRequest.h"
#include "../aliyun-cpp-sdk-core/ApiBase.h"

namespace AliYunUbsms {

class UbsmsClient : public AliYunCore::ApiBase {
private:
	AliYunCore::DefaultProfile &Profile;
public:
	UbsmsClient(AliYunCore::DefaultProfile &pf);
	void setProfile(AliYunCore::DefaultProfile &pf);
	std::string getResponse(AliYunCore::BaseRequest &request);
	~UbsmsClient(){};
};
class UbsmsDescribeBusinessStatusRequest : public AliYunCore::BaseRequest {
public:
	UbsmsDescribeBusinessStatusRequest();
};
class UbsmsNotifyUserBusinessCommandRequest : public AliYunCore::BaseRequest {
public:
	UbsmsNotifyUserBusinessCommandRequest();
};
class UbsmsSetUserBusinessStatusRequest : public AliYunCore::BaseRequest {
public:
	UbsmsSetUserBusinessStatusRequest();
};
class UbsmsSetUserBusinessStatusesRequest : public AliYunCore::BaseRequest {
public:
	UbsmsSetUserBusinessStatusesRequest();
};


}

#endif