/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

#include <string>
#include <map>
#include "../aliyun-cpp-sdk-core/utils.h"
#include "../aliyun-cpp-sdk-core/DefaultProfile.h"
#include "../aliyun-cpp-sdk-core/BaseRequest.h"
#include "../aliyun-cpp-sdk-core/ApiBase.h"
#include "UbsmsService.h"

using std::string;
using namespace AliYunCore;

namespace AliYunUbsms {

UbsmsClient::UbsmsClient(DefaultProfile &pf):Profile(pf){}
void UbsmsClient::setProfile(DefaultProfile &pf){
	this->Profile = pf;
}
string UbsmsClient::getResponse(BaseRequest &request){
	request.RegionId = this->Profile.RegionId;		
	request.AccessKeyId = this->Profile.AccessKeyId;
	request.AccessKeySecret = this->Profile.AccessKeySecret;
	return ApiBase::getResponse(request);
}
UbsmsDescribeBusinessStatusRequest::UbsmsDescribeBusinessStatusRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "ubsms";
	this->Action = "DescribeBusinessStatus";
	this->Version = "2015-06-23";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["callerBid"] = "";
	this->KPMap["callerBid"] = "Query";
	this->KVMap["Password"] = "";
	this->KPMap["Password"] = "Query";

}
UbsmsNotifyUserBusinessCommandRequest::UbsmsNotifyUserBusinessCommandRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "ubsms";
	this->Action = "NotifyUserBusinessCommand";
	this->Version = "2015-06-23";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["Uid"] = "";
	this->KPMap["Uid"] = "Query";
	this->KVMap["ServiceCode"] = "";
	this->KPMap["ServiceCode"] = "Query";
	this->KVMap["Cmd"] = "";
	this->KPMap["Cmd"] = "Query";
	this->KVMap["Region"] = "";
	this->KPMap["Region"] = "Query";
	this->KVMap["InstanceId"] = "";
	this->KPMap["InstanceId"] = "Query";
	this->KVMap["ClientToken"] = "";
	this->KPMap["ClientToken"] = "Query";
	this->KVMap["Password"] = "";
	this->KPMap["Password"] = "Query";

}
UbsmsSetUserBusinessStatusRequest::UbsmsSetUserBusinessStatusRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "ubsms";
	this->Action = "SetUserBusinessStatus";
	this->Version = "2015-06-23";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["Uid"] = "";
	this->KPMap["Uid"] = "Query";
	this->KVMap["Service"] = "";
	this->KPMap["Service"] = "Query";
	this->KVMap["StatusKey"] = "";
	this->KPMap["StatusKey"] = "Query";
	this->KVMap["StatusValue"] = "";
	this->KPMap["StatusValue"] = "Query";

}
UbsmsSetUserBusinessStatusesRequest::UbsmsSetUserBusinessStatusesRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "ubsms";
	this->Action = "SetUserBusinessStatuses";
	this->Version = "2015-06-23";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["Uid"] = "";
	this->KPMap["Uid"] = "Query";
	this->KVMap["ServiceCode"] = "";
	this->KPMap["ServiceCode"] = "Query";
	this->KVMap["StatusKey1"] = "";
	this->KPMap["StatusKey1"] = "Query";
	this->KVMap["StatusValue1"] = "";
	this->KPMap["StatusValue1"] = "Query";
	this->KVMap["StatusKey2"] = "";
	this->KPMap["StatusKey2"] = "Query";
	this->KVMap["StatusValue2"] = "";
	this->KPMap["StatusValue2"] = "Query";
	this->KVMap["StatusKey3"] = "";
	this->KPMap["StatusKey3"] = "Query";
	this->KVMap["StatusValue3"] = "";
	this->KPMap["StatusValue3"] = "Query";
	this->KVMap["StatusKey4"] = "";
	this->KPMap["StatusKey4"] = "Query";
	this->KVMap["StatusValue4"] = "";
	this->KPMap["StatusValue4"] = "Query";
	this->KVMap["StatusKey5"] = "";
	this->KPMap["StatusKey5"] = "Query";
	this->KVMap["StatusValue5"] = "";
	this->KPMap["StatusValue5"] = "Query";
	this->KVMap["StatusKey6"] = "";
	this->KPMap["StatusKey6"] = "Query";
	this->KVMap["StatusValue6"] = "";
	this->KPMap["StatusValue6"] = "Query";
	this->KVMap["StatusKey7"] = "";
	this->KPMap["StatusKey7"] = "Query";
	this->KVMap["StatusValue7"] = "";
	this->KPMap["StatusValue7"] = "Query";
	this->KVMap["StatusKey8"] = "";
	this->KPMap["StatusKey8"] = "Query";
	this->KVMap["StatusValue8"] = "";
	this->KPMap["StatusValue8"] = "Query";
	this->KVMap["StatusKey9"] = "";
	this->KPMap["StatusKey9"] = "Query";
	this->KVMap["StatusValue9"] = "";
	this->KPMap["StatusValue9"] = "Query";
	this->KVMap["StatusKey10"] = "";
	this->KPMap["StatusKey10"] = "Query";
	this->KVMap["StatusValue10"] = "";
	this->KPMap["StatusValue10"] = "Query";
	this->KVMap["Password"] = "";
	this->KPMap["Password"] = "Query";

}

	
}