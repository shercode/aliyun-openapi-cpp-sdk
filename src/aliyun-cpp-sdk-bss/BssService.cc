/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

#include <string>
#include <map>
#include "../aliyun-cpp-sdk-core/utils.h"
#include "../aliyun-cpp-sdk-core/DefaultProfile.h"
#include "../aliyun-cpp-sdk-core/BaseRequest.h"
#include "../aliyun-cpp-sdk-core/ApiBase.h"
#include "BssService.h"

using std::string;
using namespace AliYunCore;

namespace AliYunBss {

BssClient::BssClient(DefaultProfile &pf):Profile(pf){}
void BssClient::setProfile(DefaultProfile &pf){
	this->Profile = pf;
}
string BssClient::getResponse(BaseRequest &request){
	request.RegionId = this->Profile.RegionId;		
	request.AccessKeyId = this->Profile.AccessKeyId;
	request.AccessKeySecret = this->Profile.AccessKeySecret;
	return ApiBase::getResponse(request);
}
BssDescribeCashDetailRequest::BssDescribeCashDetailRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTPS";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "bss";
	this->Action = "DescribeCashDetail";
	this->Version = "2014-07-14";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 

}
BssDescribeCouponDetailRequest::BssDescribeCouponDetailRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTPS";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "bss";
	this->Action = "DescribeCouponDetail";
	this->Version = "2014-07-14";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["CouponNumber"] = "";
	this->KPMap["CouponNumber"] = "Query";

}
BssDescribeCouponListRequest::BssDescribeCouponListRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTPS";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "bss";
	this->Action = "DescribeCouponList";
	this->Version = "2014-07-14";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["Status"] = "";
	this->KPMap["Status"] = "Query";
	this->KVMap["StartDeliveryTime"] = "";
	this->KPMap["StartDeliveryTime"] = "Query";
	this->KVMap["EndDeliveryTime"] = "";
	this->KPMap["EndDeliveryTime"] = "Query";
	this->KVMap["PageSize"] = "";
	this->KPMap["PageSize"] = "Query";
	this->KVMap["PageNum"] = "";
	this->KPMap["PageNum"] = "Query";

}
BssSetResourceBusinessStatusRequest::BssSetResourceBusinessStatusRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "bss";
	this->Action = "SetResourceBusinessStatus";
	this->Version = "2014-07-14";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["ResourceType"] = "";
	this->KPMap["ResourceType"] = "Query";
	this->KVMap["ResourceId"] = "";
	this->KPMap["ResourceId"] = "Query";
	this->KVMap["BusinessStatus"] = "";
	this->KPMap["BusinessStatus"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";

}

	
}