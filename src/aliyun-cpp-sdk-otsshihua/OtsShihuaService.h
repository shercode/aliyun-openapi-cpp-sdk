/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
 
#ifndef ALIYUNOTSSHIHUA_OTSSHIHUASERVICE_H_
#define ALIYUNOTSSHIHUA_OTSSHIHUASERVICE_H_

#include "../aliyun-cpp-sdk-core/BaseRequest.h"
#include "../aliyun-cpp-sdk-core/ApiBase.h"

namespace AliYunOtsShihua {

class OtsShihuaClient : public AliYunCore::ApiBase {
private:
	AliYunCore::DefaultProfile &Profile;
public:
	OtsShihuaClient(AliYunCore::DefaultProfile &pf);
	void setProfile(AliYunCore::DefaultProfile &pf);
	std::string getResponse(AliYunCore::BaseRequest &request);
	~OtsShihuaClient(){};
};
class OtsShihuaDeleteInstanceRequest : public AliYunCore::BaseRequest {
public:
	OtsShihuaDeleteInstanceRequest();
};
class OtsShihuaGetInstanceRequest : public AliYunCore::BaseRequest {
public:
	OtsShihuaGetInstanceRequest();
};
class OtsShihuaInsertInstanceRequest : public AliYunCore::BaseRequest {
public:
	OtsShihuaInsertInstanceRequest();
};
class OtsShihuaListInstanceRequest : public AliYunCore::BaseRequest {
public:
	OtsShihuaListInstanceRequest();
};


}

#endif