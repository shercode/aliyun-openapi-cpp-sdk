/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

#include <string>
#include <map>
#include "../aliyun-cpp-sdk-core/utils.h"
#include "../aliyun-cpp-sdk-core/DefaultProfile.h"
#include "../aliyun-cpp-sdk-core/BaseRequest.h"
#include "../aliyun-cpp-sdk-core/ApiBase.h"
#include "OtsShihuaService.h"

using std::string;
using namespace AliYunCore;

namespace AliYunOtsShihua {

OtsShihuaClient::OtsShihuaClient(DefaultProfile &pf):Profile(pf){}
void OtsShihuaClient::setProfile(DefaultProfile &pf){
	this->Profile = pf;
}
string OtsShihuaClient::getResponse(BaseRequest &request){
	request.RegionId = this->Profile.RegionId;		
	request.AccessKeyId = this->Profile.AccessKeyId;
	request.AccessKeySecret = this->Profile.AccessKeySecret;
	return ApiBase::getResponse(request);
}
OtsShihuaDeleteInstanceRequest::OtsShihuaDeleteInstanceRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "POST";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "otsshihua";
	this->Action = "DeleteInstance";
	this->Version = "2015-10-26";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["InstanceName"] = "";
	this->KPMap["InstanceName"] = "Query";

}
OtsShihuaGetInstanceRequest::OtsShihuaGetInstanceRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "otsshihua";
	this->Action = "GetInstance";
	this->Version = "2015-10-26";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["InstanceName"] = "";
	this->KPMap["InstanceName"] = "Query";

}
OtsShihuaInsertInstanceRequest::OtsShihuaInsertInstanceRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "POST";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "otsshihua";
	this->Action = "InsertInstance";
	this->Version = "2015-10-26";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["InstanceName"] = "";
	this->KPMap["InstanceName"] = "Query";
	this->KVMap["ClusterType"] = "";
	this->KPMap["ClusterType"] = "Query";
	this->KVMap["Description"] = "";
	this->KPMap["Description"] = "Query";

}
OtsShihuaListInstanceRequest::OtsShihuaListInstanceRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "otsshihua";
	this->Action = "ListInstance";
	this->Version = "2015-10-26";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 

}

	
}