/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

#include <string>
#include <map>
#include "../aliyun-cpp-sdk-core/utils.h"
#include "../aliyun-cpp-sdk-core/DefaultProfile.h"
#include "../aliyun-cpp-sdk-core/BaseRequest.h"
#include "../aliyun-cpp-sdk-core/ApiBase.h"
#include "SlbService.h"

using std::string;
using namespace AliYunCore;

namespace AliYunSlb {

SlbClient::SlbClient(DefaultProfile &pf):Profile(pf){}
void SlbClient::setProfile(DefaultProfile &pf){
	this->Profile = pf;
}
string SlbClient::getResponse(BaseRequest &request){
	request.RegionId = this->Profile.RegionId;		
	request.AccessKeyId = this->Profile.AccessKeyId;
	request.AccessKeySecret = this->Profile.AccessKeySecret;
	return ApiBase::getResponse(request);
}
SlbAddBackendServersRequest::SlbAddBackendServersRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "slb";
	this->Action = "AddBackendServers";
	this->Version = "2014-05-15";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["LoadBalancerId"] = "";
	this->KPMap["LoadBalancerId"] = "Query";
	this->KVMap["BackendServers"] = "";
	this->KPMap["BackendServers"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";

}
SlbAddListenerWhiteListItemRequest::SlbAddListenerWhiteListItemRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "slb";
	this->Action = "AddListenerWhiteListItem";
	this->Version = "2014-05-15";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["LoadBalancerId"] = "";
	this->KPMap["LoadBalancerId"] = "Query";
	this->KVMap["ListenerPort"] = "";
	this->KPMap["ListenerPort"] = "Query";
	this->KVMap["SourceItems"] = "";
	this->KPMap["SourceItems"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";

}
SlbCreateLoadBalancerRequest::SlbCreateLoadBalancerRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "slb";
	this->Action = "CreateLoadBalancer";
	this->Version = "2014-05-15";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["AddressType"] = "";
	this->KPMap["AddressType"] = "Query";
	this->KVMap["InternetChargeType"] = "";
	this->KPMap["InternetChargeType"] = "Query";
	this->KVMap["Bandwidth"] = "";
	this->KPMap["Bandwidth"] = "Query";
	this->KVMap["ClientToken"] = "";
	this->KPMap["ClientToken"] = "Query";
	this->KVMap["LoadBalancerName"] = "";
	this->KPMap["LoadBalancerName"] = "Query";
	this->KVMap["VpcId"] = "";
	this->KPMap["VpcId"] = "Query";
	this->KVMap["VSwitchId"] = "";
	this->KPMap["VSwitchId"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";
	this->KVMap["MasterZoneId"] = "";
	this->KPMap["MasterZoneId"] = "Query";
	this->KVMap["SlaveZoneId"] = "";
	this->KPMap["SlaveZoneId"] = "Query";
	this->KVMap["MaxConnLimit"] = "";
	this->KPMap["MaxConnLimit"] = "Query";
	this->KVMap["SecurityStatus"] = "";
	this->KPMap["SecurityStatus"] = "Query";

}
SlbCreateLoadBalancerHTTPListenerRequest::SlbCreateLoadBalancerHTTPListenerRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "slb";
	this->Action = "CreateLoadBalancerHTTPListener";
	this->Version = "2014-05-15";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["LoadBalancerId"] = "";
	this->KPMap["LoadBalancerId"] = "Query";
	this->KVMap["Bandwidth"] = "";
	this->KPMap["Bandwidth"] = "Query";
	this->KVMap["ListenerPort"] = "";
	this->KPMap["ListenerPort"] = "Query";
	this->KVMap["BackendServerPort"] = "";
	this->KPMap["BackendServerPort"] = "Query";
	this->KVMap["XForwardedFor"] = "";
	this->KPMap["XForwardedFor"] = "Query";
	this->KVMap["Scheduler"] = "";
	this->KPMap["Scheduler"] = "Query";
	this->KVMap["StickySession"] = "";
	this->KPMap["StickySession"] = "Query";
	this->KVMap["StickySessionType"] = "";
	this->KPMap["StickySessionType"] = "Query";
	this->KVMap["CookieTimeout"] = "";
	this->KPMap["CookieTimeout"] = "Query";
	this->KVMap["Cookie"] = "";
	this->KPMap["Cookie"] = "Query";
	this->KVMap["HealthCheck"] = "";
	this->KPMap["HealthCheck"] = "Query";
	this->KVMap["HealthCheckDomain"] = "";
	this->KPMap["HealthCheckDomain"] = "Query";
	this->KVMap["HealthCheckURI"] = "";
	this->KPMap["HealthCheckURI"] = "Query";
	this->KVMap["HealthyThreshold"] = "";
	this->KPMap["HealthyThreshold"] = "Query";
	this->KVMap["UnhealthyThreshold"] = "";
	this->KPMap["UnhealthyThreshold"] = "Query";
	this->KVMap["HealthCheckTimeout"] = "";
	this->KPMap["HealthCheckTimeout"] = "Query";
	this->KVMap["HealthCheckConnectPort"] = "";
	this->KPMap["HealthCheckConnectPort"] = "Query";
	this->KVMap["HealthCheckInterval"] = "";
	this->KPMap["HealthCheckInterval"] = "Query";
	this->KVMap["HealthCheckHttpCode"] = "";
	this->KPMap["HealthCheckHttpCode"] = "Query";
	this->KVMap["MaxConnLimit"] = "";
	this->KPMap["MaxConnLimit"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";

}
SlbCreateLoadBalancerHTTPSListenerRequest::SlbCreateLoadBalancerHTTPSListenerRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "slb";
	this->Action = "CreateLoadBalancerHTTPSListener";
	this->Version = "2014-05-15";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["LoadBalancerId"] = "";
	this->KPMap["LoadBalancerId"] = "Query";
	this->KVMap["Bandwidth"] = "";
	this->KPMap["Bandwidth"] = "Query";
	this->KVMap["ListenerPort"] = "";
	this->KPMap["ListenerPort"] = "Query";
	this->KVMap["BackendServerPort"] = "";
	this->KPMap["BackendServerPort"] = "Query";
	this->KVMap["XForwardedFor"] = "";
	this->KPMap["XForwardedFor"] = "Query";
	this->KVMap["Scheduler"] = "";
	this->KPMap["Scheduler"] = "Query";
	this->KVMap["StickySession"] = "";
	this->KPMap["StickySession"] = "Query";
	this->KVMap["StickySessionType"] = "";
	this->KPMap["StickySessionType"] = "Query";
	this->KVMap["CookieTimeout"] = "";
	this->KPMap["CookieTimeout"] = "Query";
	this->KVMap["Cookie"] = "";
	this->KPMap["Cookie"] = "Query";
	this->KVMap["HealthCheck"] = "";
	this->KPMap["HealthCheck"] = "Query";
	this->KVMap["HealthCheckDomain"] = "";
	this->KPMap["HealthCheckDomain"] = "Query";
	this->KVMap["HealthCheckURI"] = "";
	this->KPMap["HealthCheckURI"] = "Query";
	this->KVMap["HealthyThreshold"] = "";
	this->KPMap["HealthyThreshold"] = "Query";
	this->KVMap["UnhealthyThreshold"] = "";
	this->KPMap["UnhealthyThreshold"] = "Query";
	this->KVMap["HealthCheckTimeout"] = "";
	this->KPMap["HealthCheckTimeout"] = "Query";
	this->KVMap["HealthCheckConnectPort"] = "";
	this->KPMap["HealthCheckConnectPort"] = "Query";
	this->KVMap["HealthCheckInterval"] = "";
	this->KPMap["HealthCheckInterval"] = "Query";
	this->KVMap["HealthCheckHttpCode"] = "";
	this->KPMap["HealthCheckHttpCode"] = "Query";
	this->KVMap["ServerCertificateId"] = "";
	this->KPMap["ServerCertificateId"] = "Query";
	this->KVMap["MaxConnLimit"] = "";
	this->KPMap["MaxConnLimit"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";

}
SlbCreateLoadBalancerTCPListenerRequest::SlbCreateLoadBalancerTCPListenerRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "slb";
	this->Action = "CreateLoadBalancerTCPListener";
	this->Version = "2014-05-15";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["LoadBalancerId"] = "";
	this->KPMap["LoadBalancerId"] = "Query";
	this->KVMap["ListenerPort"] = "";
	this->KPMap["ListenerPort"] = "Query";
	this->KVMap["BackendServerPort"] = "";
	this->KPMap["BackendServerPort"] = "Query";
	this->KVMap["Bandwidth"] = "";
	this->KPMap["Bandwidth"] = "Query";
	this->KVMap["Scheduler"] = "";
	this->KPMap["Scheduler"] = "Query";
	this->KVMap["PersistenceTimeout"] = "";
	this->KPMap["PersistenceTimeout"] = "Query";
	this->KVMap["HealthyThreshold"] = "";
	this->KPMap["HealthyThreshold"] = "Query";
	this->KVMap["UnhealthyThreshold"] = "";
	this->KPMap["UnhealthyThreshold"] = "Query";
	this->KVMap["HealthCheckConnectTimeout"] = "";
	this->KPMap["HealthCheckConnectTimeout"] = "Query";
	this->KVMap["HealthCheckConnectPort"] = "";
	this->KPMap["HealthCheckConnectPort"] = "Query";
	this->KVMap["healthCheckInterval"] = "";
	this->KPMap["healthCheckInterval"] = "Query";
	this->KVMap["HealthCheckDomain"] = "";
	this->KPMap["HealthCheckDomain"] = "Query";
	this->KVMap["HealthCheckURI"] = "";
	this->KPMap["HealthCheckURI"] = "Query";
	this->KVMap["HealthCheckHttpCode"] = "";
	this->KPMap["HealthCheckHttpCode"] = "Query";
	this->KVMap["HealthCheckType"] = "";
	this->KPMap["HealthCheckType"] = "Query";
	this->KVMap["MaxConnLimit"] = "";
	this->KPMap["MaxConnLimit"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";

}
SlbCreateLoadBalancerUDPListenerRequest::SlbCreateLoadBalancerUDPListenerRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "slb";
	this->Action = "CreateLoadBalancerUDPListener";
	this->Version = "2014-05-15";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["LoadBalancerId"] = "";
	this->KPMap["LoadBalancerId"] = "Query";
	this->KVMap["ListenerPort"] = "";
	this->KPMap["ListenerPort"] = "Query";
	this->KVMap["BackendServerPort"] = "";
	this->KPMap["BackendServerPort"] = "Query";
	this->KVMap["Bandwidth"] = "";
	this->KPMap["Bandwidth"] = "Query";
	this->KVMap["Scheduler"] = "";
	this->KPMap["Scheduler"] = "Query";
	this->KVMap["PersistenceTimeout"] = "";
	this->KPMap["PersistenceTimeout"] = "Query";
	this->KVMap["HealthyThreshold"] = "";
	this->KPMap["HealthyThreshold"] = "Query";
	this->KVMap["UnhealthyThreshold"] = "";
	this->KPMap["UnhealthyThreshold"] = "Query";
	this->KVMap["HealthCheckConnectTimeout"] = "";
	this->KPMap["HealthCheckConnectTimeout"] = "Query";
	this->KVMap["HealthCheckConnectPort"] = "";
	this->KPMap["HealthCheckConnectPort"] = "Query";
	this->KVMap["healthCheckInterval"] = "";
	this->KPMap["healthCheckInterval"] = "Query";
	this->KVMap["MaxConnLimit"] = "";
	this->KPMap["MaxConnLimit"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";

}
SlbDeleteLoadBalancerRequest::SlbDeleteLoadBalancerRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "slb";
	this->Action = "DeleteLoadBalancer";
	this->Version = "2014-05-15";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["LoadBalancerId"] = "";
	this->KPMap["LoadBalancerId"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";

}
SlbDeleteLoadBalancerListenerRequest::SlbDeleteLoadBalancerListenerRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "slb";
	this->Action = "DeleteLoadBalancerListener";
	this->Version = "2014-05-15";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["LoadBalancerId"] = "";
	this->KPMap["LoadBalancerId"] = "Query";
	this->KVMap["ListenerPort"] = "";
	this->KPMap["ListenerPort"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";

}
SlbDeleteServerCertificateRequest::SlbDeleteServerCertificateRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "slb";
	this->Action = "DeleteServerCertificate";
	this->Version = "2014-05-15";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["ServerCertificateId"] = "";
	this->KPMap["ServerCertificateId"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";

}
SlbDescribeHealthStatusRequest::SlbDescribeHealthStatusRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "slb";
	this->Action = "DescribeHealthStatus";
	this->Version = "2014-05-15";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["LoadBalancerId"] = "";
	this->KPMap["LoadBalancerId"] = "Query";
	this->KVMap["ListenerPort"] = "";
	this->KPMap["ListenerPort"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";

}
SlbDescribeListenerAccessControlAttributeRequest::SlbDescribeListenerAccessControlAttributeRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "slb";
	this->Action = "DescribeListenerAccessControlAttribute";
	this->Version = "2014-05-15";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["LoadBalancerId"] = "";
	this->KPMap["LoadBalancerId"] = "Query";
	this->KVMap["ListenerPort"] = "";
	this->KPMap["ListenerPort"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";

}
SlbDescribeLoadBalancerAttributeRequest::SlbDescribeLoadBalancerAttributeRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "slb";
	this->Action = "DescribeLoadBalancerAttribute";
	this->Version = "2014-05-15";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["LoadBalancerId"] = "";
	this->KPMap["LoadBalancerId"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";

}
SlbDescribeLoadBalancerHTTPListenerAttributeRequest::SlbDescribeLoadBalancerHTTPListenerAttributeRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "slb";
	this->Action = "DescribeLoadBalancerHTTPListenerAttribute";
	this->Version = "2014-05-15";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["LoadBalancerId"] = "";
	this->KPMap["LoadBalancerId"] = "Query";
	this->KVMap["ListenerPort"] = "";
	this->KPMap["ListenerPort"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";

}
SlbDescribeLoadBalancerHTTPSListenerAttributeRequest::SlbDescribeLoadBalancerHTTPSListenerAttributeRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "slb";
	this->Action = "DescribeLoadBalancerHTTPSListenerAttribute";
	this->Version = "2014-05-15";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["LoadBalancerId"] = "";
	this->KPMap["LoadBalancerId"] = "Query";
	this->KVMap["ListenerPort"] = "";
	this->KPMap["ListenerPort"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";

}
SlbDescribeLoadBalancersRequest::SlbDescribeLoadBalancersRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "slb";
	this->Action = "DescribeLoadBalancers";
	this->Version = "2014-05-15";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["ServerId"] = "";
	this->KPMap["ServerId"] = "Query";
	this->KVMap["LoadBalancerId"] = "";
	this->KPMap["LoadBalancerId"] = "Query";
	this->KVMap["AddressType"] = "";
	this->KPMap["AddressType"] = "Query";
	this->KVMap["InternetChargeType"] = "";
	this->KPMap["InternetChargeType"] = "Query";
	this->KVMap["VpcId"] = "";
	this->KPMap["VpcId"] = "Query";
	this->KVMap["VSwitchId"] = "";
	this->KPMap["VSwitchId"] = "Query";
	this->KVMap["NetworkType"] = "";
	this->KPMap["NetworkType"] = "Query";
	this->KVMap["Address"] = "";
	this->KPMap["Address"] = "Query";
	this->KVMap["SecurityStatus"] = "";
	this->KPMap["SecurityStatus"] = "Query";
	this->KVMap["MasterZoneId"] = "";
	this->KPMap["MasterZoneId"] = "Query";
	this->KVMap["SlaveZoneId"] = "";
	this->KPMap["SlaveZoneId"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";

}
SlbDescribeLoadBalancerTCPListenerAttributeRequest::SlbDescribeLoadBalancerTCPListenerAttributeRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "slb";
	this->Action = "DescribeLoadBalancerTCPListenerAttribute";
	this->Version = "2014-05-15";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["LoadBalancerId"] = "";
	this->KPMap["LoadBalancerId"] = "Query";
	this->KVMap["ListenerPort"] = "";
	this->KPMap["ListenerPort"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";

}
SlbDescribeLoadBalancerUDPListenerAttributeRequest::SlbDescribeLoadBalancerUDPListenerAttributeRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "slb";
	this->Action = "DescribeLoadBalancerUDPListenerAttribute";
	this->Version = "2014-05-15";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["LoadBalancerId"] = "";
	this->KPMap["LoadBalancerId"] = "Query";
	this->KVMap["ListenerPort"] = "";
	this->KPMap["ListenerPort"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";

}
SlbDescribeLocationsRequest::SlbDescribeLocationsRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "slb";
	this->Action = "DescribeLocations";
	this->Version = "2014-05-15";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";
	this->KVMap["Namespace"] = "";
	this->KPMap["Namespace"] = "Query";
	this->KVMap["NamespaceUid"] = "";
	this->KPMap["NamespaceUid"] = "Query";

}
SlbDescribeRegionsRequest::SlbDescribeRegionsRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "slb";
	this->Action = "DescribeRegions";
	this->Version = "2014-05-15";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";

}
SlbDescribeRegions4LocationRequest::SlbDescribeRegions4LocationRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "slb";
	this->Action = "DescribeRegions4Location";
	this->Version = "2014-05-15";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";

}
SlbDescribeServerCertificateRequest::SlbDescribeServerCertificateRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "slb";
	this->Action = "DescribeServerCertificate";
	this->Version = "2014-05-15";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["ServerCertificateId"] = "";
	this->KPMap["ServerCertificateId"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";

}
SlbDescribeServerCertificatesRequest::SlbDescribeServerCertificatesRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "slb";
	this->Action = "DescribeServerCertificates";
	this->Version = "2014-05-15";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["ServerCertificateId"] = "";
	this->KPMap["ServerCertificateId"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";

}
SlbDescribeZonesRequest::SlbDescribeZonesRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "slb";
	this->Action = "DescribeZones";
	this->Version = "2014-05-15";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";

}
SlbModifyLoadBalancerInternetSpecRequest::SlbModifyLoadBalancerInternetSpecRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "slb";
	this->Action = "ModifyLoadBalancerInternetSpec";
	this->Version = "2014-05-15";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["LoadBalancerId"] = "";
	this->KPMap["LoadBalancerId"] = "Query";
	this->KVMap["InternetChargeType"] = "";
	this->KPMap["InternetChargeType"] = "Query";
	this->KVMap["Bandwidth"] = "";
	this->KPMap["Bandwidth"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";
	this->KVMap["MasterZoneId"] = "";
	this->KPMap["MasterZoneId"] = "Query";
	this->KVMap["SlaveZoneId"] = "";
	this->KPMap["SlaveZoneId"] = "Query";
	this->KVMap["MaxConnLimit"] = "";
	this->KPMap["MaxConnLimit"] = "Query";
	this->KVMap["SecurityStatus"] = "";
	this->KPMap["SecurityStatus"] = "Query";

}
SlbRemoveBackendServersRequest::SlbRemoveBackendServersRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "slb";
	this->Action = "RemoveBackendServers";
	this->Version = "2014-05-15";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["LoadBalancerId"] = "";
	this->KPMap["LoadBalancerId"] = "Query";
	this->KVMap["BackendServers"] = "";
	this->KPMap["BackendServers"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";

}
SlbRemoveListenerWhiteListItemRequest::SlbRemoveListenerWhiteListItemRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "slb";
	this->Action = "RemoveListenerWhiteListItem";
	this->Version = "2014-05-15";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["LoadBalancerId"] = "";
	this->KPMap["LoadBalancerId"] = "Query";
	this->KVMap["ListenerPort"] = "";
	this->KPMap["ListenerPort"] = "Query";
	this->KVMap["SourceItems"] = "";
	this->KPMap["SourceItems"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";

}
SlbSetBackendServersRequest::SlbSetBackendServersRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "slb";
	this->Action = "SetBackendServers";
	this->Version = "2014-05-15";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["LoadBalancerId"] = "";
	this->KPMap["LoadBalancerId"] = "Query";
	this->KVMap["BackendServers"] = "";
	this->KPMap["BackendServers"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";

}
SlbSetListenerAccessControlStatusRequest::SlbSetListenerAccessControlStatusRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "slb";
	this->Action = "SetListenerAccessControlStatus";
	this->Version = "2014-05-15";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["LoadBalancerId"] = "";
	this->KPMap["LoadBalancerId"] = "Query";
	this->KVMap["ListenerPort"] = "";
	this->KPMap["ListenerPort"] = "Query";
	this->KVMap["AccessControlStatus"] = "";
	this->KPMap["AccessControlStatus"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";

}
SlbSetLoadBalancerHTTPListenerAttributeRequest::SlbSetLoadBalancerHTTPListenerAttributeRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "slb";
	this->Action = "SetLoadBalancerHTTPListenerAttribute";
	this->Version = "2014-05-15";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["LoadBalancerId"] = "";
	this->KPMap["LoadBalancerId"] = "Query";
	this->KVMap["ListenerPort"] = "";
	this->KPMap["ListenerPort"] = "Query";
	this->KVMap["Bandwidth"] = "";
	this->KPMap["Bandwidth"] = "Query";
	this->KVMap["XForwardedFor"] = "";
	this->KPMap["XForwardedFor"] = "Query";
	this->KVMap["Scheduler"] = "";
	this->KPMap["Scheduler"] = "Query";
	this->KVMap["StickySession"] = "";
	this->KPMap["StickySession"] = "Query";
	this->KVMap["StickySessionType"] = "";
	this->KPMap["StickySessionType"] = "Query";
	this->KVMap["CookieTimeout"] = "";
	this->KPMap["CookieTimeout"] = "Query";
	this->KVMap["Cookie"] = "";
	this->KPMap["Cookie"] = "Query";
	this->KVMap["HealthCheck"] = "";
	this->KPMap["HealthCheck"] = "Query";
	this->KVMap["HealthCheckDomain"] = "";
	this->KPMap["HealthCheckDomain"] = "Query";
	this->KVMap["HealthCheckURI"] = "";
	this->KPMap["HealthCheckURI"] = "Query";
	this->KVMap["HealthyThreshold"] = "";
	this->KPMap["HealthyThreshold"] = "Query";
	this->KVMap["UnhealthyThreshold"] = "";
	this->KPMap["UnhealthyThreshold"] = "Query";
	this->KVMap["HealthCheckTimeout"] = "";
	this->KPMap["HealthCheckTimeout"] = "Query";
	this->KVMap["HealthCheckInterval"] = "";
	this->KPMap["HealthCheckInterval"] = "Query";
	this->KVMap["HealthCheckConnectPort"] = "";
	this->KPMap["HealthCheckConnectPort"] = "Query";
	this->KVMap["HealthCheckHttpCode"] = "";
	this->KPMap["HealthCheckHttpCode"] = "Query";
	this->KVMap["MaxConnLimit"] = "";
	this->KPMap["MaxConnLimit"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";

}
SlbSetLoadBalancerHTTPSListenerAttributeRequest::SlbSetLoadBalancerHTTPSListenerAttributeRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "slb";
	this->Action = "SetLoadBalancerHTTPSListenerAttribute";
	this->Version = "2014-05-15";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["LoadBalancerId"] = "";
	this->KPMap["LoadBalancerId"] = "Query";
	this->KVMap["ListenerPort"] = "";
	this->KPMap["ListenerPort"] = "Query";
	this->KVMap["Bandwidth"] = "";
	this->KPMap["Bandwidth"] = "Query";
	this->KVMap["XForwardedFor"] = "";
	this->KPMap["XForwardedFor"] = "Query";
	this->KVMap["Scheduler"] = "";
	this->KPMap["Scheduler"] = "Query";
	this->KVMap["StickySession"] = "";
	this->KPMap["StickySession"] = "Query";
	this->KVMap["StickySessionType"] = "";
	this->KPMap["StickySessionType"] = "Query";
	this->KVMap["CookieTimeout"] = "";
	this->KPMap["CookieTimeout"] = "Query";
	this->KVMap["Cookie"] = "";
	this->KPMap["Cookie"] = "Query";
	this->KVMap["HealthCheck"] = "";
	this->KPMap["HealthCheck"] = "Query";
	this->KVMap["HealthCheckDomain"] = "";
	this->KPMap["HealthCheckDomain"] = "Query";
	this->KVMap["HealthCheckURI"] = "";
	this->KPMap["HealthCheckURI"] = "Query";
	this->KVMap["HealthyThreshold"] = "";
	this->KPMap["HealthyThreshold"] = "Query";
	this->KVMap["UnhealthyThreshold"] = "";
	this->KPMap["UnhealthyThreshold"] = "Query";
	this->KVMap["HealthCheckTimeout"] = "";
	this->KPMap["HealthCheckTimeout"] = "Query";
	this->KVMap["HealthCheckInterval"] = "";
	this->KPMap["HealthCheckInterval"] = "Query";
	this->KVMap["HealthCheckConnectPort"] = "";
	this->KPMap["HealthCheckConnectPort"] = "Query";
	this->KVMap["HealthCheckHttpCode"] = "";
	this->KPMap["HealthCheckHttpCode"] = "Query";
	this->KVMap["ServerCertificateId"] = "";
	this->KPMap["ServerCertificateId"] = "Query";
	this->KVMap["MaxConnLimit"] = "";
	this->KPMap["MaxConnLimit"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";

}
SlbSetLoadBalancerNameRequest::SlbSetLoadBalancerNameRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "slb";
	this->Action = "SetLoadBalancerName";
	this->Version = "2014-05-15";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["LoadBalancerId"] = "";
	this->KPMap["LoadBalancerId"] = "Query";
	this->KVMap["LoadBalancerName"] = "";
	this->KPMap["LoadBalancerName"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";

}
SlbSetLoadBalancerStatusRequest::SlbSetLoadBalancerStatusRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "slb";
	this->Action = "SetLoadBalancerStatus";
	this->Version = "2014-05-15";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["LoadBalancerId"] = "";
	this->KPMap["LoadBalancerId"] = "Query";
	this->KVMap["LoadBalancerStatus"] = "";
	this->KPMap["LoadBalancerStatus"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";

}
SlbSetLoadBalancerTCPListenerAttributeRequest::SlbSetLoadBalancerTCPListenerAttributeRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "slb";
	this->Action = "SetLoadBalancerTCPListenerAttribute";
	this->Version = "2014-05-15";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["LoadBalancerId"] = "";
	this->KPMap["LoadBalancerId"] = "Query";
	this->KVMap["ListenerPort"] = "";
	this->KPMap["ListenerPort"] = "Query";
	this->KVMap["Bandwidth"] = "";
	this->KPMap["Bandwidth"] = "Query";
	this->KVMap["Scheduler"] = "";
	this->KPMap["Scheduler"] = "Query";
	this->KVMap["PersistenceTimeout"] = "";
	this->KPMap["PersistenceTimeout"] = "Query";
	this->KVMap["HealthyThreshold"] = "";
	this->KPMap["HealthyThreshold"] = "Query";
	this->KVMap["UnhealthyThreshold"] = "";
	this->KPMap["UnhealthyThreshold"] = "Query";
	this->KVMap["HealthCheckConnectTimeout"] = "";
	this->KPMap["HealthCheckConnectTimeout"] = "Query";
	this->KVMap["HealthCheckConnectPort"] = "";
	this->KPMap["HealthCheckConnectPort"] = "Query";
	this->KVMap["HealthCheckInterval"] = "";
	this->KPMap["HealthCheckInterval"] = "Query";
	this->KVMap["HealthCheckDomain"] = "";
	this->KPMap["HealthCheckDomain"] = "Query";
	this->KVMap["HealthCheckURI"] = "";
	this->KPMap["HealthCheckURI"] = "Query";
	this->KVMap["HealthCheckHttpCode"] = "";
	this->KPMap["HealthCheckHttpCode"] = "Query";
	this->KVMap["HealthCheckType"] = "";
	this->KPMap["HealthCheckType"] = "Query";
	this->KVMap["SynProxy"] = "";
	this->KPMap["SynProxy"] = "Query";
	this->KVMap["MaxConnLimit"] = "";
	this->KPMap["MaxConnLimit"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";

}
SlbSetLoadBalancerUDPListenerAttributeRequest::SlbSetLoadBalancerUDPListenerAttributeRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "slb";
	this->Action = "SetLoadBalancerUDPListenerAttribute";
	this->Version = "2014-05-15";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["LoadBalancerId"] = "";
	this->KPMap["LoadBalancerId"] = "Query";
	this->KVMap["ListenerPort"] = "";
	this->KPMap["ListenerPort"] = "Query";
	this->KVMap["Bandwidth"] = "";
	this->KPMap["Bandwidth"] = "Query";
	this->KVMap["Scheduler"] = "";
	this->KPMap["Scheduler"] = "Query";
	this->KVMap["PersistenceTimeout"] = "";
	this->KPMap["PersistenceTimeout"] = "Query";
	this->KVMap["HealthyThreshold"] = "";
	this->KPMap["HealthyThreshold"] = "Query";
	this->KVMap["UnhealthyThreshold"] = "";
	this->KPMap["UnhealthyThreshold"] = "Query";
	this->KVMap["HealthCheckConnectTimeout"] = "";
	this->KPMap["HealthCheckConnectTimeout"] = "Query";
	this->KVMap["HealthCheckConnectPort"] = "";
	this->KPMap["HealthCheckConnectPort"] = "Query";
	this->KVMap["HealthCheckInterval"] = "";
	this->KPMap["HealthCheckInterval"] = "Query";
	this->KVMap["MaxConnLimit"] = "";
	this->KPMap["MaxConnLimit"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";

}
SlbSetServerCertificateNameRequest::SlbSetServerCertificateNameRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "slb";
	this->Action = "SetServerCertificateName";
	this->Version = "2014-05-15";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["ServerCertificateId"] = "";
	this->KPMap["ServerCertificateId"] = "Query";
	this->KVMap["ServerCertificateName"] = "";
	this->KPMap["ServerCertificateName"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";

}
SlbStartLoadBalancerListenerRequest::SlbStartLoadBalancerListenerRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "slb";
	this->Action = "StartLoadBalancerListener";
	this->Version = "2014-05-15";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["LoadBalancerId"] = "";
	this->KPMap["LoadBalancerId"] = "Query";
	this->KVMap["ListenerPort"] = "";
	this->KPMap["ListenerPort"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";

}
SlbStopLoadBalancerListenerRequest::SlbStopLoadBalancerListenerRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "slb";
	this->Action = "StopLoadBalancerListener";
	this->Version = "2014-05-15";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["LoadBalancerId"] = "";
	this->KPMap["LoadBalancerId"] = "Query";
	this->KVMap["ListenerPort"] = "";
	this->KPMap["ListenerPort"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";

}
SlbUploadServerCertificateRequest::SlbUploadServerCertificateRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "slb";
	this->Action = "UploadServerCertificate";
	this->Version = "2014-05-15";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["ServerCertificate"] = "";
	this->KPMap["ServerCertificate"] = "Query";
	this->KVMap["PrivateKey"] = "";
	this->KPMap["PrivateKey"] = "Query";
	this->KVMap["ServerCertificateName"] = "";
	this->KPMap["ServerCertificateName"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";

}

	
}