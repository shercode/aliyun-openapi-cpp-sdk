/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
 
#ifndef ALIYUNSLB_SLBSERVICE_H_
#define ALIYUNSLB_SLBSERVICE_H_

#include "../aliyun-cpp-sdk-core/BaseRequest.h"
#include "../aliyun-cpp-sdk-core/ApiBase.h"

namespace AliYunSlb {

class SlbClient : public AliYunCore::ApiBase {
private:
	AliYunCore::DefaultProfile &Profile;
public:
	SlbClient(AliYunCore::DefaultProfile &pf);
	void setProfile(AliYunCore::DefaultProfile &pf);
	std::string getResponse(AliYunCore::BaseRequest &request);
	~SlbClient(){};
};
class SlbAddBackendServersRequest : public AliYunCore::BaseRequest {
public:
	SlbAddBackendServersRequest();
};
class SlbAddListenerWhiteListItemRequest : public AliYunCore::BaseRequest {
public:
	SlbAddListenerWhiteListItemRequest();
};
class SlbCreateLoadBalancerRequest : public AliYunCore::BaseRequest {
public:
	SlbCreateLoadBalancerRequest();
};
class SlbCreateLoadBalancerHTTPListenerRequest : public AliYunCore::BaseRequest {
public:
	SlbCreateLoadBalancerHTTPListenerRequest();
};
class SlbCreateLoadBalancerHTTPSListenerRequest : public AliYunCore::BaseRequest {
public:
	SlbCreateLoadBalancerHTTPSListenerRequest();
};
class SlbCreateLoadBalancerTCPListenerRequest : public AliYunCore::BaseRequest {
public:
	SlbCreateLoadBalancerTCPListenerRequest();
};
class SlbCreateLoadBalancerUDPListenerRequest : public AliYunCore::BaseRequest {
public:
	SlbCreateLoadBalancerUDPListenerRequest();
};
class SlbDeleteLoadBalancerRequest : public AliYunCore::BaseRequest {
public:
	SlbDeleteLoadBalancerRequest();
};
class SlbDeleteLoadBalancerListenerRequest : public AliYunCore::BaseRequest {
public:
	SlbDeleteLoadBalancerListenerRequest();
};
class SlbDeleteServerCertificateRequest : public AliYunCore::BaseRequest {
public:
	SlbDeleteServerCertificateRequest();
};
class SlbDescribeHealthStatusRequest : public AliYunCore::BaseRequest {
public:
	SlbDescribeHealthStatusRequest();
};
class SlbDescribeListenerAccessControlAttributeRequest : public AliYunCore::BaseRequest {
public:
	SlbDescribeListenerAccessControlAttributeRequest();
};
class SlbDescribeLoadBalancerAttributeRequest : public AliYunCore::BaseRequest {
public:
	SlbDescribeLoadBalancerAttributeRequest();
};
class SlbDescribeLoadBalancerHTTPListenerAttributeRequest : public AliYunCore::BaseRequest {
public:
	SlbDescribeLoadBalancerHTTPListenerAttributeRequest();
};
class SlbDescribeLoadBalancerHTTPSListenerAttributeRequest : public AliYunCore::BaseRequest {
public:
	SlbDescribeLoadBalancerHTTPSListenerAttributeRequest();
};
class SlbDescribeLoadBalancersRequest : public AliYunCore::BaseRequest {
public:
	SlbDescribeLoadBalancersRequest();
};
class SlbDescribeLoadBalancerTCPListenerAttributeRequest : public AliYunCore::BaseRequest {
public:
	SlbDescribeLoadBalancerTCPListenerAttributeRequest();
};
class SlbDescribeLoadBalancerUDPListenerAttributeRequest : public AliYunCore::BaseRequest {
public:
	SlbDescribeLoadBalancerUDPListenerAttributeRequest();
};
class SlbDescribeLocationsRequest : public AliYunCore::BaseRequest {
public:
	SlbDescribeLocationsRequest();
};
class SlbDescribeRegionsRequest : public AliYunCore::BaseRequest {
public:
	SlbDescribeRegionsRequest();
};
class SlbDescribeRegions4LocationRequest : public AliYunCore::BaseRequest {
public:
	SlbDescribeRegions4LocationRequest();
};
class SlbDescribeServerCertificateRequest : public AliYunCore::BaseRequest {
public:
	SlbDescribeServerCertificateRequest();
};
class SlbDescribeServerCertificatesRequest : public AliYunCore::BaseRequest {
public:
	SlbDescribeServerCertificatesRequest();
};
class SlbDescribeZonesRequest : public AliYunCore::BaseRequest {
public:
	SlbDescribeZonesRequest();
};
class SlbModifyLoadBalancerInternetSpecRequest : public AliYunCore::BaseRequest {
public:
	SlbModifyLoadBalancerInternetSpecRequest();
};
class SlbRemoveBackendServersRequest : public AliYunCore::BaseRequest {
public:
	SlbRemoveBackendServersRequest();
};
class SlbRemoveListenerWhiteListItemRequest : public AliYunCore::BaseRequest {
public:
	SlbRemoveListenerWhiteListItemRequest();
};
class SlbSetBackendServersRequest : public AliYunCore::BaseRequest {
public:
	SlbSetBackendServersRequest();
};
class SlbSetListenerAccessControlStatusRequest : public AliYunCore::BaseRequest {
public:
	SlbSetListenerAccessControlStatusRequest();
};
class SlbSetLoadBalancerHTTPListenerAttributeRequest : public AliYunCore::BaseRequest {
public:
	SlbSetLoadBalancerHTTPListenerAttributeRequest();
};
class SlbSetLoadBalancerHTTPSListenerAttributeRequest : public AliYunCore::BaseRequest {
public:
	SlbSetLoadBalancerHTTPSListenerAttributeRequest();
};
class SlbSetLoadBalancerNameRequest : public AliYunCore::BaseRequest {
public:
	SlbSetLoadBalancerNameRequest();
};
class SlbSetLoadBalancerStatusRequest : public AliYunCore::BaseRequest {
public:
	SlbSetLoadBalancerStatusRequest();
};
class SlbSetLoadBalancerTCPListenerAttributeRequest : public AliYunCore::BaseRequest {
public:
	SlbSetLoadBalancerTCPListenerAttributeRequest();
};
class SlbSetLoadBalancerUDPListenerAttributeRequest : public AliYunCore::BaseRequest {
public:
	SlbSetLoadBalancerUDPListenerAttributeRequest();
};
class SlbSetServerCertificateNameRequest : public AliYunCore::BaseRequest {
public:
	SlbSetServerCertificateNameRequest();
};
class SlbStartLoadBalancerListenerRequest : public AliYunCore::BaseRequest {
public:
	SlbStartLoadBalancerListenerRequest();
};
class SlbStopLoadBalancerListenerRequest : public AliYunCore::BaseRequest {
public:
	SlbStopLoadBalancerListenerRequest();
};
class SlbUploadServerCertificateRequest : public AliYunCore::BaseRequest {
public:
	SlbUploadServerCertificateRequest();
};


}

#endif