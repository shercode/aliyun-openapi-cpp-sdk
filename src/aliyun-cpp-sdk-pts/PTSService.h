/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
 
#ifndef ALIYUNPTS_PTSSERVICE_H_
#define ALIYUNPTS_PTSSERVICE_H_

#include "../aliyun-cpp-sdk-core/BaseRequest.h"
#include "../aliyun-cpp-sdk-core/ApiBase.h"

namespace AliYunPTS {

class PTSClient : public AliYunCore::ApiBase {
private:
	AliYunCore::DefaultProfile &Profile;
public:
	PTSClient(AliYunCore::DefaultProfile &pf);
	void setProfile(AliYunCore::DefaultProfile &pf);
	std::string getResponse(AliYunCore::BaseRequest &request);
	~PTSClient(){};
};
class PTSCreateTransactionRequest : public AliYunCore::BaseRequest {
public:
	PTSCreateTransactionRequest();
};
class PTSGetKeySecretRequest : public AliYunCore::BaseRequest {
public:
	PTSGetKeySecretRequest();
};
class PTSGetScriptRequest : public AliYunCore::BaseRequest {
public:
	PTSGetScriptRequest();
};
class PTSGetTasksRequest : public AliYunCore::BaseRequest {
public:
	PTSGetTasksRequest();
};
class PTSReportLogSampleRequest : public AliYunCore::BaseRequest {
public:
	PTSReportLogSampleRequest();
};
class PTSReportTestSampleRequest : public AliYunCore::BaseRequest {
public:
	PTSReportTestSampleRequest();
};
class PTSReportVuserRequest : public AliYunCore::BaseRequest {
public:
	PTSReportVuserRequest();
};
class PTSSendWangWangRequest : public AliYunCore::BaseRequest {
public:
	PTSSendWangWangRequest();
};
class PTSSetScenarioStatusRequest : public AliYunCore::BaseRequest {
public:
	PTSSetScenarioStatusRequest();
};
class PTSSetTaskStatusRequest : public AliYunCore::BaseRequest {
public:
	PTSSetTaskStatusRequest();
};
class PTSStopTaskRequest : public AliYunCore::BaseRequest {
public:
	PTSStopTaskRequest();
};


}

#endif