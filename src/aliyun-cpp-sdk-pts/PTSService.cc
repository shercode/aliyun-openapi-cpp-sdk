/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

#include <string>
#include <map>
#include "../aliyun-cpp-sdk-core/utils.h"
#include "../aliyun-cpp-sdk-core/DefaultProfile.h"
#include "../aliyun-cpp-sdk-core/BaseRequest.h"
#include "../aliyun-cpp-sdk-core/ApiBase.h"
#include "PTSService.h"

using std::string;
using namespace AliYunCore;

namespace AliYunPTS {

PTSClient::PTSClient(DefaultProfile &pf):Profile(pf){}
void PTSClient::setProfile(DefaultProfile &pf){
	this->Profile = pf;
}
string PTSClient::getResponse(BaseRequest &request){
	request.RegionId = this->Profile.RegionId;		
	request.AccessKeyId = this->Profile.AccessKeyId;
	request.AccessKeySecret = this->Profile.AccessKeySecret;
	return ApiBase::getResponse(request);
}
PTSCreateTransactionRequest::PTSCreateTransactionRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "pts";
	this->Action = "CreateTransaction";
	this->Version = "2015-08-01";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["ScriptId"] = "";
	this->KPMap["ScriptId"] = "Query";
	this->KVMap["TransactionName"] = "";
	this->KPMap["TransactionName"] = "Query";

}
PTSGetKeySecretRequest::PTSGetKeySecretRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "pts";
	this->Action = "GetKeySecret";
	this->Version = "2015-08-01";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 

}
PTSGetScriptRequest::PTSGetScriptRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "pts";
	this->Action = "GetScript";
	this->Version = "2015-08-01";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["ScriptId"] = "";
	this->KPMap["ScriptId"] = "Query";
	this->KVMap["Tfsname"] = "";
	this->KPMap["Tfsname"] = "Query";

}
PTSGetTasksRequest::PTSGetTasksRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "pts";
	this->Action = "GetTasks";
	this->Version = "2015-08-01";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["Status"] = "";
	this->KPMap["Status"] = "Query";

}
PTSReportLogSampleRequest::PTSReportLogSampleRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "pts";
	this->Action = "ReportLogSample";
	this->Version = "2015-08-01";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["Wskey"] = "";
	this->KPMap["Wskey"] = "Query";
	this->KVMap["ScenarioId"] = "";
	this->KPMap["ScenarioId"] = "Query";
	this->KVMap["LogSample"] = "";
	this->KPMap["LogSample"] = "Query";

}
PTSReportTestSampleRequest::PTSReportTestSampleRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "pts";
	this->Action = "ReportTestSample";
	this->Version = "2015-08-01";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["TestSample"] = "";
	this->KPMap["TestSample"] = "Query";

}
PTSReportVuserRequest::PTSReportVuserRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "pts";
	this->Action = "ReportVuser";
	this->Version = "2015-08-01";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["Wskey"] = "";
	this->KPMap["Wskey"] = "Query";
	this->KVMap["ScenarioId"] = "";
	this->KPMap["ScenarioId"] = "Query";
	this->KVMap["Vuser"] = "";
	this->KPMap["Vuser"] = "Query";
	this->KVMap["GmtCreated"] = "";
	this->KPMap["GmtCreated"] = "Query";

}
PTSSendWangWangRequest::PTSSendWangWangRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "pts";
	this->Action = "SendWangWang";
	this->Version = "2015-08-01";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["To"] = "";
	this->KPMap["To"] = "Query";
	this->KVMap["Title"] = "";
	this->KPMap["Title"] = "Query";
	this->KVMap["Msg"] = "";
	this->KPMap["Msg"] = "Query";

}
PTSSetScenarioStatusRequest::PTSSetScenarioStatusRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "pts";
	this->Action = "SetScenarioStatus";
	this->Version = "2015-08-01";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["Wskey"] = "";
	this->KPMap["Wskey"] = "Query";
	this->KVMap["ScenarioId"] = "";
	this->KPMap["ScenarioId"] = "Query";
	this->KVMap["Status"] = "";
	this->KPMap["Status"] = "Query";
	this->KVMap["NodeIp"] = "";
	this->KPMap["NodeIp"] = "Query";

}
PTSSetTaskStatusRequest::PTSSetTaskStatusRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "pts";
	this->Action = "SetTaskStatus";
	this->Version = "2015-08-01";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["Wskey"] = "";
	this->KPMap["Wskey"] = "Query";
	this->KVMap["Status"] = "";
	this->KPMap["Status"] = "Query";

}
PTSStopTaskRequest::PTSStopTaskRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "pts";
	this->Action = "StopTask";
	this->Version = "2015-08-01";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["TaskId"] = "";
	this->KPMap["TaskId"] = "Query";
	this->KVMap["Type"] = "";
	this->KPMap["Type"] = "Query";
	this->KVMap["Msg"] = "";
	this->KPMap["Msg"] = "Query";

}

	
}