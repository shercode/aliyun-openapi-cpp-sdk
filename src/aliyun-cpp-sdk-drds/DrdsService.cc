/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

#include <string>
#include <map>
#include "../aliyun-cpp-sdk-core/utils.h"
#include "../aliyun-cpp-sdk-core/DefaultProfile.h"
#include "../aliyun-cpp-sdk-core/BaseRequest.h"
#include "../aliyun-cpp-sdk-core/ApiBase.h"
#include "DrdsService.h"

using std::string;
using namespace AliYunCore;

namespace AliYunDrds {

DrdsClient::DrdsClient(DefaultProfile &pf):Profile(pf){}
void DrdsClient::setProfile(DefaultProfile &pf){
	this->Profile = pf;
}
string DrdsClient::getResponse(BaseRequest &request){
	request.RegionId = this->Profile.RegionId;		
	request.AccessKeyId = this->Profile.AccessKeyId;
	request.AccessKeySecret = this->Profile.AccessKeySecret;
	return ApiBase::getResponse(request);
}
DrdsAlterTableRequest::DrdsAlterTableRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "drds";
	this->Action = "AlterTable";
	this->Version = "2015-04-13";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["DrdsInstanceId"] = "";
	this->KPMap["DrdsInstanceId"] = "Query";
	this->KVMap["DbName"] = "";
	this->KPMap["DbName"] = "Query";
	this->KVMap["DdlSql"] = "";
	this->KPMap["DdlSql"] = "Query";

}
DrdsCancelDDLTaskRequest::DrdsCancelDDLTaskRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "drds";
	this->Action = "CancelDDLTask";
	this->Version = "2015-04-13";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["DrdsInstanceId"] = "";
	this->KPMap["DrdsInstanceId"] = "Query";
	this->KVMap["DbName"] = "";
	this->KPMap["DbName"] = "Query";
	this->KVMap["TaskId"] = "";
	this->KPMap["TaskId"] = "Query";

}
DrdsCreateDrdsDBRequest::DrdsCreateDrdsDBRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "drds";
	this->Action = "CreateDrdsDB";
	this->Version = "2015-04-13";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["DrdsInstanceId"] = "";
	this->KPMap["DrdsInstanceId"] = "Query";
	this->KVMap["DbName"] = "";
	this->KPMap["DbName"] = "Query";
	this->KVMap["Encode"] = "";
	this->KPMap["Encode"] = "Query";
	this->KVMap["Password"] = "";
	this->KPMap["Password"] = "Query";
	this->KVMap["RdsInstances"] = "";
	this->KPMap["RdsInstances"] = "Query";

}
DrdsCreateDrdsInstanceRequest::DrdsCreateDrdsInstanceRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "drds";
	this->Action = "CreateDrdsInstance";
	this->Version = "2015-04-13";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["Description"] = "";
	this->KPMap["Description"] = "Query";
	this->KVMap["ZoneId"] = "";
	this->KPMap["ZoneId"] = "Query";
	this->KVMap["Type"] = "";
	this->KPMap["Type"] = "Query";
	this->KVMap["Quantity"] = "";
	this->KPMap["Quantity"] = "Query";
	this->KVMap["Specification"] = "";
	this->KPMap["Specification"] = "Query";
	this->KVMap["PayType"] = "";
	this->KPMap["PayType"] = "Query";
	this->KVMap["VpcId"] = "";
	this->KPMap["VpcId"] = "Query";
	this->KVMap["VswitchId"] = "";
	this->KPMap["VswitchId"] = "Query";

}
DrdsCreateIndexRequest::DrdsCreateIndexRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "drds";
	this->Action = "CreateIndex";
	this->Version = "2015-04-13";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["DrdsInstanceId"] = "";
	this->KPMap["DrdsInstanceId"] = "Query";
	this->KVMap["DbName"] = "";
	this->KPMap["DbName"] = "Query";
	this->KVMap["DdlSql"] = "";
	this->KPMap["DdlSql"] = "Query";

}
DrdsCreateTableRequest::DrdsCreateTableRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "drds";
	this->Action = "CreateTable";
	this->Version = "2015-04-13";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["DrdsInstanceId"] = "";
	this->KPMap["DrdsInstanceId"] = "Query";
	this->KVMap["DbName"] = "";
	this->KPMap["DbName"] = "Query";
	this->KVMap["DdlSql"] = "";
	this->KPMap["DdlSql"] = "Query";
	this->KVMap["ShardType"] = "";
	this->KPMap["ShardType"] = "Query";
	this->KVMap["ShardKey"] = "";
	this->KPMap["ShardKey"] = "Query";
	this->KVMap["AllowFullTableScan"] = "";
	this->KPMap["AllowFullTableScan"] = "Query";

}
DrdsDeleteDrdsDBRequest::DrdsDeleteDrdsDBRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "drds";
	this->Action = "DeleteDrdsDB";
	this->Version = "2015-04-13";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["DrdsInstanceId"] = "";
	this->KPMap["DrdsInstanceId"] = "Query";
	this->KVMap["DbName"] = "";
	this->KPMap["DbName"] = "Query";

}
DrdsDescribeCreateDrdsInstanceStatusRequest::DrdsDescribeCreateDrdsInstanceStatusRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "drds";
	this->Action = "DescribeCreateDrdsInstanceStatus";
	this->Version = "2015-04-13";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["DrdsInstanceId"] = "";
	this->KPMap["DrdsInstanceId"] = "Query";

}
DrdsDescribeDDLTaskRequest::DrdsDescribeDDLTaskRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "drds";
	this->Action = "DescribeDDLTask";
	this->Version = "2015-04-13";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["DrdsInstanceId"] = "";
	this->KPMap["DrdsInstanceId"] = "Query";
	this->KVMap["DbName"] = "";
	this->KPMap["DbName"] = "Query";
	this->KVMap["TaskId"] = "";
	this->KPMap["TaskId"] = "Query";

}
DrdsDescribeDrdsDBRequest::DrdsDescribeDrdsDBRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "drds";
	this->Action = "DescribeDrdsDB";
	this->Version = "2015-04-13";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["DrdsInstanceId"] = "";
	this->KPMap["DrdsInstanceId"] = "Query";
	this->KVMap["DbName"] = "";
	this->KPMap["DbName"] = "Query";

}
DrdsDescribeDrdsDBIpWhiteListRequest::DrdsDescribeDrdsDBIpWhiteListRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "drds";
	this->Action = "DescribeDrdsDBIpWhiteList";
	this->Version = "2015-04-13";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["DrdsInstanceId"] = "";
	this->KPMap["DrdsInstanceId"] = "Query";
	this->KVMap["DbName"] = "";
	this->KPMap["DbName"] = "Query";

}
DrdsDescribeDrdsDBsRequest::DrdsDescribeDrdsDBsRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "drds";
	this->Action = "DescribeDrdsDBs";
	this->Version = "2015-04-13";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["DrdsInstanceId"] = "";
	this->KPMap["DrdsInstanceId"] = "Query";

}
DrdsDescribeDrdsInstanceRequest::DrdsDescribeDrdsInstanceRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "drds";
	this->Action = "DescribeDrdsInstance";
	this->Version = "2015-04-13";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["DrdsInstanceId"] = "";
	this->KPMap["DrdsInstanceId"] = "Query";

}
DrdsDescribeDrdsInstancesRequest::DrdsDescribeDrdsInstancesRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "drds";
	this->Action = "DescribeDrdsInstances";
	this->Version = "2015-04-13";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["Type"] = "";
	this->KPMap["Type"] = "Query";

}
DrdsDropIndexesRequest::DrdsDropIndexesRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "drds";
	this->Action = "DropIndexes";
	this->Version = "2015-04-13";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["DrdsInstanceId"] = "";
	this->KPMap["DrdsInstanceId"] = "Query";
	this->KVMap["DbName"] = "";
	this->KPMap["DbName"] = "Query";
	this->KVMap["Table"] = "";
	this->KPMap["Table"] = "Query";
	this->KVMap["Indexes"] = "";
	this->KPMap["Indexes"] = "Query";

}
DrdsDropTablesRequest::DrdsDropTablesRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "drds";
	this->Action = "DropTables";
	this->Version = "2015-04-13";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["DrdsInstanceId"] = "";
	this->KPMap["DrdsInstanceId"] = "Query";
	this->KVMap["DbName"] = "";
	this->KPMap["DbName"] = "Query";
	this->KVMap["Tables"] = "";
	this->KPMap["Tables"] = "Query";

}
DrdsListUnCompleteTasksRequest::DrdsListUnCompleteTasksRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "drds";
	this->Action = "ListUnCompleteTasks";
	this->Version = "2015-04-13";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["DrdsInstanceId"] = "";
	this->KPMap["DrdsInstanceId"] = "Query";
	this->KVMap["DbName"] = "";
	this->KPMap["DbName"] = "Query";

}
DrdsModifyDrdsDBPasswdRequest::DrdsModifyDrdsDBPasswdRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "drds";
	this->Action = "ModifyDrdsDBPasswd";
	this->Version = "2015-04-13";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["DrdsInstanceId"] = "";
	this->KPMap["DrdsInstanceId"] = "Query";
	this->KVMap["DbName"] = "";
	this->KPMap["DbName"] = "Query";
	this->KVMap["NewPasswd"] = "";
	this->KPMap["NewPasswd"] = "Query";

}
DrdsModifyDrdsInstanceDescriptionRequest::DrdsModifyDrdsInstanceDescriptionRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "drds";
	this->Action = "ModifyDrdsInstanceDescription";
	this->Version = "2015-04-13";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["DrdsInstanceId"] = "";
	this->KPMap["DrdsInstanceId"] = "Query";
	this->KVMap["Description"] = "";
	this->KPMap["Description"] = "Query";

}
DrdsModifyDrdsIpWhiteListRequest::DrdsModifyDrdsIpWhiteListRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "drds";
	this->Action = "ModifyDrdsIpWhiteList";
	this->Version = "2015-04-13";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["DrdsInstanceId"] = "";
	this->KPMap["DrdsInstanceId"] = "Query";
	this->KVMap["DbName"] = "";
	this->KPMap["DbName"] = "Query";
	this->KVMap["IpWhiteList"] = "";
	this->KPMap["IpWhiteList"] = "Query";
	this->KVMap["Mode"] = "";
	this->KPMap["Mode"] = "Query";

}
DrdsRemoveDrdsInstanceRequest::DrdsRemoveDrdsInstanceRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "drds";
	this->Action = "RemoveDrdsInstance";
	this->Version = "2015-04-13";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["DrdsInstanceId"] = "";
	this->KPMap["DrdsInstanceId"] = "Query";

}

	
}