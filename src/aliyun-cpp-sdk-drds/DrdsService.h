/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
 
#ifndef ALIYUNDRDS_DRDSSERVICE_H_
#define ALIYUNDRDS_DRDSSERVICE_H_

#include "../aliyun-cpp-sdk-core/BaseRequest.h"
#include "../aliyun-cpp-sdk-core/ApiBase.h"

namespace AliYunDrds {

class DrdsClient : public AliYunCore::ApiBase {
private:
	AliYunCore::DefaultProfile &Profile;
public:
	DrdsClient(AliYunCore::DefaultProfile &pf);
	void setProfile(AliYunCore::DefaultProfile &pf);
	std::string getResponse(AliYunCore::BaseRequest &request);
	~DrdsClient(){};
};
class DrdsAlterTableRequest : public AliYunCore::BaseRequest {
public:
	DrdsAlterTableRequest();
};
class DrdsCancelDDLTaskRequest : public AliYunCore::BaseRequest {
public:
	DrdsCancelDDLTaskRequest();
};
class DrdsCreateDrdsDBRequest : public AliYunCore::BaseRequest {
public:
	DrdsCreateDrdsDBRequest();
};
class DrdsCreateDrdsInstanceRequest : public AliYunCore::BaseRequest {
public:
	DrdsCreateDrdsInstanceRequest();
};
class DrdsCreateIndexRequest : public AliYunCore::BaseRequest {
public:
	DrdsCreateIndexRequest();
};
class DrdsCreateTableRequest : public AliYunCore::BaseRequest {
public:
	DrdsCreateTableRequest();
};
class DrdsDeleteDrdsDBRequest : public AliYunCore::BaseRequest {
public:
	DrdsDeleteDrdsDBRequest();
};
class DrdsDescribeCreateDrdsInstanceStatusRequest : public AliYunCore::BaseRequest {
public:
	DrdsDescribeCreateDrdsInstanceStatusRequest();
};
class DrdsDescribeDDLTaskRequest : public AliYunCore::BaseRequest {
public:
	DrdsDescribeDDLTaskRequest();
};
class DrdsDescribeDrdsDBRequest : public AliYunCore::BaseRequest {
public:
	DrdsDescribeDrdsDBRequest();
};
class DrdsDescribeDrdsDBIpWhiteListRequest : public AliYunCore::BaseRequest {
public:
	DrdsDescribeDrdsDBIpWhiteListRequest();
};
class DrdsDescribeDrdsDBsRequest : public AliYunCore::BaseRequest {
public:
	DrdsDescribeDrdsDBsRequest();
};
class DrdsDescribeDrdsInstanceRequest : public AliYunCore::BaseRequest {
public:
	DrdsDescribeDrdsInstanceRequest();
};
class DrdsDescribeDrdsInstancesRequest : public AliYunCore::BaseRequest {
public:
	DrdsDescribeDrdsInstancesRequest();
};
class DrdsDropIndexesRequest : public AliYunCore::BaseRequest {
public:
	DrdsDropIndexesRequest();
};
class DrdsDropTablesRequest : public AliYunCore::BaseRequest {
public:
	DrdsDropTablesRequest();
};
class DrdsListUnCompleteTasksRequest : public AliYunCore::BaseRequest {
public:
	DrdsListUnCompleteTasksRequest();
};
class DrdsModifyDrdsDBPasswdRequest : public AliYunCore::BaseRequest {
public:
	DrdsModifyDrdsDBPasswdRequest();
};
class DrdsModifyDrdsInstanceDescriptionRequest : public AliYunCore::BaseRequest {
public:
	DrdsModifyDrdsInstanceDescriptionRequest();
};
class DrdsModifyDrdsIpWhiteListRequest : public AliYunCore::BaseRequest {
public:
	DrdsModifyDrdsIpWhiteListRequest();
};
class DrdsRemoveDrdsInstanceRequest : public AliYunCore::BaseRequest {
public:
	DrdsRemoveDrdsInstanceRequest();
};


}

#endif