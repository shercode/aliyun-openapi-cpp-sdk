/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

#include <string>
#include <map>
#include "../aliyun-cpp-sdk-core/utils.h"
#include "../aliyun-cpp-sdk-core/DefaultProfile.h"
#include "../aliyun-cpp-sdk-core/BaseRequest.h"
#include "../aliyun-cpp-sdk-core/ApiBase.h"
#include "RKvstoreService.h"

using std::string;
using namespace AliYunCore;

namespace AliYunRKvstore {

RKvstoreClient::RKvstoreClient(DefaultProfile &pf):Profile(pf){}
void RKvstoreClient::setProfile(DefaultProfile &pf){
	this->Profile = pf;
}
string RKvstoreClient::getResponse(BaseRequest &request){
	request.RegionId = this->Profile.RegionId;		
	request.AccessKeyId = this->Profile.AccessKeyId;
	request.AccessKeySecret = this->Profile.AccessKeySecret;
	return ApiBase::getResponse(request);
}
RKvstoreActivateInstanceRequest::RKvstoreActivateInstanceRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "r-kvstore";
	this->Action = "ActivateInstance";
	this->Version = "2015-01-01";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";
	this->KVMap["InstanceId"] = "";
	this->KPMap["InstanceId"] = "Query";

}
RKvstoreCreateInstanceRequest::RKvstoreCreateInstanceRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "r-kvstore";
	this->Action = "CreateInstance";
	this->Version = "2015-01-01";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";
	this->KVMap["InstanceName"] = "";
	this->KPMap["InstanceName"] = "Query";
	this->KVMap["Password"] = "";
	this->KPMap["Password"] = "Query";
	this->KVMap["Capacity"] = "";
	this->KPMap["Capacity"] = "Query";
	this->KVMap["ZoneId"] = "";
	this->KPMap["ZoneId"] = "Query";
	this->KVMap["Config"] = "";
	this->KPMap["Config"] = "Query";
	this->KVMap["ChargeType"] = "";
	this->KPMap["ChargeType"] = "Query";
	this->KVMap["Period"] = "";
	this->KPMap["Period"] = "Query";
	this->KVMap["Token"] = "";
	this->KPMap["Token"] = "Query";

}
RKvstoreCreateInstancesRequest::RKvstoreCreateInstancesRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "r-kvstore";
	this->Action = "CreateInstances";
	this->Version = "2015-01-01";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";
	this->KVMap["Instances"] = "";
	this->KPMap["Instances"] = "Query";
	this->KVMap["Token"] = "";
	this->KPMap["Token"] = "Query";
	this->KVMap["AutoPay"] = "";
	this->KPMap["AutoPay"] = "Query";

}
RKvstoreDataOperateRequest::RKvstoreDataOperateRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "r-kvstore";
	this->Action = "DataOperate";
	this->Version = "2015-01-01";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";
	this->KVMap["InstanceId"] = "";
	this->KPMap["InstanceId"] = "Query";
	this->KVMap["Command"] = "";
	this->KPMap["Command"] = "Query";

}
RKvstoreDeactivateInstanceRequest::RKvstoreDeactivateInstanceRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "r-kvstore";
	this->Action = "DeactivateInstance";
	this->Version = "2015-01-01";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";
	this->KVMap["InstanceId"] = "";
	this->KPMap["InstanceId"] = "Query";

}
RKvstoreDeleteInstanceRequest::RKvstoreDeleteInstanceRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "r-kvstore";
	this->Action = "DeleteInstance";
	this->Version = "2015-01-01";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";
	this->KVMap["InstanceId"] = "";
	this->KPMap["InstanceId"] = "Query";

}
RKvstoreDescribeCommodityRequest::RKvstoreDescribeCommodityRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "r-kvstore";
	this->Action = "DescribeCommodity";
	this->Version = "2015-01-01";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";
	this->KVMap["CommodityCode"] = "";
	this->KPMap["CommodityCode"] = "Query";
	this->KVMap["InstanceId"] = "";
	this->KPMap["InstanceId"] = "Query";
	this->KVMap["OrderType"] = "";
	this->KPMap["OrderType"] = "Query";

}
RKvstoreDescribeConnectionDomainRequest::RKvstoreDescribeConnectionDomainRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "r-kvstore";
	this->Action = "DescribeConnectionDomain";
	this->Version = "2015-01-01";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";
	this->KVMap["ConnectionDomain"] = "";
	this->KPMap["ConnectionDomain"] = "Query";

}
RKvstoreDescribeHistoryMonitorValuesRequest::RKvstoreDescribeHistoryMonitorValuesRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "r-kvstore";
	this->Action = "DescribeHistoryMonitorValues";
	this->Version = "2015-01-01";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";
	this->KVMap["InstanceId"] = "";
	this->KPMap["InstanceId"] = "Query";
	this->KVMap["StartTime"] = "";
	this->KPMap["StartTime"] = "Query";
	this->KVMap["EndTime"] = "";
	this->KPMap["EndTime"] = "Query";
	this->KVMap["IntervalForHistory"] = "";
	this->KPMap["IntervalForHistory"] = "Query";
	this->KVMap["MonitorKeys"] = "";
	this->KPMap["MonitorKeys"] = "Query";

}
RKvstoreDescribeInstanceConfigRequest::RKvstoreDescribeInstanceConfigRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "r-kvstore";
	this->Action = "DescribeInstanceConfig";
	this->Version = "2015-01-01";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";
	this->KVMap["InstanceId"] = "";
	this->KPMap["InstanceId"] = "Query";

}
RKvstoreDescribeInstanceCountRequest::RKvstoreDescribeInstanceCountRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "r-kvstore";
	this->Action = "DescribeInstanceCount";
	this->Version = "2015-01-01";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";

}
RKvstoreDescribeInstancesRequest::RKvstoreDescribeInstancesRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "r-kvstore";
	this->Action = "DescribeInstances";
	this->Version = "2015-01-01";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";
	this->KVMap["InstanceIds"] = "";
	this->KPMap["InstanceIds"] = "Query";
	this->KVMap["InstanceStatus"] = "";
	this->KPMap["InstanceStatus"] = "Query";
	this->KVMap["ChargeType"] = "";
	this->KPMap["ChargeType"] = "Query";
	this->KVMap["PageNumber"] = "";
	this->KPMap["PageNumber"] = "Query";
	this->KVMap["PageSize"] = "";
	this->KPMap["PageSize"] = "Query";

}
RKvstoreDescribeMonitorItemsRequest::RKvstoreDescribeMonitorItemsRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "r-kvstore";
	this->Action = "DescribeMonitorItems";
	this->Version = "2015-01-01";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";

}
RKvstoreDescribeMonitorValuesRequest::RKvstoreDescribeMonitorValuesRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "r-kvstore";
	this->Action = "DescribeMonitorValues";
	this->Version = "2015-01-01";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";
	this->KVMap["InstanceIds"] = "";
	this->KPMap["InstanceIds"] = "Query";
	this->KVMap["MonitorKeys"] = "";
	this->KPMap["MonitorKeys"] = "Query";

}
RKvstoreDescribePriceRequest::RKvstoreDescribePriceRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "r-kvstore";
	this->Action = "DescribePrice";
	this->Version = "2015-01-01";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";
	this->KVMap["Capacity"] = "";
	this->KPMap["Capacity"] = "Query";
	this->KVMap["OrderType"] = "";
	this->KPMap["OrderType"] = "Query";
	this->KVMap["ZoneId"] = "";
	this->KPMap["ZoneId"] = "Query";
	this->KVMap["ChargeType"] = "";
	this->KPMap["ChargeType"] = "Query";
	this->KVMap["Period"] = "";
	this->KPMap["Period"] = "Query";
	this->KVMap["Quantity"] = "";
	this->KPMap["Quantity"] = "Query";
	this->KVMap["InstanceId"] = "";
	this->KPMap["InstanceId"] = "Query";

}
RKvstoreDescribeRegionsRequest::RKvstoreDescribeRegionsRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "r-kvstore";
	this->Action = "DescribeRegions";
	this->Version = "2015-01-01";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";

}
RKvstoreDescribeUserInfoRequest::RKvstoreDescribeUserInfoRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "r-kvstore";
	this->Action = "DescribeUserInfo";
	this->Version = "2015-01-01";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";

}
RKvstoreFlushInstanceRequest::RKvstoreFlushInstanceRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "r-kvstore";
	this->Action = "FlushInstance";
	this->Version = "2015-01-01";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";
	this->KVMap["InstanceId"] = "";
	this->KPMap["InstanceId"] = "Query";

}
RKvstoreModifyInstanceAttributeRequest::RKvstoreModifyInstanceAttributeRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "r-kvstore";
	this->Action = "ModifyInstanceAttribute";
	this->Version = "2015-01-01";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";
	this->KVMap["InstanceId"] = "";
	this->KPMap["InstanceId"] = "Query";
	this->KVMap["InstanceName"] = "";
	this->KPMap["InstanceName"] = "Query";
	this->KVMap["NewPassword"] = "";
	this->KPMap["NewPassword"] = "Query";

}
RKvstoreModifyInstanceCapacityRequest::RKvstoreModifyInstanceCapacityRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "r-kvstore";
	this->Action = "ModifyInstanceCapacity";
	this->Version = "2015-01-01";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";
	this->KVMap["InstanceId"] = "";
	this->KPMap["InstanceId"] = "Query";
	this->KVMap["Capacity"] = "";
	this->KPMap["Capacity"] = "Query";

}
RKvstoreModifyInstanceConfigRequest::RKvstoreModifyInstanceConfigRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "r-kvstore";
	this->Action = "ModifyInstanceConfig";
	this->Version = "2015-01-01";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";
	this->KVMap["InstanceId"] = "";
	this->KPMap["InstanceId"] = "Query";
	this->KVMap["Config"] = "";
	this->KPMap["Config"] = "Query";

}
RKvstoreRenewInstanceRequest::RKvstoreRenewInstanceRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "r-kvstore";
	this->Action = "RenewInstance";
	this->Version = "2015-01-01";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";
	this->KVMap["InstanceId"] = "";
	this->KPMap["InstanceId"] = "Query";
	this->KVMap["Capacity"] = "";
	this->KPMap["Capacity"] = "Query";
	this->KVMap["Period"] = "";
	this->KPMap["Period"] = "Query";
	this->KVMap["AutoPay"] = "";
	this->KPMap["AutoPay"] = "Query";

}
RKvstoreTransformToPrePaidRequest::RKvstoreTransformToPrePaidRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "r-kvstore";
	this->Action = "TransformToPrePaid";
	this->Version = "2015-01-01";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";
	this->KVMap["InstanceId"] = "";
	this->KPMap["InstanceId"] = "Query";
	this->KVMap["Period"] = "";
	this->KPMap["Period"] = "Query";
	this->KVMap["AutoPay"] = "";
	this->KPMap["AutoPay"] = "Query";

}
RKvstoreVerifyPasswordRequest::RKvstoreVerifyPasswordRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "r-kvstore";
	this->Action = "VerifyPassword";
	this->Version = "2015-01-01";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";
	this->KVMap["InstanceId"] = "";
	this->KPMap["InstanceId"] = "Query";
	this->KVMap["Password"] = "";
	this->KPMap["Password"] = "Query";

}

	
}