/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
 
#ifndef ALIYUNRKVSTORE_RKVSTORESERVICE_H_
#define ALIYUNRKVSTORE_RKVSTORESERVICE_H_

#include "../aliyun-cpp-sdk-core/BaseRequest.h"
#include "../aliyun-cpp-sdk-core/ApiBase.h"

namespace AliYunRKvstore {

class RKvstoreClient : public AliYunCore::ApiBase {
private:
	AliYunCore::DefaultProfile &Profile;
public:
	RKvstoreClient(AliYunCore::DefaultProfile &pf);
	void setProfile(AliYunCore::DefaultProfile &pf);
	std::string getResponse(AliYunCore::BaseRequest &request);
	~RKvstoreClient(){};
};
class RKvstoreActivateInstanceRequest : public AliYunCore::BaseRequest {
public:
	RKvstoreActivateInstanceRequest();
};
class RKvstoreCreateInstanceRequest : public AliYunCore::BaseRequest {
public:
	RKvstoreCreateInstanceRequest();
};
class RKvstoreCreateInstancesRequest : public AliYunCore::BaseRequest {
public:
	RKvstoreCreateInstancesRequest();
};
class RKvstoreDataOperateRequest : public AliYunCore::BaseRequest {
public:
	RKvstoreDataOperateRequest();
};
class RKvstoreDeactivateInstanceRequest : public AliYunCore::BaseRequest {
public:
	RKvstoreDeactivateInstanceRequest();
};
class RKvstoreDeleteInstanceRequest : public AliYunCore::BaseRequest {
public:
	RKvstoreDeleteInstanceRequest();
};
class RKvstoreDescribeCommodityRequest : public AliYunCore::BaseRequest {
public:
	RKvstoreDescribeCommodityRequest();
};
class RKvstoreDescribeConnectionDomainRequest : public AliYunCore::BaseRequest {
public:
	RKvstoreDescribeConnectionDomainRequest();
};
class RKvstoreDescribeHistoryMonitorValuesRequest : public AliYunCore::BaseRequest {
public:
	RKvstoreDescribeHistoryMonitorValuesRequest();
};
class RKvstoreDescribeInstanceConfigRequest : public AliYunCore::BaseRequest {
public:
	RKvstoreDescribeInstanceConfigRequest();
};
class RKvstoreDescribeInstanceCountRequest : public AliYunCore::BaseRequest {
public:
	RKvstoreDescribeInstanceCountRequest();
};
class RKvstoreDescribeInstancesRequest : public AliYunCore::BaseRequest {
public:
	RKvstoreDescribeInstancesRequest();
};
class RKvstoreDescribeMonitorItemsRequest : public AliYunCore::BaseRequest {
public:
	RKvstoreDescribeMonitorItemsRequest();
};
class RKvstoreDescribeMonitorValuesRequest : public AliYunCore::BaseRequest {
public:
	RKvstoreDescribeMonitorValuesRequest();
};
class RKvstoreDescribePriceRequest : public AliYunCore::BaseRequest {
public:
	RKvstoreDescribePriceRequest();
};
class RKvstoreDescribeRegionsRequest : public AliYunCore::BaseRequest {
public:
	RKvstoreDescribeRegionsRequest();
};
class RKvstoreDescribeUserInfoRequest : public AliYunCore::BaseRequest {
public:
	RKvstoreDescribeUserInfoRequest();
};
class RKvstoreFlushInstanceRequest : public AliYunCore::BaseRequest {
public:
	RKvstoreFlushInstanceRequest();
};
class RKvstoreModifyInstanceAttributeRequest : public AliYunCore::BaseRequest {
public:
	RKvstoreModifyInstanceAttributeRequest();
};
class RKvstoreModifyInstanceCapacityRequest : public AliYunCore::BaseRequest {
public:
	RKvstoreModifyInstanceCapacityRequest();
};
class RKvstoreModifyInstanceConfigRequest : public AliYunCore::BaseRequest {
public:
	RKvstoreModifyInstanceConfigRequest();
};
class RKvstoreRenewInstanceRequest : public AliYunCore::BaseRequest {
public:
	RKvstoreRenewInstanceRequest();
};
class RKvstoreTransformToPrePaidRequest : public AliYunCore::BaseRequest {
public:
	RKvstoreTransformToPrePaidRequest();
};
class RKvstoreVerifyPasswordRequest : public AliYunCore::BaseRequest {
public:
	RKvstoreVerifyPasswordRequest();
};


}

#endif