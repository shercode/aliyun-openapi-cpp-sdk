/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
 
#ifndef ALIYUNBATCHCOMPUTE_BATCHCOMPUTESERVICE_H_
#define ALIYUNBATCHCOMPUTE_BATCHCOMPUTESERVICE_H_

#include "../aliyun-cpp-sdk-core/BaseRequest.h"
#include "../aliyun-cpp-sdk-core/ApiBase.h"

namespace AliYunBatchCompute {

class BatchComputeClient : public AliYunCore::ApiBase {
private:
	AliYunCore::DefaultProfile &Profile;
public:
	BatchComputeClient(AliYunCore::DefaultProfile &pf);
	void setProfile(AliYunCore::DefaultProfile &pf);
	std::string getResponse(AliYunCore::BaseRequest &request);
	~BatchComputeClient(){};
};
class BatchComputeDeleteJobRequest : public AliYunCore::BaseRequest {
public:
	BatchComputeDeleteJobRequest();
};
class BatchComputeGetJobRequest : public AliYunCore::BaseRequest {
public:
	BatchComputeGetJobRequest();
};
class BatchComputeGetJobDescriptionRequest : public AliYunCore::BaseRequest {
public:
	BatchComputeGetJobDescriptionRequest();
};
class BatchComputeGetTasksRequest : public AliYunCore::BaseRequest {
public:
	BatchComputeGetTasksRequest();
};
class BatchComputeListImagesRequest : public AliYunCore::BaseRequest {
public:
	BatchComputeListImagesRequest();
};
class BatchComputeListJobsRequest : public AliYunCore::BaseRequest {
public:
	BatchComputeListJobsRequest();
};
class BatchComputeListSnapshotsRequest : public AliYunCore::BaseRequest {
public:
	BatchComputeListSnapshotsRequest();
};
class BatchComputePostJobRequest : public AliYunCore::BaseRequest {
public:
	BatchComputePostJobRequest();
};
class BatchComputePutJobRequest : public AliYunCore::BaseRequest {
public:
	BatchComputePutJobRequest();
};
class BatchComputeReleaseJobRequest : public AliYunCore::BaseRequest {
public:
	BatchComputeReleaseJobRequest();
};
class BatchComputeStartJobRequest : public AliYunCore::BaseRequest {
public:
	BatchComputeStartJobRequest();
};
class BatchComputeStopJobRequest : public AliYunCore::BaseRequest {
public:
	BatchComputeStopJobRequest();
};


}

#endif