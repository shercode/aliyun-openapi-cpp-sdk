/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

#include <string>
#include <map>
#include "../aliyun-cpp-sdk-core/utils.h"
#include "../aliyun-cpp-sdk-core/DefaultProfile.h"
#include "../aliyun-cpp-sdk-core/BaseRequest.h"
#include "../aliyun-cpp-sdk-core/ApiBase.h"
#include "BatchComputeService.h"

using std::string;
using namespace AliYunCore;

namespace AliYunBatchCompute {

BatchComputeClient::BatchComputeClient(DefaultProfile &pf):Profile(pf){}
void BatchComputeClient::setProfile(DefaultProfile &pf){
	this->Profile = pf;
}
string BatchComputeClient::getResponse(BaseRequest &request){
	request.RegionId = this->Profile.RegionId;		
	request.AccessKeyId = this->Profile.AccessKeyId;
	request.AccessKeySecret = this->Profile.AccessKeySecret;
	return ApiBase::getResponse(request);
}
BatchComputeDeleteJobRequest::BatchComputeDeleteJobRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "DELETE";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "/jobs/[ResourceName]";
	this->ProductName = "batchcompute";
	this->Action = "DeleteJob";
	this->Version = "2015-06-30";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["ResourceName"] = "";
	this->KPMap["ResourceName"] = "Path";

}
BatchComputeGetJobRequest::BatchComputeGetJobRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "/jobs/[ResourceName]";
	this->ProductName = "batchcompute";
	this->Action = "GetJob";
	this->Version = "2015-06-30";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["ResourceName"] = "";
	this->KPMap["ResourceName"] = "Path";

}
BatchComputeGetJobDescriptionRequest::BatchComputeGetJobDescriptionRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "/jobs/[ResourceName]/description";
	this->ProductName = "batchcompute";
	this->Action = "GetJobDescription";
	this->Version = "2015-06-30";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["ResourceName"] = "";
	this->KPMap["ResourceName"] = "path";

}
BatchComputeGetTasksRequest::BatchComputeGetTasksRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "/jobs/[ResourceName]/tasks";
	this->ProductName = "batchcompute";
	this->Action = "GetTasks";
	this->Version = "2015-06-30";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["ResourceName"] = "";
	this->KPMap["ResourceName"] = "Path";

}
BatchComputeListImagesRequest::BatchComputeListImagesRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "/images";
	this->ProductName = "batchcompute";
	this->Action = "ListImages";
	this->Version = "2015-06-30";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 

}
BatchComputeListJobsRequest::BatchComputeListJobsRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "/jobs";
	this->ProductName = "batchcompute";
	this->Action = "ListJobs";
	this->Version = "2015-06-30";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 

}
BatchComputeListSnapshotsRequest::BatchComputeListSnapshotsRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "/snapshots";
	this->ProductName = "batchcompute";
	this->Action = "ListSnapshots";
	this->Version = "2015-06-30";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 

}
BatchComputePostJobRequest::BatchComputePostJobRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "POST";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "/jobs";
	this->ProductName = "batchcompute";
	this->Action = "PostJob";
	this->Version = "2015-06-30";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 

}
BatchComputePutJobRequest::BatchComputePutJobRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "PUT";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "/jobs/[ResourceName]/Priority";
	this->ProductName = "batchcompute";
	this->Action = "PutJob";
	this->Version = "2015-06-30";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["ResourceName"] = "";
	this->KPMap["ResourceName"] = "Path";

}
BatchComputeReleaseJobRequest::BatchComputeReleaseJobRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "DELETE";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "/2013-01-11/jobs/[ResourceName]";
	this->ProductName = "batchcompute";
	this->Action = "ReleaseJob";
	this->Version = "2015-06-30";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["ResourceName"] = "";
	this->KPMap["ResourceName"] = "Path";

}
BatchComputeStartJobRequest::BatchComputeStartJobRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "PUT";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "/jobs/[ResourceName]?Action=Start";
	this->ProductName = "batchcompute";
	this->Action = "StartJob";
	this->Version = "2015-06-30";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["ResourceName"] = "";
	this->KPMap["ResourceName"] = "Path";

}
BatchComputeStopJobRequest::BatchComputeStopJobRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "PUT";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "/jobs/[ResourceName]?Action=Stop";
	this->ProductName = "batchcompute";
	this->Action = "StopJob";
	this->Version = "2015-06-30";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["ResourceName"] = "";
	this->KPMap["ResourceName"] = "Path";

}

	
}