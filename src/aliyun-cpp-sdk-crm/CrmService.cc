/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

#include <string>
#include <map>
#include "../aliyun-cpp-sdk-core/utils.h"
#include "../aliyun-cpp-sdk-core/DefaultProfile.h"
#include "../aliyun-cpp-sdk-core/BaseRequest.h"
#include "../aliyun-cpp-sdk-core/ApiBase.h"
#include "CrmService.h"

using std::string;
using namespace AliYunCore;

namespace AliYunCrm {

CrmClient::CrmClient(DefaultProfile &pf):Profile(pf){}
void CrmClient::setProfile(DefaultProfile &pf){
	this->Profile = pf;
}
string CrmClient::getResponse(BaseRequest &request){
	request.RegionId = this->Profile.RegionId;		
	request.AccessKeyId = this->Profile.AccessKeyId;
	request.AccessKeySecret = this->Profile.AccessKeySecret;
	return ApiBase::getResponse(request);
}
CrmAddLabelRequest::CrmAddLabelRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "crm";
	this->Action = "AddLabel";
	this->Version = "2015-04-08";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["PK"] = "";
	this->KPMap["PK"] = "Query";
	this->KVMap["LabelName"] = "";
	this->KPMap["LabelName"] = "Query";
	this->KVMap["LabelSeries"] = "";
	this->KPMap["LabelSeries"] = "Query";
	this->KVMap["EndTime"] = "";
	this->KPMap["EndTime"] = "Query";

}
CrmCheckLabelRequest::CrmCheckLabelRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "crm";
	this->Action = "CheckLabel";
	this->Version = "2015-04-08";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["PK"] = "";
	this->KPMap["PK"] = "Query";
	this->KVMap["LabelName"] = "";
	this->KPMap["LabelName"] = "Query";
	this->KVMap["LabelSeries"] = "";
	this->KPMap["LabelSeries"] = "Query";

}
CrmDeleteLabelRequest::CrmDeleteLabelRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "crm";
	this->Action = "DeleteLabel";
	this->Version = "2015-04-08";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["PK"] = "";
	this->KPMap["PK"] = "Query";
	this->KVMap["LabelName"] = "";
	this->KPMap["LabelName"] = "Query";
	this->KVMap["LabelSeries"] = "";
	this->KPMap["LabelSeries"] = "Query";

}
CrmQueryCustomerLabelRequest::CrmQueryCustomerLabelRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "crm";
	this->Action = "QueryCustomerLabel";
	this->Version = "2015-04-08";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["LabelSeries"] = "";
	this->KPMap["LabelSeries"] = "Query";

}

	
}