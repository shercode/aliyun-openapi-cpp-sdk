/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

#include <string>
#include <map>
#include "../aliyun-cpp-sdk-core/utils.h"
#include "../aliyun-cpp-sdk-core/DefaultProfile.h"
#include "../aliyun-cpp-sdk-core/BaseRequest.h"
#include "../aliyun-cpp-sdk-core/ApiBase.h"
#include "OtsFinanceService.h"

using std::string;
using namespace AliYunCore;

namespace AliYunOtsFinance {

OtsFinanceClient::OtsFinanceClient(DefaultProfile &pf):Profile(pf){}
void OtsFinanceClient::setProfile(DefaultProfile &pf){
	this->Profile = pf;
}
string OtsFinanceClient::getResponse(BaseRequest &request){
	request.RegionId = this->Profile.RegionId;		
	request.AccessKeyId = this->Profile.AccessKeyId;
	request.AccessKeySecret = this->Profile.AccessKeySecret;
	return ApiBase::getResponse(request);
}
OtsFinanceDeleteInstanceRequest::OtsFinanceDeleteInstanceRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "POST";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "otsfinance";
	this->Action = "DeleteInstance";
	this->Version = "2013-09-12";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["InstanceName"] = "";
	this->KPMap["InstanceName"] = "Query";

}
OtsFinanceDeleteUserRequest::OtsFinanceDeleteUserRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "POST";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "otsfinance";
	this->Action = "DeleteUser";
	this->Version = "2013-09-12";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 

}
OtsFinanceGetInstanceRequest::OtsFinanceGetInstanceRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "otsfinance";
	this->Action = "GetInstance";
	this->Version = "2013-09-12";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["InstanceName"] = "";
	this->KPMap["InstanceName"] = "Query";

}
OtsFinanceGetUserRequest::OtsFinanceGetUserRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "otsfinance";
	this->Action = "GetUser";
	this->Version = "2013-09-12";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 

}
OtsFinanceInsertInstanceRequest::OtsFinanceInsertInstanceRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "POST";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "otsfinance";
	this->Action = "InsertInstance";
	this->Version = "2013-09-12";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["InstanceName"] = "";
	this->KPMap["InstanceName"] = "Query";
	this->KVMap["ClusterName"] = "";
	this->KPMap["ClusterName"] = "Query";
	this->KVMap["WriteCapacity"] = "";
	this->KPMap["WriteCapacity"] = "Query";
	this->KVMap["ReadCapacity"] = "";
	this->KPMap["ReadCapacity"] = "Query";
	this->KVMap["EntityQuota"] = "";
	this->KPMap["EntityQuota"] = "Query";
	this->KVMap["Description"] = "";
	this->KPMap["Description"] = "Query";

}
OtsFinanceInsertUserRequest::OtsFinanceInsertUserRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "POST";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "otsfinance";
	this->Action = "InsertUser";
	this->Version = "2013-09-12";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["InstanceQuota"] = "";
	this->KPMap["InstanceQuota"] = "Query";
	this->KVMap["Description"] = "";
	this->KPMap["Description"] = "Query";

}
OtsFinanceListInstanceRequest::OtsFinanceListInstanceRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "otsfinance";
	this->Action = "ListInstance";
	this->Version = "2013-09-12";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 

}
OtsFinanceUpdateInstanceRequest::OtsFinanceUpdateInstanceRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "POST";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "otsfinance";
	this->Action = "UpdateInstance";
	this->Version = "2013-09-12";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["InstanceName"] = "";
	this->KPMap["InstanceName"] = "Query";
	this->KVMap["WriteCapacity"] = "";
	this->KPMap["WriteCapacity"] = "Query";
	this->KVMap["ReadCapacity"] = "";
	this->KPMap["ReadCapacity"] = "Query";
	this->KVMap["EntityQuota"] = "";
	this->KPMap["EntityQuota"] = "Query";
	this->KVMap["Description"] = "";
	this->KPMap["Description"] = "Query";

}
OtsFinanceUpdateUserRequest::OtsFinanceUpdateUserRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "POST";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "otsfinance";
	this->Action = "UpdateUser";
	this->Version = "2013-09-12";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["InstanceQuota"] = "";
	this->KPMap["InstanceQuota"] = "Query";
	this->KVMap["Description"] = "";
	this->KPMap["Description"] = "Query";

}

	
}