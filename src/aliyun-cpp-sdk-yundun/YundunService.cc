/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

#include <string>
#include <map>
#include "../aliyun-cpp-sdk-core/utils.h"
#include "../aliyun-cpp-sdk-core/DefaultProfile.h"
#include "../aliyun-cpp-sdk-core/BaseRequest.h"
#include "../aliyun-cpp-sdk-core/ApiBase.h"
#include "YundunService.h"

using std::string;
using namespace AliYunCore;

namespace AliYunYundun {

YundunClient::YundunClient(DefaultProfile &pf):Profile(pf){}
void YundunClient::setProfile(DefaultProfile &pf){
	this->Profile = pf;
}
string YundunClient::getResponse(BaseRequest &request){
	request.RegionId = this->Profile.RegionId;		
	request.AccessKeyId = this->Profile.AccessKeyId;
	request.AccessKeySecret = this->Profile.AccessKeySecret;
	return ApiBase::getResponse(request);
}
YundunAddCNameWafRequest::YundunAddCNameWafRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "yundun";
	this->Action = "AddCNameWaf";
	this->Version = "2015-04-16";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["InstanceId"] = "";
	this->KPMap["InstanceId"] = "Query";
	this->KVMap["InstanceType"] = "";
	this->KPMap["InstanceType"] = "Query";
	this->KVMap["Domain"] = "";
	this->KPMap["Domain"] = "Query";

}
YundunBruteforceLogRequest::YundunBruteforceLogRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "yundun";
	this->Action = "BruteforceLog";
	this->Version = "2015-04-16";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["JstOwnerId"] = "";
	this->KPMap["JstOwnerId"] = "Query";
	this->KVMap["PageNumber"] = "";
	this->KPMap["PageNumber"] = "Query";
	this->KVMap["PageSize"] = "";
	this->KPMap["PageSize"] = "Query";
	this->KVMap["InstanceId"] = "";
	this->KPMap["InstanceId"] = "Query";
	this->KVMap["RecordType"] = "";
	this->KPMap["RecordType"] = "Query";

}
YundunCloseCCProtectRequest::YundunCloseCCProtectRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "yundun";
	this->Action = "CloseCCProtect";
	this->Version = "2015-04-16";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["InstanceId"] = "";
	this->KPMap["InstanceId"] = "Query";
	this->KVMap["InstanceType"] = "";
	this->KPMap["InstanceType"] = "Query";

}
YundunClosePortScanRequest::YundunClosePortScanRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "yundun";
	this->Action = "ClosePortScan";
	this->Version = "2015-04-16";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["InstanceId"] = "";
	this->KPMap["InstanceId"] = "Query";

}
YundunCloseVulScanRequest::YundunCloseVulScanRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "yundun";
	this->Action = "CloseVulScan";
	this->Version = "2015-04-16";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["InstanceId"] = "";
	this->KPMap["InstanceId"] = "Query";

}
YundunConfigDdosRequest::YundunConfigDdosRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "yundun";
	this->Action = "ConfigDdos";
	this->Version = "2015-04-16";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["InstanceId"] = "";
	this->KPMap["InstanceId"] = "Query";
	this->KVMap["InstanceType"] = "";
	this->KPMap["InstanceType"] = "Query";
	this->KVMap["FlowPosition"] = "";
	this->KPMap["FlowPosition"] = "Query";
	this->KVMap["StrategyPosition"] = "";
	this->KPMap["StrategyPosition"] = "Query";
	this->KVMap["Level"] = "";
	this->KPMap["Level"] = "Query";

}
YundunConfirmLoginRequest::YundunConfirmLoginRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "yundun";
	this->Action = "ConfirmLogin";
	this->Version = "2015-04-16";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["InstanceId"] = "";
	this->KPMap["InstanceId"] = "Query";
	this->KVMap["SourceIp"] = "";
	this->KPMap["SourceIp"] = "Query";
	this->KVMap["Time"] = "";
	this->KPMap["Time"] = "Query";

}
YundunDdosFlowGraphRequest::YundunDdosFlowGraphRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "yundun";
	this->Action = "DdosFlowGraph";
	this->Version = "2015-04-16";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["InstanceId"] = "";
	this->KPMap["InstanceId"] = "Query";
	this->KVMap["InstanceType"] = "";
	this->KPMap["InstanceType"] = "Query";

}
YundunDdosLogRequest::YundunDdosLogRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "yundun";
	this->Action = "DdosLog";
	this->Version = "2015-04-16";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["JstOwnerId"] = "";
	this->KPMap["JstOwnerId"] = "Query";
	this->KVMap["PageNumber"] = "";
	this->KPMap["PageNumber"] = "Query";
	this->KVMap["PageSize"] = "";
	this->KPMap["PageSize"] = "Query";
	this->KVMap["InstanceId"] = "";
	this->KPMap["InstanceId"] = "Query";
	this->KVMap["InstanceType"] = "";
	this->KPMap["InstanceType"] = "Query";

}
YundunDeleteBackDoorFileRequest::YundunDeleteBackDoorFileRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "yundun";
	this->Action = "DeleteBackDoorFile";
	this->Version = "2015-04-16";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["JstOwnerId"] = "";
	this->KPMap["JstOwnerId"] = "Query";
	this->KVMap["InstanceId"] = "";
	this->KPMap["InstanceId"] = "Query";
	this->KVMap["Path"] = "";
	this->KPMap["Path"] = "Query";

}
YundunDeleteCNameWafRequest::YundunDeleteCNameWafRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "yundun";
	this->Action = "DeleteCNameWaf";
	this->Version = "2015-04-16";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["InstanceId"] = "";
	this->KPMap["InstanceId"] = "Query";
	this->KVMap["Domain"] = "";
	this->KPMap["Domain"] = "Query";
	this->KVMap["CnameId"] = "";
	this->KPMap["CnameId"] = "Query";
	this->KVMap["InstanceType"] = "";
	this->KPMap["InstanceType"] = "Query";

}
YundunDetectVulByIdRequest::YundunDetectVulByIdRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "yundun";
	this->Action = "DetectVulById";
	this->Version = "2015-04-16";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["InstanceId"] = "";
	this->KPMap["InstanceId"] = "Query";
	this->KVMap["VulId"] = "";
	this->KPMap["VulId"] = "Query";

}
YundunDetectVulByIpRequest::YundunDetectVulByIpRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "yundun";
	this->Action = "DetectVulByIp";
	this->Version = "2015-04-16";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["InstanceId"] = "";
	this->KPMap["InstanceId"] = "Query";
	this->KVMap["VulIp"] = "";
	this->KPMap["VulIp"] = "Query";

}
YundunGetDdosConfigOptionsRequest::YundunGetDdosConfigOptionsRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "yundun";
	this->Action = "GetDdosConfigOptions";
	this->Version = "2015-04-16";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 

}
YundunListInstanceInfosRequest::YundunListInstanceInfosRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "yundun";
	this->Action = "ListInstanceInfos";
	this->Version = "2015-04-16";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["JstOwnerId"] = "";
	this->KPMap["JstOwnerId"] = "Query";
	this->KVMap["PageNumber"] = "";
	this->KPMap["PageNumber"] = "Query";
	this->KVMap["PageSize"] = "";
	this->KPMap["PageSize"] = "Query";
	this->KVMap["Region"] = "";
	this->KPMap["Region"] = "Query";
	this->KVMap["EventType"] = "";
	this->KPMap["EventType"] = "Query";
	this->KVMap["InstanceName"] = "";
	this->KPMap["InstanceName"] = "Query";
	this->KVMap["InstanceType"] = "";
	this->KPMap["InstanceType"] = "Query";
	this->KVMap["InstanceIds"] = "";
	this->KPMap["InstanceIds"] = "Query";

}
YundunLogineventLogRequest::YundunLogineventLogRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "yundun";
	this->Action = "LogineventLog";
	this->Version = "2015-04-16";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["JstOwnerId"] = "";
	this->KPMap["JstOwnerId"] = "Query";
	this->KVMap["PageNumber"] = "";
	this->KPMap["PageNumber"] = "Query";
	this->KVMap["PageSize"] = "";
	this->KPMap["PageSize"] = "Query";
	this->KVMap["InstanceId"] = "";
	this->KPMap["InstanceId"] = "Query";
	this->KVMap["RecordType"] = "";
	this->KPMap["RecordType"] = "Query";

}
YundunOpenCCProtectRequest::YundunOpenCCProtectRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "yundun";
	this->Action = "OpenCCProtect";
	this->Version = "2015-04-16";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["InstanceId"] = "";
	this->KPMap["InstanceId"] = "Query";
	this->KVMap["InstanceType"] = "";
	this->KPMap["InstanceType"] = "Query";

}
YundunOpenPortScanRequest::YundunOpenPortScanRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "yundun";
	this->Action = "OpenPortScan";
	this->Version = "2015-04-16";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["InstanceId"] = "";
	this->KPMap["InstanceId"] = "Query";

}
YundunOpenVulScanRequest::YundunOpenVulScanRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "yundun";
	this->Action = "OpenVulScan";
	this->Version = "2015-04-16";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["InstanceId"] = "";
	this->KPMap["InstanceId"] = "Query";

}
YundunQueryDdosConfigRequest::YundunQueryDdosConfigRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "yundun";
	this->Action = "QueryDdosConfig";
	this->Version = "2015-04-16";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["InstanceId"] = "";
	this->KPMap["InstanceId"] = "Query";
	this->KVMap["InstanceType"] = "";
	this->KPMap["InstanceType"] = "Query";

}
YundunSecureCheckRequest::YundunSecureCheckRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "yundun";
	this->Action = "SecureCheck";
	this->Version = "2015-04-16";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["JstOwnerId"] = "";
	this->KPMap["JstOwnerId"] = "Query";
	this->KVMap["InstanceIds"] = "";
	this->KPMap["InstanceIds"] = "Query";

}
YundunServiceStatusRequest::YundunServiceStatusRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "yundun";
	this->Action = "ServiceStatus";
	this->Version = "2015-04-16";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["InstanceId"] = "";
	this->KPMap["InstanceId"] = "Query";

}
YundunSetDdosAutoRequest::YundunSetDdosAutoRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "yundun";
	this->Action = "SetDdosAuto";
	this->Version = "2015-04-16";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["InstanceId"] = "";
	this->KPMap["InstanceId"] = "Query";
	this->KVMap["InstanceType"] = "";
	this->KPMap["InstanceType"] = "Query";

}
YundunSetDdosQpsRequest::YundunSetDdosQpsRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "yundun";
	this->Action = "SetDdosQps";
	this->Version = "2015-04-16";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["InstanceId"] = "";
	this->KPMap["InstanceId"] = "Query";
	this->KVMap["InstanceType"] = "";
	this->KPMap["InstanceType"] = "Query";
	this->KVMap["QpsPosition"] = "";
	this->KPMap["QpsPosition"] = "Query";
	this->KVMap["Level"] = "";
	this->KPMap["Level"] = "Query";

}
YundunSummaryRequest::YundunSummaryRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "yundun";
	this->Action = "Summary";
	this->Version = "2015-04-16";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["JstOwnerId"] = "";
	this->KPMap["JstOwnerId"] = "Query";
	this->KVMap["InstanceIds"] = "";
	this->KPMap["InstanceIds"] = "Query";

}
YundunVulScanLogRequest::YundunVulScanLogRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "yundun";
	this->Action = "VulScanLog";
	this->Version = "2015-04-16";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["JstOwnerId"] = "";
	this->KPMap["JstOwnerId"] = "Query";
	this->KVMap["PageNumber"] = "";
	this->KPMap["PageNumber"] = "Query";
	this->KVMap["PageSize"] = "";
	this->KPMap["PageSize"] = "Query";
	this->KVMap["InstanceId"] = "";
	this->KPMap["InstanceId"] = "Query";
	this->KVMap["VulStatus"] = "";
	this->KPMap["VulStatus"] = "Query";

}
YundunWafInfoRequest::YundunWafInfoRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "yundun";
	this->Action = "WafInfo";
	this->Version = "2015-04-16";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["InstanceId"] = "";
	this->KPMap["InstanceId"] = "Query";
	this->KVMap["InstanceType"] = "";
	this->KPMap["InstanceType"] = "Query";

}
YundunWafLogRequest::YundunWafLogRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "yundun";
	this->Action = "WafLog";
	this->Version = "2015-04-16";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["JstOwnerId"] = "";
	this->KPMap["JstOwnerId"] = "Query";
	this->KVMap["PageNumber"] = "";
	this->KPMap["PageNumber"] = "Query";
	this->KVMap["PageSize"] = "";
	this->KPMap["PageSize"] = "Query";
	this->KVMap["InstanceId"] = "";
	this->KPMap["InstanceId"] = "Query";
	this->KVMap["InstanceType"] = "";
	this->KPMap["InstanceType"] = "Query";

}
YundunWebshellLogRequest::YundunWebshellLogRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "yundun";
	this->Action = "WebshellLog";
	this->Version = "2015-04-16";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["JstOwnerId"] = "";
	this->KPMap["JstOwnerId"] = "Query";
	this->KVMap["PageNumber"] = "";
	this->KPMap["PageNumber"] = "Query";
	this->KVMap["PageSize"] = "";
	this->KPMap["PageSize"] = "Query";
	this->KVMap["InstanceId"] = "";
	this->KPMap["InstanceId"] = "Query";
	this->KVMap["RecordType"] = "";
	this->KPMap["RecordType"] = "Query";

}

	
}