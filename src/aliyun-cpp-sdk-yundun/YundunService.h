/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
 
#ifndef ALIYUNYUNDUN_YUNDUNSERVICE_H_
#define ALIYUNYUNDUN_YUNDUNSERVICE_H_

#include "../aliyun-cpp-sdk-core/BaseRequest.h"
#include "../aliyun-cpp-sdk-core/ApiBase.h"

namespace AliYunYundun {

class YundunClient : public AliYunCore::ApiBase {
private:
	AliYunCore::DefaultProfile &Profile;
public:
	YundunClient(AliYunCore::DefaultProfile &pf);
	void setProfile(AliYunCore::DefaultProfile &pf);
	std::string getResponse(AliYunCore::BaseRequest &request);
	~YundunClient(){};
};
class YundunAddCNameWafRequest : public AliYunCore::BaseRequest {
public:
	YundunAddCNameWafRequest();
};
class YundunBruteforceLogRequest : public AliYunCore::BaseRequest {
public:
	YundunBruteforceLogRequest();
};
class YundunCloseCCProtectRequest : public AliYunCore::BaseRequest {
public:
	YundunCloseCCProtectRequest();
};
class YundunClosePortScanRequest : public AliYunCore::BaseRequest {
public:
	YundunClosePortScanRequest();
};
class YundunCloseVulScanRequest : public AliYunCore::BaseRequest {
public:
	YundunCloseVulScanRequest();
};
class YundunConfigDdosRequest : public AliYunCore::BaseRequest {
public:
	YundunConfigDdosRequest();
};
class YundunConfirmLoginRequest : public AliYunCore::BaseRequest {
public:
	YundunConfirmLoginRequest();
};
class YundunDdosFlowGraphRequest : public AliYunCore::BaseRequest {
public:
	YundunDdosFlowGraphRequest();
};
class YundunDdosLogRequest : public AliYunCore::BaseRequest {
public:
	YundunDdosLogRequest();
};
class YundunDeleteBackDoorFileRequest : public AliYunCore::BaseRequest {
public:
	YundunDeleteBackDoorFileRequest();
};
class YundunDeleteCNameWafRequest : public AliYunCore::BaseRequest {
public:
	YundunDeleteCNameWafRequest();
};
class YundunDetectVulByIdRequest : public AliYunCore::BaseRequest {
public:
	YundunDetectVulByIdRequest();
};
class YundunDetectVulByIpRequest : public AliYunCore::BaseRequest {
public:
	YundunDetectVulByIpRequest();
};
class YundunGetDdosConfigOptionsRequest : public AliYunCore::BaseRequest {
public:
	YundunGetDdosConfigOptionsRequest();
};
class YundunListInstanceInfosRequest : public AliYunCore::BaseRequest {
public:
	YundunListInstanceInfosRequest();
};
class YundunLogineventLogRequest : public AliYunCore::BaseRequest {
public:
	YundunLogineventLogRequest();
};
class YundunOpenCCProtectRequest : public AliYunCore::BaseRequest {
public:
	YundunOpenCCProtectRequest();
};
class YundunOpenPortScanRequest : public AliYunCore::BaseRequest {
public:
	YundunOpenPortScanRequest();
};
class YundunOpenVulScanRequest : public AliYunCore::BaseRequest {
public:
	YundunOpenVulScanRequest();
};
class YundunQueryDdosConfigRequest : public AliYunCore::BaseRequest {
public:
	YundunQueryDdosConfigRequest();
};
class YundunSecureCheckRequest : public AliYunCore::BaseRequest {
public:
	YundunSecureCheckRequest();
};
class YundunServiceStatusRequest : public AliYunCore::BaseRequest {
public:
	YundunServiceStatusRequest();
};
class YundunSetDdosAutoRequest : public AliYunCore::BaseRequest {
public:
	YundunSetDdosAutoRequest();
};
class YundunSetDdosQpsRequest : public AliYunCore::BaseRequest {
public:
	YundunSetDdosQpsRequest();
};
class YundunSummaryRequest : public AliYunCore::BaseRequest {
public:
	YundunSummaryRequest();
};
class YundunVulScanLogRequest : public AliYunCore::BaseRequest {
public:
	YundunVulScanLogRequest();
};
class YundunWafInfoRequest : public AliYunCore::BaseRequest {
public:
	YundunWafInfoRequest();
};
class YundunWafLogRequest : public AliYunCore::BaseRequest {
public:
	YundunWafLogRequest();
};
class YundunWebshellLogRequest : public AliYunCore::BaseRequest {
public:
	YundunWebshellLogRequest();
};


}

#endif