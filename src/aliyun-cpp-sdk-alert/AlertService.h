/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
 
#ifndef ALIYUNALERT_ALERTSERVICE_H_
#define ALIYUNALERT_ALERTSERVICE_H_

#include "../aliyun-cpp-sdk-core/BaseRequest.h"
#include "../aliyun-cpp-sdk-core/ApiBase.h"

namespace AliYunAlert {

class AlertClient : public AliYunCore::ApiBase {
private:
	AliYunCore::DefaultProfile &Profile;
public:
	AlertClient(AliYunCore::DefaultProfile &pf);
	void setProfile(AliYunCore::DefaultProfile &pf);
	std::string getResponse(AliYunCore::BaseRequest &request);
	~AlertClient(){};
};
class AlertBatchQueryProjectRequest : public AliYunCore::BaseRequest {
public:
	AlertBatchQueryProjectRequest();
};
class AlertCreateAlertRequest : public AliYunCore::BaseRequest {
public:
	AlertCreateAlertRequest();
};
class AlertCreateContactRequest : public AliYunCore::BaseRequest {
public:
	AlertCreateContactRequest();
};
class AlertCreateContactGroupRequest : public AliYunCore::BaseRequest {
public:
	AlertCreateContactGroupRequest();
};
class AlertCreateDBMetricRequest : public AliYunCore::BaseRequest {
public:
	AlertCreateDBMetricRequest();
};
class AlertCreateDBSourceRequest : public AliYunCore::BaseRequest {
public:
	AlertCreateDBSourceRequest();
};
class AlertCreateDimensionsRequest : public AliYunCore::BaseRequest {
public:
	AlertCreateDimensionsRequest();
};
class AlertCreateLevelChannelRequest : public AliYunCore::BaseRequest {
public:
	AlertCreateLevelChannelRequest();
};
class AlertCreateLogHubMetricRequest : public AliYunCore::BaseRequest {
public:
	AlertCreateLogHubMetricRequest();
};
class AlertCreateProjectRequest : public AliYunCore::BaseRequest {
public:
	AlertCreateProjectRequest();
};
class AlertDeleteAlertRequest : public AliYunCore::BaseRequest {
public:
	AlertDeleteAlertRequest();
};
class AlertDeleteContactRequest : public AliYunCore::BaseRequest {
public:
	AlertDeleteContactRequest();
};
class AlertDeleteContactGroupRequest : public AliYunCore::BaseRequest {
public:
	AlertDeleteContactGroupRequest();
};
class AlertDeleteDBMetricRequest : public AliYunCore::BaseRequest {
public:
	AlertDeleteDBMetricRequest();
};
class AlertDeleteDBSourceRequest : public AliYunCore::BaseRequest {
public:
	AlertDeleteDBSourceRequest();
};
class AlertDeleteDimensionsRequest : public AliYunCore::BaseRequest {
public:
	AlertDeleteDimensionsRequest();
};
class AlertDeleteLevelChannelRequest : public AliYunCore::BaseRequest {
public:
	AlertDeleteLevelChannelRequest();
};
class AlertDeleteLogHubMetricRequest : public AliYunCore::BaseRequest {
public:
	AlertDeleteLogHubMetricRequest();
};
class AlertDeleteProjectRequest : public AliYunCore::BaseRequest {
public:
	AlertDeleteProjectRequest();
};
class AlertDisableAlertRequest : public AliYunCore::BaseRequest {
public:
	AlertDisableAlertRequest();
};
class AlertEnableAlertRequest : public AliYunCore::BaseRequest {
public:
	AlertEnableAlertRequest();
};
class AlertGetAlertRequest : public AliYunCore::BaseRequest {
public:
	AlertGetAlertRequest();
};
class AlertGetContactRequest : public AliYunCore::BaseRequest {
public:
	AlertGetContactRequest();
};
class AlertGetContactGroupRequest : public AliYunCore::BaseRequest {
public:
	AlertGetContactGroupRequest();
};
class AlertGetDBMetricRequest : public AliYunCore::BaseRequest {
public:
	AlertGetDBMetricRequest();
};
class AlertGetDBSourceRequest : public AliYunCore::BaseRequest {
public:
	AlertGetDBSourceRequest();
};
class AlertGetDimensionsRequest : public AliYunCore::BaseRequest {
public:
	AlertGetDimensionsRequest();
};
class AlertGetLevelChannelRequest : public AliYunCore::BaseRequest {
public:
	AlertGetLevelChannelRequest();
};
class AlertGetLogHubMetricRequest : public AliYunCore::BaseRequest {
public:
	AlertGetLogHubMetricRequest();
};
class AlertGetProjectRequest : public AliYunCore::BaseRequest {
public:
	AlertGetProjectRequest();
};
class AlertGrantProjectOwnerRequest : public AliYunCore::BaseRequest {
public:
	AlertGrantProjectOwnerRequest();
};
class AlertListAlertRequest : public AliYunCore::BaseRequest {
public:
	AlertListAlertRequest();
};
class AlertListAlertStateRequest : public AliYunCore::BaseRequest {
public:
	AlertListAlertStateRequest();
};
class AlertListContactRequest : public AliYunCore::BaseRequest {
public:
	AlertListContactRequest();
};
class AlertListContactGroupRequest : public AliYunCore::BaseRequest {
public:
	AlertListContactGroupRequest();
};
class AlertListDBMetricRequest : public AliYunCore::BaseRequest {
public:
	AlertListDBMetricRequest();
};
class AlertListDBSourceRequest : public AliYunCore::BaseRequest {
public:
	AlertListDBSourceRequest();
};
class AlertListDimensionsRequest : public AliYunCore::BaseRequest {
public:
	AlertListDimensionsRequest();
};
class AlertListLevelChannelRequest : public AliYunCore::BaseRequest {
public:
	AlertListLevelChannelRequest();
};
class AlertListLogHubMetricRequest : public AliYunCore::BaseRequest {
public:
	AlertListLogHubMetricRequest();
};
class AlertListNotifyHistoryRequest : public AliYunCore::BaseRequest {
public:
	AlertListNotifyHistoryRequest();
};
class AlertListProjectRequest : public AliYunCore::BaseRequest {
public:
	AlertListProjectRequest();
};
class AlertRemoveProjectOwnerRequest : public AliYunCore::BaseRequest {
public:
	AlertRemoveProjectOwnerRequest();
};
class AlertUpdateAlertRequest : public AliYunCore::BaseRequest {
public:
	AlertUpdateAlertRequest();
};
class AlertUpdateContactRequest : public AliYunCore::BaseRequest {
public:
	AlertUpdateContactRequest();
};
class AlertUpdateContactGroupRequest : public AliYunCore::BaseRequest {
public:
	AlertUpdateContactGroupRequest();
};
class AlertUpdateDBMetricRequest : public AliYunCore::BaseRequest {
public:
	AlertUpdateDBMetricRequest();
};
class AlertUpdateDBSourceRequest : public AliYunCore::BaseRequest {
public:
	AlertUpdateDBSourceRequest();
};
class AlertUpdateDimensionsRequest : public AliYunCore::BaseRequest {
public:
	AlertUpdateDimensionsRequest();
};
class AlertUpdateLevelChannelRequest : public AliYunCore::BaseRequest {
public:
	AlertUpdateLevelChannelRequest();
};
class AlertUpdateLogHubMetricRequest : public AliYunCore::BaseRequest {
public:
	AlertUpdateLogHubMetricRequest();
};
class AlertUpdateProjectRequest : public AliYunCore::BaseRequest {
public:
	AlertUpdateProjectRequest();
};


}

#endif