/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

#include <string>
#include <map>
#include "../aliyun-cpp-sdk-core/utils.h"
#include "../aliyun-cpp-sdk-core/DefaultProfile.h"
#include "../aliyun-cpp-sdk-core/BaseRequest.h"
#include "../aliyun-cpp-sdk-core/ApiBase.h"
#include "AlertService.h"

using std::string;
using namespace AliYunCore;

namespace AliYunAlert {

AlertClient::AlertClient(DefaultProfile &pf):Profile(pf){}
void AlertClient::setProfile(DefaultProfile &pf){
	this->Profile = pf;
}
string AlertClient::getResponse(BaseRequest &request){
	request.RegionId = this->Profile.RegionId;		
	request.AccessKeyId = this->Profile.AccessKeyId;
	request.AccessKeySecret = this->Profile.AccessKeySecret;
	return ApiBase::getResponse(request);
}
AlertBatchQueryProjectRequest::AlertBatchQueryProjectRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "/projects/batchQuery";
	this->ProductName = "alert";
	this->Action = "BatchQueryProject";
	this->Version = "2015-08-15";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["Names"] = "";
	this->KPMap["Names"] = "Query";
	this->KVMap["Page"] = "";
	this->KPMap["Page"] = "Query";
	this->KVMap["PageSize"] = "";
	this->KPMap["PageSize"] = "Query";

}
AlertCreateAlertRequest::AlertCreateAlertRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "POST";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "/projects/[ProjectName]/alerts";
	this->ProductName = "alert";
	this->Action = "CreateAlert";
	this->Version = "2015-08-15";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["ProjectName"] = "";
	this->KPMap["ProjectName"] = "Path";
	this->KVMap["Alert"] = "";
	this->KPMap["Alert"] = "Body";

}
AlertCreateContactRequest::AlertCreateContactRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "POST";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "/projects/[ProjectName]/contacts";
	this->ProductName = "alert";
	this->Action = "CreateContact";
	this->Version = "2015-08-15";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["ProjectName"] = "";
	this->KPMap["ProjectName"] = "Path";
	this->KVMap["Contact"] = "";
	this->KPMap["Contact"] = "Body";

}
AlertCreateContactGroupRequest::AlertCreateContactGroupRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "POST";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "/projects/[ProjectName]/groups";
	this->ProductName = "alert";
	this->Action = "CreateContactGroup";
	this->Version = "2015-08-15";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["ProjectName"] = "";
	this->KPMap["ProjectName"] = "Path";
	this->KVMap["ContactGroup"] = "";
	this->KPMap["ContactGroup"] = "Body";

}
AlertCreateDBMetricRequest::AlertCreateDBMetricRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "POST";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "/projects/[ProjectName]/dbMetrics";
	this->ProductName = "alert";
	this->Action = "CreateDBMetric";
	this->Version = "2015-08-15";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["ProjectName"] = "";
	this->KPMap["ProjectName"] = "Path";
	this->KVMap["Metric"] = "";
	this->KPMap["Metric"] = "Body";

}
AlertCreateDBSourceRequest::AlertCreateDBSourceRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "POST";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "/projects/[ProjectName]/sources";
	this->ProductName = "alert";
	this->Action = "CreateDBSource";
	this->Version = "2015-08-15";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["ProjectName"] = "";
	this->KPMap["ProjectName"] = "Path";
	this->KVMap["Source"] = "";
	this->KPMap["Source"] = "Body";

}
AlertCreateDimensionsRequest::AlertCreateDimensionsRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "POST";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "/projects/[ProjectName]/alerts/[AlertName]/dimensions";
	this->ProductName = "alert";
	this->Action = "CreateDimensions";
	this->Version = "2015-08-15";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["ProjectName"] = "";
	this->KPMap["ProjectName"] = "Path";
	this->KVMap["AlertName"] = "";
	this->KPMap["AlertName"] = "Path";
	this->KVMap["Dimensions"] = "";
	this->KPMap["Dimensions"] = "Body";

}
AlertCreateLevelChannelRequest::AlertCreateLevelChannelRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "POST";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "/projects/[ProjectName]/level_channels";
	this->ProductName = "alert";
	this->Action = "CreateLevelChannel";
	this->Version = "2015-08-15";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["ProjectName"] = "";
	this->KPMap["ProjectName"] = "Path";
	this->KVMap["LevelChannelSetting"] = "";
	this->KPMap["LevelChannelSetting"] = "Body";

}
AlertCreateLogHubMetricRequest::AlertCreateLogHubMetricRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "POST";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "/projects/[ProjectName]/logHubMetrics";
	this->ProductName = "alert";
	this->Action = "CreateLogHubMetric";
	this->Version = "2015-08-15";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["ProjectName"] = "";
	this->KPMap["ProjectName"] = "Path";
	this->KVMap["Metric"] = "";
	this->KPMap["Metric"] = "Body";

}
AlertCreateProjectRequest::AlertCreateProjectRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "POST";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "/projects";
	this->ProductName = "alert";
	this->Action = "CreateProject";
	this->Version = "2015-08-15";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["Project"] = "";
	this->KPMap["Project"] = "Body";

}
AlertDeleteAlertRequest::AlertDeleteAlertRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "DELETE";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "/projects/[ProjectName]/alerts/[AlertName]";
	this->ProductName = "alert";
	this->Action = "DeleteAlert";
	this->Version = "2015-08-15";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["ProjectName"] = "";
	this->KPMap["ProjectName"] = "Path";
	this->KVMap["AlertName"] = "";
	this->KPMap["AlertName"] = "Path";

}
AlertDeleteContactRequest::AlertDeleteContactRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "DELETE";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "/projects/[ProjectName]/contacts/[ContactName]";
	this->ProductName = "alert";
	this->Action = "DeleteContact";
	this->Version = "2015-08-15";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["ProjectName"] = "";
	this->KPMap["ProjectName"] = "Path";
	this->KVMap["ContactName"] = "";
	this->KPMap["ContactName"] = "Path";

}
AlertDeleteContactGroupRequest::AlertDeleteContactGroupRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "DELETE";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "/projects/[ProjectName]/groups/[GroupName]";
	this->ProductName = "alert";
	this->Action = "DeleteContactGroup";
	this->Version = "2015-08-15";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["ProjectName"] = "";
	this->KPMap["ProjectName"] = "Path";
	this->KVMap["GroupName"] = "";
	this->KPMap["GroupName"] = "Path";

}
AlertDeleteDBMetricRequest::AlertDeleteDBMetricRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "DELETE";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "/projects/[ProjectName]/dbMetrics/[MetricName]";
	this->ProductName = "alert";
	this->Action = "DeleteDBMetric";
	this->Version = "2015-08-15";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["ProjectName"] = "";
	this->KPMap["ProjectName"] = "Path";
	this->KVMap["MetricName"] = "";
	this->KPMap["MetricName"] = "Path";

}
AlertDeleteDBSourceRequest::AlertDeleteDBSourceRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "DELETE";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "/projects/[ProjectName]/sources/[SourceName]";
	this->ProductName = "alert";
	this->Action = "DeleteDBSource";
	this->Version = "2015-08-15";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["ProjectName"] = "";
	this->KPMap["ProjectName"] = "Path";
	this->KVMap["SourceName"] = "";
	this->KPMap["SourceName"] = "Path";

}
AlertDeleteDimensionsRequest::AlertDeleteDimensionsRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "DELETE";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "/projects/[ProjectName]/alerts/[AlertName]/dimensions/[DimensionsId]";
	this->ProductName = "alert";
	this->Action = "DeleteDimensions";
	this->Version = "2015-08-15";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["ProjectName"] = "";
	this->KPMap["ProjectName"] = "Path";
	this->KVMap["AlertName"] = "";
	this->KPMap["AlertName"] = "Path";
	this->KVMap["DimensionsId"] = "";
	this->KPMap["DimensionsId"] = "Path";

}
AlertDeleteLevelChannelRequest::AlertDeleteLevelChannelRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "DELETE";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "/projects/[ProjectName]/level_channels/[Level]";
	this->ProductName = "alert";
	this->Action = "DeleteLevelChannel";
	this->Version = "2015-08-15";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["ProjectName"] = "";
	this->KPMap["ProjectName"] = "Path";
	this->KVMap["Level"] = "";
	this->KPMap["Level"] = "Path";

}
AlertDeleteLogHubMetricRequest::AlertDeleteLogHubMetricRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "DELETE";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "/projects/[ProjectName]/logHubMetrics/[MetricName]";
	this->ProductName = "alert";
	this->Action = "DeleteLogHubMetric";
	this->Version = "2015-08-15";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["ProjectName"] = "";
	this->KPMap["ProjectName"] = "Path";
	this->KVMap["MetricName"] = "";
	this->KPMap["MetricName"] = "Path";

}
AlertDeleteProjectRequest::AlertDeleteProjectRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "DELETE";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "/projects/[ProjectName]";
	this->ProductName = "alert";
	this->Action = "DeleteProject";
	this->Version = "2015-08-15";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["ProjectName"] = "";
	this->KPMap["ProjectName"] = "Path";

}
AlertDisableAlertRequest::AlertDisableAlertRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "PUT";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "/projects/[ProjectName]/alerts/[AlertName]/disable";
	this->ProductName = "alert";
	this->Action = "DisableAlert";
	this->Version = "2015-08-15";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["ProjectName"] = "";
	this->KPMap["ProjectName"] = "Path";
	this->KVMap["AlertName"] = "";
	this->KPMap["AlertName"] = "Path";

}
AlertEnableAlertRequest::AlertEnableAlertRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "PUT";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "/projects/[ProjectName]/alerts/[AlertName]/enable";
	this->ProductName = "alert";
	this->Action = "EnableAlert";
	this->Version = "2015-08-15";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["ProjectName"] = "";
	this->KPMap["ProjectName"] = "Path";
	this->KVMap["AlertName"] = "";
	this->KPMap["AlertName"] = "Path";

}
AlertGetAlertRequest::AlertGetAlertRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "/projects/[ProjectName]/alerts/[AlertName]";
	this->ProductName = "alert";
	this->Action = "GetAlert";
	this->Version = "2015-08-15";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["ProjectName"] = "";
	this->KPMap["ProjectName"] = "Path";
	this->KVMap["AlertName"] = "";
	this->KPMap["AlertName"] = "Path";

}
AlertGetContactRequest::AlertGetContactRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "/projects/[ProjectName]/contacts/[ContactName]";
	this->ProductName = "alert";
	this->Action = "GetContact";
	this->Version = "2015-08-15";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["ProjectName"] = "";
	this->KPMap["ProjectName"] = "Path";
	this->KVMap["ContactName"] = "";
	this->KPMap["ContactName"] = "Path";

}
AlertGetContactGroupRequest::AlertGetContactGroupRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "/projects/[ProjectName]/groups/[GroupName]";
	this->ProductName = "alert";
	this->Action = "GetContactGroup";
	this->Version = "2015-08-15";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["ProjectName"] = "";
	this->KPMap["ProjectName"] = "Path";
	this->KVMap["GroupName"] = "";
	this->KPMap["GroupName"] = "Path";

}
AlertGetDBMetricRequest::AlertGetDBMetricRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "/projects/[ProjectName]/dbMetrics/[MetricName]";
	this->ProductName = "alert";
	this->Action = "GetDBMetric";
	this->Version = "2015-08-15";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["ProjectName"] = "";
	this->KPMap["ProjectName"] = "Path";
	this->KVMap["MetricName"] = "";
	this->KPMap["MetricName"] = "Path";

}
AlertGetDBSourceRequest::AlertGetDBSourceRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "/projects/[ProjectName]/sources/[SourceName]";
	this->ProductName = "alert";
	this->Action = "GetDBSource";
	this->Version = "2015-08-15";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["ProjectName"] = "";
	this->KPMap["ProjectName"] = "Path";
	this->KVMap["SourceName"] = "";
	this->KPMap["SourceName"] = "Path";

}
AlertGetDimensionsRequest::AlertGetDimensionsRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "/projects/[ProjectName]/alerts/[AlertName]/dimensions/[DimensionsId]";
	this->ProductName = "alert";
	this->Action = "GetDimensions";
	this->Version = "2015-08-15";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["ProjectName"] = "";
	this->KPMap["ProjectName"] = "Path";
	this->KVMap["AlertName"] = "";
	this->KPMap["AlertName"] = "Path";
	this->KVMap["DimensionsId"] = "";
	this->KPMap["DimensionsId"] = "Path";

}
AlertGetLevelChannelRequest::AlertGetLevelChannelRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "/projects/[ProjectName]/level_channels/[Level]";
	this->ProductName = "alert";
	this->Action = "GetLevelChannel";
	this->Version = "2015-08-15";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["ProjectName"] = "";
	this->KPMap["ProjectName"] = "Path";
	this->KVMap["Level"] = "";
	this->KPMap["Level"] = "Path";

}
AlertGetLogHubMetricRequest::AlertGetLogHubMetricRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "/projects/[ProjectName]/logHubMetrics/[MetricName]";
	this->ProductName = "alert";
	this->Action = "GetLogHubMetric";
	this->Version = "2015-08-15";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["ProjectName"] = "";
	this->KPMap["ProjectName"] = "Path";
	this->KVMap["MetricName"] = "";
	this->KPMap["MetricName"] = "Path";

}
AlertGetProjectRequest::AlertGetProjectRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "/projects/[ProjectName]";
	this->ProductName = "alert";
	this->Action = "GetProject";
	this->Version = "2015-08-15";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["ProjectName"] = "";
	this->KPMap["ProjectName"] = "Path";

}
AlertGrantProjectOwnerRequest::AlertGrantProjectOwnerRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "/projects/grantOwner";
	this->ProductName = "alert";
	this->Action = "GrantProjectOwner";
	this->Version = "2015-08-15";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["ProjectName"] = "";
	this->KPMap["ProjectName"] = "Query";
	this->KVMap["Owners"] = "";
	this->KPMap["Owners"] = "Query";

}
AlertListAlertRequest::AlertListAlertRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "/projects/[ProjectName]/alerts";
	this->ProductName = "alert";
	this->Action = "ListAlert";
	this->Version = "2015-08-15";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["ProjectName"] = "";
	this->KPMap["ProjectName"] = "Path";
	this->KVMap["AlertName"] = "";
	this->KPMap["AlertName"] = "Query";
	this->KVMap["Page"] = "";
	this->KPMap["Page"] = "Query";
	this->KVMap["PageSize"] = "";
	this->KPMap["PageSize"] = "Query";

}
AlertListAlertStateRequest::AlertListAlertStateRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "/projects/[ProjectName]/alerts_state";
	this->ProductName = "alert";
	this->Action = "ListAlertState";
	this->Version = "2015-08-15";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["ProjectName"] = "";
	this->KPMap["ProjectName"] = "Path";
	this->KVMap["AlertName"] = "";
	this->KPMap["AlertName"] = "Query";
	this->KVMap["Dimensions"] = "";
	this->KPMap["Dimensions"] = "Query";
	this->KVMap["StartTime"] = "";
	this->KPMap["StartTime"] = "Query";
	this->KVMap["EndTime"] = "";
	this->KPMap["EndTime"] = "Query";
	this->KVMap["Page"] = "";
	this->KPMap["Page"] = "Query";
	this->KVMap["PageSize"] = "";
	this->KPMap["PageSize"] = "Query";

}
AlertListContactRequest::AlertListContactRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "/projects/[ProjectName]/contacts";
	this->ProductName = "alert";
	this->Action = "ListContact";
	this->Version = "2015-08-15";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["ProjectName"] = "";
	this->KPMap["ProjectName"] = "Path";
	this->KVMap["ContactName"] = "";
	this->KPMap["ContactName"] = "Query";
	this->KVMap["Page"] = "";
	this->KPMap["Page"] = "Query";
	this->KVMap["PageSize"] = "";
	this->KPMap["PageSize"] = "Query";

}
AlertListContactGroupRequest::AlertListContactGroupRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "/projects/[ProjectName]/groups";
	this->ProductName = "alert";
	this->Action = "ListContactGroup";
	this->Version = "2015-08-15";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["ProjectName"] = "";
	this->KPMap["ProjectName"] = "Path";
	this->KVMap["GroupName"] = "";
	this->KPMap["GroupName"] = "Query";
	this->KVMap["Page"] = "";
	this->KPMap["Page"] = "Query";
	this->KVMap["PageSize"] = "";
	this->KPMap["PageSize"] = "Query";

}
AlertListDBMetricRequest::AlertListDBMetricRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "/projects/[ProjectName]/dbMetrics";
	this->ProductName = "alert";
	this->Action = "ListDBMetric";
	this->Version = "2015-08-15";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["ProjectName"] = "";
	this->KPMap["ProjectName"] = "Path";
	this->KVMap["MetricName"] = "";
	this->KPMap["MetricName"] = "Query";
	this->KVMap["Page"] = "";
	this->KPMap["Page"] = "Query";
	this->KVMap["PageSize"] = "";
	this->KPMap["PageSize"] = "Query";

}
AlertListDBSourceRequest::AlertListDBSourceRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "/projects/[ProjectName]/sources";
	this->ProductName = "alert";
	this->Action = "ListDBSource";
	this->Version = "2015-08-15";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["ProjectName"] = "";
	this->KPMap["ProjectName"] = "Path";
	this->KVMap["SourceName"] = "";
	this->KPMap["SourceName"] = "Query";
	this->KVMap["Page"] = "";
	this->KPMap["Page"] = "Query";
	this->KVMap["PageSize"] = "";
	this->KPMap["PageSize"] = "Query";

}
AlertListDimensionsRequest::AlertListDimensionsRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "/projects/[ProjectName]/alerts/[AlertName]/dimensions";
	this->ProductName = "alert";
	this->Action = "ListDimensions";
	this->Version = "2015-08-15";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["ProjectName"] = "";
	this->KPMap["ProjectName"] = "Path";
	this->KVMap["AlertName"] = "";
	this->KPMap["AlertName"] = "Path";
	this->KVMap["Page"] = "";
	this->KPMap["Page"] = "Query";
	this->KVMap["PageSize"] = "";
	this->KPMap["PageSize"] = "Query";

}
AlertListLevelChannelRequest::AlertListLevelChannelRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "/projects/[ProjectName]/level_channels";
	this->ProductName = "alert";
	this->Action = "ListLevelChannel";
	this->Version = "2015-08-15";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["ProjectName"] = "";
	this->KPMap["ProjectName"] = "Path";
	this->KVMap["Level"] = "";
	this->KPMap["Level"] = "Query";
	this->KVMap["Page"] = "";
	this->KPMap["Page"] = "Query";
	this->KVMap["PageSize"] = "";
	this->KPMap["PageSize"] = "Query";

}
AlertListLogHubMetricRequest::AlertListLogHubMetricRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "/projects/[ProjectName]/logHubMetrics";
	this->ProductName = "alert";
	this->Action = "ListLogHubMetric";
	this->Version = "2015-08-15";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["ProjectName"] = "";
	this->KPMap["ProjectName"] = "Path";
	this->KVMap["MetricName"] = "";
	this->KPMap["MetricName"] = "Query";
	this->KVMap["Page"] = "";
	this->KPMap["Page"] = "Query";
	this->KVMap["PageSize"] = "";
	this->KPMap["PageSize"] = "Query";

}
AlertListNotifyHistoryRequest::AlertListNotifyHistoryRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "/projects/[ProjectName]/notify_history";
	this->ProductName = "alert";
	this->Action = "ListNotifyHistory";
	this->Version = "2015-08-15";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["ProjectName"] = "";
	this->KPMap["ProjectName"] = "Path";
	this->KVMap["AlertName"] = "";
	this->KPMap["AlertName"] = "Query";
	this->KVMap["Dimensions"] = "";
	this->KPMap["Dimensions"] = "Query";
	this->KVMap["StartTime"] = "";
	this->KPMap["StartTime"] = "Query";
	this->KVMap["EndTime"] = "";
	this->KPMap["EndTime"] = "Query";
	this->KVMap["Page"] = "";
	this->KPMap["Page"] = "Query";
	this->KVMap["PageSize"] = "";
	this->KPMap["PageSize"] = "Query";

}
AlertListProjectRequest::AlertListProjectRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "/projects";
	this->ProductName = "alert";
	this->Action = "ListProject";
	this->Version = "2015-08-15";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["ProjectOwner"] = "";
	this->KPMap["ProjectOwner"] = "Query";
	this->KVMap["Page"] = "";
	this->KPMap["Page"] = "Query";
	this->KVMap["PageSize"] = "";
	this->KPMap["PageSize"] = "Query";

}
AlertRemoveProjectOwnerRequest::AlertRemoveProjectOwnerRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "/projects/removeOwner";
	this->ProductName = "alert";
	this->Action = "RemoveProjectOwner";
	this->Version = "2015-08-15";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["ProjectName"] = "";
	this->KPMap["ProjectName"] = "Query";
	this->KVMap["Owners"] = "";
	this->KPMap["Owners"] = "Query";

}
AlertUpdateAlertRequest::AlertUpdateAlertRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "PUT";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "/projects/[ProjectName]/alerts/[AlertName]";
	this->ProductName = "alert";
	this->Action = "UpdateAlert";
	this->Version = "2015-08-15";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["ProjectName"] = "";
	this->KPMap["ProjectName"] = "Path";
	this->KVMap["AlertName"] = "";
	this->KPMap["AlertName"] = "Path";
	this->KVMap["Alert"] = "";
	this->KPMap["Alert"] = "Body";

}
AlertUpdateContactRequest::AlertUpdateContactRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "PUT";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "/projects/[ProjectName]/contacts/[ContactName]";
	this->ProductName = "alert";
	this->Action = "UpdateContact";
	this->Version = "2015-08-15";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["ProjectName"] = "";
	this->KPMap["ProjectName"] = "Path";
	this->KVMap["ContactName"] = "";
	this->KPMap["ContactName"] = "Path";
	this->KVMap["Contact"] = "";
	this->KPMap["Contact"] = "Body";

}
AlertUpdateContactGroupRequest::AlertUpdateContactGroupRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "PUT";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "/projects/[ProjectName]/groups/[GroupName]";
	this->ProductName = "alert";
	this->Action = "UpdateContactGroup";
	this->Version = "2015-08-15";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["ProjectName"] = "";
	this->KPMap["ProjectName"] = "Path";
	this->KVMap["GroupName"] = "";
	this->KPMap["GroupName"] = "Path";
	this->KVMap["ContactGroup"] = "";
	this->KPMap["ContactGroup"] = "Body";

}
AlertUpdateDBMetricRequest::AlertUpdateDBMetricRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "PUT";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "/projects/[ProjectName]/dbMetrics/[MetricName]";
	this->ProductName = "alert";
	this->Action = "UpdateDBMetric";
	this->Version = "2015-08-15";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["ProjectName"] = "";
	this->KPMap["ProjectName"] = "Path";
	this->KVMap["MetricName"] = "";
	this->KPMap["MetricName"] = "Path";
	this->KVMap["Metric"] = "";
	this->KPMap["Metric"] = "Body";

}
AlertUpdateDBSourceRequest::AlertUpdateDBSourceRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "PUT";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "/projects/[ProjectName]/sources/[SourceName]";
	this->ProductName = "alert";
	this->Action = "UpdateDBSource";
	this->Version = "2015-08-15";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["ProjectName"] = "";
	this->KPMap["ProjectName"] = "Path";
	this->KVMap["SourceName"] = "";
	this->KPMap["SourceName"] = "Path";
	this->KVMap["Source"] = "";
	this->KPMap["Source"] = "Body";

}
AlertUpdateDimensionsRequest::AlertUpdateDimensionsRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "PUT";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "/projects/[ProjectName]/alerts/[AlertName]/dimensions/[DimensionsId]";
	this->ProductName = "alert";
	this->Action = "UpdateDimensions";
	this->Version = "2015-08-15";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["ProjectName"] = "";
	this->KPMap["ProjectName"] = "Path";
	this->KVMap["AlertName"] = "";
	this->KPMap["AlertName"] = "Path";
	this->KVMap["DimensionsId"] = "";
	this->KPMap["DimensionsId"] = "Path";
	this->KVMap["Dimensions"] = "";
	this->KPMap["Dimensions"] = "Body";

}
AlertUpdateLevelChannelRequest::AlertUpdateLevelChannelRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "PUT";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "/projects/[ProjectName]/level_channels/[Level]";
	this->ProductName = "alert";
	this->Action = "UpdateLevelChannel";
	this->Version = "2015-08-15";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["ProjectName"] = "";
	this->KPMap["ProjectName"] = "Path";
	this->KVMap["Level"] = "";
	this->KPMap["Level"] = "Path";
	this->KVMap["LevelChannelSetting"] = "";
	this->KPMap["LevelChannelSetting"] = "Body";

}
AlertUpdateLogHubMetricRequest::AlertUpdateLogHubMetricRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "PUT";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "/projects/[ProjectName]/logHubMetrics/[MetricName]";
	this->ProductName = "alert";
	this->Action = "UpdateLogHubMetric";
	this->Version = "2015-08-15";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["ProjectName"] = "";
	this->KPMap["ProjectName"] = "Path";
	this->KVMap["MetricName"] = "";
	this->KPMap["MetricName"] = "Path";
	this->KVMap["Metric"] = "";
	this->KPMap["Metric"] = "Body";

}
AlertUpdateProjectRequest::AlertUpdateProjectRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "PUT";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "/projects/[ProjectName]";
	this->ProductName = "alert";
	this->Action = "UpdateProject";
	this->Version = "2015-08-15";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["ProjectName"] = "";
	this->KPMap["ProjectName"] = "Path";
	this->KVMap["Project"] = "";
	this->KPMap["Project"] = "Body";

}

	
}