/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
 
#ifndef ALIYUNOTS_OTSSERVICE_H_
#define ALIYUNOTS_OTSSERVICE_H_

#include "../aliyun-cpp-sdk-core/BaseRequest.h"
#include "../aliyun-cpp-sdk-core/ApiBase.h"

namespace AliYunOts {

class OtsClient : public AliYunCore::ApiBase {
private:
	AliYunCore::DefaultProfile &Profile;
public:
	OtsClient(AliYunCore::DefaultProfile &pf);
	void setProfile(AliYunCore::DefaultProfile &pf);
	std::string getResponse(AliYunCore::BaseRequest &request);
	~OtsClient(){};
};
class OtsDeleteInstanceRequest : public AliYunCore::BaseRequest {
public:
	OtsDeleteInstanceRequest();
};
class OtsDeleteUserRequest : public AliYunCore::BaseRequest {
public:
	OtsDeleteUserRequest();
};
class OtsGetInstanceRequest : public AliYunCore::BaseRequest {
public:
	OtsGetInstanceRequest();
};
class OtsGetUserRequest : public AliYunCore::BaseRequest {
public:
	OtsGetUserRequest();
};
class OtsInsertInstanceRequest : public AliYunCore::BaseRequest {
public:
	OtsInsertInstanceRequest();
};
class OtsInsertUserRequest : public AliYunCore::BaseRequest {
public:
	OtsInsertUserRequest();
};
class OtsListInstanceRequest : public AliYunCore::BaseRequest {
public:
	OtsListInstanceRequest();
};
class OtsUpdateInstanceRequest : public AliYunCore::BaseRequest {
public:
	OtsUpdateInstanceRequest();
};
class OtsUpdateUserRequest : public AliYunCore::BaseRequest {
public:
	OtsUpdateUserRequest();
};


}

#endif