/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

#include <string>
#include <map>
#include "../aliyun-cpp-sdk-core/utils.h"
#include "../aliyun-cpp-sdk-core/DefaultProfile.h"
#include "../aliyun-cpp-sdk-core/BaseRequest.h"
#include "../aliyun-cpp-sdk-core/ApiBase.h"
#include "OtsService.h"

using std::string;
using namespace AliYunCore;

namespace AliYunOts {

OtsClient::OtsClient(DefaultProfile &pf):Profile(pf){}
void OtsClient::setProfile(DefaultProfile &pf){
	this->Profile = pf;
}
string OtsClient::getResponse(BaseRequest &request){
	request.RegionId = this->Profile.RegionId;		
	request.AccessKeyId = this->Profile.AccessKeyId;
	request.AccessKeySecret = this->Profile.AccessKeySecret;
	return ApiBase::getResponse(request);
}
OtsDeleteInstanceRequest::OtsDeleteInstanceRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "POST";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "ots";
	this->Action = "DeleteInstance";
	this->Version = "2013-09-12";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["InstanceName"] = "";
	this->KPMap["InstanceName"] = "Query";

}
OtsDeleteUserRequest::OtsDeleteUserRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "POST";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "ots";
	this->Action = "DeleteUser";
	this->Version = "2013-09-12";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 

}
OtsGetInstanceRequest::OtsGetInstanceRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "ots";
	this->Action = "GetInstance";
	this->Version = "2013-09-12";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["InstanceName"] = "";
	this->KPMap["InstanceName"] = "Query";

}
OtsGetUserRequest::OtsGetUserRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "ots";
	this->Action = "GetUser";
	this->Version = "2013-09-12";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 

}
OtsInsertInstanceRequest::OtsInsertInstanceRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "POST";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "ots";
	this->Action = "InsertInstance";
	this->Version = "2013-09-12";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["InstanceName"] = "";
	this->KPMap["InstanceName"] = "Query";
	this->KVMap["ClusterName"] = "";
	this->KPMap["ClusterName"] = "Query";
	this->KVMap["WriteCapacity"] = "";
	this->KPMap["WriteCapacity"] = "Query";
	this->KVMap["ReadCapacity"] = "";
	this->KPMap["ReadCapacity"] = "Query";
	this->KVMap["EntityQuota"] = "";
	this->KPMap["EntityQuota"] = "Query";
	this->KVMap["Description"] = "";
	this->KPMap["Description"] = "Query";

}
OtsInsertUserRequest::OtsInsertUserRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "POST";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "ots";
	this->Action = "InsertUser";
	this->Version = "2013-09-12";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["InstanceQuota"] = "";
	this->KPMap["InstanceQuota"] = "Query";
	this->KVMap["Description"] = "";
	this->KPMap["Description"] = "Query";

}
OtsListInstanceRequest::OtsListInstanceRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "ots";
	this->Action = "ListInstance";
	this->Version = "2013-09-12";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 

}
OtsUpdateInstanceRequest::OtsUpdateInstanceRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "POST";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "ots";
	this->Action = "UpdateInstance";
	this->Version = "2013-09-12";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["InstanceName"] = "";
	this->KPMap["InstanceName"] = "Query";
	this->KVMap["WriteCapacity"] = "";
	this->KPMap["WriteCapacity"] = "Query";
	this->KVMap["ReadCapacity"] = "";
	this->KPMap["ReadCapacity"] = "Query";
	this->KVMap["EntityQuota"] = "";
	this->KPMap["EntityQuota"] = "Query";
	this->KVMap["Description"] = "";
	this->KPMap["Description"] = "Query";

}
OtsUpdateUserRequest::OtsUpdateUserRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "POST";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "ots";
	this->Action = "UpdateUser";
	this->Version = "2013-09-12";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["InstanceQuota"] = "";
	this->KPMap["InstanceQuota"] = "Query";
	this->KVMap["Description"] = "";
	this->KPMap["Description"] = "Query";

}

	
}