/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
 
#ifndef ALIYUNRAM_RAMSERVICE_H_
#define ALIYUNRAM_RAMSERVICE_H_

#include "../aliyun-cpp-sdk-core/BaseRequest.h"
#include "../aliyun-cpp-sdk-core/ApiBase.h"

namespace AliYunRam {

class RamClient : public AliYunCore::ApiBase {
private:
	AliYunCore::DefaultProfile &Profile;
public:
	RamClient(AliYunCore::DefaultProfile &pf);
	void setProfile(AliYunCore::DefaultProfile &pf);
	std::string getResponse(AliYunCore::BaseRequest &request);
	~RamClient(){};
};
class RamActivateServiceRequest : public AliYunCore::BaseRequest {
public:
	RamActivateServiceRequest();
};
class RamAddUserToGroupRequest : public AliYunCore::BaseRequest {
public:
	RamAddUserToGroupRequest();
};
class RamAttachPolicyToGroupRequest : public AliYunCore::BaseRequest {
public:
	RamAttachPolicyToGroupRequest();
};
class RamAttachPolicyToRoleRequest : public AliYunCore::BaseRequest {
public:
	RamAttachPolicyToRoleRequest();
};
class RamAttachPolicyToUserRequest : public AliYunCore::BaseRequest {
public:
	RamAttachPolicyToUserRequest();
};
class RamBindMFADeviceRequest : public AliYunCore::BaseRequest {
public:
	RamBindMFADeviceRequest();
};
class RamClearAccountAliasRequest : public AliYunCore::BaseRequest {
public:
	RamClearAccountAliasRequest();
};
class RamCreateAccessKeyRequest : public AliYunCore::BaseRequest {
public:
	RamCreateAccessKeyRequest();
};
class RamCreateGroupRequest : public AliYunCore::BaseRequest {
public:
	RamCreateGroupRequest();
};
class RamCreateLoginProfileRequest : public AliYunCore::BaseRequest {
public:
	RamCreateLoginProfileRequest();
};
class RamCreatePolicyRequest : public AliYunCore::BaseRequest {
public:
	RamCreatePolicyRequest();
};
class RamCreatePolicyVersionRequest : public AliYunCore::BaseRequest {
public:
	RamCreatePolicyVersionRequest();
};
class RamCreateRoleRequest : public AliYunCore::BaseRequest {
public:
	RamCreateRoleRequest();
};
class RamCreateUserRequest : public AliYunCore::BaseRequest {
public:
	RamCreateUserRequest();
};
class RamCreateVirtualMFADeviceRequest : public AliYunCore::BaseRequest {
public:
	RamCreateVirtualMFADeviceRequest();
};
class RamDeactivateServiceRequest : public AliYunCore::BaseRequest {
public:
	RamDeactivateServiceRequest();
};
class RamDeleteAccessKeyRequest : public AliYunCore::BaseRequest {
public:
	RamDeleteAccessKeyRequest();
};
class RamDeleteGroupRequest : public AliYunCore::BaseRequest {
public:
	RamDeleteGroupRequest();
};
class RamDeleteLoginProfileRequest : public AliYunCore::BaseRequest {
public:
	RamDeleteLoginProfileRequest();
};
class RamDeletePolicyRequest : public AliYunCore::BaseRequest {
public:
	RamDeletePolicyRequest();
};
class RamDeletePolicyVersionRequest : public AliYunCore::BaseRequest {
public:
	RamDeletePolicyVersionRequest();
};
class RamDeleteRoleRequest : public AliYunCore::BaseRequest {
public:
	RamDeleteRoleRequest();
};
class RamDeleteUserRequest : public AliYunCore::BaseRequest {
public:
	RamDeleteUserRequest();
};
class RamDeleteVirtualMFADeviceRequest : public AliYunCore::BaseRequest {
public:
	RamDeleteVirtualMFADeviceRequest();
};
class RamDetachPolicyFromGroupRequest : public AliYunCore::BaseRequest {
public:
	RamDetachPolicyFromGroupRequest();
};
class RamDetachPolicyFromRoleRequest : public AliYunCore::BaseRequest {
public:
	RamDetachPolicyFromRoleRequest();
};
class RamDetachPolicyFromUserRequest : public AliYunCore::BaseRequest {
public:
	RamDetachPolicyFromUserRequest();
};
class RamGetAccountAliasRequest : public AliYunCore::BaseRequest {
public:
	RamGetAccountAliasRequest();
};
class RamGetAccountSummaryRequest : public AliYunCore::BaseRequest {
public:
	RamGetAccountSummaryRequest();
};
class RamGetGroupRequest : public AliYunCore::BaseRequest {
public:
	RamGetGroupRequest();
};
class RamGetLoginProfileRequest : public AliYunCore::BaseRequest {
public:
	RamGetLoginProfileRequest();
};
class RamGetPasswordPolicyRequest : public AliYunCore::BaseRequest {
public:
	RamGetPasswordPolicyRequest();
};
class RamGetPolicyRequest : public AliYunCore::BaseRequest {
public:
	RamGetPolicyRequest();
};
class RamGetPolicyVersionRequest : public AliYunCore::BaseRequest {
public:
	RamGetPolicyVersionRequest();
};
class RamGetRoleRequest : public AliYunCore::BaseRequest {
public:
	RamGetRoleRequest();
};
class RamGetSecurityPreferenceRequest : public AliYunCore::BaseRequest {
public:
	RamGetSecurityPreferenceRequest();
};
class RamGetServiceStatusRequest : public AliYunCore::BaseRequest {
public:
	RamGetServiceStatusRequest();
};
class RamGetUserRequest : public AliYunCore::BaseRequest {
public:
	RamGetUserRequest();
};
class RamGetUserMFAInfoRequest : public AliYunCore::BaseRequest {
public:
	RamGetUserMFAInfoRequest();
};
class RamListAccessKeysRequest : public AliYunCore::BaseRequest {
public:
	RamListAccessKeysRequest();
};
class RamListEntitiesForPolicyRequest : public AliYunCore::BaseRequest {
public:
	RamListEntitiesForPolicyRequest();
};
class RamListGroupsRequest : public AliYunCore::BaseRequest {
public:
	RamListGroupsRequest();
};
class RamListGroupsForUserRequest : public AliYunCore::BaseRequest {
public:
	RamListGroupsForUserRequest();
};
class RamListPoliciesRequest : public AliYunCore::BaseRequest {
public:
	RamListPoliciesRequest();
};
class RamListPoliciesForGroupRequest : public AliYunCore::BaseRequest {
public:
	RamListPoliciesForGroupRequest();
};
class RamListPoliciesForRoleRequest : public AliYunCore::BaseRequest {
public:
	RamListPoliciesForRoleRequest();
};
class RamListPoliciesForUserRequest : public AliYunCore::BaseRequest {
public:
	RamListPoliciesForUserRequest();
};
class RamListPolicyVersionsRequest : public AliYunCore::BaseRequest {
public:
	RamListPolicyVersionsRequest();
};
class RamListRolesRequest : public AliYunCore::BaseRequest {
public:
	RamListRolesRequest();
};
class RamListUsersRequest : public AliYunCore::BaseRequest {
public:
	RamListUsersRequest();
};
class RamListUsersForGroupRequest : public AliYunCore::BaseRequest {
public:
	RamListUsersForGroupRequest();
};
class RamListVirtualMFADevicesRequest : public AliYunCore::BaseRequest {
public:
	RamListVirtualMFADevicesRequest();
};
class RamRemoveUserFromGroupRequest : public AliYunCore::BaseRequest {
public:
	RamRemoveUserFromGroupRequest();
};
class RamSetAccountAliasRequest : public AliYunCore::BaseRequest {
public:
	RamSetAccountAliasRequest();
};
class RamSetDefaultPolicyVersionRequest : public AliYunCore::BaseRequest {
public:
	RamSetDefaultPolicyVersionRequest();
};
class RamSetPasswordPolicyRequest : public AliYunCore::BaseRequest {
public:
	RamSetPasswordPolicyRequest();
};
class RamSetSecurityPreferenceRequest : public AliYunCore::BaseRequest {
public:
	RamSetSecurityPreferenceRequest();
};
class RamUnbindMFADeviceRequest : public AliYunCore::BaseRequest {
public:
	RamUnbindMFADeviceRequest();
};
class RamUpdateAccessKeyRequest : public AliYunCore::BaseRequest {
public:
	RamUpdateAccessKeyRequest();
};
class RamUpdateGroupRequest : public AliYunCore::BaseRequest {
public:
	RamUpdateGroupRequest();
};
class RamUpdateLoginProfileRequest : public AliYunCore::BaseRequest {
public:
	RamUpdateLoginProfileRequest();
};
class RamUpdateRoleRequest : public AliYunCore::BaseRequest {
public:
	RamUpdateRoleRequest();
};
class RamUpdateUserRequest : public AliYunCore::BaseRequest {
public:
	RamUpdateUserRequest();
};


}

#endif