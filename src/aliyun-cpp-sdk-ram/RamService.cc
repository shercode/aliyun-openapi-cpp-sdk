/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

#include <string>
#include <map>
#include "../aliyun-cpp-sdk-core/utils.h"
#include "../aliyun-cpp-sdk-core/DefaultProfile.h"
#include "../aliyun-cpp-sdk-core/BaseRequest.h"
#include "../aliyun-cpp-sdk-core/ApiBase.h"
#include "RamService.h"

using std::string;
using namespace AliYunCore;

namespace AliYunRam {

RamClient::RamClient(DefaultProfile &pf):Profile(pf){}
void RamClient::setProfile(DefaultProfile &pf){
	this->Profile = pf;
}
string RamClient::getResponse(BaseRequest &request){
	request.RegionId = this->Profile.RegionId;		
	request.AccessKeyId = this->Profile.AccessKeyId;
	request.AccessKeySecret = this->Profile.AccessKeySecret;
	return ApiBase::getResponse(request);
}
RamActivateServiceRequest::RamActivateServiceRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTPS";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "ram";
	this->Action = "ActivateService";
	this->Version = "2015-05-01";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["AccountId"] = "";
	this->KPMap["AccountId"] = "Query";

}
RamAddUserToGroupRequest::RamAddUserToGroupRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTPS";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "ram";
	this->Action = "AddUserToGroup";
	this->Version = "2015-05-01";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["UserName"] = "";
	this->KPMap["UserName"] = "Query";
	this->KVMap["GroupName"] = "";
	this->KPMap["GroupName"] = "Query";

}
RamAttachPolicyToGroupRequest::RamAttachPolicyToGroupRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTPS";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "ram";
	this->Action = "AttachPolicyToGroup";
	this->Version = "2015-05-01";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["PolicyType"] = "";
	this->KPMap["PolicyType"] = "Query";
	this->KVMap["PolicyName"] = "";
	this->KPMap["PolicyName"] = "Query";
	this->KVMap["GroupName"] = "";
	this->KPMap["GroupName"] = "Query";

}
RamAttachPolicyToRoleRequest::RamAttachPolicyToRoleRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTPS";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "ram";
	this->Action = "AttachPolicyToRole";
	this->Version = "2015-05-01";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["PolicyType"] = "";
	this->KPMap["PolicyType"] = "Query";
	this->KVMap["PolicyName"] = "";
	this->KPMap["PolicyName"] = "Query";
	this->KVMap["RoleName"] = "";
	this->KPMap["RoleName"] = "Query";

}
RamAttachPolicyToUserRequest::RamAttachPolicyToUserRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTPS";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "ram";
	this->Action = "AttachPolicyToUser";
	this->Version = "2015-05-01";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["PolicyType"] = "";
	this->KPMap["PolicyType"] = "Query";
	this->KVMap["PolicyName"] = "";
	this->KPMap["PolicyName"] = "Query";
	this->KVMap["UserName"] = "";
	this->KPMap["UserName"] = "Query";

}
RamBindMFADeviceRequest::RamBindMFADeviceRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTPS";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "ram";
	this->Action = "BindMFADevice";
	this->Version = "2015-05-01";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["SerialNumber"] = "";
	this->KPMap["SerialNumber"] = "Query";
	this->KVMap["UserName"] = "";
	this->KPMap["UserName"] = "Query";
	this->KVMap["AuthenticationCode1"] = "";
	this->KPMap["AuthenticationCode1"] = "Query";
	this->KVMap["AuthenticationCode2"] = "";
	this->KPMap["AuthenticationCode2"] = "Query";

}
RamClearAccountAliasRequest::RamClearAccountAliasRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTPS";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "ram";
	this->Action = "ClearAccountAlias";
	this->Version = "2015-05-01";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 

}
RamCreateAccessKeyRequest::RamCreateAccessKeyRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTPS";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "ram";
	this->Action = "CreateAccessKey";
	this->Version = "2015-05-01";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["UserName"] = "";
	this->KPMap["UserName"] = "Query";

}
RamCreateGroupRequest::RamCreateGroupRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTPS";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "ram";
	this->Action = "CreateGroup";
	this->Version = "2015-05-01";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["GroupName"] = "";
	this->KPMap["GroupName"] = "Query";
	this->KVMap["Comments"] = "";
	this->KPMap["Comments"] = "Query";

}
RamCreateLoginProfileRequest::RamCreateLoginProfileRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTPS";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "ram";
	this->Action = "CreateLoginProfile";
	this->Version = "2015-05-01";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["UserName"] = "";
	this->KPMap["UserName"] = "Query";
	this->KVMap["Password"] = "";
	this->KPMap["Password"] = "Query";
	this->KVMap["PasswordResetRequired"] = "";
	this->KPMap["PasswordResetRequired"] = "Query";
	this->KVMap["MFABindRequired"] = "";
	this->KPMap["MFABindRequired"] = "Query";

}
RamCreatePolicyRequest::RamCreatePolicyRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTPS";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "ram";
	this->Action = "CreatePolicy";
	this->Version = "2015-05-01";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["PolicyName"] = "";
	this->KPMap["PolicyName"] = "Query";
	this->KVMap["Description"] = "";
	this->KPMap["Description"] = "Query";
	this->KVMap["PolicyDocument"] = "";
	this->KPMap["PolicyDocument"] = "Query";

}
RamCreatePolicyVersionRequest::RamCreatePolicyVersionRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTPS";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "ram";
	this->Action = "CreatePolicyVersion";
	this->Version = "2015-05-01";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["PolicyName"] = "";
	this->KPMap["PolicyName"] = "Query";
	this->KVMap["PolicyDocument"] = "";
	this->KPMap["PolicyDocument"] = "Query";
	this->KVMap["SetAsDefault"] = "";
	this->KPMap["SetAsDefault"] = "Query";

}
RamCreateRoleRequest::RamCreateRoleRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTPS";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "ram";
	this->Action = "CreateRole";
	this->Version = "2015-05-01";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["RoleName"] = "";
	this->KPMap["RoleName"] = "Query";
	this->KVMap["Description"] = "";
	this->KPMap["Description"] = "Query";
	this->KVMap["AssumeRolePolicyDocument"] = "";
	this->KPMap["AssumeRolePolicyDocument"] = "Query";

}
RamCreateUserRequest::RamCreateUserRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTPS";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "ram";
	this->Action = "CreateUser";
	this->Version = "2015-05-01";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["UserName"] = "";
	this->KPMap["UserName"] = "Query";
	this->KVMap["DisplayName"] = "";
	this->KPMap["DisplayName"] = "Query";
	this->KVMap["MobilePhone"] = "";
	this->KPMap["MobilePhone"] = "Query";
	this->KVMap["Email"] = "";
	this->KPMap["Email"] = "Query";
	this->KVMap["Comments"] = "";
	this->KPMap["Comments"] = "Query";

}
RamCreateVirtualMFADeviceRequest::RamCreateVirtualMFADeviceRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTPS";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "ram";
	this->Action = "CreateVirtualMFADevice";
	this->Version = "2015-05-01";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["VirtualMFADeviceName"] = "";
	this->KPMap["VirtualMFADeviceName"] = "Query";

}
RamDeactivateServiceRequest::RamDeactivateServiceRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTPS";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "ram";
	this->Action = "DeactivateService";
	this->Version = "2015-05-01";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["AccountId"] = "";
	this->KPMap["AccountId"] = "Query";

}
RamDeleteAccessKeyRequest::RamDeleteAccessKeyRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTPS";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "ram";
	this->Action = "DeleteAccessKey";
	this->Version = "2015-05-01";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["UserName"] = "";
	this->KPMap["UserName"] = "Query";
	this->KVMap["UserAccessKeyId"] = "";
	this->KPMap["UserAccessKeyId"] = "Query";

}
RamDeleteGroupRequest::RamDeleteGroupRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTPS";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "ram";
	this->Action = "DeleteGroup";
	this->Version = "2015-05-01";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["GroupName"] = "";
	this->KPMap["GroupName"] = "Query";

}
RamDeleteLoginProfileRequest::RamDeleteLoginProfileRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTPS";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "ram";
	this->Action = "DeleteLoginProfile";
	this->Version = "2015-05-01";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["UserName"] = "";
	this->KPMap["UserName"] = "Query";

}
RamDeletePolicyRequest::RamDeletePolicyRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTPS";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "ram";
	this->Action = "DeletePolicy";
	this->Version = "2015-05-01";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["PolicyName"] = "";
	this->KPMap["PolicyName"] = "Query";

}
RamDeletePolicyVersionRequest::RamDeletePolicyVersionRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTPS";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "ram";
	this->Action = "DeletePolicyVersion";
	this->Version = "2015-05-01";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["PolicyName"] = "";
	this->KPMap["PolicyName"] = "Query";
	this->KVMap["VersionId"] = "";
	this->KPMap["VersionId"] = "Query";

}
RamDeleteRoleRequest::RamDeleteRoleRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTPS";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "ram";
	this->Action = "DeleteRole";
	this->Version = "2015-05-01";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["RoleName"] = "";
	this->KPMap["RoleName"] = "Query";

}
RamDeleteUserRequest::RamDeleteUserRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTPS";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "ram";
	this->Action = "DeleteUser";
	this->Version = "2015-05-01";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["UserName"] = "";
	this->KPMap["UserName"] = "Query";

}
RamDeleteVirtualMFADeviceRequest::RamDeleteVirtualMFADeviceRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTPS";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "ram";
	this->Action = "DeleteVirtualMFADevice";
	this->Version = "2015-05-01";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["SerialNumber"] = "";
	this->KPMap["SerialNumber"] = "Query";

}
RamDetachPolicyFromGroupRequest::RamDetachPolicyFromGroupRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTPS";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "ram";
	this->Action = "DetachPolicyFromGroup";
	this->Version = "2015-05-01";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["PolicyType"] = "";
	this->KPMap["PolicyType"] = "Query";
	this->KVMap["PolicyName"] = "";
	this->KPMap["PolicyName"] = "Query";
	this->KVMap["GroupName"] = "";
	this->KPMap["GroupName"] = "Query";

}
RamDetachPolicyFromRoleRequest::RamDetachPolicyFromRoleRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTPS";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "ram";
	this->Action = "DetachPolicyFromRole";
	this->Version = "2015-05-01";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["PolicyType"] = "";
	this->KPMap["PolicyType"] = "Query";
	this->KVMap["PolicyName"] = "";
	this->KPMap["PolicyName"] = "Query";
	this->KVMap["RoleName"] = "";
	this->KPMap["RoleName"] = "Query";

}
RamDetachPolicyFromUserRequest::RamDetachPolicyFromUserRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTPS";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "ram";
	this->Action = "DetachPolicyFromUser";
	this->Version = "2015-05-01";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["PolicyType"] = "";
	this->KPMap["PolicyType"] = "Query";
	this->KVMap["PolicyName"] = "";
	this->KPMap["PolicyName"] = "Query";
	this->KVMap["UserName"] = "";
	this->KPMap["UserName"] = "Query";

}
RamGetAccountAliasRequest::RamGetAccountAliasRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTPS";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "ram";
	this->Action = "GetAccountAlias";
	this->Version = "2015-05-01";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 

}
RamGetAccountSummaryRequest::RamGetAccountSummaryRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTPS";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "ram";
	this->Action = "GetAccountSummary";
	this->Version = "2015-05-01";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 

}
RamGetGroupRequest::RamGetGroupRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTPS";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "ram";
	this->Action = "GetGroup";
	this->Version = "2015-05-01";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["GroupName"] = "";
	this->KPMap["GroupName"] = "Query";

}
RamGetLoginProfileRequest::RamGetLoginProfileRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTPS";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "ram";
	this->Action = "GetLoginProfile";
	this->Version = "2015-05-01";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["UserName"] = "";
	this->KPMap["UserName"] = "Query";

}
RamGetPasswordPolicyRequest::RamGetPasswordPolicyRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTPS";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "ram";
	this->Action = "GetPasswordPolicy";
	this->Version = "2015-05-01";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 

}
RamGetPolicyRequest::RamGetPolicyRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTPS";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "ram";
	this->Action = "GetPolicy";
	this->Version = "2015-05-01";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["PolicyType"] = "";
	this->KPMap["PolicyType"] = "Query";
	this->KVMap["PolicyName"] = "";
	this->KPMap["PolicyName"] = "Query";

}
RamGetPolicyVersionRequest::RamGetPolicyVersionRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTPS";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "ram";
	this->Action = "GetPolicyVersion";
	this->Version = "2015-05-01";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["PolicyType"] = "";
	this->KPMap["PolicyType"] = "Query";
	this->KVMap["PolicyName"] = "";
	this->KPMap["PolicyName"] = "Query";
	this->KVMap["VersionId"] = "";
	this->KPMap["VersionId"] = "Query";

}
RamGetRoleRequest::RamGetRoleRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTPS";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "ram";
	this->Action = "GetRole";
	this->Version = "2015-05-01";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["RoleName"] = "";
	this->KPMap["RoleName"] = "Query";

}
RamGetSecurityPreferenceRequest::RamGetSecurityPreferenceRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTPS";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "ram";
	this->Action = "GetSecurityPreference";
	this->Version = "2015-05-01";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 

}
RamGetServiceStatusRequest::RamGetServiceStatusRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTPS";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "ram";
	this->Action = "GetServiceStatus";
	this->Version = "2015-05-01";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["AccountId"] = "";
	this->KPMap["AccountId"] = "Query";

}
RamGetUserRequest::RamGetUserRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTPS";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "ram";
	this->Action = "GetUser";
	this->Version = "2015-05-01";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["UserName"] = "";
	this->KPMap["UserName"] = "Query";

}
RamGetUserMFAInfoRequest::RamGetUserMFAInfoRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTPS";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "ram";
	this->Action = "GetUserMFAInfo";
	this->Version = "2015-05-01";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["UserName"] = "";
	this->KPMap["UserName"] = "Query";

}
RamListAccessKeysRequest::RamListAccessKeysRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTPS";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "ram";
	this->Action = "ListAccessKeys";
	this->Version = "2015-05-01";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["UserName"] = "";
	this->KPMap["UserName"] = "Query";

}
RamListEntitiesForPolicyRequest::RamListEntitiesForPolicyRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTPS";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "ram";
	this->Action = "ListEntitiesForPolicy";
	this->Version = "2015-05-01";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["PolicyType"] = "";
	this->KPMap["PolicyType"] = "Query";
	this->KVMap["PolicyName"] = "";
	this->KPMap["PolicyName"] = "Query";

}
RamListGroupsRequest::RamListGroupsRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTPS";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "ram";
	this->Action = "ListGroups";
	this->Version = "2015-05-01";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["Marker"] = "";
	this->KPMap["Marker"] = "Query";
	this->KVMap["MaxItems"] = "";
	this->KPMap["MaxItems"] = "Query";

}
RamListGroupsForUserRequest::RamListGroupsForUserRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTPS";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "ram";
	this->Action = "ListGroupsForUser";
	this->Version = "2015-05-01";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["UserName"] = "";
	this->KPMap["UserName"] = "Query";

}
RamListPoliciesRequest::RamListPoliciesRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTPS";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "ram";
	this->Action = "ListPolicies";
	this->Version = "2015-05-01";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["PolicyType"] = "";
	this->KPMap["PolicyType"] = "Query";
	this->KVMap["Marker"] = "";
	this->KPMap["Marker"] = "Query";
	this->KVMap["MaxItems"] = "";
	this->KPMap["MaxItems"] = "Query";

}
RamListPoliciesForGroupRequest::RamListPoliciesForGroupRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTPS";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "ram";
	this->Action = "ListPoliciesForGroup";
	this->Version = "2015-05-01";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["GroupName"] = "";
	this->KPMap["GroupName"] = "Query";

}
RamListPoliciesForRoleRequest::RamListPoliciesForRoleRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTPS";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "ram";
	this->Action = "ListPoliciesForRole";
	this->Version = "2015-05-01";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["RoleName"] = "";
	this->KPMap["RoleName"] = "Query";

}
RamListPoliciesForUserRequest::RamListPoliciesForUserRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTPS";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "ram";
	this->Action = "ListPoliciesForUser";
	this->Version = "2015-05-01";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["UserName"] = "";
	this->KPMap["UserName"] = "Query";

}
RamListPolicyVersionsRequest::RamListPolicyVersionsRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTPS";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "ram";
	this->Action = "ListPolicyVersions";
	this->Version = "2015-05-01";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["PolicyType"] = "";
	this->KPMap["PolicyType"] = "Query";
	this->KVMap["PolicyName"] = "";
	this->KPMap["PolicyName"] = "Query";

}
RamListRolesRequest::RamListRolesRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTPS";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "ram";
	this->Action = "ListRoles";
	this->Version = "2015-05-01";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["Marker"] = "";
	this->KPMap["Marker"] = "Query";
	this->KVMap["MaxItems"] = "";
	this->KPMap["MaxItems"] = "Query";

}
RamListUsersRequest::RamListUsersRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTPS";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "ram";
	this->Action = "ListUsers";
	this->Version = "2015-05-01";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["Marker"] = "";
	this->KPMap["Marker"] = "Query";
	this->KVMap["MaxItems"] = "";
	this->KPMap["MaxItems"] = "Query";

}
RamListUsersForGroupRequest::RamListUsersForGroupRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTPS";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "ram";
	this->Action = "ListUsersForGroup";
	this->Version = "2015-05-01";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["GroupName"] = "";
	this->KPMap["GroupName"] = "Query";

}
RamListVirtualMFADevicesRequest::RamListVirtualMFADevicesRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTPS";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "ram";
	this->Action = "ListVirtualMFADevices";
	this->Version = "2015-05-01";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 

}
RamRemoveUserFromGroupRequest::RamRemoveUserFromGroupRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTPS";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "ram";
	this->Action = "RemoveUserFromGroup";
	this->Version = "2015-05-01";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["UserName"] = "";
	this->KPMap["UserName"] = "Query";
	this->KVMap["GroupName"] = "";
	this->KPMap["GroupName"] = "Query";

}
RamSetAccountAliasRequest::RamSetAccountAliasRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTPS";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "ram";
	this->Action = "SetAccountAlias";
	this->Version = "2015-05-01";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["AccountAlias"] = "";
	this->KPMap["AccountAlias"] = "Query";

}
RamSetDefaultPolicyVersionRequest::RamSetDefaultPolicyVersionRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTPS";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "ram";
	this->Action = "SetDefaultPolicyVersion";
	this->Version = "2015-05-01";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["PolicyName"] = "";
	this->KPMap["PolicyName"] = "Query";
	this->KVMap["VersionId"] = "";
	this->KPMap["VersionId"] = "Query";

}
RamSetPasswordPolicyRequest::RamSetPasswordPolicyRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTPS";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "ram";
	this->Action = "SetPasswordPolicy";
	this->Version = "2015-05-01";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["MinimumPasswordLength"] = "";
	this->KPMap["MinimumPasswordLength"] = "Query";
	this->KVMap["RequireLowercaseCharacters"] = "";
	this->KPMap["RequireLowercaseCharacters"] = "Query";
	this->KVMap["RequireUppercaseCharacters"] = "";
	this->KPMap["RequireUppercaseCharacters"] = "Query";
	this->KVMap["RequireNumbers"] = "";
	this->KPMap["RequireNumbers"] = "Query";
	this->KVMap["RequireSymbols"] = "";
	this->KPMap["RequireSymbols"] = "Query";

}
RamSetSecurityPreferenceRequest::RamSetSecurityPreferenceRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTPS";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "ram";
	this->Action = "SetSecurityPreference";
	this->Version = "2015-05-01";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["EnableSaveMFATicket"] = "";
	this->KPMap["EnableSaveMFATicket"] = "Query";

}
RamUnbindMFADeviceRequest::RamUnbindMFADeviceRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTPS";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "ram";
	this->Action = "UnbindMFADevice";
	this->Version = "2015-05-01";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["UserName"] = "";
	this->KPMap["UserName"] = "Query";

}
RamUpdateAccessKeyRequest::RamUpdateAccessKeyRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTPS";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "ram";
	this->Action = "UpdateAccessKey";
	this->Version = "2015-05-01";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["UserName"] = "";
	this->KPMap["UserName"] = "Query";
	this->KVMap["UserAccessKeyId"] = "";
	this->KPMap["UserAccessKeyId"] = "Query";
	this->KVMap["Status"] = "";
	this->KPMap["Status"] = "Query";

}
RamUpdateGroupRequest::RamUpdateGroupRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTPS";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "ram";
	this->Action = "UpdateGroup";
	this->Version = "2015-05-01";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["GroupName"] = "";
	this->KPMap["GroupName"] = "Query";
	this->KVMap["NewGroupName"] = "";
	this->KPMap["NewGroupName"] = "Query";
	this->KVMap["NewComments"] = "";
	this->KPMap["NewComments"] = "Query";

}
RamUpdateLoginProfileRequest::RamUpdateLoginProfileRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTPS";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "ram";
	this->Action = "UpdateLoginProfile";
	this->Version = "2015-05-01";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["UserName"] = "";
	this->KPMap["UserName"] = "Query";
	this->KVMap["Password"] = "";
	this->KPMap["Password"] = "Query";
	this->KVMap["PasswordResetRequired"] = "";
	this->KPMap["PasswordResetRequired"] = "Query";
	this->KVMap["MFABindRequired"] = "";
	this->KPMap["MFABindRequired"] = "Query";

}
RamUpdateRoleRequest::RamUpdateRoleRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTPS";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "ram";
	this->Action = "UpdateRole";
	this->Version = "2015-05-01";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["RoleName"] = "";
	this->KPMap["RoleName"] = "Query";
	this->KVMap["NewAssumeRolePolicyDocument"] = "";
	this->KPMap["NewAssumeRolePolicyDocument"] = "Query";

}
RamUpdateUserRequest::RamUpdateUserRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTPS";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "ram";
	this->Action = "UpdateUser";
	this->Version = "2015-05-01";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["UserName"] = "";
	this->KPMap["UserName"] = "Query";
	this->KVMap["NewUserName"] = "";
	this->KPMap["NewUserName"] = "Query";
	this->KVMap["NewDisplayName"] = "";
	this->KPMap["NewDisplayName"] = "Query";
	this->KVMap["NewMobilePhone"] = "";
	this->KPMap["NewMobilePhone"] = "Query";
	this->KVMap["NewEmail"] = "";
	this->KPMap["NewEmail"] = "Query";
	this->KVMap["NewComments"] = "";
	this->KPMap["NewComments"] = "Query";

}

	
}