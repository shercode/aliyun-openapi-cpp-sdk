/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
 
#ifndef ALIYUNCMS_CMSSERVICE_H_
#define ALIYUNCMS_CMSSERVICE_H_

#include "../aliyun-cpp-sdk-core/BaseRequest.h"
#include "../aliyun-cpp-sdk-core/ApiBase.h"

namespace AliYunCms {

class CmsClient : public AliYunCore::ApiBase {
private:
	AliYunCore::DefaultProfile &Profile;
public:
	CmsClient(AliYunCore::DefaultProfile &pf);
	void setProfile(AliYunCore::DefaultProfile &pf);
	std::string getResponse(AliYunCore::BaseRequest &request);
	~CmsClient(){};
};
class CmsBatchCreateSqlMetricsRequest : public AliYunCore::BaseRequest {
public:
	CmsBatchCreateSqlMetricsRequest();
};
class CmsBatchPutDimTableDataRequest : public AliYunCore::BaseRequest {
public:
	CmsBatchPutDimTableDataRequest();
};
class CmsBatchQueryMetricRequest : public AliYunCore::BaseRequest {
public:
	CmsBatchQueryMetricRequest();
};
class CmsCreateDimTableRequest : public AliYunCore::BaseRequest {
public:
	CmsCreateDimTableRequest();
};
class CmsCreateMetricStreamRequest : public AliYunCore::BaseRequest {
public:
	CmsCreateMetricStreamRequest();
};
class CmsCreateProjectRequest : public AliYunCore::BaseRequest {
public:
	CmsCreateProjectRequest();
};
class CmsCreateSqlMetricsRequest : public AliYunCore::BaseRequest {
public:
	CmsCreateSqlMetricsRequest();
};
class CmsDeleteDimTableRequest : public AliYunCore::BaseRequest {
public:
	CmsDeleteDimTableRequest();
};
class CmsDeleteDimTableDataRequest : public AliYunCore::BaseRequest {
public:
	CmsDeleteDimTableDataRequest();
};
class CmsDeleteMetricsRequest : public AliYunCore::BaseRequest {
public:
	CmsDeleteMetricsRequest();
};
class CmsDeleteMetricStreamRequest : public AliYunCore::BaseRequest {
public:
	CmsDeleteMetricStreamRequest();
};
class CmsDeleteProjectRequest : public AliYunCore::BaseRequest {
public:
	CmsDeleteProjectRequest();
};
class CmsDescribeMetricRequest : public AliYunCore::BaseRequest {
public:
	CmsDescribeMetricRequest();
};
class CmsDescribeMetricDatumRequest : public AliYunCore::BaseRequest {
public:
	CmsDescribeMetricDatumRequest();
};
class CmsGetDimTableRequest : public AliYunCore::BaseRequest {
public:
	CmsGetDimTableRequest();
};
class CmsGetMetricsMetaRequest : public AliYunCore::BaseRequest {
public:
	CmsGetMetricsMetaRequest();
};
class CmsGetMetricStatisticsRequest : public AliYunCore::BaseRequest {
public:
	CmsGetMetricStatisticsRequest();
};
class CmsGetMetricStreamRequest : public AliYunCore::BaseRequest {
public:
	CmsGetMetricStreamRequest();
};
class CmsGetProjectRequest : public AliYunCore::BaseRequest {
public:
	CmsGetProjectRequest();
};
class CmsGetSqlMetricsRequest : public AliYunCore::BaseRequest {
public:
	CmsGetSqlMetricsRequest();
};
class CmsListDimTableRequest : public AliYunCore::BaseRequest {
public:
	CmsListDimTableRequest();
};
class CmsListMetricStreamRequest : public AliYunCore::BaseRequest {
public:
	CmsListMetricStreamRequest();
};
class CmsListProjectRequest : public AliYunCore::BaseRequest {
public:
	CmsListProjectRequest();
};
class CmsListSqlMetricsRequest : public AliYunCore::BaseRequest {
public:
	CmsListSqlMetricsRequest();
};
class CmsPutDimTableDataRequest : public AliYunCore::BaseRequest {
public:
	CmsPutDimTableDataRequest();
};
class CmsQueryIncrementalRequest : public AliYunCore::BaseRequest {
public:
	CmsQueryIncrementalRequest();
};
class CmsQueryListMetricRequest : public AliYunCore::BaseRequest {
public:
	CmsQueryListMetricRequest();
};
class CmsQueryMetricRequest : public AliYunCore::BaseRequest {
public:
	CmsQueryMetricRequest();
};
class CmsQueryMetricTopNRequest : public AliYunCore::BaseRequest {
public:
	CmsQueryMetricTopNRequest();
};
class CmsQueryStatisticsRequest : public AliYunCore::BaseRequest {
public:
	CmsQueryStatisticsRequest();
};
class CmsStartProjectRequest : public AliYunCore::BaseRequest {
public:
	CmsStartProjectRequest();
};
class CmsStatusProjectRequest : public AliYunCore::BaseRequest {
public:
	CmsStatusProjectRequest();
};
class CmsStopProjectRequest : public AliYunCore::BaseRequest {
public:
	CmsStopProjectRequest();
};
class CmsUpdateDimTableRequest : public AliYunCore::BaseRequest {
public:
	CmsUpdateDimTableRequest();
};
class CmsUpdateMetricStreamRequest : public AliYunCore::BaseRequest {
public:
	CmsUpdateMetricStreamRequest();
};
class CmsUpdateProjectRequest : public AliYunCore::BaseRequest {
public:
	CmsUpdateProjectRequest();
};
class CmsUpdateSqlMetricsRequest : public AliYunCore::BaseRequest {
public:
	CmsUpdateSqlMetricsRequest();
};


}

#endif