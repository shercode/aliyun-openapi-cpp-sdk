/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

#include <string>
#include <map>
#include "../aliyun-cpp-sdk-core/utils.h"
#include "../aliyun-cpp-sdk-core/DefaultProfile.h"
#include "../aliyun-cpp-sdk-core/BaseRequest.h"
#include "../aliyun-cpp-sdk-core/ApiBase.h"
#include "CmsService.h"

using std::string;
using namespace AliYunCore;

namespace AliYunCms {

CmsClient::CmsClient(DefaultProfile &pf):Profile(pf){}
void CmsClient::setProfile(DefaultProfile &pf){
	this->Profile = pf;
}
string CmsClient::getResponse(BaseRequest &request){
	request.RegionId = this->Profile.RegionId;		
	request.AccessKeyId = this->Profile.AccessKeyId;
	request.AccessKeySecret = this->Profile.AccessKeySecret;
	return ApiBase::getResponse(request);
}
CmsBatchCreateSqlMetricsRequest::CmsBatchCreateSqlMetricsRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "cms";
	this->Action = "BatchCreateSqlMetrics";
	this->Version = "2015-10-20";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["ProjectName"] = "";
	this->KPMap["ProjectName"] = "Query";
	this->KVMap["Sqls"] = "";
	this->KPMap["Sqls"] = "Body";
	this->KVMap["IsPublic"] = "";
	this->KPMap["IsPublic"] = "Query";

}
CmsBatchPutDimTableDataRequest::CmsBatchPutDimTableDataRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "cms";
	this->Action = "BatchPutDimTableData";
	this->Version = "2015-10-20";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["DimTableName"] = "";
	this->KPMap["DimTableName"] = "Query";
	this->KVMap["Body"] = "";
	this->KPMap["Body"] = "Body";

}
CmsBatchQueryMetricRequest::CmsBatchQueryMetricRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "cms";
	this->Action = "BatchQueryMetric";
	this->Version = "2015-10-20";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["Project"] = "";
	this->KPMap["Project"] = "Query";
	this->KVMap["Metric"] = "";
	this->KPMap["Metric"] = "Query";
	this->KVMap["Period"] = "";
	this->KPMap["Period"] = "Query";
	this->KVMap["StartTime"] = "";
	this->KPMap["StartTime"] = "Query";
	this->KVMap["EndTime"] = "";
	this->KPMap["EndTime"] = "Query";
	this->KVMap["Dimensions"] = "";
	this->KPMap["Dimensions"] = "Query";
	this->KVMap["Extend"] = "";
	this->KPMap["Extend"] = "Query";
	this->KVMap["Filter"] = "";
	this->KPMap["Filter"] = "Query";

}
CmsCreateDimTableRequest::CmsCreateDimTableRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "cms";
	this->Action = "CreateDimTable";
	this->Version = "2015-10-20";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["DimTable"] = "";
	this->KPMap["DimTable"] = "Body";

}
CmsCreateMetricStreamRequest::CmsCreateMetricStreamRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "cms";
	this->Action = "CreateMetricStream";
	this->Version = "2015-10-20";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["ProjectName"] = "";
	this->KPMap["ProjectName"] = "Query";
	this->KVMap["MetricStream"] = "";
	this->KPMap["MetricStream"] = "Body";

}
CmsCreateProjectRequest::CmsCreateProjectRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "cms";
	this->Action = "CreateProject";
	this->Version = "2015-10-20";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["Project"] = "";
	this->KPMap["Project"] = "Body";

}
CmsCreateSqlMetricsRequest::CmsCreateSqlMetricsRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "cms";
	this->Action = "CreateSqlMetrics";
	this->Version = "2015-10-20";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["ProjectName"] = "";
	this->KPMap["ProjectName"] = "Query";
	this->KVMap["Sql"] = "";
	this->KPMap["Sql"] = "Body";
	this->KVMap["IsPublic"] = "";
	this->KPMap["IsPublic"] = "Query";

}
CmsDeleteDimTableRequest::CmsDeleteDimTableRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "cms";
	this->Action = "DeleteDimTable";
	this->Version = "2015-10-20";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["DimTableName"] = "";
	this->KPMap["DimTableName"] = "Query";

}
CmsDeleteDimTableDataRequest::CmsDeleteDimTableDataRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "cms";
	this->Action = "DeleteDimTableData";
	this->Version = "2015-10-20";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["DimTableName"] = "";
	this->KPMap["DimTableName"] = "Query";
	this->KVMap["Key"] = "";
	this->KPMap["Key"] = "Query";

}
CmsDeleteMetricsRequest::CmsDeleteMetricsRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "cms";
	this->Action = "DeleteMetrics";
	this->Version = "2015-10-20";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["ProjectName"] = "";
	this->KPMap["ProjectName"] = "Query";
	this->KVMap["MetricName"] = "";
	this->KPMap["MetricName"] = "Query";

}
CmsDeleteMetricStreamRequest::CmsDeleteMetricStreamRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "cms";
	this->Action = "DeleteMetricStream";
	this->Version = "2015-10-20";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["ProjectName"] = "";
	this->KPMap["ProjectName"] = "Query";
	this->KVMap["MetricStreamName"] = "";
	this->KPMap["MetricStreamName"] = "Query";

}
CmsDeleteProjectRequest::CmsDeleteProjectRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "cms";
	this->Action = "DeleteProject";
	this->Version = "2015-10-20";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["ProjectName"] = "";
	this->KPMap["ProjectName"] = "Query";

}
CmsDescribeMetricRequest::CmsDescribeMetricRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "cms";
	this->Action = "DescribeMetric";
	this->Version = "2015-10-20";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["Project"] = "";
	this->KPMap["Project"] = "Query";
	this->KVMap["Metric"] = "";
	this->KPMap["Metric"] = "Query";
	this->KVMap["Period"] = "";
	this->KPMap["Period"] = "Query";
	this->KVMap["StartTime"] = "";
	this->KPMap["StartTime"] = "Query";
	this->KVMap["EndTime"] = "";
	this->KPMap["EndTime"] = "Query";
	this->KVMap["Dimensions"] = "";
	this->KPMap["Dimensions"] = "Query";

}
CmsDescribeMetricDatumRequest::CmsDescribeMetricDatumRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "cms";
	this->Action = "DescribeMetricDatum";
	this->Version = "2015-10-20";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["Project"] = "";
	this->KPMap["Project"] = "Query";
	this->KVMap["Metric"] = "";
	this->KPMap["Metric"] = "Query";
	this->KVMap["Period"] = "";
	this->KPMap["Period"] = "Query";
	this->KVMap["StartTime"] = "";
	this->KPMap["StartTime"] = "Query";
	this->KVMap["EndTime"] = "";
	this->KPMap["EndTime"] = "Query";
	this->KVMap["Dimensions"] = "";
	this->KPMap["Dimensions"] = "Query";
	this->KVMap["Page"] = "";
	this->KPMap["Page"] = "Query";
	this->KVMap["Length"] = "";
	this->KPMap["Length"] = "Query";
	this->KVMap["Extend"] = "";
	this->KPMap["Extend"] = "Query";

}
CmsGetDimTableRequest::CmsGetDimTableRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "cms";
	this->Action = "GetDimTable";
	this->Version = "2015-10-20";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["DimTableName"] = "";
	this->KPMap["DimTableName"] = "Query";

}
CmsGetMetricsMetaRequest::CmsGetMetricsMetaRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "cms";
	this->Action = "GetMetricsMeta";
	this->Version = "2015-10-20";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["ProjectName"] = "";
	this->KPMap["ProjectName"] = "Query";
	this->KVMap["MetricName"] = "";
	this->KPMap["MetricName"] = "Query";

}
CmsGetMetricStatisticsRequest::CmsGetMetricStatisticsRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "cms";
	this->Action = "GetMetricStatistics";
	this->Version = "2015-10-20";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["Namespace"] = "";
	this->KPMap["Namespace"] = "Query";
	this->KVMap["MetricName"] = "";
	this->KPMap["MetricName"] = "Query";
	this->KVMap["StartTime"] = "";
	this->KPMap["StartTime"] = "Query";
	this->KVMap["EndTime"] = "";
	this->KPMap["EndTime"] = "Query";
	this->KVMap["Interval"] = "";
	this->KPMap["Interval"] = "Query";
	this->KVMap["Dimensions"] = "";
	this->KPMap["Dimensions"] = "Query";
	this->KVMap["NextToken"] = "";
	this->KPMap["NextToken"] = "Query";
	this->KVMap["Length"] = "";
	this->KPMap["Length"] = "Query";

}
CmsGetMetricStreamRequest::CmsGetMetricStreamRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "cms";
	this->Action = "GetMetricStream";
	this->Version = "2015-10-20";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["ProjectName"] = "";
	this->KPMap["ProjectName"] = "Query";
	this->KVMap["MetricStreamName"] = "";
	this->KPMap["MetricStreamName"] = "Query";

}
CmsGetProjectRequest::CmsGetProjectRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "cms";
	this->Action = "GetProject";
	this->Version = "2015-10-20";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["ProjectName"] = "";
	this->KPMap["ProjectName"] = "Query";

}
CmsGetSqlMetricsRequest::CmsGetSqlMetricsRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "cms";
	this->Action = "GetSqlMetrics";
	this->Version = "2015-10-20";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["ProjectName"] = "";
	this->KPMap["ProjectName"] = "Query";
	this->KVMap["MetricName"] = "";
	this->KPMap["MetricName"] = "Query";

}
CmsListDimTableRequest::CmsListDimTableRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "cms";
	this->Action = "ListDimTable";
	this->Version = "2015-10-20";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["DimTableName"] = "";
	this->KPMap["DimTableName"] = "Query";
	this->KVMap["Page"] = "";
	this->KPMap["Page"] = "Query";
	this->KVMap["PageSize"] = "";
	this->KPMap["PageSize"] = "Query";

}
CmsListMetricStreamRequest::CmsListMetricStreamRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "cms";
	this->Action = "ListMetricStream";
	this->Version = "2015-10-20";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["ProjectName"] = "";
	this->KPMap["ProjectName"] = "Query";
	this->KVMap["MetricStreamName"] = "";
	this->KPMap["MetricStreamName"] = "Query";
	this->KVMap["Page"] = "";
	this->KPMap["Page"] = "Query";
	this->KVMap["PageSize"] = "";
	this->KPMap["PageSize"] = "Query";

}
CmsListProjectRequest::CmsListProjectRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "cms";
	this->Action = "ListProject";
	this->Version = "2015-10-20";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["ProjectOwner"] = "";
	this->KPMap["ProjectOwner"] = "Query";
	this->KVMap["Page"] = "";
	this->KPMap["Page"] = "Query";
	this->KVMap["PageSize"] = "";
	this->KPMap["PageSize"] = "Query";

}
CmsListSqlMetricsRequest::CmsListSqlMetricsRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "cms";
	this->Action = "ListSqlMetrics";
	this->Version = "2015-10-20";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["ProjectName"] = "";
	this->KPMap["ProjectName"] = "Query";
	this->KVMap["MetricName"] = "";
	this->KPMap["MetricName"] = "Query";
	this->KVMap["Page"] = "";
	this->KPMap["Page"] = "Query";
	this->KVMap["PageSize"] = "";
	this->KPMap["PageSize"] = "Query";

}
CmsPutDimTableDataRequest::CmsPutDimTableDataRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "cms";
	this->Action = "PutDimTableData";
	this->Version = "2015-10-20";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["DimTableName"] = "";
	this->KPMap["DimTableName"] = "Query";
	this->KVMap["Body"] = "";
	this->KPMap["Body"] = "Body";

}
CmsQueryIncrementalRequest::CmsQueryIncrementalRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "cms";
	this->Action = "QueryIncremental";
	this->Version = "2015-10-20";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["Project"] = "";
	this->KPMap["Project"] = "Query";
	this->KVMap["Metric"] = "";
	this->KPMap["Metric"] = "Query";
	this->KVMap["Period"] = "";
	this->KPMap["Period"] = "Query";
	this->KVMap["StartTime"] = "";
	this->KPMap["StartTime"] = "Query";
	this->KVMap["EndTime"] = "";
	this->KPMap["EndTime"] = "Query";
	this->KVMap["Dimensions"] = "";
	this->KPMap["Dimensions"] = "Query";
	this->KVMap["TargetPeriod"] = "";
	this->KPMap["TargetPeriod"] = "Query";
	this->KVMap["Columns"] = "";
	this->KPMap["Columns"] = "Query";
	this->KVMap["Extend"] = "";
	this->KPMap["Extend"] = "Query";

}
CmsQueryListMetricRequest::CmsQueryListMetricRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "cms";
	this->Action = "QueryListMetric";
	this->Version = "2015-10-20";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["Project"] = "";
	this->KPMap["Project"] = "Query";
	this->KVMap["Metric"] = "";
	this->KPMap["Metric"] = "Query";
	this->KVMap["Period"] = "";
	this->KPMap["Period"] = "Query";
	this->KVMap["StartTime"] = "";
	this->KPMap["StartTime"] = "Query";
	this->KVMap["EndTime"] = "";
	this->KPMap["EndTime"] = "Query";
	this->KVMap["Dimensions"] = "";
	this->KPMap["Dimensions"] = "Query";
	this->KVMap["Page"] = "";
	this->KPMap["Page"] = "Query";
	this->KVMap["Length"] = "";
	this->KPMap["Length"] = "Query";
	this->KVMap["Extend"] = "";
	this->KPMap["Extend"] = "Query";
	this->KVMap["Filter"] = "";
	this->KPMap["Filter"] = "Query";

}
CmsQueryMetricRequest::CmsQueryMetricRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "cms";
	this->Action = "QueryMetric";
	this->Version = "2015-10-20";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["Project"] = "";
	this->KPMap["Project"] = "Query";
	this->KVMap["Metric"] = "";
	this->KPMap["Metric"] = "Query";
	this->KVMap["Period"] = "";
	this->KPMap["Period"] = "Query";
	this->KVMap["StartTime"] = "";
	this->KPMap["StartTime"] = "Query";
	this->KVMap["EndTime"] = "";
	this->KPMap["EndTime"] = "Query";
	this->KVMap["Dimensions"] = "";
	this->KPMap["Dimensions"] = "Query";
	this->KVMap["Page"] = "";
	this->KPMap["Page"] = "Query";
	this->KVMap["Length"] = "";
	this->KPMap["Length"] = "Query";
	this->KVMap["Extend"] = "";
	this->KPMap["Extend"] = "Query";

}
CmsQueryMetricTopNRequest::CmsQueryMetricTopNRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "cms";
	this->Action = "QueryMetricTopN";
	this->Version = "2015-10-20";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["Project"] = "";
	this->KPMap["Project"] = "Query";
	this->KVMap["Metric"] = "";
	this->KPMap["Metric"] = "Query";
	this->KVMap["Period"] = "";
	this->KPMap["Period"] = "Query";
	this->KVMap["StartTime"] = "";
	this->KPMap["StartTime"] = "Query";
	this->KVMap["EndTime"] = "";
	this->KPMap["EndTime"] = "Query";
	this->KVMap["Dimensions"] = "";
	this->KPMap["Dimensions"] = "Query";
	this->KVMap["ValueKey"] = "";
	this->KPMap["ValueKey"] = "Query";
	this->KVMap["Top"] = "";
	this->KPMap["Top"] = "Query";
	this->KVMap["Extend"] = "";
	this->KPMap["Extend"] = "Query";

}
CmsQueryStatisticsRequest::CmsQueryStatisticsRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "cms";
	this->Action = "QueryStatistics";
	this->Version = "2015-10-20";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["Project"] = "";
	this->KPMap["Project"] = "Query";
	this->KVMap["Metric"] = "";
	this->KPMap["Metric"] = "Query";
	this->KVMap["Period"] = "";
	this->KPMap["Period"] = "Query";
	this->KVMap["StartTime"] = "";
	this->KPMap["StartTime"] = "Query";
	this->KVMap["EndTime"] = "";
	this->KPMap["EndTime"] = "Query";
	this->KVMap["Dimensions"] = "";
	this->KPMap["Dimensions"] = "Query";
	this->KVMap["TargetPeriod"] = "";
	this->KPMap["TargetPeriod"] = "Query";
	this->KVMap["Function"] = "";
	this->KPMap["Function"] = "Query";
	this->KVMap["Extend"] = "";
	this->KPMap["Extend"] = "Query";

}
CmsStartProjectRequest::CmsStartProjectRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "cms";
	this->Action = "StartProject";
	this->Version = "2015-10-20";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["ProjectName"] = "";
	this->KPMap["ProjectName"] = "Query";

}
CmsStatusProjectRequest::CmsStatusProjectRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "cms";
	this->Action = "StatusProject";
	this->Version = "2015-10-20";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["ProjectName"] = "";
	this->KPMap["ProjectName"] = "Query";

}
CmsStopProjectRequest::CmsStopProjectRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "cms";
	this->Action = "StopProject";
	this->Version = "2015-10-20";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["ProjectName"] = "";
	this->KPMap["ProjectName"] = "Query";

}
CmsUpdateDimTableRequest::CmsUpdateDimTableRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "cms";
	this->Action = "UpdateDimTable";
	this->Version = "2015-10-20";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["DimTableName"] = "";
	this->KPMap["DimTableName"] = "Query";
	this->KVMap["DimTable"] = "";
	this->KPMap["DimTable"] = "Body";

}
CmsUpdateMetricStreamRequest::CmsUpdateMetricStreamRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "cms";
	this->Action = "UpdateMetricStream";
	this->Version = "2015-10-20";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["ProjectName"] = "";
	this->KPMap["ProjectName"] = "Query";
	this->KVMap["MetricStreamName"] = "";
	this->KPMap["MetricStreamName"] = "Query";
	this->KVMap["MetricStream"] = "";
	this->KPMap["MetricStream"] = "Body";

}
CmsUpdateProjectRequest::CmsUpdateProjectRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "cms";
	this->Action = "UpdateProject";
	this->Version = "2015-10-20";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["ProjectName"] = "";
	this->KPMap["ProjectName"] = "Query";
	this->KVMap["Project"] = "";
	this->KPMap["Project"] = "Body";

}
CmsUpdateSqlMetricsRequest::CmsUpdateSqlMetricsRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "cms";
	this->Action = "UpdateSqlMetrics";
	this->Version = "2015-10-20";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["ProjectName"] = "";
	this->KPMap["ProjectName"] = "Query";
	this->KVMap["MetricName"] = "";
	this->KPMap["MetricName"] = "Query";
	this->KVMap["Sql"] = "";
	this->KPMap["Sql"] = "Body";
	this->KVMap["IsPublic"] = "";
	this->KPMap["IsPublic"] = "Query";

}

	
}