/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

#include <string>
#include <map>
#include "../aliyun-cpp-sdk-core/utils.h"
#include "../aliyun-cpp-sdk-core/DefaultProfile.h"
#include "../aliyun-cpp-sdk-core/BaseRequest.h"
#include "../aliyun-cpp-sdk-core/ApiBase.h"
#include "PushService.h"

using std::string;
using namespace AliYunCore;

namespace AliYunPush {

PushClient::PushClient(DefaultProfile &pf):Profile(pf){}
void PushClient::setProfile(DefaultProfile &pf){
	this->Profile = pf;
}
string PushClient::getResponse(BaseRequest &request){
	request.RegionId = this->Profile.RegionId;		
	request.AccessKeyId = this->Profile.AccessKeyId;
	request.AccessKeySecret = this->Profile.AccessKeySecret;
	return ApiBase::getResponse(request);
}
PushBatchGetDevicesInfoRequest::PushBatchGetDevicesInfoRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "push";
	this->Action = "BatchGetDevicesInfo";
	this->Version = "2015-08-27";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["Devices"] = "";
	this->KPMap["Devices"] = "Query";
	this->KVMap["AppId"] = "";
	this->KPMap["AppId"] = "Query";

}
PushPushByteMessageRequest::PushPushByteMessageRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "push";
	this->Action = "PushByteMessage";
	this->Version = "2015-08-27";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["AppId"] = "";
	this->KPMap["AppId"] = "Query";
	this->KVMap["SendType"] = "";
	this->KPMap["SendType"] = "Query";
	this->KVMap["Accounts"] = "";
	this->KPMap["Accounts"] = "Query";
	this->KVMap["DeviceIds"] = "";
	this->KPMap["DeviceIds"] = "Query";
	this->KVMap["PushContent"] = "";
	this->KPMap["PushContent"] = "Query";

}
PushRevertRpcRequest::PushRevertRpcRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "push";
	this->Action = "RevertRpc";
	this->Version = "2015-08-27";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["AppId"] = "";
	this->KPMap["AppId"] = "Query";
	this->KVMap["DeviceId"] = "";
	this->KPMap["DeviceId"] = "Query";
	this->KVMap["RpcContent"] = "";
	this->KPMap["RpcContent"] = "Query";
	this->KVMap["TimeOut"] = "";
	this->KPMap["TimeOut"] = "Query";

}

	
}