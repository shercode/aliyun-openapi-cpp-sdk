/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
 
#ifndef ALIYUNCDN_CDNSERVICE_H_
#define ALIYUNCDN_CDNSERVICE_H_

#include "../aliyun-cpp-sdk-core/BaseRequest.h"
#include "../aliyun-cpp-sdk-core/ApiBase.h"

namespace AliYunCdn {

class CdnClient : public AliYunCore::ApiBase {
private:
	AliYunCore::DefaultProfile &Profile;
public:
	CdnClient(AliYunCore::DefaultProfile &pf);
	void setProfile(AliYunCore::DefaultProfile &pf);
	std::string getResponse(AliYunCore::BaseRequest &request);
	~CdnClient(){};
};
class CdnAddCdnDomainRequest : public AliYunCore::BaseRequest {
public:
	CdnAddCdnDomainRequest();
};
class CdnDeleteCdnDomainRequest : public AliYunCore::BaseRequest {
public:
	CdnDeleteCdnDomainRequest();
};
class CdnDescribeCdnDomainBaseDetailRequest : public AliYunCore::BaseRequest {
public:
	CdnDescribeCdnDomainBaseDetailRequest();
};
class CdnDescribeCdnDomainDetailRequest : public AliYunCore::BaseRequest {
public:
	CdnDescribeCdnDomainDetailRequest();
};
class CdnDescribeCdnDomainLogsRequest : public AliYunCore::BaseRequest {
public:
	CdnDescribeCdnDomainLogsRequest();
};
class CdnDescribeCdnMonitorDataRequest : public AliYunCore::BaseRequest {
public:
	CdnDescribeCdnMonitorDataRequest();
};
class CdnDescribeCdnServiceRequest : public AliYunCore::BaseRequest {
public:
	CdnDescribeCdnServiceRequest();
};
class CdnDescribeOneMinuteDataRequest : public AliYunCore::BaseRequest {
public:
	CdnDescribeOneMinuteDataRequest();
};
class CdnDescribeRefreshTasksRequest : public AliYunCore::BaseRequest {
public:
	CdnDescribeRefreshTasksRequest();
};
class CdnDescribeUserDomainsRequest : public AliYunCore::BaseRequest {
public:
	CdnDescribeUserDomainsRequest();
};
class CdnModifyCdnServiceRequest : public AliYunCore::BaseRequest {
public:
	CdnModifyCdnServiceRequest();
};
class CdnOpenCdnServiceRequest : public AliYunCore::BaseRequest {
public:
	CdnOpenCdnServiceRequest();
};
class CdnPushObjectCacheRequest : public AliYunCore::BaseRequest {
public:
	CdnPushObjectCacheRequest();
};
class CdnRefreshObjectCachesRequest : public AliYunCore::BaseRequest {
public:
	CdnRefreshObjectCachesRequest();
};
class CdnStartCdnDomainRequest : public AliYunCore::BaseRequest {
public:
	CdnStartCdnDomainRequest();
};
class CdnStopCdnDomainRequest : public AliYunCore::BaseRequest {
public:
	CdnStopCdnDomainRequest();
};


}

#endif