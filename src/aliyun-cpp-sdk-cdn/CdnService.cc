/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

#include <string>
#include <map>
#include "../aliyun-cpp-sdk-core/utils.h"
#include "../aliyun-cpp-sdk-core/DefaultProfile.h"
#include "../aliyun-cpp-sdk-core/BaseRequest.h"
#include "../aliyun-cpp-sdk-core/ApiBase.h"
#include "CdnService.h"

using std::string;
using namespace AliYunCore;

namespace AliYunCdn {

CdnClient::CdnClient(DefaultProfile &pf):Profile(pf){}
void CdnClient::setProfile(DefaultProfile &pf){
	this->Profile = pf;
}
string CdnClient::getResponse(BaseRequest &request){
	request.RegionId = this->Profile.RegionId;		
	request.AccessKeyId = this->Profile.AccessKeyId;
	request.AccessKeySecret = this->Profile.AccessKeySecret;
	return ApiBase::getResponse(request);
}
CdnAddCdnDomainRequest::CdnAddCdnDomainRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "cdn";
	this->Action = "AddCdnDomain";
	this->Version = "2014-11-11";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["DomainName"] = "";
	this->KPMap["DomainName"] = "Query";
	this->KVMap["SslFlag"] = "";
	this->KPMap["SslFlag"] = "Query";
	this->KVMap["SourceType"] = "";
	this->KPMap["SourceType"] = "Query";
	this->KVMap["CdnType"] = "";
	this->KPMap["CdnType"] = "Query";
	this->KVMap["Sources"] = "";
	this->KPMap["Sources"] = "Query";

}
CdnDeleteCdnDomainRequest::CdnDeleteCdnDomainRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "cdn";
	this->Action = "DeleteCdnDomain";
	this->Version = "2014-11-11";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["DomainName"] = "";
	this->KPMap["DomainName"] = "Query";

}
CdnDescribeCdnDomainBaseDetailRequest::CdnDescribeCdnDomainBaseDetailRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "cdn";
	this->Action = "DescribeCdnDomainBaseDetail";
	this->Version = "2014-11-11";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["DomainName"] = "";
	this->KPMap["DomainName"] = "Query";

}
CdnDescribeCdnDomainDetailRequest::CdnDescribeCdnDomainDetailRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "cdn";
	this->Action = "DescribeCdnDomainDetail";
	this->Version = "2014-11-11";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["DomainName"] = "";
	this->KPMap["DomainName"] = "Query";

}
CdnDescribeCdnDomainLogsRequest::CdnDescribeCdnDomainLogsRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "cdn";
	this->Action = "DescribeCdnDomainLogs";
	this->Version = "2014-11-11";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["DomainName"] = "";
	this->KPMap["DomainName"] = "Query";
	this->KVMap["LogDay"] = "";
	this->KPMap["LogDay"] = "Query";

}
CdnDescribeCdnMonitorDataRequest::CdnDescribeCdnMonitorDataRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "cdn";
	this->Action = "DescribeCdnMonitorData";
	this->Version = "2014-11-11";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["DomainName"] = "";
	this->KPMap["DomainName"] = "Query";
	this->KVMap["StartTime"] = "";
	this->KPMap["StartTime"] = "Query";
	this->KVMap["EndTime"] = "";
	this->KPMap["EndTime"] = "Query";

}
CdnDescribeCdnServiceRequest::CdnDescribeCdnServiceRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "cdn";
	this->Action = "DescribeCdnService";
	this->Version = "2014-11-11";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";

}
CdnDescribeOneMinuteDataRequest::CdnDescribeOneMinuteDataRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "cdn";
	this->Action = "DescribeOneMinuteData";
	this->Version = "2014-11-11";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["DomainName"] = "";
	this->KPMap["DomainName"] = "Query";
	this->KVMap["DataTime"] = "";
	this->KPMap["DataTime"] = "Query";

}
CdnDescribeRefreshTasksRequest::CdnDescribeRefreshTasksRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "cdn";
	this->Action = "DescribeRefreshTasks";
	this->Version = "2014-11-11";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["TaskId"] = "";
	this->KPMap["TaskId"] = "Query";
	this->KVMap["ObjectPath"] = "";
	this->KPMap["ObjectPath"] = "Query";
	this->KVMap["PageNumber"] = "";
	this->KPMap["PageNumber"] = "Query";
	this->KVMap["PageSize"] = "";
	this->KPMap["PageSize"] = "Query";

}
CdnDescribeUserDomainsRequest::CdnDescribeUserDomainsRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "cdn";
	this->Action = "DescribeUserDomains";
	this->Version = "2014-11-11";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["PageSize"] = "";
	this->KPMap["PageSize"] = "Query";
	this->KVMap["PageNumber"] = "";
	this->KPMap["PageNumber"] = "Query";

}
CdnModifyCdnServiceRequest::CdnModifyCdnServiceRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "cdn";
	this->Action = "ModifyCdnService";
	this->Version = "2014-11-11";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["InternetChargeType"] = "";
	this->KPMap["InternetChargeType"] = "Query";

}
CdnOpenCdnServiceRequest::CdnOpenCdnServiceRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "cdn";
	this->Action = "OpenCdnService";
	this->Version = "2014-11-11";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["InternetChargeType"] = "";
	this->KPMap["InternetChargeType"] = "Query";

}
CdnPushObjectCacheRequest::CdnPushObjectCacheRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "cdn";
	this->Action = "PushObjectCache";
	this->Version = "2014-11-11";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["ObjectPath"] = "";
	this->KPMap["ObjectPath"] = "Query";

}
CdnRefreshObjectCachesRequest::CdnRefreshObjectCachesRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "cdn";
	this->Action = "RefreshObjectCaches";
	this->Version = "2014-11-11";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["ObjectPath"] = "";
	this->KPMap["ObjectPath"] = "Query";
	this->KVMap["ObjectType"] = "";
	this->KPMap["ObjectType"] = "Query";

}
CdnStartCdnDomainRequest::CdnStartCdnDomainRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "cdn";
	this->Action = "StartCdnDomain";
	this->Version = "2014-11-11";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["DomainName"] = "";
	this->KPMap["DomainName"] = "Query";

}
CdnStopCdnDomainRequest::CdnStopCdnDomainRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "cdn";
	this->Action = "StopCdnDomain";
	this->Version = "2014-11-11";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["DomainName"] = "";
	this->KPMap["DomainName"] = "Query";

}

	
}