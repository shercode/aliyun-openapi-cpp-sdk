/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

#include <string>
#include <map>
#include "../aliyun-cpp-sdk-core/utils.h"
#include "../aliyun-cpp-sdk-core/DefaultProfile.h"
#include "../aliyun-cpp-sdk-core/BaseRequest.h"
#include "../aliyun-cpp-sdk-core/ApiBase.h"
#include "AceOpsService.h"

using std::string;
using namespace AliYunCore;

namespace AliYunAceOps {

AceOpsClient::AceOpsClient(DefaultProfile &pf):Profile(pf){}
void AceOpsClient::setProfile(DefaultProfile &pf){
	this->Profile = pf;
}
string AceOpsClient::getResponse(BaseRequest &request){
	request.RegionId = this->Profile.RegionId;		
	request.AccessKeyId = this->Profile.AccessKeyId;
	request.AccessKeySecret = this->Profile.AccessKeySecret;
	return ApiBase::getResponse(request);
}
AceOpsqueryRequest::AceOpsqueryRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "ace-ops";
	this->Action = "query";
	this->Version = "2015-09-09";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["group"] = "";
	this->KPMap["group"] = "Query";
	this->KVMap["name"] = "";
	this->KPMap["name"] = "Query";
	this->KVMap["ip"] = "";
	this->KPMap["ip"] = "Query";
	this->KVMap["softversion"] = "";
	this->KPMap["softversion"] = "Query";

}
AceOpsreportRequest::AceOpsreportRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "ace-ops";
	this->Action = "report";
	this->Version = "2015-09-09";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["group"] = "";
	this->KPMap["group"] = "Query";
	this->KVMap["name"] = "";
	this->KPMap["name"] = "Query";
	this->KVMap["ip"] = "";
	this->KPMap["ip"] = "Query";
	this->KVMap["type"] = "";
	this->KPMap["type"] = "Query";
	this->KVMap["softversion"] = "";
	this->KPMap["softversion"] = "Query";
	this->KVMap["config"] = "";
	this->KPMap["config"] = "Query";

}

	
}