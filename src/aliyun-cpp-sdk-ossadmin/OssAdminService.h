/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
 
#ifndef ALIYUNOSSADMIN_OSSADMINSERVICE_H_
#define ALIYUNOSSADMIN_OSSADMINSERVICE_H_

#include "../aliyun-cpp-sdk-core/BaseRequest.h"
#include "../aliyun-cpp-sdk-core/ApiBase.h"

namespace AliYunOssAdmin {

class OssAdminClient : public AliYunCore::ApiBase {
private:
	AliYunCore::DefaultProfile &Profile;
public:
	OssAdminClient(AliYunCore::DefaultProfile &pf);
	void setProfile(AliYunCore::DefaultProfile &pf);
	std::string getResponse(AliYunCore::BaseRequest &request);
	~OssAdminClient(){};
};
class OssAdminBindBucketVipRequest : public AliYunCore::BaseRequest {
public:
	OssAdminBindBucketVipRequest();
};
class OssAdminCreateImgVpcRequest : public AliYunCore::BaseRequest {
public:
	OssAdminCreateImgVpcRequest();
};
class OssAdminCreateOssVpcRequest : public AliYunCore::BaseRequest {
public:
	OssAdminCreateOssVpcRequest();
};
class OssAdminDeleteImgVpcRequest : public AliYunCore::BaseRequest {
public:
	OssAdminDeleteImgVpcRequest();
};
class OssAdminDeleteOssVpcRequest : public AliYunCore::BaseRequest {
public:
	OssAdminDeleteOssVpcRequest();
};
class OssAdminGetBucketVipsRequest : public AliYunCore::BaseRequest {
public:
	OssAdminGetBucketVipsRequest();
};
class OssAdminGetImgVpcInfoRequest : public AliYunCore::BaseRequest {
public:
	OssAdminGetImgVpcInfoRequest();
};
class OssAdminGetOssVpcInfoRequest : public AliYunCore::BaseRequest {
public:
	OssAdminGetOssVpcInfoRequest();
};
class OssAdminUnBindBucketVipRequest : public AliYunCore::BaseRequest {
public:
	OssAdminUnBindBucketVipRequest();
};


}

#endif