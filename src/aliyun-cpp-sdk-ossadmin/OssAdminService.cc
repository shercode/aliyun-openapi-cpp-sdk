/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

#include <string>
#include <map>
#include "../aliyun-cpp-sdk-core/utils.h"
#include "../aliyun-cpp-sdk-core/DefaultProfile.h"
#include "../aliyun-cpp-sdk-core/BaseRequest.h"
#include "../aliyun-cpp-sdk-core/ApiBase.h"
#include "OssAdminService.h"

using std::string;
using namespace AliYunCore;

namespace AliYunOssAdmin {

OssAdminClient::OssAdminClient(DefaultProfile &pf):Profile(pf){}
void OssAdminClient::setProfile(DefaultProfile &pf){
	this->Profile = pf;
}
string OssAdminClient::getResponse(BaseRequest &request){
	request.RegionId = this->Profile.RegionId;		
	request.AccessKeyId = this->Profile.AccessKeyId;
	request.AccessKeySecret = this->Profile.AccessKeySecret;
	return ApiBase::getResponse(request);
}
OssAdminBindBucketVipRequest::OssAdminBindBucketVipRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "ossadmin";
	this->Action = "BindBucketVip";
	this->Version = "2015-05-20";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";
	this->KVMap["Region"] = "";
	this->KPMap["Region"] = "Query";
	this->KVMap["VpcId"] = "";
	this->KPMap["VpcId"] = "Query";
	this->KVMap["Vip"] = "";
	this->KPMap["Vip"] = "Query";
	this->KVMap["BucketName"] = "";
	this->KPMap["BucketName"] = "Query";

}
OssAdminCreateImgVpcRequest::OssAdminCreateImgVpcRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "ossadmin";
	this->Action = "CreateImgVpc";
	this->Version = "2015-05-20";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";
	this->KVMap["Region"] = "";
	this->KPMap["Region"] = "Query";
	this->KVMap["VirtualSwitchId"] = "";
	this->KPMap["VirtualSwitchId"] = "Query";
	this->KVMap["VpcId"] = "";
	this->KPMap["VpcId"] = "Query";
	this->KVMap["Label"] = "";
	this->KPMap["Label"] = "Query";

}
OssAdminCreateOssVpcRequest::OssAdminCreateOssVpcRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "ossadmin";
	this->Action = "CreateOssVpc";
	this->Version = "2015-05-20";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";
	this->KVMap["Region"] = "";
	this->KPMap["Region"] = "Query";
	this->KVMap["VirtualSwitchId"] = "";
	this->KPMap["VirtualSwitchId"] = "Query";
	this->KVMap["VpcId"] = "";
	this->KPMap["VpcId"] = "Query";
	this->KVMap["Label"] = "";
	this->KPMap["Label"] = "Query";

}
OssAdminDeleteImgVpcRequest::OssAdminDeleteImgVpcRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "ossadmin";
	this->Action = "DeleteImgVpc";
	this->Version = "2015-05-20";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";
	this->KVMap["Region"] = "";
	this->KPMap["Region"] = "Query";
	this->KVMap["VpcId"] = "";
	this->KPMap["VpcId"] = "Query";
	this->KVMap["Label"] = "";
	this->KPMap["Label"] = "Query";

}
OssAdminDeleteOssVpcRequest::OssAdminDeleteOssVpcRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "ossadmin";
	this->Action = "DeleteOssVpc";
	this->Version = "2015-05-20";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";
	this->KVMap["Region"] = "";
	this->KPMap["Region"] = "Query";
	this->KVMap["VpcId"] = "";
	this->KPMap["VpcId"] = "Query";
	this->KVMap["Label"] = "";
	this->KPMap["Label"] = "Query";

}
OssAdminGetBucketVipsRequest::OssAdminGetBucketVipsRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "ossadmin";
	this->Action = "GetBucketVips";
	this->Version = "2015-05-20";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";
	this->KVMap["BucketName"] = "";
	this->KPMap["BucketName"] = "Query";

}
OssAdminGetImgVpcInfoRequest::OssAdminGetImgVpcInfoRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "ossadmin";
	this->Action = "GetImgVpcInfo";
	this->Version = "2015-05-20";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";
	this->KVMap["Region"] = "";
	this->KPMap["Region"] = "Query";
	this->KVMap["VpcId"] = "";
	this->KPMap["VpcId"] = "Query";
	this->KVMap["Label"] = "";
	this->KPMap["Label"] = "Query";

}
OssAdminGetOssVpcInfoRequest::OssAdminGetOssVpcInfoRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "ossadmin";
	this->Action = "GetOssVpcInfo";
	this->Version = "2015-05-20";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";
	this->KVMap["Region"] = "";
	this->KPMap["Region"] = "Query";
	this->KVMap["VpcId"] = "";
	this->KPMap["VpcId"] = "Query";
	this->KVMap["Label"] = "";
	this->KPMap["Label"] = "Query";

}
OssAdminUnBindBucketVipRequest::OssAdminUnBindBucketVipRequest(){
	//set the default value of the public parameters. 
	this->HttpMethod = "GET";
	this->Protocol = "HTTP";
	this->Domain = "";
	this->Path = "";
	this->ProductName = "ossadmin";
	this->Action = "UnBindBucketVip";
	this->Version = "2015-05-20";
	this->Format = "JSON";
	this->SignatureMethod = "HMAC-SHA1";
	this->SignatureVersion = "1.0";
	
	//set the position and default value for the business parameters. 
	this->KVMap["OwnerId"] = "";
	this->KPMap["OwnerId"] = "Query";
	this->KVMap["ResourceOwnerAccount"] = "";
	this->KPMap["ResourceOwnerAccount"] = "Query";
	this->KVMap["ResourceOwnerId"] = "";
	this->KPMap["ResourceOwnerId"] = "Query";
	this->KVMap["OwnerAccount"] = "";
	this->KPMap["OwnerAccount"] = "Query";
	this->KVMap["Region"] = "";
	this->KPMap["Region"] = "Query";
	this->KVMap["VpcId"] = "";
	this->KPMap["VpcId"] = "Query";
	this->KVMap["Vip"] = "";
	this->KPMap["Vip"] = "Query";
	this->KVMap["BucketName"] = "";
	this->KPMap["BucketName"] = "Query";

}

	
}