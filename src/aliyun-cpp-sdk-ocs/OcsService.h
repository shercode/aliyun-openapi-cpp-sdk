/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
 
#ifndef ALIYUNOCS_OCSSERVICE_H_
#define ALIYUNOCS_OCSSERVICE_H_

#include "../aliyun-cpp-sdk-core/BaseRequest.h"
#include "../aliyun-cpp-sdk-core/ApiBase.h"

namespace AliYunOcs {

class OcsClient : public AliYunCore::ApiBase {
private:
	AliYunCore::DefaultProfile &Profile;
public:
	OcsClient(AliYunCore::DefaultProfile &pf);
	void setProfile(AliYunCore::DefaultProfile &pf);
	std::string getResponse(AliYunCore::BaseRequest &request);
	~OcsClient(){};
};
class OcsActivateInstanceRequest : public AliYunCore::BaseRequest {
public:
	OcsActivateInstanceRequest();
};
class OcsAddAuthenticIPRequest : public AliYunCore::BaseRequest {
public:
	OcsAddAuthenticIPRequest();
};
class OcsCreateInstanceRequest : public AliYunCore::BaseRequest {
public:
	OcsCreateInstanceRequest();
};
class OcsDataOperateRequest : public AliYunCore::BaseRequest {
public:
	OcsDataOperateRequest();
};
class OcsDeactivateInstanceRequest : public AliYunCore::BaseRequest {
public:
	OcsDeactivateInstanceRequest();
};
class OcsDeleteInstanceRequest : public AliYunCore::BaseRequest {
public:
	OcsDeleteInstanceRequest();
};
class OcsDescribeAuthenticIPRequest : public AliYunCore::BaseRequest {
public:
	OcsDescribeAuthenticIPRequest();
};
class OcsDescribeHistoryMonitorValuesRequest : public AliYunCore::BaseRequest {
public:
	OcsDescribeHistoryMonitorValuesRequest();
};
class OcsDescribeInstancesRequest : public AliYunCore::BaseRequest {
public:
	OcsDescribeInstancesRequest();
};
class OcsDescribeMonitorItemsRequest : public AliYunCore::BaseRequest {
public:
	OcsDescribeMonitorItemsRequest();
};
class OcsDescribeMonitorValuesRequest : public AliYunCore::BaseRequest {
public:
	OcsDescribeMonitorValuesRequest();
};
class OcsDescribeRegionsRequest : public AliYunCore::BaseRequest {
public:
	OcsDescribeRegionsRequest();
};
class OcsDescribeSecurityIpsRequest : public AliYunCore::BaseRequest {
public:
	OcsDescribeSecurityIpsRequest();
};
class OcsDescribeZonesRequest : public AliYunCore::BaseRequest {
public:
	OcsDescribeZonesRequest();
};
class OcsFlushInstanceRequest : public AliYunCore::BaseRequest {
public:
	OcsFlushInstanceRequest();
};
class OcsModifyInstanceAttributeRequest : public AliYunCore::BaseRequest {
public:
	OcsModifyInstanceAttributeRequest();
};
class OcsModifyInstanceCapacityRequest : public AliYunCore::BaseRequest {
public:
	OcsModifyInstanceCapacityRequest();
};
class OcsModifySecurityIpsRequest : public AliYunCore::BaseRequest {
public:
	OcsModifySecurityIpsRequest();
};
class OcsRemoveAuthenticIPRequest : public AliYunCore::BaseRequest {
public:
	OcsRemoveAuthenticIPRequest();
};
class OcsReplaceAuthenticIPRequest : public AliYunCore::BaseRequest {
public:
	OcsReplaceAuthenticIPRequest();
};
class OcsVerifyPasswordRequest : public AliYunCore::BaseRequest {
public:
	OcsVerifyPasswordRequest();
};


}

#endif